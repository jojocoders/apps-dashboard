# RELASE NOTE

# Version 2.38.0 (8 Feb 2023)

- IMPLEMENT - Visibility Condition Action Button (Detail View)
- IMPLEMENT - Visibility Condition Action Button (Table View)
- IMPLEMENT - Default Currency
- IMPLEMENT - New Field Currency Selection

# Version 2.38.1 (9 Feb 2023)

- BUGFIX - Bulk Update on Multiple UI External API

# Version 2.38.2 (9 Feb 2023)

- BUGFIX - Component input type text & number
- BUGFIX - Label relation record unknown

# Version 2.39.0 (15 Feb 2023)

- HOTFIX - user_role index on endpoint user/employee/update
- IMPROVEMENT - Improvement new currency selection
- IMPLEMENT - Multiple options on search option Export View
- IMPLEMENT - Validation new password on app
- IMPLEMENT - New validation password user component on app

# Version 2.39.1 (17 Feb 2023)

- BUGFIX - Visibility action button on detail source external api

# Version 2.39.2 (21 Feb 2023)

- IMPLEMENT - onchange script on text area

# Version 2.40.0 (22 Feb 2023)

- IMPLEMENT - show option app

# Version 2.40.1 (24 feb 2023)

- HOTFIX - Native Button Update

# Version 2.40.2 (27 feb 2023)

- IMPLEMENT - Custom pagination on dataset server side
- IMPLEMENT - Change template import co attendance to lokal
- IMPLEMENT - Update timezone on co attendance

# Version 2.41.0 (28 feb 2023)

- IMPLEMENT - Maintenance Mode

# Version 2.42.0 (08 mar 2023)

- IMPLEMENT - field currecy selection on group
- IMPLEMENT - Add maintenance mode on component access management
- IMPLEMENT - Fix missing ui filed dynamic field type setting
- IMPLEMENT - Improve input type number allow minus
- IMPLEMENT - Return library v-viewer

# Version 2.42.1 (14 mar 2023)

- IMPROVEMENT -  submit record & change endpoint submit (workflow)

# Version 2.43.0 (15 mar 2023)

- BUGFIX - get count summary block workflow
- BUGFIX - hide native button import on table view
- BUGFIX - dynamic field
- IMPROVEMENT - Improvement log UI minimum on workflow all type
- IMPLEMENT - Add paramter param_embed_url pada action button
- IMPROVEMENT - Improvement redirect action button
- IMPROVEMENT - Improvement ui fraud information pop up on attendance
- IMPROVEMENT - Improvement List & Detail Attendance on Component Attendance Monitoring
- IMPLEMENT - Handle row expansion server table ext api

# Version 2.43.1 (17 mar 2023)

- BUGFIX - Handle to show 0 value on table
- BUGFIX - alignment pada tableview saat format currency
- HOTFIX - handle condition without setup native_buttons
- HOTFIX - filter function null on table canvas

# Version 2.43.2 (21 mar 2023)

- HOTFIX - filter function on canvas table
- HOTFIX - component datetime freeze
- HOTFIX - Handle takeout payroll service pada update position di employee setup
- HOTFIX - payload delete shift

# Version 2.43.3 (24 mar 2023)

- HOTFIX - Error response external api

# Version 2.43.4 (27 mar 2023)

- HOTFIX - Mandatory field currency

# Version 2.43.5 (28 mar 2023)

- IMPROVEMENT - Type field decimal add limit
- HOTFIX - Timeframe filter table default type timestamp
- IMPLEMENT - export view on UI source dataset
- HOTFIX - Change sort advance report
- HOTFIX - Count response external api

# Version 2.43.6 (30 mar 2023)

- HOTFIX - Takeout and hide sorting on table view
- IMPROVEMENT - Implement count data for UI source dataset

# Version 2.43.7 (5 apr 2023)

- IMPROVEMENT - Handle editable block workflow yang block custom
- IMPROVEMENT - Handle _user on API UI Source ext. API
- BUGFIX - Handle pop up success update to draft on multiple UI
- BUGFIX - Value currency 0 on detail

# Version 2.43.8 (6 apr 2023)

- HOTFIX - Currency can't paste
- HOTFIX - Default null if type number empty
- HOTFIX - Allow minus value currency on dynamic field
- HOTFIX - Default value 0 on detail type number and currency on dynamic field
- HOTFIX - Filter timestamp on export view standard
- HOTFIX - Clear filter on export view

# Version 2.44.0 (11 apr 2023)

- IMPROVEMENT - Note need revision
- IMPROVEMENT - Activity Logout
- IMPLEMENT - Native button update multiple ui
- IMPLEMENT - New endpoint change password & implement password encryption
- IMPLEMENT - New endpoint forgot password & password encryption
- IMPLEMENT - Update employee with password encryption on component user_setup
- IMPLEMENT - Custom message before action using function param (Action Button)
- IMPLEMENT - Custom message before action using function param (Native Button)
- IMPLEMENT - Refresh token + handle session
- IMPLEMENT - New endpoint login + encryption
- IMPLEMENT - Captcha login
- IMPROVEMENT - Relation record autoshow false set default options limit 20

# Version 2.44.1 (14 apr 2023)

- IMPLEMENT - Font Poppins New Service

# Version 2.45.0 (5 May 2023)

- IMPLEMENT - Row per page table view
- IMPLEMENT - Freeze header table view
- IMPROVEMENT - Skeleton get list apps

# Version 2.45.1 (9 May 2023)

- IMPLEMENT - Onchange Script type currency selection
- IMPROVEMENT - Keypress enter on filter
- IMPLEMENT - Hide checkbox on table app by config
- IMPLEMENT - Bulk Delete Workflow Requester
- IMPROVEMENT - Showing delete icon for all row on table group & field group
- IMPROVEMENT - Show all list currency selection popover
- HOTFIX - RR ext api preview data

# Version 2.45.2 (17 May 2023)

- IMPROVEMENT - Get data record calender by view
- HOTFIX - Filter end_date search options list 00:00:00
- IMPLEMENT - Skip Null Value filter function
- IMPLEMENT - new endpoint to get record dataset on App
- IMPLEMENT - new endpoint to get detail dataset on App
- IMPROVEMENT - Redirect with historical parent (queryParamsParent)
- IMPROVEMENT - script redirect for workflow on multiple UI

# Version 2.45.3 (19 May 2023)

- HOTFIX - Create record type relation record id undefined
- IMPROVEMENT - Search options user selection support column created_by & udated_by
- IMPROVEMENT - _hitFunction on change script

# Version 2.45.4 (23 May 2023)

- IMPROVEMENT - Handle default currency for target currency
- IMPROVEMENT - Retrieve data relation record support get value from relation external api
- IMPROVEMENT - Select all data multiple relation record support non auto show all

# Version 2.45.5 (25 May 2023)

- IMPROVEMENT - Refresh button on Activity Import & Activity Export
- IMPLEMENT - Cancel Import (Advance Report)
- HOTFIX - Payload change password encryption

# Version 2.45.6 (14 Jun 2023)

- IMPLEMENT - Column table type currency selection
- HOTFIX - filter timestamp on firefox
- HOTFIX - fix error multiple relation record
- IMPROVEMENT - posible close modal create & detail on key press esc

# Version 2.46.0 (5 Jul 2023)

- IMPROVEMENT - List application home page

# Version 2.46.1 (1 Aug 2023)

- HOTFIX - Handle redirect to tab multiple UI workflow
- HOTFIX - Handle to show button UI child on parent multiple UI status DRAFT
- HOTFIX - Handle query params impact to multi component page
- IMPLEMENT - New type bulk update/action by function
- IMPROVEMENT - Upgrade Value Min Max Options Currency
- HOTFIX - z-index modal activity import and hide column action table group by condition

# Version 2.46.2 (8 Aug 2023)

- IMPROVEMENT - dropdown all position at home page

# Version 2.46.3 (14 Aug 2023)

- IMPLEMENT - New endpoint get detail workflow & non workflow
- HOTFIX - Handle to remove webpack JS file
- HOTFIX - Handle version information disclosure

# Version 2.46.4 (21 Aug 2023)

- IMPLEMENT - underscore field properties group
- HOTFIX - field separator on external api source view
- IMPLEMENT - filed properties disable add more and delete row on type group
- HOTFIX - disabled on approved by me

# Version 2.46.5 (20 Sep 2023)

- HOTFIX - handle filename export from content disposition
- IMPROVEMENT - Add error retry in progress component
- BUGFIX - Fix editor js more than one

# Version 2.47.0 (03 Oct 2023)

- IMPLEMENT - Multiple relation record on search options
- HOTFIX - handle logout api on navbar app

# Version 2.48.0 (19 Oct 2023)

- HOTFIX - Message error bulking update
- HOTFIX - Export view multiple ui payload
- IMPROVEMENT - Update limit clint side table view & export view to 10.000

# Version 2.49.0 (31 Oct 2023)

- IMPLEMENT - New login LDAP
- IMPROVEMENT - change title view

# Version 2.50.0 (06 Nov 2023)

- HOTFIX - breadcrumbs multiple details change from localstorage to params
- HOTFIX - fix times module location not same as the detailed data
- HOTFIX - issue id elemen 0 on component child ui
- HOTFIX - error render export view component inside multiple UI
- HOTFIX - maintenance mode and action approver if open page from url
- HOTFIX - update maps on auto fill
- IMPROVEMENT - show powered info by env
- NEW FEATURE - implement Initial Value

# Version 2.51.0 (17 Jan 2024)

- NEW FEATURE - Login with OTP

# Version 2.52.0 (18 Jan 2024)

- NEW FEATURE - Tag filter
- NEW FEATURE - New field slider
- NEW FEATURE - Open detail master reslation record
- NEW FEATURE - New field text label

# Version 2.53.0 (29 Jan 2024)

- IMPROVEMENT - show background blur by env

# Version 2.54.0 (31 Jan 2024)

- NEW FEATURE - New field progress bar
- NEW FEATURE - Notification navbar

# Version 2.55.0 (06 Feb 2024)

- IMPROVEMENT - new timeframe all time on canvas

# Version 2.56.0 (23 Feb 2024)

- NEW FEATURE - Implement timezone by config & by env
- NEW FEATURE - Login with Azure
- NEW FEATURE - Notification page screen
- IMPROVEMENT - Tag filter can send body
- IMPROVEMENT - Type field text label have type
- NEW FEATURE - Implement sort on calendar view
- IMPROVEMENT - refetch once open single & multiple relation record ext api
- IMPROVEMENT - label join organigram login by env
- HOTFIX - issue editable by workflow layer
- IMPROVEMENT - default ui & payload type group when beggining
- IMPROVEMENT - Implement hyperlink on field text label
- NEW FEATURE - Implement disable_redirect_detail_page
- HOTFIX - Problem message screen that close too quickly
- HOTFIX - Double text Please Wait in loading screen

# Version 2.57.0 (27 Mar 2024)

- NEW FEATURE - Implement hidden log workflow by config
- IMPROVEMENT - Custom css calendar day view
- HOTFIX - Close modal with esc then reopen modal another raw data not change
- IMPROVEMENT - Restore component hierarchy
- IMPROVEMENT - Hide tab absent monitor attendance
- IMPROVEMENT - Improve UI component flow rules

# Version 2.58.0 (08 Mei 2024)

- NEW FEATURE - Add button refresh data table view, requester view, and approver view
- IMPROVEMENT - Implement x_axis_limit and sort by setup on bar chart

# Version 2.59.0 (08 Oct 2024)
- HOTFIX - fix filename attachment not show if props changa value
- NEW FEATURE - add env FONT_COLOR_LOGIN
- HOTFIX - fix race condition getDataRelationRecord with params

# Version 2.60.0 (22 Oct 2024)
- NEW FEATURE - implement new widget shortcut page
- NEW FEATURE - implement message type native button

# Version 2.61.0 (28 Nov 2024)
- HOTFIX - Fix end of month timestamp calender view on created

# Version 2.62.0 (06 Dec 2024)
- NEW FEATURE - implement private file type field attachment
- NEW FEATURE - implement intercom

# Version 2.63.0 (31 Dec 2024)
- BUGFIX - Error required text editor & code editor
- BUGFIX - popover currency selection still can be clicked when the field is disabled
- BUGFIX - Error count summary block data manager
- IMPROVEMENT - Position popover tools text editor
- BUGFIX - Fix get value column with type currency selection on table view (ext api and server side pagination off)

# Version 2.64.0 (22 Jan 2025)
- NEW FEATURE - Custom login page

# Version 2.64.1 (05 Feb 2025)
- IMPROVEMENT - Fix pentest issue image url

# Version 2.64.2 (07 Feb 2025)
- IMPROVEMENT - Fix pentest issue image url in profile navbar
- BUGFIX - error sentry & sanitize regex

# Version 2.64.3 (11 Feb 2025)
- NEW FEATURE - Add new header for support company gen type