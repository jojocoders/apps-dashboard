/* eslint-disable no-shadow */
import jApiSession from '@/services/jApiSession';

const state = {
  session_history: null,
  data_otp_session: null,
};

const mutations = {
  setSessionHistory(state, data) {
    state.session_history = data;
  },
  setDataOtpSession(state, data) {
    state.data_otp_session = data;
  },
};

const actions = {
  async getAllSessionHistory({ commit, dispatch }, params) {
    try {
      const res = await jApiSession.getAllSessionHistory(params);
      if (!res.error) {
        commit('setSessionHistory', res.data);
      } else {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async requestSessionDestroyOtp({ commit, dispatch }) {
    let res;
    try {
      res = await jApiSession.requestSessionDestroyOtp();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDataOtpSession', res.data);
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async forceLogoutSession({ dispatch }, req) {
    let res;
    try {
      res = await jApiSession.forceLogout(req);
      if (res.error || res.error_session) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
