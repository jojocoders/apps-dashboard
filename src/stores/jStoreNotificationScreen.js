/* eslint-disable no-shadow,consistent-return,no-param-reassign */

const state = {
  // Set default state(s) here
  info_screen: false,
  info_message: '',

  warning_screen: false,
  warning_message: '',

  problem_screen: false,
  problem_message: '',
  problem_code: '',
  isFailed: false,

  success_screen: false,
  success_message: '',

  loading_screen: false,
  cover_loading_screen: false,
  loading_message: '',

  maintenance_screen: false,
  maintenance_message: '',

  // From payroll
  info_action: 0,
  problem_action: 0,
  warning_action: 0,
  success_action: 0,
  loading_action: 0,
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  toggleStateInfo(state, message) {
    state.info_screen = !state.info_screen;
    if (message) {
      state.info_message = message;
    }
  },
  toggleStateWarning(state, message) {
    state.warning_screen = !state.warning_screen;
    if (message) {
      state.warning_message = message;
    }
  },
  toggleStateFailed(state) {
    state.isFailed = true;
  },
  toggleResetStateFailed(state) {
    state.isFailed = false;
  },
  toggleStateProblem(state, message) {
    state.problem_screen = !state.problem_screen;
    if (message) {
      if (message.error_code !== undefined) {
        state.problem_message = message.error_message;
        state.problem_code = message.error_code;
      } else {
        state.problem_message = message;
        state.problem_code = '';
      }
    }
  },
  toggleStateSuccess(state, message) {
    state.success_screen = !state.success_screen;
    if (message) {
      state.success_message = message;
    }
  },
  toggleStateLoading(state, message) {
    state.loading_screen = !state.loading_screen;
    if (message) {
      state.loading_message = message;
    }
  },
  showStateLoading(state, message) {
    state.loading_screen = true;
    if (message) {
      state.loading_message = message;
    }
  },
  hideStateLoading(state, message) {
    state.loading_screen = false;
    if (message) {
      state.loading_message = message;
    }
  },
  showStateCoverLoading(state, message) {
    state.cover_loading_screen = true;
    if (message) {
      state.loading_message = message;
    }
  },
  toggleStateMaintenance(state, message) {
    state.maintenance_screen = !state.maintenance_screen;
    if (message) {
      state.maintenance_message = message;
    }
  },

  // From payroll
  stateCloseLoading() {
    state.loading_action += 1;
    state.loading_screen = false;
  },
  stateShowInfo(state, message) {
    state.info_action += 1;
    state.info_screen = true;
    if (message) {
      state.info_message = message;
    } else {
      state.info_message = '';
    }
  },
  stateCloseInfo() {
    state.info_action += 1;
    state.info_screen = false;
  },
  stateShowWarning(state, message) {
    state.warning_action += 1;
    state.warning_screen = true;
    if (message) {
      state.warning_message = message;
    } else {
      state.warning_message = '';
    }
  },
  stateCloseWarning() {
    state.warning_action += 1;
    state.warning_screen = false;
  },
  stateShowFailed(state) {
    state.isFailed = true;
  },
  stateShowProblem(state, message) {
    state.problem_action += 1;
    state.problem_screen = true;
    if (message) {
      state.problem_message = message;
    } else {
      state.problem_message = 'Something wrong, Please try again later';
    }
  },
  stateCloseProblem() {
    state.problem_action += 1;
    state.problem_screen = false;
    state.isFailed = false;
  },
  stateShowSuccess(state, message) {
    state.success_action += 1;
    state.success_screen = true;
    if (message) {
      state.success_message = message;
    } else {
      state.success_message = 'Success';
    }
  },
  stateCloseSuccess() {
    state.success_action += 1;
    state.success_screen = false;
  },
};

const actions = {
  toggleInfo({ commit }, message) {
    commit('toggleStateInfo', message);
  },
  toggleWarning({ commit }, message) {
    commit('toggleStateWarning', message);
  },
  toggleFailed({ commit }, message) {
    commit('toggleStateFailed');
    commit('toggleStateProblem', message);
  },
  toggleProblem({ commit }, message) {
    commit('toggleStateProblem', message);
  },
  toggleSuccess({ commit }, message) {
    commit('toggleStateSuccess', message);
  },
  toggleLoading({ commit }, message) {
    commit('toggleStateLoading', message);
  },
  showLoading({ commit }, message) {
    commit('showStateLoading', message);
  },
  hideLoading({ commit }, message) {
    commit('hideStateLoading', message);
  },
  showCoverLoading({ commit }, message) {
    commit('showStateCoverLoading', message);
  },
  toggleMaintenance({ commit }, data) {
    if (data.show) commit('toggleStateMaintenance', data.message);
  },

  // From payroll
  closeLoading({ commit }) {
    commit('stateCloseLoading');
  },
  showProblem({ commit }, message) {
    commit('stateShowProblem', message);
  },
  closeProblem({ commit }) {
    commit('stateCloseProblem');
  },
  showFailed({ commit }, message) {
    commit('stateShowFailed');
    commit('stateShowProblem', message);
  },
  showInfo({ commit }, message) {
    commit('stateShowInfo', message);
  },
  closeInfo({ commit }) {
    commit('stateCloseInfo');
  },
  showWarning({ commit }, message) {
    commit('stateShowWarning', message);
  },
  closeWarning({ commit }) {
    commit('stateCloseWarning');
  },
  showSuccess({ commit }, message) {
    commit('stateShowSuccess', message);
  },
  closeSuccess({ commit }) {
    commit('stateCloseSuccess');
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
