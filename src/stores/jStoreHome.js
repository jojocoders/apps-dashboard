/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import router from '@/router';
import apiHome from '@/services/jApiHome';
import apiFavorite from '@/services/jApiFavorite';

const state = {
  // Set default state(s) here
  applications_list: [],
  group_applications_list: [],
  pages_list: [],
  sidebar: false,
  group_apps_list: [],
  fav_apps_list: [],
  banner_list: [],
  list_widget_detail: [],
  timesheet_detail: null,
  attendance_detail: null,
  list_quota_leave: [],

  // force upload photo
  modal_upload_photo: false,

  // New state app
  list_apps: [],
  list_favs_apps: [],
  list_group_apps: [],
  is_finish_load_app: false,

  tab_profile: null,
};

const mutations = {
  setApplicationsList(state, payload) {
    state.applications_list = payload;
  },
  setGroupApplicationsList(state, payload) {
    state.group_applications_list = payload;
  },
  setSidebar(state, payload) {
    state.sidebar = payload;
  },
  setGroupAppsList(state, payload) {
    state.group_apps_list = payload;
  },
  setFavoriteApplication(state, payload) {
    state.fav_apps_list = payload;
  },
  setBanner(state, payload) {
    state.banner_list = payload;
  },
  setListWidgetDetail(state, data) {
    state.list_widget_detail = data;
  },
  setTimesheetDetail(state, data) {
    state.timesheet_detail = data;
  },
  setAttendanceDetail(state, data) {
    state.attendance_detail = data;
  },
  setQuotaLeave(state, data) {
    state.list_quota_leave = data;
  },
  setModalUploadPhoto(state, data) {
    state.modal_upload_photo = data;
  },
  setListApps(state, data) {
    state.list_apps = data;
  },
  setListFavsApps(state, data) {
    state.list_favs_apps = data;
  },
  setListGroupApps(state, data) {
    state.list_group_apps = data;
  },
  setFinishLoadApp(state, data) {
    state.is_finish_load_app = data;
  },
  setTabProfile(state, data) {
    state.tab_profile = data;
  },
};

const actions = {
  async actionGetApplicationsList({ dispatch, commit }, data) {
    try {
      const res = await apiHome.getApplicationsList(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setApplicationsList', res.data);
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetGroupApplicationsList({ dispatch, commit }, data) {
    try {
      const res = await apiHome.getGroupApplicationsList(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setGroupApplicationsList', res.data);
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // for single select
  async actionGetGroupAppsList({ dispatch, commit }, data) {
    try {
      const res = await apiHome.getGroupAppsList(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setGroupAppsList', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionCreateApplication({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiHome.createApplication(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionCreatePage({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiHome.createPage(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionUpdatePage({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiHome.updatePage(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionDeletePage({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiHome.deletePage(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionGetListIcons({ dispatch }, data) {
    try {
      const res = await apiHome.getListIcons(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // favorite application
  async actionGetFavoriteApplication({ dispatch, commit }, data) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiFavorite.getFavoriteApplication(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setFavoriteApplication', res.data);
        return res.data;
        // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionCreateFavoriteApplication({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiFavorite.createFavoriteApplication(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionUpdateFavoriteApplication({ dispatch }, { appFavId, param }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiFavorite.updateFavoriteApplication(appFavId, param);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionDeleteFavoriteApplication({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiFavorite.deleteFavoriteApplication(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionGetBanner({ dispatch, commit }, params) {
    try {
      const res = await apiHome.getCompanyBanner(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setBanner', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  toogleSidebar({ commit }, data) {
    commit('setSidebar', data);
  },

  // WIDGET
  async getListWidgetDetail({ commit, dispatch }, req) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const listWidget = await apiHome.getListWidgetDetail(req.params);
      commit('setListWidgetDetail', listWidget.data);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getTimesheetDetail({ commit, dispatch }, req) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const timesheetDetail = await apiHome.getTimesheetDetail(req);
      commit('setTimesheetDetail', timesheetDetail.data);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getAttendanceDetail({ commit, dispatch }) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const attendanceDetail = await apiHome.getAttendanceDetail();
      commit('setAttendanceDetail', attendanceDetail.data);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getOrganigramId({ dispatch }, req) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    let res;
    try {
      const response = await apiHome.getOrganigramId(req);
      res = response;
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err;
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async getQuotaLeave({ commit, dispatch }, req) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const quotaLeave = await apiHome.getQuotaLeave(req);
      commit('setQuotaLeave', quotaLeave.data);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetListNotification({ dispatch }, data) {
    try {
      const res = await apiHome.getListNotification(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionReadNotification({ dispatch }, data) {
    try {
      const res = await apiHome.readNotification(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionDeleteNotification({ dispatch }, data) {
    try {
      const res = await apiHome.deleteNotification(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
