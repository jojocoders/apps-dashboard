/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiTaxLocation from '@/services/jApiTaxLocation';

const state = {

};

const mutations = {

};

const actions = {
  // get list tax location
  async actionGetTaxLocationList({ dispatch }, { reqData, callback }) {
    try {
      const listTax = await jApiTaxLocation.getTaxLocationList(reqData);
      const res = listTax.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  // TAX LOCATION
  async actionCreateTaxLocation({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTaxLocation.createTaxLocation(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateTaxLocation({ dispatch }, { params, taxLocationId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTaxLocation.updateTaxLocation(params, taxLocationId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteTaxLocation({ dispatch }, taxLocationId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTaxLocation.deleteTaxLocation(taxLocationId);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
