/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jUserAPIManager from '@/services/payroll/jUserAPIManager';

const state = {
  responseAction: 0,
  errorMessage: 'Failed',
  headerValidationList: [],
  invalidHeaderList: [],
  allowances: [],
  benefits: [],
  deductions: [],
  bonuses: [],
  overtimes: [],
};

const mutations = {
  setInvalidHeaderList(state, data) {
    state.invalidHeaderList = data;
  },
  onResetResponseAction() {
    state.responseAction = 0;
  },
  actionOnRequestFailed(state, data) {
    state.errorMessage = data.message;
    state.responseAction = 99;
  },
  actionOnRequestGetAllowanceSuccess(state, data) {
    state.allowances = data.allowances;
  },
  actionOnRequestGetDeductionSuccess(state, data) {
    state.deductions = data.deductions;
  },
  actionOnRequestGetBenefitSuccess(state, data) {
    state.benefits = data.benefits;
    state.benefits.forEach((element) => {
      state.headerValidationList.push(`benefit__${element.name}`);
    });
  },
  actionOnRequestGetBonusSuccess(state, data) {
    state.bonuses = data.bonus;
  },
  actionOnRequestGetOvertimesSuccess(state, data) {
    state.overtimes = data.overtimes;
  },
  setHeaderValidationList(state, data) {
    state.headerValidationList.push(data);
  },
};

const actions = {
  onSetInvalidHeaderList({ commit }, data) {
    commit('setInvalidHeaderList', data);
  },
  resetResponseAction({ commit }) {
    commit('onResetResponseAction');
  },

  requestGetAllowanceList({ commit }) {
    jUserAPIManager.requestGetAllowanceList()
      .then((response) => {
        commit('actionOnRequestGetAllowanceSuccess', response.data);
      })
      .catch(() => {
        commit('actionOnRequestFailed');
      });
  },
  requestGetBenefitList({ commit }) {
    jUserAPIManager.requestGetBenefitList()
      .then((response) => {
        commit('actionOnRequestGetBenefitSuccess', response.data);
        commit('setHeaderValidationList', response.data.benefits);
      })
      .catch(() => {
        commit('actionOnRequestFailed');
      });
  },
  requestGetDeductionList({ commit }) {
    jUserAPIManager.requestGetDeductionList()
      .then((response) => {
        commit('actionOnRequestGetDeductionSuccess', response.data);
      })
      .catch(() => {
        commit('actionOnRequestFailed');
      });
  },
  requestGetBonusList({ commit }) {
    jUserAPIManager.requestGetBonusList()
      .then((response) => {
        commit('actionOnRequestGetBonusSuccess', response.data);
      })
      .catch(() => {
        commit('actionOnRequestFailed');
      });
  },
  requestGetOvertimeList({ commit }) {
    jUserAPIManager.requestGetOvertime()
      .then((response) => {
        commit('actionOnRequestGetOvertimesSuccess', response.data);
      })
      .catch(() => {
        commit('actionOnRequestFailed');
      });
  },
  setHeaderValidationList({ commit }, header) {
    commit('setHeaderValidationList', header);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
