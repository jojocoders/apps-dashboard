/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiMaintenanceMode from '@/services/jApiMaintenanceMode';

const state = {
  maintenance_mode_list: [],
};

const mutations = {
  setMaintenanceModeDetail(state, payload) {
    state.maintenance_mode_list = payload;
  },
};

const actions = {
  async actionGetMaintenanceModeList({ dispatch, commit }, data) {
    try {
      const res = await apiMaintenanceMode.getMaintenanceModeList(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setMaintenanceModeDetail', res.data);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionCreateMaintenanceMode({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiMaintenanceMode.createMaintenanceMode(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionUpdateMaintenanceMode({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiMaintenanceMode.updateMaintenanceMode(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionTerminateMaintenanceMode({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiMaintenanceMode.terminateMaintenanceMode(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
