/* eslint-disable no-shadow,consistent-return,no-param-reassign */

const state = {
  // Set default state(s) here
  sidebar: false,
  search_suggestion: false,
  search_query: null,
  is_focus_in: false,
};

const mutations = {
  setSidebar(state, payload) {
    state.sidebar = payload;
  },
  setSearchSuggestion(state, payload) {
    state.search_suggestion = payload;
  },
  setSearchQuery(state, payload) {
    state.search_query = payload;
  },
  setFocusIn(state, payload) {
    state.is_focus_in = payload;
  },
};

const actions = {
  toogleSidebar({ commit }, data) {
    commit('setSidebar', data);
  },
  toggleSearchSuggestion({ commit }, data) {
    commit('setSearchSuggestion', data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
