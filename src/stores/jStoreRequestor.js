
import router from '@/router';
import apiRequestor from '@/services/jApiRequestor';
import apiRecord from '@/services/jApiRecord';

const state = {
  summary_block: [],
  record_deleted: false,
  record_closed: false,
};

const mutations = {
  // eslint-disable-next-line
  showSummaryBlock(state, data) {
    state.summary_block = data.data;
  },

  // eslint-disable-next-line
  deletedData(state) {
    state.record_deleted = !state.record_deleted;
  },

  // eslint-disable-next-line
  closedData(state) {
    state.record_closed = !state.record_closed;
    // router.push('/requestor/closed');
  },

  // eslint-disable-next-line
  cancelData(state) {
    state.record_closed = !state.record_closed;
    // router.push('/requestor/cancel');
  },
};

const actions = {
  async getSummaryBlock({ commit, dispatch }) {
    try {
      const getSummaryBlock = await apiRequestor.getSummaryBlock();
      commit('showSummaryBlock', getSummaryBlock);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // async sentDataDraftRequestor({ commit, dispatch }) {
  //   try {
  //     const getSummaryBlock = await apiRequestor.getSummaryBlock();
  //     commit('showSummaryBlock', getSummaryBlock);
  //   } catch (err) {
  //     dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
  //   }
  // },

  async sentDataDraftRequestor({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const res = await apiRequestor.sentDataDraftRequestor(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      setTimeout(() => {
        router.push('/requestor/sent');
      }, 1500);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async deleteDataDraftRequestor({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const res = await apiRecord.deleteDataRequestor(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      setTimeout(() => {
        commit('deletedData');
      }, 1500);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async closedDataApproveRequestor({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const res = await apiRequestor.closedDataApproveRequestor(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('closedData');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async cancelDataApproveRequestor({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const res = await apiRequestor.cancelDataApproveRequestor(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('cancelData');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getUser({ dispatch }, { reqData, callback }) {
    try {
      const listDivision = await apiRequestor.getEmployee(reqData);
      const res = listDivision.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.name} (${lab.email})`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getUserNew({ dispatch }, { reqData, callback }) {
    try {
      const listDivision = await apiRequestor.getEmployeeNew(reqData);
      const res = listDivision.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.profile.first_name} ${lab.profile.last_name} (${lab.email})`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getRelation({ dispatch }, { reqData, callback }) {
    try {
      const listRelation = await apiRequestor.getRelationRecord(reqData);
      const res = listRelation.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async generateReportPDF({ dispatch }, reqData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      const res = await apiRequestor.getReportPDF(reqData);
      window.open(res.data.url, '_blank');
      response = res;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return response;
  },

};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
