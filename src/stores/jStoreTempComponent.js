import jApiTempCompPayroll from '@/services/jApiTempCompPayroll';

const state = {
  exportRequestData: [],
  yearList: [],
  filterList: [{
    id: 'employee_id',
    text: 'Employee ID',
  }, {
    id: 'name',
    text: 'Employee Name',
  }, {
    id: 'component',
    text: 'Component',
  }, {
    id: 'payment_date',
    text: 'Payment Date',
  }, {
    id: 'amount',
    text: 'Amount',
  }, {
    id: 'description',
    text: 'Description',
  }, {
    id: 'uploaded_date',
    text: 'Uploaded Date',
  }],
  componentList: [{
    id: 'allowance',
    text: 'Allowance',
  }, {
    id: 'bonus',
    text: 'Bonus',
  }, {
    id: 'deduction',
    text: 'Deduction',
  }],
  componentNameList: null,
  uiData: {
    payrollId: null,
    selectedMonth: 1,
    selectedYear: 2019,
    selectedFilter: null,
    searchQuery: null,
    selectedFilterComponentName: null,
    selectedFilterComponentType: null,
    payment_date_end: null,
    payment_date_start: null,
    uploaded_date_end: null,
    uploaded_date_start: null,
    pagination: {
      limit: 0,
      page: 0,
    },
  },
  formTemporary: {
    temporaryId: null,
    payrollTemporaryComponentId: null,
    value: null,
    payment_date: null,
    description: null,
  },
  responseAction: 0,
  errorMessage: 'Failed',
  successMessage: 'Success',
  stateForLoadExportRequestData: 0,
  url: '',
  // new
  detailTemp: {},
  successUpdate: false,
};

const mutations = {
  // eslint-disable-next-line
  setDetail(state, data) {
    state.detailTemp = data;
  },
  setSuccessUpdate() {
    state.successUpdate = true;
  },
  unsetSuccessUpdate() {
    state.successUpdate = false;
  },
};

const actions = {
  async getAllTemporaryCompData({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });

    try {
      const res = await jApiTempCompPayroll.getAllTemporaryCompData(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('appData');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async updateTemporaryCompData({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });
    try {
      // eslint-disable-next-line
      const res = await jApiTempCompPayroll.updateTemporaryCompData(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Successfully update temporary component data', { root: true });
      commit('setSuccessUpdate');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async deleteTemporaryCompData({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });
    try {
      // eslint-disable-next-line
      const res = await jApiTempCompPayroll.deleteTemporaryCompData(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Successfully delete temporary component data', { root: true });
      commit('setSuccessUpdate');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async generateTemporaryCompData({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });
    try {
      await jApiTempCompPayroll.generateTemporaryCompData(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Success Generate Temporary Component', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async importTemporaryComp({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTempCompPayroll.importTemporaryComp(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', 'Success Import Data', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async downloadTemporaryComp({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTempCompPayroll.downloadTemporaryComp(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      window.open(res.data.url, '_blank');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
