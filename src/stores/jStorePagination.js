import apiRecord from '@/services/jApiRecord';
/* eslint-disable no-shadow,consistent-return,no-param-reassign */

const state = {
  // Set default state(s) here

};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {

};

const actions = {
  async listCategoryinRecord({ dispatch }, { reqData, callback }) {
    try {
      const resData = await apiRecord.listCategoryinRecord(reqData);
      const res = resData.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
