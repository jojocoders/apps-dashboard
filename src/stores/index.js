/* eslint-disable max-len */
import Vue from 'vue';
import Vuex from 'vuex';

// core/pro stores
import jLogin from './jLogin';
import jStorePosition from './jStorePosition';
import jFlows from './jFlows';
import jStoreDivisionFlow from './jStoreDivisionFlow';
import jRules from './jRules';
import jOrganigram from './jOrganigram';
import jStoreCompanyLocation from './jStoreCompanyLocation';

// times leave & timesheet stores
import jStoreLeaveClaimer from './leave/jStoreLeaveClaimer';
import jStoreLeaveApprover from './leave/jStoreLeaveApprover';
import jStoreLeaveConfig from './leave/jStoreLeaveConfig';
import jStoreLeaveConfigMapping from './leave/jStoreLeaveConfigMapping';
import jStoreReportLeave from './leave/jStoreReportLeave';
import jStoreAttendance from './jStoreAttendance';
import jStoreCoAttendance from './jStoreCoAttendance';
import jStoreLeave from './jStoreLeave';
import jStoreOvertime from './jStoreOvertime';
import jStoreTimesheetClaimerList from './timesheet/claimer/jStoreTimesheetClaimerList';
import jStoreTimesheetClaimerCreate from './timesheet/claimer/jStoreTimesheetClaimerCreate';
import jStoreTimesheetConfigTaskAssignment from './timesheet/config/jStoreTimesheetConfigTaskAssignment';
import jStoreTimesheetConfigTaskPolicy from './timesheet/config/jStoreTimesheetConfigTaskPolicy';
import jStoreTimesheetConfigTaskProject from './timesheet/config/jStoreTimesheetConfigTaskProject';
import jStoreReportTimesheet from './timesheet/report/jStoreReportTimesheet';

// payroll
import jStoreGlobalOptionsPayroll from './jStoreGlobalOptionsPayroll';
import jStorePayrollArea from './jStorePayrollArea';
import jStorePayrollEmployee from './jStorePayrollEmployee';

// Modules import downhere
import jStoreLogin from './jStoreLogin';
import jStoreLogout from './jStoreLogout';
import jStoreNotificationScreen from './jStoreNotificationScreen';
import jStoreHome from './jStoreHome';
import jStoreApplication from './jStoreApplication';
import jStoreAccessManagement from './jStoreAccessManagement';
import jStoreDashboard from './jStoreDashboard';
import jStoreFormula from './jStoreFormula';
import jStoreRecordForm from './jStoreRecordForm';
import jStoreRequestor from './jStoreRequestor';
import jStoreApprover from './jStoreApprover';
import jStoreOrgchart from './jStoreOrgchart';
import jStoreSetting from './jStoreSetting';
import jStorePrivilege from './jStorePrivilege';
import jStoreDynamicWorkflow from './jStoreDynamicWorkflow';
import jStoreSignature from './jStoreSignature';
import jStoreDocument from './jStoreDocument';
import jStoreFormUi from './jStoreFormUi';
import jStoreTaxLocation from './jStoreTaxLocation';
import jStoreCore from './jStoreCore';
import jStoreUser from './jStoreUser';
import jStoreProfile from './jStoreProfile';
import jStoreTempComponent from './jStoreTempComponent';
import jStoreEmployee from './jStoreEmployee';
import jStoreImport from './jStoreImport';
import jStorePayrollInfo from './jStorePayrollInfo';
import jStoreAddPayroll from './jStoreAddPayroll';
import jStoreDynamicFieldSetting from './jStoreDynamicFieldSetting';
import jStoreCompanySetting from './jStoreCompanySetting';
import jStoreMaintenanceMode from './jStoreMaintenanceMode';
import jStoreSession from './jStoreSession';

// Initialize vuex
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    // Include imported modules in here
    jStoreLogin,
    jStoreLogout,
    jStoreNotificationScreen,
    jStoreHome,
    jStoreApplication,
    jStoreAccessManagement,
    jStoreDashboard,
    jStoreFormula,
    jStoreRecordForm,
    jStoreRequestor,
    jStoreApprover,
    jStoreOrgchart,
    jStoreSetting,
    jStorePrivilege,
    jStoreDynamicWorkflow,
    jStoreSignature,
    jStoreAttendance,
    jStoreCoAttendance,
    jStoreLeave,
    jStoreOvertime,
    jStoreDocument,
    jStoreFormUi,
    jStoreTaxLocation,
    jStoreCore,
    jStoreUser,
    jStoreImport,
    jStorePayrollInfo,
    jStoreAddPayroll,
    jStoreDynamicFieldSetting,
    jStoreCompanySetting,
    jStoreMaintenanceMode,
    jStoreSession,

    // timesheet
    jStoreTimesheetClaimerList,
    jStoreTimesheetClaimerCreate,
    jStoreReportTimesheet,
    jStoreTimesheetConfigTaskAssignment,
    jStoreTimesheetConfigTaskPolicy,
    jStoreTimesheetConfigTaskProject,

    // leave
    jStoreLeaveClaimer,
    jStoreLeaveApprover,
    jStoreLeaveConfig,
    jStoreLeaveConfigMapping,
    jStoreReportLeave,

    // payroll
    jStoreGlobalOptionsPayroll,
    jStorePayrollArea,
    jStorePayrollEmployee,

    // core
    jFlows,
    jLogin,
    jOrganigram,
    jRules,
    jStoreCompanyLocation,
    jStoreDivisionFlow,
    jStorePosition,
    jStoreProfile,
    jStoreTempComponent,
    jStoreEmployee,
  },
  strict: debug,
});
