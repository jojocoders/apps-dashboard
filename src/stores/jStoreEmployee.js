/* eslint-disable no-shadow,consistent-return,no-param-reassign,no-unused-vars */
import jApiEmployee from '@/services/jApiEmployee';
// import router from '@/router';

const state = {
  employee_created: false,
  employee_detail: null,
  organigram_detail: null,
  list_company: null,
  list_country: [],
  list_province: [],
  list_city: [],
  list_bank: [],
  list_teams: [],
  list_education_information: [],
  list_emergency_contact: [],
  list_family_relations: [],
  list_medical_records: [],
  list_employee_documents: [],
  list_career_histories: [],
  used_facilities_on_board: [],
  list_facilities_on_board: [],
  list_on_boarding: [],
  list_action: [],
  reason: [],
  list_user_historical: [],
  list_type_employee: [],
  list_type_child: [],
  activity_logs: [],
  list_reason: [],
  list_grade: [],
  mapping_organigram: [],
  mapping_grade: [],
  mapping_employment: [],
  mapping_employment_child: [],
};

const mutations = {
  employeeCreated(state) {
    state.employee_created = !state.employee_created;
    // router.push('/company/employee');
  },

  showDetailEmployee(state, data) {
    state.employee_detail = data.user;
  },

  showListType(state, data) {
    state.list_type_employee = data;
  },

  showListCompany(state, data) {
    data.forEach((val) => {
      val.name = `${val.code} - ${val.name}`;
    });
    state.list_company = data;
  },

  showListCountry(state, data) {
    state.list_country = data.data;
  },

  showListProvince(state, data) {
    state.list_province = data.data;
  },

  showListCity(state, data) {
    state.list_city = data.data;
  },

  showListBank(state, data) {
    state.list_bank = data;
  },

  showDetailOrganigram(state, data) {
    state.organigram_detail = data.data;
  },

  setMappingOrganigram(state, data) {
    state.mapping_organigram = data;
  },

  setReasonList(state, data) {
    data.forEach((lab, index) => {
      data[index].reason_id = data[index].id;
      data[index].id = lab.name;
    });
    state.list_reason = data;
  },

  setMasterGradeList(state, data) {
    data.forEach((lab, index) => {
      data[index].label = `${lab.code} - ${lab.name}`;
    });
    state.list_grade = data;
  },

  showListTeams(state, data) {
    state.list_teams = data;
  },

  mutateGetListAction(state, data) {
    state.list_action = data.data;
  },
  mutateReasonUpdate(state, data) {
    state.reason = data;
  },
  mutateListUserHistorical(state, data) {
    state.list_user_historical = data.data.data;
  },
  setActivityLogs(state, data) {
    state.activity_logs = data.data;
  },
  setMappingEmployment(state, data) {
    state.mapping_employment = data;
  },
  setMappingEmploymentChild(state, data) {
    state.mapping_employment_child = data;
  },
  setMappingGrade(state, data) {
    state.mapping_grade = data;
  },
  updateEmployeeDetail(state, data) {
    if (data.contract_type) {
      state.employee_detail.company.contract_type = data.contract_type.toUpperCase();
    }
    if (data.join_date) {
      state.employee_detail.company.join_date = data.join_date;
    }
    if (data.resign_date) {
      state.employee_detail.company.resign_date = data.resign_date;
    }
  },
  updateEmployeeType(state, data) {
    if (data.employment_type) {
      state.employee_detail.company.contract_type = data.employment_type.name.toUpperCase();
    } else {
      state.employee_detail.company.contract_type = '';
    }
  },
};

const actions = {
  async createEmployee({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createEmployee = await jApiEmployee.createEmployee(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createEmployee.message, { root: true });
      commit('employeeCreated');
      return createEmployee;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createEmployeeAuthService({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createEmployee = await jApiEmployee.createEmployeeAuthService(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createEmployee.message, { root: true });
      commit('employeeCreated');
      return createEmployee;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async detailEmployee({ commit, dispatch }, employeeId) {
    try {
      const detailEmployee = await jApiEmployee.detailEmployee(employeeId);
      commit('showDetailEmployee', detailEmployee);
      dispatch('detailOrganigram', { user_company_id: detailEmployee.user.user_company_id });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async detailEmployeeR4({ commit, dispatch }) {
    try {
      const detailEmployee = await jApiEmployee.detailEmployeeR4();
      commit('showDetailEmployee', detailEmployee);
      dispatch('detailOrganigram', { user_company_id: detailEmployee.user.company.user_company_id });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateEmployee({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateEmployee = await jApiEmployee.updateEmployee(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Successfully update your profile', { root: true });
      commit('employeeCreated');
      commit('updateEmployeeDetail', updateEmployee.data);
      return updateEmployee;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async changePasswordEmployee({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const response = await jApiEmployee.changePasswordEmployee(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Successfully change password', { root: true });
      return response;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async updateProfile({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateEmployee = await jApiEmployee.updateProfile(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Successfully update your profile', { root: true });
      commit('employeeCreated');
      commit('updateEmployeeDetail', updateEmployee.data);
      return updateEmployee;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async updateWithReason({ dispatch }, { id, payload }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateWithReason(id, payload);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      return err.response.data;
    }
  },

  async deleteEmployee({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const deleteEmployee = await jApiEmployee.deleteEmployee(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', deleteEmployee.message, { root: true });
      commit('employeeCreated');
      return deleteEmployee;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async getTypeEmployee({ dispatch, commit }, reqData) {
    try {
      const listEmployee = await jApiEmployee.listEmployeeType(reqData);
      const type = listEmployee.data;
      type.forEach((lab, index) => {
        type[index].label = lab.name;
      });
      commit('showListType', type);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getChildByEmployment({ dispatch, commit }, reqData) {
    try {
      const listChild = await jApiEmployee.getChildByEmployment(reqData);
      const child = listChild.data;
      child.forEach((lab, index) => {
        child[index].label = lab.name;
      });
      commit('showListChild', child);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListBank({ dispatch }, { reqData, callback }) {
    try {
      const listBank = await jApiEmployee.listBank(reqData);
      const res = listBank.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListBankAll({ commit, dispatch }, reqData) {
    try {
      const listBank = await jApiEmployee.listBank(reqData);
      const res = listBank.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      commit('showListBank', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListCompany({ commit, dispatch }, req) {
    try {
      const listCompany = await jApiEmployee.listCompany(req);
      commit('showListCompany', listCompany.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListCountry({ commit, dispatch }) {
    try {
      const listCountry = await jApiEmployee.listCountry();
      commit('showListCountry', listCountry);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListProvince({ commit, dispatch }, req) {
    try {
      const listProvince = await jApiEmployee.listProvince(req);
      commit('showListProvince', listProvince);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListCity({ commit, dispatch }, req) {
    try {
      const listCity = await jApiEmployee.listCity(req);
      commit('showListCity', listCity);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListTeams({ commit, dispatch }, reqData) {
    try {
      const listTeams = await jApiEmployee.listTeams(reqData);
      const newVal = listTeams.data;
      newVal.forEach((lab, index) => {
        newVal[index].name = lab.title;
      });
      commit('showListTeams', newVal);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetReasonList({ commit, dispatch }) {
    try {
      const res = await jApiEmployee.getReasonList();
      const newVal = res.data;
      newVal.forEach((lab, index) => {
        newVal[index].name = lab.name;
      });
      commit('setReasonList', newVal);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListDivision({ dispatch }, { reqData, callback }) {
    try {
      const listDivision = jApiEmployee.getDivisionList(reqData);
      const res = listDivision.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.data.message, { root: true });
    }
  },

  async detailOrganigram({ commit }, employeeId) {
    try {
      const detailOrganigram = await jApiEmployee.detailOrganigram(employeeId);
      commit('showDetailOrganigram', detailOrganigram);
    } catch (err) {
      // eslint-disable-next-line max-len
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetMappingOrganigram({ dispatch, commit }, params) {
    try {
      const res = await jApiEmployee.getMappingOrganigram(params);
      commit('setMappingOrganigram', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateOrganigram({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateOrganigram = await jApiEmployee.updateOrganigram(req.payload);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateOrganigram.message, { root: true });
      dispatch('detailOrganigram', { user_company_id: req.user_company_id });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async getListPosition({ dispatch }, { reqData, callback, label = 'name' }) {
    try {
      const listPosition = await jApiEmployee.listPosition(reqData);
      const res = [...listPosition.data];
      res.forEach((lab, index) => {
        // eslint-disable-next-line
        // res[index].name = `${lab.id} - ${lab.code} - ${lab.name} ${typeof lab.user === 'undefined' ? '(vacant)' : ''}`;
        // eslint-disable-next-line
        res[index].user_name = lab.user ? (lab.user.profile ? (`${lab.user.profile.first_name ? lab.user.profile.first_name : ''} ${lab.user.profile.last_name ? lab.user.profile.last_name : ''}`) : '') : '';
        // eslint-disable-next-line
        res[index].email = lab.user ? (lab.user.email ? lab.user.email : '-') : '-';
        res[index].label = lab[label];
      });
      callback(null, [...res]);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListAction({ commit }, employeeId) {
    const listAction = await jApiEmployee.getListAction(employeeId);
    commit('mutateGetListAction', listAction);
  },

  async deleteListAssignment({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      await jApiEmployee.deleteListAssignment(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Deleted', { root: true });
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        commit('mutateError');
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async updateReason({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const reasonUpdate = await jApiEmployee.updateReason(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Updated', { root: true });
      commit('mutateReasonUpdate', reasonUpdate);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        commit('mutateError');
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async generateEmployeeId({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.generateEmployeeId(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      return err.response.data;
    }
  },

  mutateListUserHistorical({ commit }, message) {
    commit('mutateListUserHistorical', message);
  },

  async actionGetActivityLogs({ commit }, params) {
    try {
      const res = await jApiEmployee.getActivityLogs(params);
      commit('setActivityLogs', res.data);
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem',
      //   err.response.data.message, { root: true });
    }
  },

  async actionCreateActivityLog({ dispatch }, params) {
    try {
      const res = await jApiEmployee.createActivityLog(params);
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem',
      // err.response.data.message, { root: true });
    }
  },

  // MAPPING GRADE
  async actionGetMasterGradeList({ commit, dispatch }, params) {
    try {
      const res = await jApiEmployee.getGradeList(params);
      const newVal = res.data.data;
      newVal.forEach((lab, index) => {
        newVal[index].name = lab.name;
      });
      commit('setMasterGradeList', newVal);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetMappingGrade({ dispatch, commit }, params) {
    try {
      const res = await jApiEmployee.getMappingGrade(params);
      commit('setMappingGrade', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateMappingGrade({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createMappingGrade(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateMappingGrade({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateMappingGrade(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteMappingGrade({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.deleteMappingGrade(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // MAPPING EMPLOYMENT
  async actionGetMappingEmployment({ dispatch, commit }, params) {
    try {
      const res = await jApiEmployee.getMappingEmployment(params);
      commit('setMappingEmployment', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateMappingEmployment({ dispatch, commit }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createMappingEmployment(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('updateEmployeeType', res.data);
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateMappingEmployment({ dispatch, commit }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateMappingEmployment(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('updateEmployeeType', res.data);
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteMappingEmployment({ dispatch, commit }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.deleteMappingEmployment(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('updateEmployeeType', res.data);
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // MAPPING EMPLOYMENT CHILD
  async actionGetMappingEmploymentChild({ dispatch, commit }, params) {
    try {
      const res = await jApiEmployee.getMappingEmploymentChild(params);
      commit('setMappingEmploymentChild', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateMappingEmploymentChild({ dispatch, commit }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createMappingEmploymentChild(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('updateEmployeeType', res.data);
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateMappingEmploymentChild({ dispatch, commit }, { id, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateMappingEmploymentChild(id, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('updateEmployeeType', res.data);
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteMappingEmploymentChild({ dispatch, commit }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.deleteMappingEmploymentChild(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('updateEmployeeType', res.data);
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // Export historical to email
  async actionExportHistoricalToEMail({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.exportHistoricalToEMail(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionCreateMappingPosition({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createMappingPosition(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionUpdateMappingPosition({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateMappingPosition(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async getListPositionDefault({ dispatch }) {
    let res = null;
    try {
      const reqData = {
        pagination: {
          limit: 50,
          page: 1,
          column: '',
          ascending: true,
          query: '',
          query_type: 'position',
        },
      };
      const listPosition = await jApiEmployee.listPosition(reqData);
      res = listPosition.data;
      res.forEach((lab, index) => {
        // eslint-disable-next-line
        // res[index].name = `${lab.id} - ${lab.code} - ${lab.name} ${typeof lab.user === 'undefined' ? '(vacant)' : ''}`;
        // eslint-disable-next-line
        res[index].user_name = lab.user ? (lab.user.profile ? (`${lab.user.profile.first_name ? lab.user.profile.first_name : ''} ${lab.user.profile.last_name ? lab.user.profile.last_name : ''}`) : '') : '';
        // eslint-disable-next-line
        res[index].email = lab.user ? (lab.user.email ? lab.user.email : '-') : '-';
        res[index].label = lab.name;
      });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
