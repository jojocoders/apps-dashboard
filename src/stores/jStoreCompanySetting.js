/* eslint-disable no-shadow */
import jApiCompanySetting from '../services/jApiCompanySetting';

const state = {
  force_change_password: false,
  advance_report: false,
  company_detail: null,
};

const mutations = {
  setForceChangePassword(state, payload) {
    state.force_change_password = payload.force_change_password;
  },
  setAdvanceReport(state, payload) {
    state.advance_report = payload;
  },
  setCompanyDetail(state, payload) {
    state.company_detail = payload;
  },
};

const actions = {
  async actionGetForceChangePassword({ dispatch, commit }) {
    try {
      const res = await jApiCompanySetting.getCompanySetting();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setForceChangePassword', res.data);
        commit('setAdvanceReport', res.data.advance_report);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.message, { root: true });
    }
  },

  async actionGetCompanyDetail({ dispatch, commit }) {
    try {
      const res = await jApiCompanySetting.getComapnyDetail();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setCompanyDetail', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.message, { root: true });
    }
  },
};


export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
