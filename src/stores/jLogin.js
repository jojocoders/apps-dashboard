/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import axios from 'axios';
import router from '@/router';
import apiGateway from '@/services/jApiGateway';
// import Vue from 'vue';
import apiProfile from '@/services/jApiProfile';
import apiUser from '@/services/jApiUser';
// const api = axios.create({
//   baseURL: process.env.GATE_URL,
// });

const state = {
  // Set default state(s) here
  idToken: null,
  refreshToken: null,

  profile_data: null,
  is_refresh_token: false,
  login_failed: false,
  currencyFormat: {
    digitGroupSeparator: '.',
    decimalCharacter: ',',
    decimalCharacterAlternative: '.',
    currencySymbol: 'Rp',
    currencySymbolPlacement: 'p',
    roundingMethod: 'U',
    minimumValue: '0',
    maximumValue: '99999999999999',
  },
  data_expire: null,
  roles: [],
  package: [],
  // userId: null,
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  loginFailed(state) {
    state.login_failed = !state.login_failed;
  },

  saveToken(state, data) {
    function parseJwt(token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      return JSON.parse(window.atob(base64));
    }
    state.data_expire = parseJwt(data.token);
    state.idToken = data.token;
    state.refreshToken = data.refresh_token;
    state.is_refresh_token = !state.is_refresh_token;
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
    // state.userId = data.refresh_token;
  },

  setInLocalStorage(state) {
    localStorage.setItem('idTokenalt', state.idToken);
    const tokenData = {
      value: state.idToken,
      expire: state.data_expire.exp * 1000,
    };
    const idToken = {
      value: state.idToken,
      expire: 10 * 60 * 1000,
    };
    localStorage.setItem('Token', JSON.stringify(tokenData));
    // Vue.ls.set('idToken', state.idToken, 60 * 60 * 1000);
    // Vue.ls.set('Token', state.idToken, 60 * 60 * 1000);
    // Vue.ls.set('idToken', state.idToken, 10 * 60 * 1000);
    localStorage.setItem('idToken', JSON.stringify(idToken));
    // Vue.ls.set('Token', state.idToken, 15 * 60 * 1000);
    // localStorage.setItem('userId', state.userId);
    localStorage.setItem('refreshToken', state.refreshToken);
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },

  setProfile(state, payload) {
    state.profile_data = payload;
    localStorage.setItem('email', state.profile_data.email);
    localStorage.setItem('photo', state.profile_data.photo_url);
    localStorage.setItem('firstName', state.profile_data.first_name);
    localStorage.setItem('lastName', state.profile_data.last_name);
    localStorage.setItem('userCompanyId', state.profile_data.user_company_id);
    localStorage.setItem('companyName', '');
    localStorage.setItem('daysRemaining', '15');
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },

  doLogin() {
    const role = JSON.parse(localStorage.getItem('role'));
    if (role.includes(1) || role.includes(2)) {
      router.push('/company/organization');
    } else if (role.includes(17) || role.includes(18)) {
      window.location = '/costcenter';
    }
  },

  setRole(state, roleData) {
    const role = roleData.data.role.states;
    const roleString = roleData.data.role.names;
    const packages = roleData.data.package.states;
    state.roles = role;
    state.package = packages;
    localStorage.setItem('package', JSON.stringify(state.package));
    localStorage.setItem('role', JSON.stringify(role));
    localStorage.setItem('roleString', JSON.stringify(roleString));
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },
};

const actions = {
  async loginAccount({ commit, dispatch }, authData) {
    dispatch('jNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.login(authData);
      commit('saveToken', auth);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile(auth);
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      commit('doLogin');
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
      dispatch('jNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async loginRefreshTokenApi({ commit, dispatch }, authData) {
    dispatch('jNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.login(authData);
      commit('saveToken', auth);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getLoginProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
      commit('loginFailed');
      dispatch('jNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async refreshTokenApi({ commit, dispatch }, req) {
    dispatch('jNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const newToken = await apiGateway.refreshToken(req);
      commit('saveToken', newToken);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getLoginProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
      dispatch('jNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getProfileAndRole({ commit, dispatch }) {
    dispatch('jNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      // const getProfil = await apiProfile.getLoginProfile();
      const getRole = await apiUser.userAuthorize();
      // commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jNotificationScreen/hideLoading', '', { root: true });
      dispatch('jNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async refreshTokenApiSingle({ commit, dispatch }, req) {
    try {
      const newToken = await apiGateway.refreshToken(req);
      commit('saveToken', newToken);
      commit('setInLocalStorage');
    } catch (err) {
      dispatch('jNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
