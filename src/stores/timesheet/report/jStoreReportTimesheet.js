/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiReportTimesheet from '@/services/timesheet/report/jApiReportTimesheet';

const state = {
  employee_list: [],
  task_list: [],
  project_list: [],
  field_list: [],
  summary_list: [],
  module_set: [],
};

const mutations = {
  showListEmployee(state, data) {
    state.employee_list = data.data.data;
  },

  showListTask(state, data) {
    state.task_list = data.data.data;
  },

  showListProject(state, data) {
    state.project_list = data.data.data;
  },

  showFormFields(state, data) {
    state.field_list = data.data.data.additional_info;
  },

  mutateModuleSet(state, data) {
    state.module_set = data.data;
  },

  mutateGetSummary(state, data) {
    state.summary_list = data.data;
  },

};

const actions = {
  async getListEmployee({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportTimesheet.getEmployeeList(rootId);
      commit('showListEmployee', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async getListTask({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportTimesheet.getTaskList(rootId);
      commit('showListTask', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListProject({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportTimesheet.getProjectList(rootId);
      commit('showListProject', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async getSummaryTable({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportTimesheet.getSummaryReport(rootId);
      commit('showSummaryTable', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateSummaryXLS({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportTimesheet.generateSummaryXLS(rootId);
      // window.location.href = data.data.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', data.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateDetailXLS({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportTimesheet.generateDetailXLS(rootId);
      window.location.href = data.data.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // Form-Project
  async getFormByTaskId({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportTimesheet.getFormByTaskId(rootId);
      commit('showFormFields', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
  async getModuleSetting({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportTimesheet.getModuleSetting(rootId);
      commit('mutateModuleSet', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateFormXLS({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportTimesheet.generateFormXLS(rootId);
      window.location.href = data.data.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', 'Failed to generate report.', { root: true });
    }
  },
  mutateGetSummary({ commit }, message) {
    commit('mutateGetSummary', message);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
