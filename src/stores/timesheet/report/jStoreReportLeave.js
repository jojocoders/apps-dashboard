/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiReportLeave from '@/services/leave/jApiReportLeave';

const state = {
  employee_list: [],
  leave_type_list: [],
  team_list: [],
  employee_list_organigram: [],
  org_unit_list: [],
};

const mutations = {
  showListEmployee(state, data) {
    state.employee_list = data.data.data;
  },

  showListEmployeeOrganigram(state, data) {
    state.employee_list_organigram = data.data.data;
  },

  showListLeaveType(state, data) {
    state.leave_type_list = data.data.data;
  },

  showListTeam(state, data) {
    state.team_list = data.data.data;
  },

  showListOrgUnit(state, data) {
    state.org_unit_list = data.data.data;
  },

};

const actions = {
  async getListEmployee({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportLeave.getEmployeeList(rootId);
      commit('showListEmployee', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async actionGetEmployeeRestrictionOrganigram({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportLeave.getEmployeeRestrictionOrganigram(rootId);
      commit('showListEmployeeOrganigram', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
  async actionGetListUserRestriction({ dispatch }, { reqData, callback }) {
    try {
      const listEmployee = await jApiReportLeave.getEmployeeRestrictionOrganigram(reqData);
      const res = listEmployee.data.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.employee_id} - ${lab.first_name} ${lab.last_name}`;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListLeaveType({ commit, dispatch }) {
    try {
      const list = await jApiReportLeave.getLeaveTypeList();
      commit('showListLeaveType', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListLeaveTypeByUser({ commit, dispatch }, req) {
    try {
      const list = await jApiReportLeave.getLeaveTypeByUser(req);
      commit('showListLeaveType', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListTeam({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportLeave.getTeamList(rootId);
      commit('showListTeam', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListOrgUnit({ commit, dispatch }, rootId) {
    try {
      const list = await jApiReportLeave.getOrgUnitList(rootId);
      commit('showListOrgUnit', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateSummaryXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportLeave.generateSummaryXLS(rootId);
      if (data.data.data.url) {
        window.location.href = data.data.data.url;
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateLeaveXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportLeave.generateLeaveXLS(rootId);
      if (data.data.data.url) {
        window.location.href = data.data.data.url;
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateLeavePDF({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportLeave.generateLeavePDF(rootId);
      if (data.data.filePath) {
        window.open(data.data.filePath);
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async generateLeaveDetailXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiReportLeave.generateLeaveDetailXLS(rootId);
      if (data.data.data.url) {
        window.location.href = data.data.data.url;
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
