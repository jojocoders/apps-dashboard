import router from '@/router';
import jApiTimesheetConfigTaskAssignment from '@/services/timesheet/config/jApiTimesheetConfigTaskAssignment';
import jApiTimesheetConfigTaskProject from '@/services/timesheet/config/jApiTimesheetConfigTaskProject';

const state = {
  task_assignment_order: null,

  assignment_data: {
    detail_assignment: null,
    list_assignment: null,
    assignment_deleted: 0,
  },

  position_list: [],
  division_list: [],
  task_list: [],
  project_list: [],

  organigram_detail: null,
  position_selected: [],
  division_selected: [],
};

const mutations = {
  // eslint-disable-next-line no-shadow
  showOrganigramDetail(state, data) {
    state.organigram_detail = data.data;
  },

  // eslint-disable-next-line no-shadow
  showListPosition(state, data) {
    state.position_list = data.data.data;
  },
  // eslint-disable-next-line no-shadow
  showListDivision(state, data) {
    state.division_list = data.data.data;
  },
  // eslint-disable-next-line no-shadow
  showListTask(state, data) {
    state.task_list = data.data.data;
  },
  // eslint-disable-next-line no-shadow
  showListProject(state, data) {
    state.project_list = data.data.data;
  },
  // eslint-disable-next-line no-shadow
  showDetailAssignment(state, data) {
    state.assignment_data.detail_assignment = data.data;
  },
  // eslint-disable-next-line no-shadow
  assignmentDeleted(state) {
    state.assignment_data.assignment_deleted += 1;
  },
};

const actions = {

  async getOrgDetail({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const list = await jApiTimesheetConfigTaskAssignment.getOrgDetail(rootId);
      commit('showOrganigramDetail', list);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListPosition({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const list = await jApiTimesheetConfigTaskAssignment.getPositionFormList(rootId);
      commit('showListPosition', list);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListDivision({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const list = await jApiTimesheetConfigTaskAssignment.getDivisionFormList(rootId);
      commit('showListDivision', list);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListTask({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const list = await jApiTimesheetConfigTaskAssignment.getTaskFormList(rootId);
      commit('showListTask', list);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
  },

  async getListTaskAsync({ dispatch }, { reqData, callback }) {
    try {
      const listTask = await jApiTimesheetConfigTaskAssignment.getTaskFormList(reqData);
      const res = listTask.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // get list Task project
  async getListTaskProject({ dispatch }, { reqData, callback }) {
    try {
      const listTaskProject = await jApiTimesheetConfigTaskAssignment.getTaskFormList(reqData);
      const res = listTaskProject.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getListProject({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const list = await jApiTimesheetConfigTaskProject.getProjectFormList(rootId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('showListProject', list);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async createTaskAssignment({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    let res;
    try {
      res = await jApiTimesheetConfigTaskAssignment.createTaskAssignment(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task Assignment Created', { root: true });
      // router.push('/timesheet/assignment/list');
      router.go(-1);
    } catch (err) {
      res = err;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', 'Error Created Assignment', { root: true });
    }
    return res;
  },

  async getAssignmentDetail({ commit, dispatch }, assignmentId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      // eslint-disable-next-line max-len
      const detailAssignment = await jApiTimesheetConfigTaskAssignment.getAssignmentDetail(assignmentId);
      commit('showDetailAssignment', detailAssignment);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getAssignmentDetailOnEvent({ dispatch }, assignmentId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    let res;
    try {
      // eslint-disable-next-line max-len
      res = await jApiTimesheetConfigTaskAssignment.getAssignmentDetail(assignmentId);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  // eslint-disable-next-line no-unused-vars
  async updateTaskAssignmentDetail({ dispatch, commit }, assignmentData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await jApiTimesheetConfigTaskAssignment.updateTaskAssignmentDetail(assignmentData);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task assignment edited', { root: true });
      // router.push('/timesheet/assignment/list');
      router.go(-1);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async deleteAssignment({ commit, dispatch }, assignmentId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await jApiTimesheetConfigTaskAssignment.deleteTaskAssignment(assignmentId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task assignment deleted', { root: true });
      commit('assignmentDeleted');
      // router.push('/timesheet/assignment/list');
      router.go(-1);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
