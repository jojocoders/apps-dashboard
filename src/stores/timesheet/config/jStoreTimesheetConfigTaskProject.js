import router from '@/router';
import jApiTimesheetConfigTaskProject from '@/services/timesheet/config/jApiTimesheetConfigTaskProject';

const state = {

  project_data: {
    detail_project: null,
    list_project: null,
    project_deleted: 0,
  },
  policy_data: {
    detail_policy: null,
  },
  form_list: [],
  project_list: [],
  error_submit: '',
};

const mutations = {

  // eslint-disable-next-line no-shadow
  showListForm(state, data) {
    state.form_list = data.data.additional_info_block;
  },
  // eslint-disable-next-line no-shadow
  showListProject(state, data) {
    state.project_list = data.data.data;
  },
  // eslint-disable-next-line no-shadow
  showDetailProject(state, data) {
    state.project_data.detail_project = data.data;
  },
  // eslint-disable-next-line no-shadow
  projectDeleted(state) {
    state.project_data.project_deleted += 1;
  },
  // eslint-disable-next-line no-shadow
  setErrorSubmit(state, data) {
    state.error_submit = data;
  },
};

const actions = {
  async getListForm({ commit, dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const listForm = await jApiTimesheetConfigTaskProject.getFormList(rootId);
      commit('showListForm', listForm);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async getListProject({ commit, dispatch }, rootId) {
    let response;
    try {
      const listProject = await jApiTimesheetConfigTaskProject.getProjectFormList(rootId);
      commit('showListProject', listProject);
      response = listProject.data.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
    return response;
  },

  async getListProjectAsync({ dispatch }, { reqData, callback }) {
    try {
      const listProject = await jApiTimesheetConfigTaskProject.getProjectFormList(reqData);
      const res = listProject.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async createTaskProject({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTimesheetConfigTaskProject.createTaskProject(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task Project Created', { root: true });
      // router.push('/timesheet/project/list');
      return res;
    } catch (err) {
      commit('setErrorSubmit', err.response.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      return err.response.data;
    }
  },

  async getProjectDetail({ commit, dispatch }, projectId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailProject = await jApiTimesheetConfigTaskProject.getProjectDetail(projectId);
      commit('showDetailProject', detailProject);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // eslint-disable-next-line no-unused-vars
  async updateTaskProjectDetail({ dispatch, commit }, projectData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await jApiTimesheetConfigTaskProject.updateTaskProjectDetail(projectData);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task project edited', { root: true });
      // router.push('/timesheet/project/list');
      router.go(-1);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async deleteProject({ commit, dispatch }, projectId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await jApiTimesheetConfigTaskProject.deleteTaskProject(projectId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task project deleted', { root: true });
      commit('projectDeleted');
      // router.push('/timesheet/project/list');
      router.go(-1);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async exportTaskProject({ dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiTimesheetConfigTaskProject.exportTaskProject();
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
