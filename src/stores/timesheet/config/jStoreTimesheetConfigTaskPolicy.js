/* eslint-disable no-shadow */
import jApiTimesheetConfigTaskPolicy from '@/services/timesheet/config/jApiTimesheetConfigTaskPolicy';

const state = {

  policy_data: {
    detail_policy: null,
  },

};

const mutations = {

  showDetailPolicy(state, data) {
    state.policy_data.detail_policy = data.data;
  },

};

const actions = {

  async getPolicyDetail({ commit, dispatch }, projectId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailPolicy = await jApiTimesheetConfigTaskPolicy.getPolicyDetail(projectId);
      commit('showDetailPolicy', detailPolicy);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createTaskPolicy({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'please wait...', { root: true });
    try {
      await jApiTimesheetConfigTaskPolicy.createTaskPolicy(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Task Policy Saved', { root: true });
      window.location.reload();
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
