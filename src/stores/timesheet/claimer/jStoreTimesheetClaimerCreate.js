/* eslint-disable import/no-named-as-default */
/* eslint-disable no-shadow */
// eslint-disable-next-line import/no-named-as-default-member
import jApiTimesheetClaimerCreate from '@/services/timesheet/claimer/jApiTimesheetClaimerCreate';

const state = {
  data_task_list_by_date: [],
  data_task_list: [],
  data_task_detail: [],
  is_still_running_task: false,
  success_update: '',
  error_update: '',
  field_list: [],
  field_list_update: [],
  data_task_new: null,
  data_logs: [],
  project_swimlane: null,
  organigram_id: null,
  data_approval_logs: [],
};

const mutations = {
  setTaskByDateList(state, data) {
    state.data_task_list_by_date = data;
  },
  setTaskList(state, data) {
    // state.data_task_list = data.map(val => ({ ...val, text: val.name }));
    // eslint-disable-next-line
    data.forEach(val => val.text = val.name);
    state.data_task_list = data;
  },
  setTaskDetail(state, data) {
    state.data_task_detail = data;
  },
  setEmptyTaskByDateList(state) {
    state.data_task_list_by_date = [];
  },
  setRunningStatus(state, data) {
    state.is_still_running_task = data.status;
  },
  setSuccessUpdate(state, data) {
    state.success_update = data;
    state.data_task_new = data;
  },
  setErrorUpdate(state, data) {
    state.error_update = data;
  },
  showFormFields(state, data) {
    state.field_list = data;
  },
  showFormFieldsDetail(state, data) {
    state.field_list_update = data;
  },
  setTaskLog(state, data) {
    state.data_logs = data;
  },
  setSwimlaneProject(state, data) {
    state.project_swimlane = data.data;
  },
  setOrgId(state, data) {
    state.organigram_id = data.data;
  },
  setLogApproval(state, data) {
    state.data_approval_logs = data;
  },
};

const actions = {
  async getListTaskByDate({ commit }, req) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const dataTaskByDate = await jApiTimesheetClaimerCreate.getListTaskByDate(req);
      commit('setTaskByDateList', dataTaskByDate.data);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setEmptyTaskByDateList');
    }
  },
  async getListTask({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const dataTask = await jApiTimesheetClaimerCreate.getListTask(req);
      commit('setTaskList', dataTask.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getListTaskDetailById({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const detailTask = await jApiTimesheetClaimerCreate.getListTaskDetailById(req);
      commit('setTaskDetail', detailTask.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async update({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const res = await jApiTimesheetClaimerCreate.updatedTime(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setSuccessUpdate', res.data);
      commit('setErrorUpdate', res);
    } catch (err) {
      commit('setErrorUpdate', err.response.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async updateNotes({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const res = await jApiTimesheetClaimerCreate.updateNotes(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setSuccessUpdate', res.data);
    } catch (err) {
      commit('setErrorUpdate', err.response.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getRunningTask({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const runningTask = await jApiTimesheetClaimerCreate.getRunningTask(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setRunningStatus', runningTask.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getFormByTaskId({ commit, dispatch }, rootId) {
    try {
      const list = await jApiTimesheetClaimerCreate.getFormByTaskId(rootId);
      commit('showFormFields', list.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
  async getForm({ commit, dispatch }, rootId) {
    try {
      const list = await jApiTimesheetClaimerCreate.getForm(rootId);
      commit('showFormFieldsDetail', list.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
  async createAdditionalData({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      await jApiTimesheetClaimerCreate.createAdditionalData(req);
      dispatch('getForm', { task_id: req.task_id });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getTaskLog({ commit, dispatch }, req) {
    try {
      const res = await jApiTimesheetClaimerCreate.getTaskLog(req);
      commit('setTaskLog', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getProjectSwimlane({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiTimesheetClaimerCreate.getProjectSwimlane(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setSwimlaneProject', res);
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getUser({ dispatch }, { reqData, callback }) {
    try {
      const listDivision = await jApiTimesheetClaimerCreate.getEmployee(reqData);
      const res = listDivision.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.name} (${lab.email})`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getRelation({ dispatch }, { reqData, callback }) {
    try {
      const listRelation = await jApiTimesheetClaimerCreate.getRelationRecord(reqData);
      const res = listRelation.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListTaskByOrganigram({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiTimesheetClaimerCreate.getOrgId(
        { user_company_id: data.user_company_id },
      );
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setOrgId', res);
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        try {
          const param = Object.assign({}, data);
          param.organigram_id = res.data[0].id;

          const dataTaskByDate = await jApiTimesheetClaimerCreate.getListTaskByDate(param);
          commit('setTaskByDateList', dataTaskByDate.data);
          dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        } catch (err) {
          dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
          commit('setEmptyTaskByDateList');
          dispatch('jStoreNotificationScreen/toggleInfo', 'There is no task, please add new one!', { root: true });
        }
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getLogApproval({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiTimesheetClaimerCreate.getLogApproval(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        if (res.data) {
          // eslint-disable-next-line
          let logArr = [];
          res.data.map((item) => {
            item.log_member.map((log) => {
              // eslint-disable-next-line
              log.photo = log.photo_url;
              // eslint-disable-next-line
              log.html = `<p class="c--${log.status} m__b--0 f--bold">${log.message}</p><i class="c--grey">${log.date_time}</i>`;
              // eslint-disable-next-line
              log.date_time = `Duration: ${log.duration ? log.duration : '0'}`;
              // eslint-disable-next-line
              log.message = '';

              logArr.push(log);
              return false;
            });
            return false;
          });
          commit('setLogApproval', logArr);
        }
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
