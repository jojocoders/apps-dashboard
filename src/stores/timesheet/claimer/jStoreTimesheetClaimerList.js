// import router from '@/router';
import jApiTimesheetClaimerList from '@/services/timesheet/claimer/jApiTimesheetClaimerList';

const state = {
  summary_block: '',
  response_reported_data: [],
  response_deleted_data: [],
};

const mutations = {
  // eslint-disable-next-line no-shadow
  getSummaryBlock(state, data) {
    state.summary_block = data.data;
  },
  // eslint-disable-next-line no-shadow
  getSummaryBlockApprover(state, data) {
    state.summary_block = data.data;
  },
  // eslint-disable-next-line no-shadow
  getResponseReportedData(state, data) {
    state.response_reported_data = data;
  },
  // eslint-disable-next-line no-shadow
  getResponseDeletedData(state, data) {
    state.response_deleted_data = data;
  },
};

const actions = {
  async getSummaryBlock({ commit, dispatch }) {
    try {
      const getSummaryBlock = await jApiTimesheetClaimerList.getSummaryBlock();
      commit('getSummaryBlock', getSummaryBlock);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getSummaryBlockApprover({ commit, dispatch }) {
    try {
      const getSummaryBlockApprover = await jApiTimesheetClaimerList.getSummaryBlockApprover();
      commit('getSummaryBlockApprover', getSummaryBlockApprover);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async reportTimesheetDraftData({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const dataDraft = await jApiTimesheetClaimerList.reportDataDraft(req);
      commit('getResponseReportedData', dataDraft.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', dataDraft.message, { root: true });
      // router.push('/timesheet/claimer/reported');
      return dataDraft;
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      // dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
      return err.response.data;
    }
  },

  async deleteTimesheetDraftData({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const dataDraft = await jApiTimesheetClaimerList.deleteDataDraft(req);
      commit('getResponseDeletedData', dataDraft.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', dataDraft.message, { root: true });
      // router.push('/timesheet/claimer/reported');
      return dataDraft;
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      // dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
      return err.response.data;
    }
  },

  async approveTimesheetDraftData({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const dataDraft = await jApiTimesheetClaimerList.approveDataDraft(req);
      commit('getResponseReportedData', dataDraft.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', dataDraft.message, { root: true });
      // router.push('/timesheet/claimer/reported');
      return dataDraft;
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      // dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
      return err.response.data;
    }
  },

  async rejectTimesheetDraftData({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const dataDraft = await jApiTimesheetClaimerList.rejectDataDraft(req);
      commit('getResponseReportedData', dataDraft.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', dataDraft.message, { root: true });
      // router.push('/timesheet/claimer/reported');
      return dataDraft;
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      // dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
      return err.response.data;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
