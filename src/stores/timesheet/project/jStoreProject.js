/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import router from '@/router';
import apiProject from '@/services/timesheet/project/jApiProject';

const state = {
  // Set default state(s) here
  project_data: {
    project_created: false,
    project_updated: false,
    project_deleted: false,

    detail_project: null,
    export_project: null,
    list_project: null,
    total_length_project: null,
    data_project_created: null,

    list_positions: null,
    list_rules: null,
    list_periods: null,
    list_division: null,
  },
  temporary_data_project: {
    data_project: null,
    valid: false,
    mode: 'create',
    params: null,
  },
  detail_period: null,
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  mutateTemporaryProjectMode(state, payload) {
    state.temporary_data_project.mode = payload.mode;
    state.temporary_data_project.params = payload.params;
  },

  mutateTemporaryProjectValid(state, payload) {
    state.temporary_data_project.valid = payload;
  },

  mutateTemporaryProjectData(state, payload) {
    state.temporary_data_project.data_project = payload;
  },

  // show project has been created
  showProjectCreated(state, payload) {
    state.project_data.project_created = !state.project_data.project_created;
    state.project_data.data_project_created = payload.data;
  },

  // show project has been updated
  showProjectUpdated(state) {
    state.project_data.project_updated = !state.project_data.project_updated;
    router.replace('/project');
  },

  // show project has been deleted
  showProjectDeleted(state) {
    state.project_data.project_deleted = !state.project_data.project_deleted;
    router.replace('/project');
  },

  // show detail project
  showDetailProject(state, payload) {
    state.project_data.detail_project = payload.data;
  },

  // show list positions
  showListPositions(state, payload) {
    const data = payload.data;
    data.forEach((e) => {
      e.email = e.user ? e.user.email : '-';
      const profil = e.email !== '-';
      if (profil) {
        const nameUser = e.user.profile ? e.user.profile : { first_name: '', last_name: '' };
        const firstName = nameUser.first_name ? nameUser.first_name : '';
        const lastName = nameUser.last_name ? nameUser.last_name : '';
        e.name_user = `${firstName} ${lastName}`;
      }
    });
    state.project_data.list_positions = data;
  },

  // show list division
  showListDivision(state, payload) {
    state.project_data.list_division = payload.data;
  },

  // show list rules
  showListRules(state, payload) {
    state.project_data.list_rules = payload.data;
  },

  // show list period
  showListPeriods(state, payload) {
    state.project_data.list_periods = payload.data;
  },

  // show detail period
  showDetailPeriod(state, payload) {
    state.detail_period = payload.data;
  },

  showListProject(state, payload) {
    state.project_data.list_project = payload.data;
    state.project_data.total_length_project = payload.length;
  },

  showExportProject(state, payload) {
    state.project_data.export_project = payload.data;
  },

  showPeriodUpdated(state) {
    if (state.temporary_data_project.mode === 'create') {
      router.replace('/project/create');
    } else {
      router.replace(`/project/edit/${state.temporary_data_project.params.id}`);
    }
  },

  showPeriodCreated(state) {
    if (state.temporary_data_project.mode === 'create') {
      router.replace('/project/create');
    } else {
      router.replace(`/project/edit/${state.temporary_data_project.params.id}`);
    }
  },
};

const actions = {

  // create new project
  async createNewProject({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const createProject = await apiProject.createProject(data);
      if (data.division_id !== undefined && data.division_id !== null) {
        const dataUpdateProject = {
          division_id: data.division_id,
          project_id: createProject.data.id,
          is_delete: false,
        };
        dispatch('divisionUpdateProject', dataUpdateProject);
      }
      commit('showProjectCreated', createProject);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createProject.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async divisionUpdateProject({ dispatch }, data) {
    try {
      await apiProject.divisionUpdateProject(data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // update project
  async updateProject({ dispatch, commit, state }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateProject = await apiProject.updateProject(data);
      let req;
      if (data.division_id !== null && data.division_id !== undefined) {
        req = {
          division_id: data.division_id,
          project_id: data.id,
          is_delete: false,
        };
        dispatch('divisionUpdateProject', req);
      } else {
        req = {
          division_id: state.project_data.detail_project.division_id,
          project_id: data.id,
          is_delete: true,
        };
        if (state.project_data.detail_project.division_id !== 0) {
          dispatch('divisionUpdateProject', req);
        }
      }
      commit('showProjectUpdated');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateProject.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // update project
  async deleteProject({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const deleteProject = await apiProject.removeProject(data);
      commit('showProjectDeleted');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', deleteProject.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get detail project
  async detailProject({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailProject = await apiProject.detailProject(data);
      commit('showDetailProject', detailProject);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list positions
  async getListProject({ dispatch, commit }, data) {
    try {
      const getListProject = await apiProject.listProject(data);
      commit('showListProject', getListProject.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list positions
  async exportProject({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getListProject = await apiProject.exportProject(data);
      commit('showExportProject', getListProject);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list positions
  async getListPositions({ dispatch, commit }, data) {
    try {
      const getListPositions = await apiProject.listOrganigram(data);
      commit('showListPositions', getListPositions);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list division
  async getListDivision({ dispatch, commit }, data) {
    try {
      const getListDivisions = await apiProject.listDivision(data);
      commit('showListDivision', getListDivisions);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list rules
  async getListRules({ dispatch, commit }, data) {
    try {
      const getListRules = await apiProject.listRule(data);
      commit('showListRules', getListRules);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list periods
  async getListPeriods({ dispatch, commit }, data) {
    try {
      const getListPeriods = await apiProject.listPeriod(data);
      commit('showListPeriods', getListPeriods);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createPeriodProject({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await apiProject.createPeriod(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('showPeriodCreated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailPeriodProject({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiProject.detailPeriod(data);
      commit('showDetailPeriod', res);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updatePeriod({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await apiProject.updatePeriod(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('showPeriodUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListDivisions({ dispatch }, { reqData, callback }) {
    try {
      const listDivision = await apiProject.listDivision(reqData);
      const res = listDivision.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListByDivisions({ dispatch }, { reqData, callback }) {
    try {
      const listOrganigramByDivision = await apiProject.listOrganigramByDivision(reqData);
      const res = listOrganigramByDivision.data;
      res.forEach((lab, index) => {
        const fullName = lab.user ? `- ${lab.user.profile.first_name} ${lab.user.profile.last_name}` : '';
        res[index].label = `${lab.name} ${fullName}`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListPosition({ dispatch }, { reqData, callback, label = 'name' }) {
    try {
      const listPosition = await apiProject.listPosition(reqData);
      const res = [...listPosition.data];
      res.forEach((lab, index) => {
        // eslint-disable-next-line
        res[index].name = lab.name;
        // eslint-disable-next-line
        res[index].user_name = lab.user ? (lab.user.profile ? (`${lab.user.profile.first_name ? lab.user.profile.first_name : ''} ${lab.user.profile.last_name ? lab.user.profile.last_name : ''}`) : '') : '';
        // eslint-disable-next-line
        res[index].email = lab.user ? (lab.user.email ? lab.user.email : '-') : '-';
        res[index].label = lab[label];
      });
      callback(null, [...res]);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
