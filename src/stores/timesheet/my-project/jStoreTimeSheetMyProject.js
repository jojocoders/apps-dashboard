// import router from '@/router';
import jApiMyProject from '@/services/timesheet/my-project/jApiMyProject';

const state = {
  isLoaded: false,
  list_task_assign: null,
  detail_project: [],
};

const getters = {

};

const mutations = {

  // eslint-disable-next-line no-shadow
  setUserRestric(state, data) {
    state.user_restriction = data.data;
  },

  // eslint-disable-next-line no-shadow
  setDetailProject(state, data) {
    state.detail_project = state.detail_project.concat(data.data);
  },


  // eslint-disable-next-line no-shadow
  setListTaskAssign(state, data) {
    state.list_task_assign = data.data;
  },

};

const actions = {
  async getListTaskAssign({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiMyProject.getListTaskAssign();
      await commit('setListTaskAssign', res);
      // dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      // dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem',
        err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
