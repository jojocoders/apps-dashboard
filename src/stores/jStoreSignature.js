/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiSignature from '@/services/jApiSignature';

const state = {
  pdf_list: [],
  employee_list: [],
};

const mutations = {
  setDataPDF(state, data) {
    state.pdf_list = data.data;
  },

  setListEmployee(state, data) {
    const dataEmployee = data.data;
    dataEmployee.forEach((lab, index) => {
      dataEmployee[index].label = lab.fullname;
      dataEmployee[index].name = lab.fullname;
    });
    state.employee_list = dataEmployee;
  },
};

const actions = {
  async actionGetDocumentList({ dispatch, commit }) {
    let res;
    try {
      res = await jApiSignature.getDocumentList();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetDocumentListRequestor({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiSignature.getDocumentListRequestor(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetDocumentListApprover({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiSignature.getDocumentListApprover(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionUploadDocument({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      const response = await jApiSignature.uploadDocument(data);
      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async getListEmployee({ dispatch }, { reqData, callback }) {
    try {
      const listEmployee = await jApiSignature.getListEmployee(reqData);
      const res = listEmployee.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.name} (${lab.email})`;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListAllEmployee({ dispatch, commit }) {
    let res;
    try {
      res = await jApiSignature.getListAllEmployee();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setListEmployee', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionSubmitDocument({ dispatch }, { id, param }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      const response = await jApiSignature.submitDocument(id, param);
      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async actionRejectDocument({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      const response = await jApiSignature.rejectDocument(id);
      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async actionConfirmDocument({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      response = await jApiSignature.confirmDocument(id);
      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        // dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
        return response;
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async actionSignDocument({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      const response = await jApiSignature.signDocument(data);
      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
