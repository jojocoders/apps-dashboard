/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import router from '@/router';
import jApiApplication from '@/services/jApiApplication';

const state = {
  // Set default state(s) here
  side_menu: [],
  application_detail: null,
  list_component: [],
  form_detail: null,
  list_by_form: [],
  list_batch_custom: [],
  side_menu_portal: [],
  portal_detail: null,
  list_component_portal: [],
};

const mutations = {
  setSideMenuList(state, payload) {
    // Sort by order
    const sortedArray = payload.sort((a, b) => {
      let comparison = 0;
      if (a.order > b.order) {
        comparison = 1;
      } else if (a.order < b.order) {
        comparison = -1;
      }
      return comparison;
    });
    state.side_menu = sortedArray;
  },

  setApplicationDetail(state, payload) {
    state.application_detail = payload;
  },

  showListComponent(state, data) {
    state.list_component = data;
  },

  setListByForm(state, data) {
    state.list_by_form = data;
  },

  setListBatchCustom(state, data) {
    state.list_batch_custom = data;
  },

  setSideMenuListPortal(state, payload) {
    // Sort by order
    const sortedArray = payload.sort((a, b) => {
      let comparison = 0;
      if (a.order > b.order) {
        comparison = 1;
      } else if (a.order < b.order) {
        comparison = -1;
      }
      return comparison;
    });
    state.side_menu_portal = sortedArray;
  },

  setPortalDetail(state, payload) {
    state.portal_detail = payload;
  },

  showListComponentPortal(state, data) {
    state.list_component_portal = data;
  },
};

const actions = {
  // ACTIONS IN SIDE MENU
  async actionGetSideMenuList({ dispatch, commit }, { applicationId, param }) {
    let res;
    try {
      res = await jApiApplication.getSideMenuList(applicationId, param);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setSideMenuList', res.data);
      }
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  async actionGetPagesList({ dispatch }, { applicationId, param }) {
    try {
      const res = await jApiApplication.getSideMenuList(applicationId, param);
      if (res.error) dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionGetApplicationDetail({ dispatch, commit }, { applicationId, param }) {
    try {
      const res = await jApiApplication.getApplicationDetail(applicationId, param);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setApplicationDetail', res.data);
      }
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
  },

  // -----------------------------------------------------------------------------------------
  // ACTIONS IN SETTING TAB
  // get form_ids
  async getListComponent({ commit, dispatch }, req) {
    try {
      const listComponent = await jApiApplication.listComponent(req.params, req.pages_id);
      commit('showListComponent', listComponent.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListComponentNocode({ dispatch }, req) {
    let res;
    try {
      res = await jApiApplication.listComponentNocode(req.params, req.pages_id);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  // Version with return response
  async getDetailForms({ dispatch }, data) {
    let res;
    try {
      res = await jApiApplication.detailForm(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async cekValidateForm({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    let res;
    try {
      res = await jApiApplication.cekValidateForm(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async cekValidateFormStarted({ dispatch }, data) {
    let res;
    try {
      res = await jApiApplication.cekValidateForm(data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  // ----------------------------------------------------------------------------------------
  // ACTIONS IN PAGES TAB

  // FORM COMPONENT
  // get ids
  async getTransactionListByForm({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiApplication.getTransactionListByForm(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setListByForm', res.data);
      }
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }

    return res;
  },

  // hit last endpoint
  async getTransactionListBatchCustom({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiApplication.getTransactionListBatchCustom(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setListBatchCustom', res.data);
      }
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  async createAdditionalData({ dispatch }, formData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      // first hit
      const response = await jApiApplication.createSubmitAdditionalData(formData);

      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return response;
  },

  async updateAdditionalData({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      const response = await jApiApplication.updateAdditionalData(data);
      if (response.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },


  // WORKFLOW COMPONENT
  async getDataSummaryBlock({ dispatch }, data) {
    let res;
    try {
      res = await jApiApplication.dataSummary(data.params);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async getDataSummaryAppBlock({ dispatch }, data) {
    let res;
    try {
      res = await jApiApplication.dataSummaryApp(data.params);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async generateReportPDF({ dispatch }, reqData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const reportPDF = await jApiApplication.getReportPDF(reqData);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      window.open(reportPDF.data.url, '_blank');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // Company Setting
  async actionGetUserCompanySetting({ dispatch }, useLoading) {
    if (useLoading) dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiApplication.getUserCompanySetting();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else if (!res.error) {
        if (useLoading) dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      if (useLoading) dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
