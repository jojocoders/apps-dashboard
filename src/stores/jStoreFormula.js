/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiFormula from '@/services/jApiFormula';

const state = {
  // Set default state(s) here

};

const getters = {

};

const mutations = {

};

const actions = {
  async createFormula({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.createFormula(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err, { root: true });
    }
  },

  //  formula operation section
  async getFormulaOperationsByFormula({ dispatch }, { formulaId, data }) {
    try {
      const res = await jApiFormula.getFormulaOperationsByFormula(formulaId, data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createFormulaOperation({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.createFormulaOperation(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async updateFormulaOperation({ dispatch }, { formulaOpsId, data }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.updateFormulaOperation(formulaOpsId, data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteFormulaOperation({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.deleteFormulaOperation(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // operation section
  async getOperationsByFormulaOperation({ dispatch }, { formulaOpsId, data }) {
    try {
      const res = await jApiFormula.getOperationsByFormulaOperation(formulaOpsId, data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createOperation({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.createOperation(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async updateOperation({ dispatch }, { opsId, data }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.updateOperation(opsId, data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteOperation({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormula.deleteOperation(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
