/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiOrgchart from '@/services/jApiOrgchart';
import jApiEmployee from '@/services/jApiEmployee';
import router from '@/router';

const state = {
  data_organigram: [],
  data_organigram_new: [],
  data_children: [],
  organigram_detail: null,
  organigram_top: null,
  // state for showing button on orgchart
  is_showing_button: false,
  position_updated: false,
};

const mutations = {
  showListChart(state, data) {
    const dataChildren = [];
    data.childs.forEach((val) => {
      const child = {
        id: val.position_id,
        name: val.position_name,
        description: `${val.first_name} ${val.last_name}`,
        image: val.image_url,
        status: 'child',
        is_child: true,
        children: [{
          is_button: true,
          parent_id: val.position_id,
          parent_name: val.position_name,
          children: [],
        }],
      };
      dataChildren.push(child);
    });
    const btnChild = {
      is_button: true,
      parent_id: data.position_id,
      parent_name: data.position_name,
      children: [],
    };
    dataChildren.push(btnChild);
    if (typeof data.parent.position_id !== 'undefined') {
      const convertData = [
        {
          id: data.parent.position_id,
          name: data.parent.position_name,
          description: `${data.parent.first_name} ${data.parent.last_name}`,
          image: data.parent.image_url,
          status: 'parent',
          is_child: false,
          children: [
            {
              id: data.position_id,
              name: data.position_name,
              description: `${data.first_name} ${data.last_name}`,
              image: data.image_url,
              status: 'dirinya',
              is_child: false,
              children: dataChildren,
            },
            {
              is_button: true,
              parent_id: data.parent.position_id,
              parent_name: data.parent.position_name,
              children: [],
            },
          ],
        },
      ];
      state.data_organigram = convertData;
    } else {
      const convertData = [
        {
          id: data.position_id,
          name: data.position_name,
          description: `${data.first_name} ${data.last_name}`,
          image: data.image_url,
          status: 'dirinya',
          is_child: false,
          children: dataChildren,
        },
      ];
      state.data_organigram = convertData;
    }
  },

  showListChartMore(state, data) {
    state.data_children = [];
    data.childs.forEach((val) => {
      const child = {
        id: val.position_id,
        name: val.position_name,
        description: `${val.first_name} ${val.last_name}`,
        image: val.image_url,
        is_child: true,
        children: [{
          is_button: true,
          parent_id: val.position_id,
          parent_name: val.position_name,
          children: [],
        }],
      };
      state.data_children.push(child);
    });
  },

  showListChartNew(state, data) {
    const dataHierarchy = [];
    dataHierarchy.push([
      {
        id: data.position_id,
        name: `${data.first_name} ${data.last_name}`,
        position: data.position_name,
        photo: data.image_url,
        showChild: true,
        totalChilds: data.childs.length,
      },
    ]);

    const dataChilds = [];
    data.childs.forEach((val, index) => {
      const child = {
        id: val.position_id,
        name: `${val.first_name} ${val.last_name}`,
        position: val.position_name,
        photo: val.image_url,
        showChild: index === 0,
        totalChilds: val.total_childs,
      };
      dataChilds.push(child);
    });
    dataHierarchy.push(dataChilds);
    state.data_organigram_new = dataHierarchy;
  },

  showListChartFirst(state, data) {
    const dataChilds = [];
    data.childs.forEach((val) => {
      const child = {
        id: val.position_id,
        name: `${val.first_name} ${val.last_name}`,
        position: val.position_name,
        photo: val.image_url,
        totalChilds: val.total_childs,
      };
      dataChilds.push(child);
    });
    state.data_organigram_new.push(dataChilds);
  },

  showListChartChoose(state, data) {
    const dataChilds = [];
    data.results.childs.forEach((val) => {
      const child = {
        id: val.position_id,
        name: `${val.first_name} ${val.last_name}`,
        position: val.position_name,
        photo: val.image_url,
        totalChilds: val.total_childs,
      };
      dataChilds.push(child);
    });
    state.data_organigram_new[data.indexData].forEach((val, index) => {
      if (val.position === data.positionName) {
        state.data_organigram_new[data.indexData][index].showChild = true;
      } else {
        state.data_organigram_new[data.indexData][index].showChild = false;
      }
    });
    state.data_organigram_new.splice(data.indexData + 1, 1000);
    state.data_organigram_new.push(dataChilds);
  },

  chartUpdated() {
    router.push('/company/organization/orgchart');
  },

  showDetailOrganigram(state, data) {
    state.organigram_detail = data.data;
  },

  showTopOrganigram(state, data) {
    state.organigram_top = data.data;
  },

  updatePosition(state) {
    state.position_updated = !state.position_updated;
  },
};

const actions = {
  async getListChart({ commit, dispatch }, req) {
    try {
      const listChart = await jApiOrgchart.listChart(req);
      commit('showListChart', listChart.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', 'Data Not Found', { root: true });
    }
  },

  async getListChartMore({ commit, dispatch }, req) {
    try {
      const listChart = await jApiOrgchart.listChart(req);
      commit('showListChartMore', listChart.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', 'Data Not Found', { root: true });
    }
  },

  async getListChartNew({ commit, dispatch }, req) {
    try {
      const listChart = await jApiOrgchart.listChart(req);
      commit('showListChartNew', listChart.data);

      if (listChart.data.childs.length > 0) {
        const params = {
          query_type: 'position_name',
          query: listChart.data.childs[0].position_name,
        };
        dispatch('getListChartFirst', params);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', 'Data Not Found', { root: true });
    }
  },

  async getListChartFirst({ commit, dispatch }, req) {
    try {
      const listChart = await jApiOrgchart.listChart(req);
      commit('showListChartFirst', listChart.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', 'Data Not Found', { root: true });
    }
  },

  async getListChartChoose({ commit, dispatch }, req) {
    try {
      const params = {
        query_type: 'position_name',
        query: req.position_name,
      };
      const listChart = await jApiOrgchart.listChart(params);
      const dataSent = {
        results: listChart.data,
        indexData: req.indexData,
        positionName: req.position_name,
      };
      commit('showListChartChoose', dataSent);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', 'Data Not Found', { root: true });
    }
  },

  async createChart({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    const request = {
      query_type: 'position_name',
      query: req.organigram_head_name,
    };
    try {
      const createChart = await jApiOrgchart.createChart(req);
      dispatch('getListChart', request);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createChart.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteChart({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createChart = await jApiOrgchart.deleteChart(req);
      commit('chartUpdated');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createChart.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async detailOrganigram({ commit }, employeeId) {
    try {
      const detailOrganigram = await jApiEmployee.detailOrganigram(employeeId);
      commit('showDetailOrganigram', detailOrganigram);
    } catch (err) {
      // eslint-disable-next-line
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getTopPosition({ commit }) {
    try {
      const topOrganigram = await jApiEmployee.topOrganigram();
      commit('showTopOrganigram', topOrganigram);
    } catch (err) {
      // eslint-disable-next-line
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async detailOrganigramById({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    let res;
    try {
      res = await jApiOrgchart.detailOrganigram(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async getListParent({ dispatch }, { reqData, callback }) {
    try {
      const listParent = await jApiOrgchart.getListParent(reqData);
      const res = listParent.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListEmployee({ dispatch }, { reqData, callback }) {
    try {
      const listEmployee = await jApiOrgchart.getListEmployee(reqData);
      const res = listEmployee.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.name} (${lab.email})`;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updatePosition({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatePos = await jApiOrgchart.updatePosition(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updatePos.message, { root: true });
      commit('updatePosition');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
