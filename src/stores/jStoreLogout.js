import jApiGateway from '@/services/jApiGateway';

const actions = {
  async logoutAccount() {
    try {
      const response = await jApiGateway.logout();
      return response;
    } catch (err) {
      return err.response;
    }
  },
};

export default {
  namespaced: true,
  actions,
};
