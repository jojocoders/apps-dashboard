/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiOvertimeApprover from '@/services/overtime/jApiOvertimeApprover';
import jApiOvertimeClaimer from '@/services/overtime/jApiOvertimeClaimer';
import jApiOvertimeConfig from '@/services/overtime/jApiOvertimeConfig';
import jApiOvertimeReport from '@/services/overtime/jApiOvertimeReport';

const state = {
  formula_detail: [],
  policy_detail: [],
  type_detail: [],
  overtime_type_list: [],
  attendances_list: [],
  overtime_detail: [],
  additional_info: [],
  times_features: [],
  overtime_logs: [],
};

const mutations = {
  setFormulaDetail(state, data) {
    state.formula_detail = data.data;
  },
  setPolicyDetail(state, data) {
    state.policy_detail = data.data;
  },
  setTypeDetail(state, data) {
    state.type_detail = data.data;
  },
  setTypeList(state, data) {
    state.overtime_type_list = data.data;
  },
  setAttendanceList(state, data) {
    state.attendances_list = data.data;
  },
  setOvertimeDetail(state, data) {
    state.overtime_detail = data;
  },
  setAdditionalInfo(state, data) {
    state.additional_info = data.data;
  },
  setListTimesFeatures(state, data) {
    state.times_features = data.data;
  },
  setOvertimeLogs(state, data) {
    state.overtime_logs = data.data;
  },
};

const actions = {
  // FORMULA
  async actionGetListFormula({ dispatch }, { reqData, callback }) {
    try {
      const listFormula = await jApiOvertimeConfig.getListFormula(reqData);
      const res = listFormula.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async actionGetDetailFormula({ dispatch, commit }, id) {
    try {
      const res = await jApiOvertimeConfig.getDetailFormula(id);
      commit('setFormulaDetail', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateFormula({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.createFormula(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateFormula({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.updateFormula(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteFormula({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.deleteFormula(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // POLICY
  async actionGetListPolicy({ dispatch }, { reqData, callback }) {
    try {
      const listPolicy = await jApiOvertimeConfig.getListPolicy(reqData);
      const res = listPolicy.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async actionGetDetailPolicy({ dispatch, commit }, id) {
    try {
      const res = await jApiOvertimeConfig.getDetailPolicy(id);
      commit('setPolicyDetail', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreatePolicy({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.createPolicy(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdatePolicy({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.updatePolicy(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeletePolicy({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.deletePolicy(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // OVERTIME TYPE
  async actionGetListOvertimeType({ dispatch, commit }, params) {
    try {
      const res = await jApiOvertimeClaimer.getListClaimerType(params);
      commit('setTypeList', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetDetailOvertimeType({ dispatch, commit }, id) {
    try {
      const res = await jApiOvertimeConfig.getDetailOvertimeType(id);
      commit('setTypeDetail', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateOvertimeType({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.createOvertimeType(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateOvertimeType({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.updateOvertimeType(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteOvertimeType({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeConfig.deleteOvertimeType(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetListForm({ dispatch }, { reqData, callback }) {
    try {
      const listForm = await jApiOvertimeConfig.getListForm(reqData);
      const res = listForm.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // REPORT
  async generateOvertimeSummaryXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiOvertimeReport.generateOvertimeSummaryXLS(rootId);
      window.location.href = data.data.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async generateOvertimeDetailXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiOvertimeReport.generateOvertimeDetailXLS(rootId);
      window.location.href = data.data.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // SUMMARY BLOCK
  async actionGetSummaryBlockClaimer({ dispatch }) {
    try {
      const res = await jApiOvertimeClaimer.getSummaryBlock();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetSummaryBlockApprover({ dispatch }) {
    try {
      const res = await jApiOvertimeApprover.getSummaryBlock();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // CLAIMER
  async actionGetListAttendance({ dispatch, commit }, params) {
    try {
      const res = await jApiOvertimeClaimer.getListClaimerAttendance(params);
      commit('setAttendanceList', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetListAdditionalInfo({ dispatch, commit }, params) {
    try {
      const res = await jApiOvertimeClaimer.getListAdditionalInfo(params);
      commit('setAdditionalInfo', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateClaimer({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeClaimer.createClaimer(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // APPROVAL
  async actionGetDetailOvertime({ dispatch, commit }, params) {
    try {
      const res = await jApiOvertimeApprover.getDetailOvertime(params);
      commit('setOvertimeDetail', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionChangeStatusOvertime({ dispatch }, { id, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiOvertimeApprover.changeStatusOvertime(id, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetListTimesFeatures({ dispatch, commit }) {
    try {
      const res = await jApiOvertimeConfig.getListTimesFeatures();
      commit('setListTimesFeatures', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // LOGS
  async actionGetLogs({ dispatch, commit }, params) {
    try {
      const res = await jApiOvertimeClaimer.getListLogs(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setOvertimeLogs', res.data);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
