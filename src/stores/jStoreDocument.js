/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiDocument from '@/services/jApiDocument';

const state = {
  layer_one_list: [],
  layer_two_list: [],
};

const mutations = {
  setFolderAndDocumentList(state, data) {
    state.layer_one_list = data.data;
  },

  setDocumentList(state, data) {
    state.layer_two_list = data.data;
  },
};

const actions = {

  // Folder
  async actionGetFolderAndDocumentList({ dispatch, commit }, req) {
    let res;
    try {
      res = await jApiDocument.getFolderAndDocumentList(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setFolderAndDocumentList', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionGetFolderDetail({ dispatch, commit }, req) {
    let res;
    try {
      res = await jApiDocument.getFolderDetail(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDocumentList', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionCreateFolder({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.createFolder(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionEditFolder({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.editFolder(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionDeleteFolder({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.deleteFolder(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  // Document
  async actionGetDocumentDetail({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.getDocumentDetail(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionShareDocument({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.shareDocument(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionCreateDocument({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.createDocument(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionEditDocument({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.editDocument(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionDeleteDocument({ dispatch }, req) {
    let res;
    try {
      res = await jApiDocument.deleteDocument(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDataPDF', res);
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
