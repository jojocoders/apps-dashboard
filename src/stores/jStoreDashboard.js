/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiDashboard from '@/services/jApiDashboard';

const state = {
  // Set default state(s) here
  state_dashboard: [],
  state_dashboard_url: null,
  list_analytic: [],
};

const getters = {
  // Change value of state(s) with getter
  getDashboardByCode(state) {
    const dashboards = {};
    state.state_dashboard.forEach((dashboard) => {
      dashboards[dashboard.dashboard_code] = dashboard;
    });
    return dashboards;
  },
};

const mutations = {
  setDashboardList(state, payload) {
    state.state_dashboard = payload;
    localStorage.setItem('dashboardMenu', JSON.stringify(payload));
  },
  setDashboardUrl(state, payload) {
    state.state_dashboard_url = payload.embedUrl;
  },
  showlistAnalytic(state, data) {
    data.forEach((val) => {
      // eslint-disable-next-line
      val.id = val.dashboard_code
    });
    state.list_analytic = data;
  },
};

const actions = {

  async actionGetDashboardList({ dispatch, commit }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiDashboard.getDashboardList();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        // commit('setDashboardList', res);
        commit('showlistAnalytic', res);
        // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
  },

  async actionGetDashboardUrl({ dispatch, commit }, data) {
    try {
      const res = await apiDashboard.getDashboardUrl(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDashboardUrl', res);
      }
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
