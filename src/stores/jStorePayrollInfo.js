/* eslint-disable no-shadow,consistent-return,no-param-reassign, max-len */
import jApiPayrollInfo from '@/services/jApiPayrollInfo';

const state = {
  tax_info: [],
  list_benefits: [],
  uiData: {
    bpjsk_id: null,
    bpjskk_id: null,
  },
  master_payroll_area: [],
  master_payscale: [],
  company_regions: [],
  cost_center: [],
  extends_run_payroll: null,
  normal_stop_payment: null,
  update_availability: null,
  access_payroll_info: null,
  tax_status_info_updated: null,
};

const mutations = {
  setTaxInfo(state, data) {
    state.tax_info = data;
  },
  setBenefits(state, data) {
    state.list_benefits = data;
  },
  setMasterPayrollArea(state, data) {
    data.forEach((lab, index) => {
      data[index].id = lab.id;
      data[index].text = lab.name;
      data[index].label = lab.name;
    });
    state.master_payroll_area = data;
  },
  setMasterPayscale(state, data) {
    data.forEach((lab, index) => {
      data[index].id = lab.id;
      data[index].text = lab.name;
      data[index].label = lab.name;
    });
    state.master_payscale = data;
  },
  setCompanyRegion(state, data) {
    data.forEach((lab, index) => {
      data[index].id = lab.id;
      data[index].text = lab.name;
      data[index].label = lab.name;
    });
    state.company_regions = data;
  },
  setCostCenter(state, data) {
    data.forEach((lab, index) => {
      data[index].id = lab.id;
      data[index].text = lab.name;
      data[index].label = lab.name;
    });
    state.cost_center = data;
  },
  setExtendsRun(state, data) {
    state.extends_run_payroll = data;
  },
  setStopPayment(state, data) {
    state.normal_stop_payment = data;
  },
  setAvailability(state, data) {
    state.update_availability = data;
  },
  setAccess(state, data) {
    state.access_payroll_info = data;
  },
  setTaxStatusInfoUpdated(state, data) {
    state.tax_status_info_updated = data;
  },
};

const actions = {
  // BASIC SALARY
  async actionSaveBasicSalary({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.saveBasicSalary(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      if (err.response.data.errors && err.response.data.errors[0].code && err.response.data.errors[0].code === 'INTCMS001') {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', 'Employee join date is not set', { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
      return err.response.data;
    }
  },
  async actionDeleteBasicSalary({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deleteBasicSalary(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionSaveMappingSalaryConfig({ dispatch }, { payrollId, recurringId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.saveMappingSalaryConfig(payrollId, recurringId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // COST CENTER MASTER
  async actionGetMasterCostCenter({ dispatch, commit }) {
    try {
      const res = await jApiPayrollInfo.getMasterCostCenter();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        commit('setCostCenter', res.data.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // COST CENTER
  async actionCreateCostCenter({ dispatch }, { payrollId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createCostCenter(payrollId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateCostCenter({ dispatch }, { payrollId, params, costId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updateCostCenter(payrollId, params, costId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteCostCenter({ dispatch }, { payrollId, costId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deleteCostCenter(payrollId, costId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // TAX INFORMATION
  async actionGetTaxInformation({ commit }, payrollId) {
    try {
      const res = await jApiPayrollInfo.getTaxInformation(payrollId);
      commit('setTaxInfo', res.data);
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionSaveTaxInformation({ dispatch }, { payrollId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.saveTaxInformation(payrollId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // TAX CONFIG
  async actionCreateTaxConfig({ dispatch }, { payrollId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createTaxConfig(payrollId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateTaxConfig({ dispatch }, { payrollId, params, taxId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updateTaxConfig(payrollId, params, taxId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteTaxConfig({ dispatch }, { payrollId, taxId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deleteTaxConfig(payrollId, taxId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // TAX STATUS
  async actionCreateTaxStatus({ dispatch }, { payrollId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createTaxStatus(payrollId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateTaxStatus({ dispatch }, { payrollId, params, taxId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updateTaxStatus(payrollId, params, taxId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteTaxStatus({ dispatch }, { payrollId, taxId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deleteTaxStatus(payrollId, taxId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // BENEFITS
  async actionGetBenefits({ dispatch, commit }, payrollId) {
    try {
      const res = await jApiPayrollInfo.getBenefits(payrollId);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        commit('setBenefits', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateBenefits({ dispatch }, { payrollId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createBenefits(payrollId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
        // dispatch('actionGetBenefits', res.data.payroll_id);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateBenefits({ dispatch }, { payrollId, params, benefitId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updateBenefits(payrollId, params, benefitId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
        // dispatch('actionGetBenefits', res.data.payroll_id);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteBenefits({ dispatch }, { payrollId, benefitId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deleteBenefits(payrollId, benefitId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
        // dispatch('actionGetBenefits', res.data.payroll_id);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // COMPONENT NUMBER
  async actionCreateComponentNumber({ dispatch }, { params, componentId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createComponentNumber(params, componentId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionSetComponentNumber({ dispatch }, { params, componentId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.setComponentNumber(params, componentId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateComponentNumber({ dispatch }, { params, numberId, componentId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updateComponentNumber(params, numberId, componentId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteComponentNumber({ dispatch }, { numberId, componentId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deleteComponentNumber(numberId, componentId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // PAYROLL AREA MASTER
  async actionGetMasterPayrollArea({ dispatch, commit }) {
    try {
      const res = await jApiPayrollInfo.getMasterPayrollArea();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        commit('setMasterPayrollArea', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // PAYROLL AREA
  async actionCreatePayrollArea({ dispatch }, { userCompanyId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createPayrollArea(userCompanyId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdatePayrollArea({ dispatch }, { userCompanyId, params, payrollAreaId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updatePayrollArea(userCompanyId, params, payrollAreaId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeletePayrollArea({ dispatch }, { userCompanyId, payrollAreaId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deletePayrollArea(userCompanyId, payrollAreaId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // PAYSCALE MASTER
  async actionGetMasterPayscale({ dispatch, commit }) {
    try {
      const res = await jApiPayrollInfo.getMasterPayscale();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        commit('setMasterPayscale', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // PAYSCALE EMPLOYEE
  async actionCreatePayscale({ dispatch }, { userCompanyId, params, payscaleId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createPayscale(userCompanyId, params, payscaleId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdatePayscale({ dispatch }, { userCompanyId, params, payscaleId, payscaleEmployeeId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.updatePayscale(userCompanyId, params, payscaleId, payscaleEmployeeId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeletePayscale({ dispatch }, { userCompanyId, payscaleId, payscaleEmployeeId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.deletePayscale(userCompanyId, payscaleId, payscaleEmployeeId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // COMPANY REGION
  async actionGetCompanyRegion({ dispatch, commit }) {
    try {
      const res = await jApiPayrollInfo.getCompanyRegion();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        commit('setCompanyRegion', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // NORMAL STOP PAYMENT
  async actionGetNormalStopPayment({ commit }, userCompanyId) {
    try {
      const res = await jApiPayrollInfo.getNormalStopPayment(userCompanyId);
      commit('setStopPayment', res.data);
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionSetNormalStopPayment({ dispatch }, { userCompanyId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.setNormalStopPayment(userCompanyId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionCreateNormalStopPayment({ dispatch }, { userCompanyId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.createNormalStopPayment(userCompanyId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // EXTENDS RUN PAYROLL
  async actionGetExtendsRunPayroll({ commit }, userCompanyId) {
    try {
      const res = await jApiPayrollInfo.getExtendsRunPayroll(userCompanyId);
      commit('setExtendsRun', res.data);
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionSetExtendsRunPayroll({ dispatch }, { userCompanyId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayrollInfo.setExtendsRunPayroll(userCompanyId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // CHECK AVAILABILITY UPDATE EMPLOYEE
  async actionCheckAvailability({ commit, dispatch }, params) {
    try {
      const res = await jApiPayrollInfo.checkAvailability(params);
      if (!res.data) {
        dispatch('jStoreNotificationScreen/toggleProblem', 'Failed to update employee data because employee still process in payroll', { root: true });
      }
      commit('setAvailability', res.data);
      return res.data;
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionCheckAccessPayrollInfo({ commit }, params) {
    try {
      const res = await jApiPayrollInfo.checkAccessPayrollInfo(params);
      commit('setAccess', res.data);
    } catch (err) {
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionSetAccessNull({ commit }) {
    commit('setAccess', null);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
