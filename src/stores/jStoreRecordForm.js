/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import router from '@/router';
import apiRecord from '@/services/jApiRecord';
import apiBoard from '@/services/jApiBoard';
import apiRequestor from '@/services/jApiRequestor';
import apiEmployee from '@/services/jApiEmployee';

const state = {
  field_list: [],
  field_list_resubmit: [],
  is_detail: false,
  name_form: '',
  description_form: '',
  name_record: '',
  user_company_id: 0,
  status_transaction: 'ready',
  detail_form: null,
  detail_forms: null,
  categories_layer: null,
  // unused
  updated_card: false,
  data_relations: null,
  refresh_data_relations: false,
  created_record: false,
  logs_data: [],
  show_modal_board: false,
};

const mutations = {
  setDataForm(state, data) {
    if (state.is_detail) {
      state.field_list = data.additional_data;
      state.status_transaction = data.status;
      state.name_record = data.name;
      state.name_form = data.form.name;
      state.description_form = data.description;
      state.user_company_id = data.user_company_id;
      state.detail_form = data;
    } else {
      state.field_list = data.additional_info;
      state.name_form = data.name;
      state.description_form = data.description;
      state.detail_form = data;
    }
  },

  setDataForms(state, data) {
    state.field_list_resubmit = data.additional_data;
    state.detail_forms = data;
  },

  setDataRelation(state, data) {
    state.data_relations = data;
    state.refresh_data_relations = !state.refresh_data_relations;
  },

  setDataRelationStageName(state, { dataRelation, index }) {
    // find data_relations record and insert stage_name properties
    dataRelation.forEach((el) => {
      const targetIndex = state.data_relations.forms[index].records.findIndex(x =>
        x.record_id === el.value_id,
      );
      state.data_relations.forms[index].records[targetIndex].stage_name = el.board_stage_name;
    });

    state.refresh_data_relations = !state.refresh_data_relations;
  },

  changeIsDetail(state, data) {
    state.is_detail = data;
  },

  showLayerByChild(state, data) {
    state.categories_layer = data.data;
  },

  // unused
  updatedCard(state) {
    state.updated_card = !state.updated_card;
  },

  recordCreated(state) {
    state.created_record = !state.created_record;
  },

  showLogs(state, data) {
    state.logs_data = data.data;
  },

  setCloseModalBoard(state) {
    state.show_modal_board = !state.show_modal_board;
  },
};

const actions = {
  async getFormFieldList({ commit, dispatch }, data) {
    commit('changeIsDetail', false);

    try {
      const formFieldList = await apiRecord.listFieldForm(data);
      commit('setDataForm', formFieldList);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getDetailRecord({ commit, dispatch }, data) {
    commit('changeIsDetail', true);

    try {
      const formFieldList = await apiRecord.getDetailRecord(data);
      commit('setDataForm', formFieldList.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getRelationRecord({ commit, dispatch }, data) {
    try {
      const formFieldList = await apiRecord.getRelationRecord(data);
      commit('setDataRelation', formFieldList.data);

      // let recordList = [];
      formFieldList.data.forms.forEach((el, index) => {
        if (el.records.length > 0) {
          const recordList = [];
          el.records.forEach((el2) => {
            recordList.push(el2.record_id);
          });

          if (recordList.length > 0) {
            dispatch('getRelationRecordStageName', { recordList, index });
          }
        }
      });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getRelationRecordStageName({ dispatch, commit }, { recordList, index }) {
    try {
      const res = await apiBoard.getRelationRecordStageName(recordList);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else if (res.data) {
        const dataRelation = res.data;
        commit('setDataRelationStageName', { dataRelation, index });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailRecords({ commit, dispatch }, data) {
    try {
      const formFieldList = await apiRecord.getDetailRecord(data);
      commit('setDataForms', formFieldList.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async createAdditionalData({ dispatch }, { formData, cardData }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      // first hit
      const res = await apiRecord.createAdditionalData(formData);
      // second hit
      response = await apiBoard.createCard({
        ...cardData,
        input_id: Number(res.data[0].id),
      });

      if (response.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }

      setTimeout(() => {
        const recentPage = JSON.parse(localStorage.getItem('prevBoardRoute'));
        router.push(recentPage);
      }, 1500);
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async createAdditionalDataTab({ dispatch }, formData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      // first hit
      const response = await apiRecord.createAdditionalData(formData);

      if (response.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }

      setTimeout(() => {
        window.close();
      }, 1500);
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async createAdditionalDataNew({ commit, dispatch }, { formData, cardData, boardID }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      // first hit
      const res = await apiRecord.createAdditionalData(formData);
      // second hit
      response = await apiBoard.createCardNew({
        ...cardData,
        value_id: Number(res.data[0].id),
      }, boardID);

      if (response.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }

      setTimeout(() => {
        commit('setCloseModalBoard');
      }, 1500);
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async createAdditionalDataRelated({ dispatch }, formData) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let response;
    try {
      response = await apiRecord.createAdditionalData(formData);
      if (response.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', response.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleSuccess', response.message, { root: true });
      }

      setTimeout(() => {
        const recentPage = JSON.parse(localStorage.getItem('prevBoardRoute'));
        router.push(recentPage);
      }, 1500);
    } catch (err) {
      response = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
    return response;
  },

  async createAdditionalDatas({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });

    try {
      const res = await apiRecord.createAdditionalDatas(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('recordCreated');
      return res.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getLayerByChild({ commit, dispatch }, data) {
    try {
      const res = await apiRecord.getLayerByChild(data);
      commit('showLayerByChild', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async updateAdditionalData({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });

    try {
      const res = await apiRecord.updateAdditionalData(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('recordCreated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async updateAndSubmitAdditionalData({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });

    try {
      await apiRecord.updateAdditionalData(data);
      await apiRequestor.sentDataDraftRequestor({ ids: [Number(data.id)] });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', 'Success update and submit record.', { root: true });
      commit('recordCreated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async deleteDataRequestor({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });

    try {
      const res = await apiRecord.deleteDataRequestor(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('recordCreated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getLogs({ commit, dispatch }, data) {
    try {
      const res = await apiRecord.getLogs(data);
      commit('showLogs', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async closeModalBoards({ dispatch, commit }) {
    try {
      commit('setCloseModalBoard');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async detailEmployee({ dispatch }, employeeId) {
    let res;
    try {
      res = await apiEmployee.detailEmployee(employeeId);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        res = res.user;
      }
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },

};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
