/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiEmployee from '@/services/jApiEmployee';

const state = {
  cost_center_detail: null,
  org_unit_list: [],
};

const mutations = {
  setDetailCostCenter(state, payload) {
    state.cost_center_detail = payload;
  },
  setListDivision(state, payload) {
    state.org_unit_list = payload;
  },
};

const actions = {
  // GRADE
  async actionCreateGrade({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createGrade(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateGrade({ dispatch }, { params, gradeId }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateGrade(params, gradeId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteGrade({ dispatch }, gradeId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.deleteGrade(gradeId);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // GRADE
  async actionGetCostCenterDetail({ dispatch, commit }, params) {
    try {
      const res = await jApiEmployee.getCostCenterDetail(params);
      commit('setDetailCostCenter', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionCreateCostCenter({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createCostCenter(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateCostCenter({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateCostCenter(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteCostCenter({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.deleteCostCenter(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // COST CENTER MAPPING ORG UNIT
  async actionCreateMappingCostCenterDivision({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.createMappingCostCenterDivision(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateMappingCostCenterDivision({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.updateMappingCostCenterDivision(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteMappingCostCenterDivision({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiEmployee.deleteMappingCostCenterDivision(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionGetListDivisions({ dispatch, commit }, params) {
    try {
      const res = await jApiEmployee.getListDivisions(params);
      commit('setListDivision', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
