/* eslint-disable no-shadow,consistent-return,no-param-reassign, max-len */
import apiCompanyLocation from '@/services/jApiCompanyLocation';

const state = {
  // Set default state(s) here
  list_company_location: null,
  listTaxLocation: null,
  loading: true,
};

const getters = {
  // Change value of state(s) with getter
};

const mutations = {
  mutateCreateCompanyLocation(state, data) {
    state.error = data;
  },

  mutateListCompanyLocation(state, data) {
    state.list_company_location = data;
  },

  taxLocationSuccess(state, data) {
    state.listTaxLocation = data.data.map(val => (Object.assign(val, { name: val.name })));
  },
};

const actions = {
  async getTaxLocation({ commit, dispatch }, req) {
    try {
      const res = await apiCompanyLocation.companyTaxLocation(req);
      commit('taxLocationSuccess', res);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async getDetailTaxLocation({ dispatch }, locationId) {
    try {
      const res = await apiCompanyLocation.getDetailTaxLocation(locationId);
      return res;
    } catch (err) {
      if (err.response.data.errors) {
        // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        throw err;
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async createTaxLocation({ dispatch }, req) {
    try {
      await apiCompanyLocation.createTaxLocation(req);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async updateTaxLocation({ dispatch }, req) {
    try {
      await apiCompanyLocation.updateTaxLocation(req);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async createCompanyLocation({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await apiCompanyLocation.companyCreateLocation(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('mutateCreateCompanyLocation', res);
      return res;
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
      return err.response.data;
    }
  },

  async companyDeleteLocation({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await apiCompanyLocation.companyDeleteLocation(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async companyUpdateLocation({ dispatch }, { id, req }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await apiCompanyLocation.companyUpdateLocation(id, req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
      return err.response.data;
    }
  },

  async getListCompanyLocation({ dispatch, commit }, data) {
    try {
      const getListCompanyLocation = await apiCompanyLocation.companyListLocation(data);
      commit('getListCompanyLocation', getListCompanyLocation);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetCities({ dispatch }, { reqData, callback }) {
    try {
      const listCities = await apiCompanyLocation.getCities(reqData);
      const res = listCities.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
