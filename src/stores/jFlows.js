/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import router from '@/router';
import apiFlow from '@/services/jApiFlow';
import apiOrganigram from '@/services/jApiOrganigram';
import apiDivision from '@/services/jApiDivision';
import apiUser from '@/services/jApiUser';

const state = {
  flows_table: [],
  users_list: [],
  divisions_list: [],
  by_divisions_list: [],
  new_division: null,
  is_flow_deleted: false,
  detail_flow: null,
  all_flow_list: null,
  flow_list: null,
  total_length_flow: null,
  detail_sla: null,

  sla_created: false,
  sla_updated: false,

  manage_flow: {
    is_open: false,
    data: null,
  },
  manage_sla: {
    is_open: false,
  },

  manage_rule: {
    is_open: false,
  },
};

const mutations = {
  // list flow
  showListFlow(state, data) {
    state.flows_table = data;
  },

  // list select users
  showListUsers(state, data) {
    state.users_list = data;
  },

  // list divisions
  showListDivisions(state, data) {
    state.divisions_list = data;
  },

  // list by divisions
  showListByDivisions(state, data) {
    state.by_divisions_list = data;
  },

  // show new division
  showNewDivisions(state, data) {
    state.new_division = data;
  },

  // flow was deleted
  flowDeleted(state) {
    state.is_flow_deleted = !state.is_flow_deleted;
  },

  // detail flows
  detailFlow(state, payload) {
    state.detail_flow = payload.data;
  },

  // set all flows
  setAllFlows(state, payload) {
    state.total_length_flow = payload.length;
    state.all_flow_list = payload.data;
  },

  setAllFlowsNoExport(state, payload) {
    state.flow_list = payload.data;
  },

  setDetailSLA(state, payload) {
    state.detail_sla = payload.data;
  },

  showSLACreated(state) {
    state.sla_created = !state.sla_created;
  },

  showSLAUpdated(state) {
    state.sla_updated = !state.sla_updated;
  },

  setOpenSLA(state) {
    state.manage_sla.is_open = !state.manage_sla.is_open;
  },

  setOpenFlow(state) {
    state.manage_flow.is_open = !state.manage_flow.is_open;
  },

  setOpenRule(state) {
    state.manage_rule.is_open = !state.manage_rule.is_open;
  },

  setFlowData(state, payload) {
    state.manage_flow.data = payload;
  },

  flowCreatedOrUpdated() {
    setTimeout(() => {
      router.go(-1);
    }, 1000);
  },
};

const actions = {
  // get list table flow
  async getListFlow({ commit, dispatch }) {
    try {
      const listFlow = await apiFlow.listFlow();
      commit('showListFlow', listFlow);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list pagination position
  async getListPosition({ commit, dispatch }, { reqData, callback }) {
    try {
      const listPosition = await apiFlow.listPosition(reqData);
      const res = [...listPosition.data];
      res.forEach((lab, index) => {
        // eslint-disable-next-line
        res[index].name = lab.name;
        // eslint-disable-next-line
        res[index].user_name = lab.user ? (lab.user.profile ? (`${lab.user.profile.first_name ? lab.user.profile.first_name : ''} ${lab.user.profile.last_name ? lab.user.profile.last_name : ''}`) : '') : '';
        // eslint-disable-next-line
        res[index].email = lab.user ? (lab.user.email ? lab.user.email : '-') : '-';
        res[index].label = `${lab.id} - ${lab.code} - ${lab.name}`;
      });
      callback(null, [...res]);
      commit('showListFlow', listPosition);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list user
  async getListUsers({ commit, dispatch }) {
    try {
      const listUser = await apiUser.listUser();
      commit('showListUsers', listUser);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list Divisions
  async getListDivisions({ commit, dispatch }, { reqData, callback }) {
    try {
      const listDivision = await apiDivision.listDivision(reqData);
      commit('showListDivisions', listDivision.data);
      const res = listDivision.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.code} - ${lab.name}`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // list by divisions
  async getListByDivisions({ commit, dispatch }, { reqData, callback }) {
    try {
      const listOrganigramByDivision = await apiOrganigram.listOrganigramByDivision(reqData);
      commit('showListByDivisions', listOrganigramByDivision);
      const res = listOrganigramByDivision.data;
      res.forEach((lab, index) => {
        const fullName = lab.user ? `- ${lab.user.profile.first_name} ${lab.user.profile.last_name}` : '';
        res[index].label = `${lab.name} ${fullName}`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create new division in flow
  async createDivisionInFlow({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createDivision = await apiDivision.createDivision(data.division);
      data.organigram.divisions.push(createDivision.id);
      const nextReq = data.organigram;
      dispatch('createOrganigramDivision', nextReq);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create new organigram in divisions
  async createOrganigramDivision({ commit, dispatch }, nextReq) {
    try {
      const createOrganigram = await apiOrganigram.createOrganigram(nextReq);
      commit('showNewDivisions', createOrganigram);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create new flow
  async createNewFlow({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const create = await apiFlow.createFlow(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', create.message, { root: true });
      commit('flowCreatedOrUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create flow by division
  async createFlowByDivision({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      const create = await apiFlow.createFlowByDivision(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', create.message, { root: true });
      commit('flowCreatedOrUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // remove flow
  async removeFlow({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const removeFlow = await apiFlow.removeFlow(data);
      commit('flowDeleted');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', removeFlow.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailFlow({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailFlow = await apiFlow.detailFlow(data);
      commit('detailFlow', detailFlow);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // remove flow
  async updateFlow({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const update = await apiFlow.updateFlow(data);
      commit('flowUpdated');
      dispatch('jStoreNotificationScreen/toggleSuccess', update.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });

      commit('flowCreatedOrUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // update flow by division

  async updateFlowByDivision({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const update = await apiFlow.updateFlowByDivision(data);
      dispatch('jStoreNotificationScreen/toggleSuccess', update.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('flowCreatedOrUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getAllFlows({ dispatch, commit }, data) {
    try {
      const getList = await apiFlow.listFlow(data);
      if (typeof data.export === 'boolean') {
        commit('setAllFlowsNoExport', getList);
      } else {
        commit('setAllFlows', getList);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailSLA({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailSLA = await apiFlow.getDetailSLA(data);
      commit('setDetailSLA', detailSLA);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createFlowSLA({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createSLA = await apiFlow.createSLA(data);
      commit('showSLACreated');
      dispatch('jStoreNotificationScreen/toggleSuccess', createSLA.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateFlowSLA({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateSLA = await apiFlow.updateSLA(data);
      commit('showSLAUpdated');
      dispatch('jStoreNotificationScreen/toggleSuccess', updateSLA.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailPosition({ dispatch }, data) {
    let res;
    try {
      res = await apiOrganigram.getDetailPositionOld(data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
