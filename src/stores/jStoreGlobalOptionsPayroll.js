import Constant from '@/utils/constant-payroll';

const state = {
  salaryConfigList: [
    {
      text: 'Taxable',
      value: Constant.SalaryConfigurationTaxable,
    }, {
      text: 'No Taxable',
      value: Constant.SalaryConfigurationNonTaxable,
    },
  ],
  salaryTypeList: [
    {
      text: 'Monthly',
      value: Constant.SalaryTypeMonthly,
    }, {
      text: 'Daily',
      value: Constant.SalaryTypeDaily,
    },
  ],
  taxConfigList: [
    {
      text: 'Gross',
      value: Constant.TaxConfigurationGross,
    },
    {
      text: 'Nett',
      value: Constant.TaxConfigurationNett,
    },
    {
      text: 'Gross Up',
      value: Constant.TaxConfigurationGrossUp,
    },
  ],
  idTypeList: [
    {
      text: 'KTP / Identity Card',
      value: Constant.IdTypeKTP,
    },
    {
      text: 'Passport',
      value: Constant.IdTypePassport,
    },
  ],
  nationalityList: [
    {
      text: 'Warga Negara Indonesia (WNI)',
      value: 'Warga Negara Indonesia (WNI)',
    },
    {
      text: 'Warga Negara Asing (WNA)',
      value: 'Warga Negara Asing (WNA)',
    },
  ],
  genderList: [
    {
      text: 'Male',
      value: Constant.GenderMale,
    },
    {
      text: 'Female',
      value: Constant.GenderFemale,
    },
  ],
  maritalStatusList: [
    {
      text: 'Single',
      value: Constant.MaritalStatusSingle,
    },
    {
      text: 'Married',
      value: Constant.MaritalStatusMarried,
    },
    {
      text: 'Divorced',
      value: Constant.MaritalStatusDivorced,
    },
    {
      text: 'Widowed',
      value: Constant.MaritalStatusWidowed,
    },
  ],
  religionList: [
    {
      text: 'Islam',
      value: Constant.ReligionIslam,
    },
    {
      text: 'Protestant',
      value: Constant.ReligionProtestant,
    },
    {
      text: 'Catholic',
      value: Constant.ReligionCatholic,
    },
    {
      text: 'Hindu',
      value: Constant.ReligionHindu,
    },
    {
      text: 'Buddha',
      value: Constant.ReligionBuddha,
    },
    {
      text: 'Konghucu',
      value: Constant.ReligionKonghucu,
    },
    {
      text: 'Others',
      value: Constant.ReligionOthers,
    },
  ],
  allowance_type_list: [
    {
      text: 'Monthly',
      value: Constant.AllowanceTypeMonthly,
    }, {
      text: 'Daily',
      value: Constant.AllowanceTypeDaily,
    }, {
      text: 'Daily by JojoTimes',
      value: Constant.AllowanceTypeDailyByJojoTimes,
    }, {
      text: 'Daily by Manual Attendance',
      value: Constant.AllowanceTypeDailyByManualAttendance,
    }, {
      text: 'Fix Amount',
      value: Constant.AllowanceTypeFixAmount,
    }, {
      text: 'Fix Amount by Working Hours Jojotimes',
      value: Constant.AllowanceTypeFixAmountByWorkingHoursJojotimes,
    }, {
      text: 'Custom Daily',
      value: Constant.AllowanceTypeCustomDaily,
    }, {
      text: 'Mix Rule',
      value: Constant.AllowanceTypeMixRule,
    }, {
      text: 'Fix Amount By Overtime Duration',
      value: Constant.AllowanceTypeFixAmountByOvertimeDuration,
    },
  ],
  reimbursement_query: [
    {
      text: 'Employee ID',
      value: 'employee_id',
    }, {
      text: 'Name',
      value: 'name',
    }, {
      text: 'Code',
      value: 'code',
    }, {
      text: 'Title',
      value: 'code',
    }, {
      text: 'Reimbursement No',
      value: 'reimbursement_no',
    },
  ],
  monthList: [
    {
      text: 'January',
      value: 1,
    },
    {
      text: 'February',
      value: 2,
    },
    {
      text: 'March',
      value: 3,
    },
    {
      text: 'April',
      value: 4,
    },
    {
      text: 'May',
      value: 5,
    },
    {
      text: 'June',
      value: 6,
    },
    {
      text: 'July',
      value: 7,
    },
    {
      text: 'August',
      value: 8,
    },
    {
      text: 'September',
      value: 9,
    },
    {
      text: 'October',
      value: 10,
    },
    {
      text: 'November',
      value: 11,
    }, {
      text: 'December',
      value: 12,
    },
  ],
};

export default {
  namespaced: true,
  state,
};
