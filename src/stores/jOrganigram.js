/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import Vue from 'vue';
import router from '@/router';
import apiOrganigram from '@/services/jApiOrganigram';
import apiDivision from '@/services/jApiDivision';
import apiUser from '@/services/jApiUser';

const state = {
  // Set default state(s) here
  // organigrams_table: [],
  title_deleted: false,
  new_title_added: false,
  division_deleted: false,
  new_division_added: false,

  division_updated: false,
  title_updated: false,
  detail_division: null,
  all_division_list: null,
  all_title_list: null,

  division_list: null,
  title_list: null,

  email_id: false,

  total_length_title: 0,
  total_length_division: 0,

  division_data: [],

  position_detail_sync: null,
  position_detail_sync_updated: false,
  position_sync_log: {
    organigram_data: null,
    log: [],
  },
};

const getters = {
  // Change value of state(s) with getter
  parent_divisions_list() {
    if (state.division_list) {
      const listData = [...state.division_list];
      listData.unshift({ id: 0, name: '--- none ---' });
      return listData;
    }
    return null;
  },

};

function addOrReplace(object, arr) {
  const array = arr;
  const indexnya = array.map(el => el.id).indexOf(object.id);
  if (indexnya === -1) {
    array.push(object);
  } else {
    if (object.is_has_child === true) {
      Vue.set(object, 'children', [{ child: 'child' }]);
    }
    Vue.set(array, indexnya, object);
  }
}

const mutations = {
  // list organigrams
  showListOrganigrams(state, data) {
    state.orgaigrams_table = data;
  },

  // title deleted
  titleDeleted(state) {
    state.title_deleted = !state.title_deleted;
  },

  // division deleted
  divisionDeleted(state) {
    state.division_deleted = !state.division_deleted;
  },

  // new title added
  addTitle(state) {
    state.new_title_added = !state.new_title_added;
  },

  // new division added
  addDivision(state) {
    state.new_division_added = !state.new_division_added;
  },

  // update division
  divisionUpdated(state) {
    state.division_updated = !state.division_updated;
  },

  // update title
  titleUpdated(state) {
    state.title_updated = !state.title_updated;
  },

  // show detail division
  showDetailDivision(state, data) {
    state.detail_division = data.data;
  },

  // set all divisions_list
  setAllDivisions(state, payload) {
    state.all_division_list = payload.data;
    state.total_length_division = payload.length;
  },

  setAllDivisionsNoExport(state, payload) {
    state.division_list = payload.data;
  },

  // set all title
  setAllTitle(state, payload) {
    state.total_length_title = payload.length;
    state.all_title_list = payload.data;
  },

  setAllTitleNoExport(state, payload) {
    state.title_list = payload.data;
  },

  // email id
  gotEmailID() {
    state.email_id = !state.email_id;
  },

  showChartDivision(state, data) {
    const dataChart = data.data;
    dataChart.forEach((val) => {
      const a = val;
      if (a.is_has_child === true) {
        Vue.set(a, 'children', [{ child: 'child' }]);
      }
      Vue.set(a, 'parent', dataChart);
    });
    const dataOrganigram = dataChart;
    dataOrganigram.push({
      show_more: {
        parent: {
          id: 0,
          children: dataChart,
        },
        length: dataChart.length,
      },
    });
    state.division_data = dataOrganigram;
  },

  showMoreDivision(state, data) {
    const chld = data.dataCC.chld;
    const prnt = data.dataCC.prnt;
    const cc = data.listDivision.data;
    const prevLength = chld.length - 1;
    chld.pop();
    let ccIndex = 0;
    while (ccIndex < cc.length) {
      addOrReplace(cc[ccIndex], chld);
      ccIndex += 1;
    }
    for (let i = prevLength; i < chld.length; i += 1) {
      if (prnt.children[i].is_has_child === true) {
        Vue.set(prnt.children[i], 'children', [{ child: 'child' }]);
      }
      Vue.set(prnt.children[i], 'parent', chld);
    }
    prnt.children.push({
      show_more: {
        parent: prnt,
        length: prnt.children.length,
      },
    });
  },
  setChildDivision(state, data) {
    const a = data.dataCC;
    Vue.set(a, 'children', data.listDivision.data);
    a.children.forEach((b) => {
      const child = b;
      if (child.is_has_child === true) {
        Vue.set(child, 'children', [{ child: 'child' }]);
      }
      Vue.set(child, 'parent', a);
    });
    a.children.push({
      show_more: {
        parent: a,
        length: a.children.length,
      },
    });
    const indexnya = state.division_data.map(el => el.id).indexOf(a.id);
    Vue.set(state.division_data, indexnya, a);
  },

  // important for email data
  setDetailPositionSync(state, payload) {
    state.position_detail_sync = payload.data;
  },

  setDetailPositionSyncLog(state, payload) {
    state.position_sync_log.log = payload.data;
  },

  setOrganigramDetail(state, payload) {
    state.position_sync_log.organigram_data = payload;
    state.position_sync_log.log = [];
  },

  setUpdatedPositionSync(state) {
    state.position_detail_sync_updated = !state.position_detail_sync_updated;
  },
};

const actions = {
  // get list table organigrams
  // getListOrganigrams({ commit, dispatch }, url, data) {
  //   axios.post(url, data)
  //     .catch((err) => {
  //       commit('jStoreNotificationScreen/getProblemMessage', err.message, { root: true });
  //       dispatch('jStoreNotificationScreen/toggleProblem', {}, { root: true });
  //     });
  // },

  // create new Division
  async createNewDivision({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createDivision = await apiDivision.createDivision(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createDivision.message, { root: true });
      router.push('/organigram');
      commit('addDivision');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // Delete Division
  async removeDivision({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const removeDivision = await apiDivision.removeDivision(data);
      commit('divisionDeleted');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', removeDivision.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateDivision({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateDivision = await apiDivision.updateDivision(data);
      commit('divisionUpdated');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateDivision.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create new Job title
  async createNewTitle({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await apiOrganigram.createOrganigram(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      router.push('/organigram');
      commit('addTitle');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // Delete job title
  async removeTitle({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const removeOrg = await apiOrganigram.removeOrganigram(data);
      commit('titleDeleted');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', removeOrg.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateTitle({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateOrg = await apiOrganigram.updateOrganigram(data);
      commit('titleUpdated');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateOrg.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailDivision({ dispatch, commit }, idDivision) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailDivision = await apiDivision.detailDivision(idDivision);
      commit('showDetailDivision', detailDivision);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getAllDivisions({ dispatch, commit }, idDivision) {
    try {
      const allDivision = await apiDivision.listDivision(idDivision);
      if (typeof idDivision.export === 'boolean') {
        commit('setAllDivisionsNoExport', allDivision);
      } else {
        commit('setAllDivisions', allDivision);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailPositionSync({ dispatch, commit }, idPosition) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailPositionSync = await apiOrganigram.getDetailPositionSync(idPosition);
      commit('setDetailPositionSync', detailPositionSync);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailPosition({ dispatch, commit }, idPosition) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailPosition = await apiOrganigram.getDetailPosition(idPosition);
      commit('setDetailPositionSync', detailPosition);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailPositionSyncLog({ dispatch, commit }, idPosition) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailPositionSyncLog = await apiOrganigram.getDetailPositionSyncLog(idPosition);
      commit('setDetailPositionSyncLog', detailPositionSyncLog);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updatePositionSync({ dispatch, commit }, idPosition) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatePositionSync = await apiOrganigram.updatePositionSync(idPosition);
      commit('setUpdatedPositionSync');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updatePositionSync.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getAllTitle({ dispatch, commit }, reqList) {
    try {
      const allOrganigram = await apiOrganigram.listOrganigram(reqList);
      if (typeof reqList.export === 'boolean') {
        commit('setAllTitleNoExport', allOrganigram);
      } else {
        commit('setAllTitle', allOrganigram);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getEmailId({ commit }, reqList) {
    try {
      const emailID = await apiUser.emailId(reqList);
      commit('gotEmailID');
      return emailID;
    } catch (err) {
      commit('gotEmailID');
      // eslint-disable-next-line max-len
      // dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return null;
    }
  },

  async getOrganigramChart({ commit, dispatch }, dataUser) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      if (dataUser.children !== undefined) {
        const listDivision = await apiOrganigram.getOrganigramChart(dataUser.data_req);
        const dataCC = {
          prnt: dataUser.parent,
          chld: dataUser.children,
        };
        commit('showMoreDivision', { listDivision, dataCC });
      } else if (dataUser.parent) {
        const listDivision = await apiOrganigram.getOrganigramChart(dataUser.data_req);
        const dataCC = dataUser.parent;
        commit('setChildDivision', { listDivision, dataCC });
      } else {
        const dataReq = await apiOrganigram.getOrganigramChart(dataUser);
        commit('showChartDivision', dataReq);
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async exportPositionToEmail({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiOrganigram.exportPositionToEmail(req);
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async exportDivisionToEmail({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiOrganigram.exportDivisionToEmail(req);
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
