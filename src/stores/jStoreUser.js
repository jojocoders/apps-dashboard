/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import router from '@/router';
import jApiUser from '@/services/jApiUser';

const state = {
  // Set default state(s) here
  user_restriction_organigram: [],
  user_restriction_monitor: [],
  user_restriction_report: [],
};

const mutations = {
  setUserRestrictOrganigram(state, data) {
    state.user_restriction_organigram = data.data.data;
  },
  setUserRestrictMonitor(state, data) {
    state.user_restriction_monitor = data.data.data;
  },
  setUserRestrictReport(state, data) {
    state.user_restriction_report = data.data.data;
  },
};

const actions = {
  async actionGetUserRestrictionOrganigram({ commit, dispatch }, req) {
    try {
      const res = await jApiUser.getUserRestrictionOrganigram(req);
      commit('setUserRestrictOrganigram', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetListUserRestriction({ dispatch }, { reqData, callback }) {
    try {
      const listEmployee = await jApiUser.getUserRestrictionOrganigram(reqData);
      const res = listEmployee.data.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.employee_id} - ${lab.first_name} ${lab.last_name}`;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetUserRestrictionMonitor({ commit, dispatch }, req) {
    try {
      const res = await jApiUser.getUserRestrictionMonitor(req);
      commit('setUserRestrictMonitor', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetUserRestrictionReport({ commit, dispatch }, req) {
    try {
      const res = await jApiUser.getUserRestrictionReport(req);
      commit('setUserRestrictReport', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
