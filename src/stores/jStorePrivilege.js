/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import router from '@/router';
import apiPrivilege from '@/services/jApiPrivilege';
import apiEmployee from '@/services/jApiEmployee';

const state = {
  // Set default state(s) here
  priv_page_list: [],
  organigram_detail: [],
};

const mutations = {
  setPrivPage(state, payload) {
    state.priv_page_list = payload;
  },
  setOrganigram(state, payload) {
    state.organigram_detail = payload;
  },
};

const actions = {
  async actionGetPrivilegeApp({ dispatch }, data) {
    try {
      const res = await apiPrivilege.getPrivilegeApp(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionCreatePrivilegeApp({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiPrivilege.createPrivilegeApp(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetPrivilegePage({ dispatch, commit }, data) {
    try {
      const res = await apiPrivilege.getPrivilegePage(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setPrivPage', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionCreatePrivilegePage({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiPrivilege.createPrivilegePage(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionDeletePrivilegePage({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await apiPrivilege.deletePrivilegePage(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionDetailOrganigram({ dispatch, commit }, data) {
    try {
      const res = await apiEmployee.detailOrganigram(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setOrganigram', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
