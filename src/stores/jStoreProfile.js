/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import router from '@/router';
import jApiProfile from '@/services/jApiProfile';

const state = {
  // Set default state(s) here
  data_otp: null,
  data_password: null,
  success_change_password: false,
  data_login_otp: null,
};

const mutations = {
  setUserShowOtp(state, data) {
    state.data_otp = data;
  },
  unsetUserShowOtp(state) {
    state.data_otp = '';
  },
  setUserChangePassword(state, data) {
    state.data_password = data;
  },
  unsetUserChangePassword(state) {
    state.data_password = '';
  },
  setChangeSuccess(state) {
    state.success_change_password = true;
  },
  unsetChangeSuccess(state) {
    state.success_change_password = false;
  },
  setDataLoginOtp(state, data) {
    state.data_login_otp = data;
  },
};

const actions = {
  async requestChangePassword({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await jApiProfile.requestChangePassword(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('setUserShowOtp', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async requestForgotPassword({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await jApiProfile.requestForgotPassword(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setUserShowOtp', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async verifyOtpForgot({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await jApiProfile.verifyOtpForgot(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setUserChangePassword', res.data);
      commit('unsetUserShowOtp');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async resetPassword({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    commit('unsetChangeSuccess');
    try {
      const res = await jApiProfile.resetPasswordAuthService(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('unsetUserChangePassword');
      commit('setChangeSuccess');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async verifyOtp({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await jApiProfile.verifyOtp(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('setUserChangePassword', res.data);
      commit('unsetUserShowOtp');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async changePassword({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    commit('unsetChangeSuccess');
    try {
      const res = await jApiProfile.changePasswordAuthService(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('unsetUserChangePassword');
      commit('setChangeSuccess');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async requestOtpLogin({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await jApiProfile.requestOtpLogin(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setDataLoginOtp', req);
      commit('setUserShowOtp', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
