import jUserAPIManager from '@/services/payroll/jUserAPIManager';
import jCompanyChartAPIManager from '@/services/payroll/jCompanyChartAPIManager';
import jPayrollAPIManager from '@/services/payroll/jPayrollAPIManager';
import jConstant from '@/utils/constant-payroll';
import jApiPayroll from '../services/jApiPayroll';

const state = {
  url: '',
  responseAction: 0,
  errorMessage: 'Failed',
  optionsJob: [],
  optionsTitle: [],
  optionsDivision: [],
  form: {
    status: 0,
    organization: {
      id: 0,
      name: '',
    },
    position: {
      id: 0,
      name: '',
    },
    job: {
      id: 0,
      name: '',
    },
    start_date: null,
    key: '',
  },
};

const actions = {
  onSetUrl({ commit }, data) {
    commit('actionSetUrl', data);
  },
  requestGetEmployee({ commit }) {
    return jUserAPIManager.requestGetUsers(state.url)
      .catch((error) => {
        if (error.response && error.response.data && error.response.data.message) {
          commit('actionOnRequestFailed', error.response.data.message);
        } else {
          commit('actionOnRequestFailed', 'Something error, please try again later');
        }
      });
  },
  async getEmployeeTemplate({ dispatch }) {
    try {
      const res = await jPayrollAPIManager.requestGetImportEmployee();
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async importEmployee({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiPayroll.importPayroll(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.messages, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.messages, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  requestSuccess({ commit }) {
    commit('actionOnSuccessGetUsers');
  },
  resetResponseAction({ commit }) {
    commit('resetResponseAction');
  },
  generateChart(listOfData, result, deep = 0) {
    let space = '';
    for (let i = 0; i < deep; i += 1) {
      space += '-';
    }

    listOfData.forEach((data) => {
      result.push({
        id: data.id,
        text: space + data.name,
        is_delete: false,
      });

      if (data.children && data.children.length > 0) {
        actions.generateChart(data.children, result, deep + 1);
      }
    });
  },
  requestLoadJobList({ commit }) {
    commit('updateStateForLoadJobList', 1);
    jCompanyChartAPIManager.requestGetJobChart()
      .then((response) => {
        commit('updateStateForLoadJobList', 0);
        commit('onRequestJobListSuccess', response.data);
      })
      .catch(() => {
        commit('updateStateForLoadJobList', 99);
      });
  },
  requestLoadDivisionList({ commit }) {
    commit('updateStateForLoadDivisionList', 1);
    jCompanyChartAPIManager.requestGetOrganizationChart()
      .then((response) => {
        commit('updateStateForLoadDivisionList', 0);
        commit('onRequestDivisionListSuccess', response.data);
      })
      .catch(() => {
        commit('updateStateForLoadDivisionList', 99);
      });
  },
  requestLoadTitleList({ commit }) {
    commit('updateStateForLoadTitleList', 1);
    jCompanyChartAPIManager.requestGetPositionChart()
      .then((response) => {
        commit('updateStateForLoadTitleList', 0);
        commit('onRequestTitleListSuccess', response.data);
      })
      .catch(() => {
        commit('updateStateForLoadTitleList', 99);
      });
  },
  resetFormData({ commit }) {
    commit('onResetFormData');
  },
  requestExportEmployee({ commit }) {
    jPayrollAPIManager.requestExportPayrollRequest(null, 0, 0, jConstant.ExportTypeEmployee,
    )
      .then((response) => {
        commit('onSuccessSendRequestExportEmployee', response.data);
      })
      .catch((error) => {
        if (error.response && error.response.data && error.response.data.messages) {
          commit('actionOnRequestFailed', error.response.data.messages);
        } else {
          commit('actionOnRequestFailed', 'Something error, please try again later');
        }
      });
  },
};

const mutations = {
  resetResponseAction() {
    state.responseAction = 0;
  },
  actionOnSuccessGetUsers() {
    state.responseAction = 1;
  },
  actionOnRequestFailed(mState, data) {
    state.errorMessage = data;
    state.responseAction = 99;
  },
  actionSetUrl(mState, data) {
    state.url = data;
  },
  onResetFormData() {
    state.form.status = 0;
    state.form.organization = {
      id: 0,
      name: '',
    };
    state.form.position = {
      id: 0,
      name: '',
    };
    state.form.job = {
      id: 0,
      name: '',
    };
    state.form.start_date = null;
    state.form.key = '';
  },
  updateStateForLoadJobList(mState, value) {
    state.stateForLoadJobList = value;
  },
  updateStateForLoadDivisionList(mState, value) {
    state.stateForLoadDivisionList = value;
  },
  updateStateForLoadTitleList(mState, value) {
    state.stateForLoadTitleList = value;
  },
  onRequestJobListSuccess(mState, data) {
    state.optionsJob = [];
    actions.generateChart(data.charts, state.optionsJob);
  },
  onRequestDivisionListSuccess(mState, data) {
    state.optionsDivision = [];
    actions.generateChart(data.charts, state.optionsDivision);
  },
  onRequestTitleListSuccess(mState, data) {
    state.optionsTitle = [];
    actions.generateChart(data.charts, state.optionsTitle);
  },
  onSuccessSendRequestExportEmployee(mState, data) {
    state.successMessage = data.messages;
    state.responseAction = 2;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
