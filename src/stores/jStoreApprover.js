
import apiApprover from '@/services/jApiApprover';

const state = {
  summary_block: [],
  data_profile: {},
  record_approve: false,

  show_modal: true,
  show_modal_detail: true,
};

const mutations = {
  // eslint-disable-next-line
  showSummaryBlock(state, data) {
    state.summary_block = data.data;
  },

  // eslint-disable-next-line
  getDataProfile(state, data) {
    state.data_profile = data.user;
  },

  // eslint-disable-next-line
  appData(state) {
    state.record_approve = !state.record_approve;
    // router.push('/requestor/cancel');
  },

  // eslint-disable-next-line
  setShowModal(state) {
    state.show_modal = true;
  },

  // eslint-disable-next-line
   setHideModal(state) {
    state.show_modal = false;
  },

  // eslint-disable-next-line
  setShowModalDetail(state) {
    state.show_modal_detail = true;
  },

  // eslint-disable-next-line
   setHideModalDetail(state) {
    state.show_modal_detail = false;
  },
};

const actions = {
  async getSummaryBlock({ commit, dispatch }) {
    try {
      const getSummaryBlock = await apiApprover.getSummaryBlock();
      commit('showSummaryBlock', getSummaryBlock);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async sentDataDraftRequestor({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });
    try {
      const res = await apiApprover.getSummaryBlock();
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('showSummaryBlock', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async getDataProfileRequestor({ commit, dispatch }, data) {
    try {
      const res = await apiApprover.getDataProfile(data);
      commit('getDataProfile', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async sentDataDraftApprover({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });

    try {
      const res = await apiApprover.sentDataDraftApprover(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('appData');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },


  async sentDataUpdate({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });

    try {
      const res = await apiApprover.sentDataUpdate(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async sentDataApproverAndUpdate({ commit, dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });

    try {
      const res = await apiApprover.sentDataApproverAndUpdate(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('appData');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async hideModal({ commit, dispatch }) {
    try {
      commit('setHideModal');
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async showModal({ commit, dispatch }) {
    try {
      commit('setShowModal');
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
