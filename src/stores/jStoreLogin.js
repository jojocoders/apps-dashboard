/* eslint-disable no-empty */
/* eslint-disable no-shadow,consistent-return,no-param-reassign,no-unused-vars,no-console */
// import axios from 'axios';
import router from '@/router';
import apiGateway from '@/services/jApiGateway';
import apiProfile from '@/services/jApiProfile';
import apiUser from '@/services/jApiUser';
// import apiSettings from '@/services/jApiSettings';

const state = {
  // Set default state(s) here
  idToken: null,
  refreshToken: null,
  refresh_token: null,
  profile_data: null,
  currencyFormat: {
    digitGroupSeparator: '.',
    decimalCharacter: ',',
    decimalCharacterAlternative: '.',
    currencySymbol: 'Rp',
    currencySymbolPlacement: 'p',
    roundingMethod: 'U',
    minimumValue: '0',
    maximumValue: '99999999999999',
  },
  is_refresh_token: false,
  login_failed: false,
  data_expire: null,
  data_expire_refresh_token: null,
  roles: [],
  packages: [],
  features: [],
  attendanceStatus: false,
  errorAuthGoogle: '',
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  loginFailed(state) {
    state.login_failed = !state.login_failed;
  },

  saveToken(state, data) {
    function parseJwt(token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      return JSON.parse(window.atob(base64));
    }
    state.data_expire = parseJwt(data.token);
    state.idToken = data.token;
    state.refreshToken = data.refresh_token;
    state.is_refresh_token = !state.is_refresh_token;
    localStorage.setItem('gateUrl', process.env.GATE_URL);
  },

  saveTokenV2(state, data) {
    function parseJwt(token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      return JSON.parse(window.atob(base64));
    }
    state.data_expire = parseJwt(data.token);
    state.idToken = data.token;
    // state.refreshToken = data.refresh_token;
    // state.is_refresh_token = !state.is_refresh_token;
  },

  setCurrencyFormat(state) {
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },

  setRefreshToken(state, data) {
    function parseJwt(token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      return JSON.parse(window.atob(base64));
    }
    state.refresh_token = data.refresh_token;
    state.data_expire_refresh_token = parseJwt(data.refresh_token);

    const tokenData = {
      value: state.refresh_token,
      expire: state.data_expire_refresh_token.exp * 1000,
    };
    localStorage.setItem('refresh_token', JSON.stringify(tokenData));
  },

  setInLocalStorage(state) {
    const tokenData = {
      value: state.idToken,
      expire: state.data_expire.exp * 1000,
    };
    localStorage.setItem('Token', JSON.stringify(tokenData));
    localStorage.setItem('refreshToken', state.refreshToken);
    localStorage.setItem('currency_format', JSON.stringify(state.currencyFormat));
  },

  doLogin() {
    window.location = '/';
  },

  backLogin(state, data) {
    state.errorAuthGoogle = data;
    router.push('/login');
  },

  setProfile(state, payload) {
    state.profile_data = payload;
    localStorage.setItem('email', state.profile_data.email);
    localStorage.setItem('photo', state.profile_data.photo_url);
    localStorage.setItem('firstName', state.profile_data.first_name);
    localStorage.setItem('lastName', state.profile_data.last_name);
    localStorage.setItem('userCompanyId', state.profile_data.user_company_id);
    localStorage.setItem('user_company_id', state.profile_data.user_company_id);
    localStorage.setItem('companyName', '');
    localStorage.setItem('daysRemaining', '15');
    localStorage.setItem('fullName', `${state.profile_data.first_name} ${state.profile_data.last_name}`);
    localStorage.setItem('phoneNumber', state.profile_data.phone_number);
    localStorage.setItem('createdAt', state.profile_data.created_date);
    localStorage.setItem('gender', state.profile_data.gender);
  },

  setRole(state, roleData) {
    const role = roleData.data.role.states;
    const roleString = roleData.data.role.names;
    const packages = roleData.data.package.states;
    state.roles = role;
    state.packages = packages;
    localStorage.setItem('role', JSON.stringify(role));
    localStorage.setItem('roleString', JSON.stringify(roleString));
    localStorage.setItem('userRole', roleData.data.user_role);
  },

  setFeature(state, featureData) {
    const features = [];
    featureData.data.forEach((e) => {
      if (e.status === 1) {
        features.push(e.setting);
      }
    });
    state.features = features;
  },

  setAttendance(state, attendance) {
    state.attendanceStatus = attendance.is_have_attendance;
  },
};

const actions = {
  async loginAccount({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.login(authData);
      commit('saveToken', auth);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      // const getFeature = await apiSettings.listFeature();
      // const attendanceStatus = await apiProfile.checkAttendance();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      // commit('setFeature', getFeature);
      // commit('setAttendance', attendanceStatus);
      commit('doLogin');
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
  },

  async loginAccountV2({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.loginV2(authData);
      if (auth.data.token) {
        commit('saveToken', auth.data);
        commit('setRefreshToken', auth.data);
        commit('setInLocalStorage');
        const getProfil = await apiProfile.getProfile();
        const getRole = await apiUser.userAuthorize();
        commit('setProfile', getProfil);
        commit('setRole', getRole);
        commit('doLogin');
      } else {
        dispatch('jStoreNotificationScreen/toggleProblem', 'Token is empty', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
  },

  async loginAccountLdap({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.loginLdap(authData);
      if (auth.data.token) {
        commit('saveToken', auth.data);
        commit('setRefreshToken', auth.data);
        commit('setInLocalStorage');
        const getProfil = await apiProfile.getProfile();
        const getRole = await apiUser.userAuthorize();
        commit('setProfile', getProfil);
        commit('setRole', getRole);
        commit('doLogin');
      } else {
        dispatch('jStoreNotificationScreen/toggleProblem', 'Token is empty', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
  },

  async loginAccountCustom({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.loginCustom(authData);
      if (auth.data.token) {
        commit('saveToken', auth.data);
        commit('setRefreshToken', auth.data);
        commit('setInLocalStorage');
        const getProfil = await apiProfile.getProfile();
        const getRole = await apiUser.userAuthorize();
        commit('setProfile', getProfil);
        commit('setRole', getRole);
        commit('doLogin');
      } else {
        dispatch('jStoreNotificationScreen/toggleProblem', 'Token is empty', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
  },

  async loginRefreshTokenApi({ commit, dispatch }, authData) {
    dispatch('jStoreNotificationScreen/toggleLoading', 'Please wait...', { root: true });
    try {
      const auth = await apiGateway.login(authData);
      commit('saveToken', auth);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      // const getFeature = await apiSettings.listFeature();
      // const attendanceStatus = await apiProfile.checkAttendance();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      // commit('setFeature', getFeature);
      // commit('setAttendance', attendanceStatus);
      dispatch('jStoreNotificationScreen/toggleLoading', '', { root: true });
    } catch (err) {
      commit('loginFailed');
    }
  },

  async refreshTokenApi({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const newToken = await apiGateway.refreshToken(req);
      commit('saveToken', newToken);
      commit('setInLocalStorage');
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
    }
  },

  async getProfileAndRole({ commit }) {
    try {
      const getProfil = await apiProfile.getProfile();
      const getRole = await apiUser.userAuthorize();
      // const getFeature = await apiSettings.listFeature();
      // const attendanceStatus = await apiProfile.checkAttendance();
      commit('setProfile', getProfil);
      commit('setRole', getRole);
      // commit('setFeature', getFeature);
      // commit('setAttendance', attendanceStatus);
    } catch (err) {
    }
  },

  async refreshTokenApiSingle({ commit }, req) {
    try {
      const newToken = await apiGateway.refreshToken(req);
      commit('saveToken', newToken);
      commit('setInLocalStorage');
    } catch (err) {
    }
  },

  async getProfile({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getProfil = await apiProfile.getProfile();
      commit('setProfile', getProfil);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
    }
  },

  async getKeyPublicRSA({ dispatch }) {
    let res;
    try {
      res = await apiGateway.getKeyRSA();
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async refreshTokenApiV2({ commit }) {
    try {
      const newToken = await apiGateway.refreshTokenV2();
      commit('saveTokenV2', newToken.data);
      // commit('setRefreshToken', newToken.data);
      commit('setInLocalStorage');
    } catch (err) {
    }
  },

  async setTokenByVerificationAuth({ commit, dispatch }, req) {
    try {
      if (req.token) {
        commit('saveToken', req);
        commit('setRefreshToken', req);
        commit('setInLocalStorage');
        const getProfil = await apiProfile.getProfile();
        const getRole = await apiUser.userAuthorize();
        commit('setProfile', getProfil);
        commit('setRole', getRole);
        if (req.application_id && req.application_name && req.pages_id && req.pages_name) {
          window.location = `/app/${req.application_name}/${req.application_id}/${req.pages_name}/${req.pages_id}`;
        } else {
          commit('doLogin');
        }
      } else {
        dispatch('jStoreNotificationScreen/toggleProblem', 'Token is empty', { root: true });
      }
    } catch (err) {
      commit('backLogin', 'Login failed');
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
