/* eslint-disable no-shadow,consistent-return,no-param-reassign,no-console,no-unused-vars,operator-assignment,max-len */
// import router from '@/router';
import jApiFormUi from '@/services/jApiFormUi';

const state = {
  setup_form_ui: [],
  setup_multi_form_ui: [],
  setup_form_data: [],
  data_record: null,
  data_element: null,
  count_to_back: 1,
  data_otp: null,
  app_security_type: null,
  form_data_parent: null,
  form_ui_parent: null,
  form_data_child: null,
  action_approve_update: false,
  reload_data_component: false,
  is_full_screen_comp: false,
  id_full_screen_comp: null,
};

const mutations = {
  setSetupFormUi(state, payload) {
    state.setup_form_ui = payload;
  },
  setSetupMultiFormUi(state, payload) {
    state.setup_multi_form_ui = payload;
  },
  setSetupFormData(state, payload) {
    state.setup_form_data = payload;
  },
  setDataRecord(state, payload) {
    state.data_record = payload;
  },
  setDataElement(state, payload) {
    state.data_element = payload;
  },
  addCountToBack(state) {
    state.count_to_back = state.count_to_back + 1;
  },
  resetCountToBack(state) {
    state.count_to_back = 1;
  },
  setUserShowOtp(state, data) {
    state.data_otp = data;
  },
  unsetUserShowOtp(state) {
    state.data_otp = '';
  },
  setSecurityType(state, data) {
    state.app_security_type = data;
  },
  setFormdataParent(state, payload) {
    state.form_data_parent = payload;
  },
  setFormUiParent(state, payload) {
    state.form_ui_parent = payload;
  },
  setFormdataChild(state, payload) {
    state.form_data_child = payload;
  },
  setActionApproveUpdate(state, payload) {
    state.action_approve_update = !state.action_approve_update;
  },
  setReloadDataComponent(state, payload) {
    state.reload_data_component = payload;
  },
  setFullScreenComp(state, payload) {
    state.is_full_screen_comp = payload.is_full_screen_comp;
    state.id_full_screen_comp = payload.id_full_screen_comp;
  },
};

const actions = {
  async actionGetSetupFormUi({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getSetupFormUi(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setSetupFormUi', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetSetupMultiFormUi({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getSetupMultiFormUi(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setSetupMultiFormUi', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetSetupFormData({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const res = await jApiFormUi.getSetupFormData(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setSetupFormData', res.data);
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetRecordData({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordData(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordDataV2({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordDataV2(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordCount({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordData(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.count;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.count;
  },

  async actionGetRecordCountV2({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordDataV2(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.count;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.count;
  },

  async actionGetRecordRequesterData({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordRequesterData(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordRequesterDataV2({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordRequesterDataV2(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordRequesterCount({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordRequesterData(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.count;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.count;
  },

  async actionGetRecordRequesterCountV2({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordRequesterDataV2(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.count;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.count;
  },

  async getDataSummaryBlock({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.dataSummary(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async findFormData({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.listFormData(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async createRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.createRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async deleteRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.deleteRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async submitRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.submitRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  async submitRecordV2({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.submitRecordV2(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  async saveDraftRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.saveDraftRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async saveDraftRecordOnly({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.saveDraftRecord(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async getLogRecord({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.logRecord(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordApproverData({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordApproverData(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordApproverDataV2({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordApproverDataV2(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetRecordApproverCount({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordApproverData(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.count;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.count;
  },

  async actionGetRecordApproverCountV2({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getRecordApproverDataV2(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.count;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.count;
  },

  async getDataSummaryBlockApp({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.dataSummaryApp(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async requestOTP({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.requestOTP(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setUserShowOtp', res.data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  async approveRecord({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.approveRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('unsetUserShowOtp');
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async approveUpdateRecord({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.approveUpdateRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('unsetUserShowOtp');
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async approveAllRecord({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.approveAllRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('unsetUserShowOtp');
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async rejectRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.rejectRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async revisionRecord({ dispatch }, data) {
    let res;
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    try {
      res = await jApiFormUi.revisionRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async closeRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.closeRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async cancelRecord({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.cancelRecord(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
    }
    return res;
  },

  async setValueRecord({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.createRecord(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
    return res;
  },

  async setUpdateRecord({ dispatch, commit }, data) {
    let res;
    try {
      res = await jApiFormUi.createRecord(data);
      if (res.error_session && res.security_type === 'password') {
        commit('setSecurityType', 'password');
      } else if (res.error_session && res.security_type === 'otp') {
        commit('setSecurityType', 'otp');
      }
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },

  async actionGetCanvas({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    let res;
    try {
      res = await jApiFormUi.getDataCanvas(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetEmployeeAutofill({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getEmployeeAutofill(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res.data;
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.data;
  },

  async actionButton({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.actionButton(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionButtonMultiple({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.actionButtonMultiple(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async actionGetFunctionData({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getFunctiondata(data);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res.data;
  },

  async verifyPassword({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.verifyPassword(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },

  async requestOtp({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.requestOtp(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
    }
    return res.data;
  },

  async verifyOtp({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait..', { root: true });
    let res;
    try {
      res = await jApiFormUi.verifyOtp(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error_session) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },

  async getListDataGroupByCompanyId({ commit, dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.listDataGroupByCompanyId();
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },

  async getListTableByGroupV2({ commit, dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.listTableByGroupV2(data);
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },

  async getDetailDatasetByGroup({ commit, dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.detailDatasetByGroup(data);
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },

  async listRecordDataset({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getListRecordByDatasetId(data);
    } catch (err) {
      res = err.response.data;
    }
    return res.data;
  },

  async countRecordDataset({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getCountRecordByDatasetId(data);
    } catch (err) {
      res = err.response.data;
    }
    return res.data;
  },

  // Export
  async actionGenerateStandardExport({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.generateStandardExport(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  async actionGenerateAdvanceExport({ dispatch }, params) {
    let res;
    try {
      res = await jApiFormUi.generateAdvanceExport(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', {}, { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message || err.response.message, { root: true });
    }
    return res;
  },
  async actionGetActivityList({ dispatch }, params) {
    let res;
    try {
      res = await jApiFormUi.getActivityList(params);
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message || err.response.message, { root: true });
    }
    return res;
  },
  async actionGenerateFileAdvanceExport({ dispatch }, params) {
    let res;
    try {
      res = await jApiFormUi.generateFileAdvanceExport(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message || err.response.message, { root: true });
    }
    return res;
  },

  // IMPORT
  async actionCancelImportActivity({ dispatch }, params) {
    let res;
    try {
      res = await jApiFormUi.cancelImport(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      res = err.response.data;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message || err.response.message, { root: true });
    }
    return res;
  },

  // New Endpoint Get Detail Data Record
  async actionGetDetailRecordData({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getDetailRecordData(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },
  async actionGetDetailRecordDataReq({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getDetailRecordDataReq(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },
  async actionGetDetailRecordDataApp({ dispatch }, data) {
    let res;
    try {
      res = await jApiFormUi.getDetailRecordDataApp(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        return res;
      }
    } catch (err) {
      res = err.response.data;
    }
    return res;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
