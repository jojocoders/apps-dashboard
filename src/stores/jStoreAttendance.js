/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiAttendance from '@/services/jApiAttendance';
import jApiEmployee from '@/services/jApiEmployee';

const state = {
  project_list: [],
  attendances_list: [],
  detail_attendance: [],
  detail_group_attendance: [],
  monitor_attended_list: [],
  monitor_absence_list: [],
  monitor_leave_list: [],
  shift_list: [],
  team_list: [],
  employee_list: [],
  reminder_detail: [],
  holiday_detail: [],
  shift_detail: [],
  employee_detail: [],
  daily_shift: [],
  venue_detail: [],
  role_setting: [],
  attendance_today: null,
  attendance_fivedays: null,
  attendance_tendays: null,
  employment_list: [],
  detail_fraud_attendance: null,
};

const mutations = {
  setProjectList(state, data) {
    state.project_list = data.data;
  },
  setAttendancesList(state, data) {
    state.attendances_list = data.data;
  },
  setDetailAttendance(state, data) {
    state.detail_attendance = data.data;
  },
  setDetailGroupAttendance(state, data) {
    state.detail_group_attendance = data.data;
  },
  setMonitorAttendedList(state, data) {
    state.monitor_attended_list = data.data;
  },
  setMonitorAbsenceList(state, data) {
    state.monitor_absence_list = data.data;
  },
  setMonitorLeaveList(state, data) {
    state.monitor_leave_list = data.data;
  },
  setShiftList(state, data) {
    state.shift_list = data.data.data;
  },
  setTeamList(state, data) {
    state.team_list = data.data.data;
    // state.team_list = Array.from(new Set(data.data.data.map(a => a.title)))
    //   .map(title => data.data.data.find(a => a.title === title));
  },
  setEmployeeList(state, data) {
    state.employee_list = data.data.data;
  },
  setReminderDetail(state, data) {
    state.reminder_detail = data.data;
  },
  setHolidayDetail(state, data) {
    state.holiday_detail = data.data;
  },
  setShiftDetail(state, data) {
    state.shift_detail = data.data;
  },
  setEmployeeDetail(state, data) {
    state.employee_detail = data.data;
  },
  setDailyShift(state, data) {
    state.daily_shift = data.data;
  },
  setVenueDetail(state, data) {
    state.venue_detail = data.data;
  },
  setRoleSetting(state, data) {
    state.role_setting = data.data;
  },

  setAttendanceToday(state, data) {
    state.attendance_today = data.data;
  },

  setAttendanceFivedays(state, data) {
    state.attendance_fivedays = data.data;
  },

  setAttendanceTendays(state, data) {
    state.attendance_tendays = data.data;
  },

  setEmploymentList(state, data) {
    state.employment_list = data.data;
  },

  setDetailFraudAttendance(state, data) {
    state.detail_fraud_attendance = data.data;
  },
};

const actions = {
  async actionGetProjectList({ dispatch, commit }, { data }) {
    try {
      const res = await jApiAttendance.getProjectList(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setProjectList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetAttendances({ dispatch, commit }, req) {
    try {
      const res = await jApiAttendance.getAttendances(req);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setAttendancesList', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetDetailAttendance({ dispatch, commit }, data) {
    try {
      const res = await jApiAttendance.getDetailAttendance(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDetailAttendance', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetDetailGroupAttendance({ dispatch, commit }, data) {
    try {
      const res = await jApiAttendance.getDetailGroupAttendance(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDetailGroupAttendance', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetMonitorAttended({ dispatch, commit }, { data }) {
    try {
      const res = await jApiAttendance.getMonitorAttended(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setMonitorAttendedList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetMonitorAbsences({ dispatch, commit }, { data }) {
    try {
      const res = await jApiAttendance.getMonitorAbsence(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setMonitorAbsenceList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetMonitorLeave({ dispatch, commit }, { data }) {
    try {
      const res = await jApiAttendance.getMonitorLeave(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setMonitorLeaveList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetDetailFraudAttendance({ dispatch, commit }, params) {
    try {
      const res = await jApiAttendance.getDetailFraudAttendance(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDetailFraudAttendance', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // ATTENDANCE REPORT
  async actionGetShift({ dispatch, commit }, params) {
    try {
      const res = await jApiAttendance.getShift(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setShiftList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetShiftAsync({ dispatch }, { reqData, callback }) {
    try {
      const listShift = await jApiAttendance.getShift(reqData);
      const res = listShift.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async actionGetProjectAsync({ dispatch }, { reqData, callback }) {
    try {
      const listProject = await jApiAttendance.getProjectList(reqData);
      const res = listProject.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async actionGetTeam({ dispatch, commit }, params) {
    try {
      const res = await jApiAttendance.getTeam(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setTeamList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetEmployee({ dispatch, commit }, params) {
    try {
      const res = await jApiAttendance.getEmployee(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setEmployeeList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async generateSummaryXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.generateSummaryXLS(rootId);
      if (res.data.data.url) {
        window.location.href = res.data.data.url;
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async generateDetailXLS({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const data = await jApiAttendance.generateDetailXLS(rootId);
      window.location.href = data.data.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
  //  AUTO ATTENDANCE

  async actionSetAutoAttendance({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.setAutoAttendance(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionUpdateAutoAttendance({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.setUpdateAutoAttendance(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // ATTENDANCE CONFIG
  async actionGetRoleSetting({ dispatch, commit }) {
    try {
      const res = await jApiAttendance.getRoleSetting();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setRoleSetting', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  // code
  async actionGetAttendanceCodeDetail({ dispatch, commit }, companyId, id) {
    try {
      const res = await jApiAttendance.getAttendanceCodeDetail(companyId, id);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setCodeDetail', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionSetAttendanceCode({ dispatch }, { companyId, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.setAttendanceCode(companyId, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (!res.error) {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteAttendanceCode({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.deleteAttendanceCode(id);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // reminder
  async actionGetReminderDetail({ dispatch, commit }, id) {
    try {
      const res = await jApiAttendance.getReminderDetail(id);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setReminderDetail', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateReminder({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.createReminder(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateReminder({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.updateReminder(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteReminder({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.deleteReminder(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // holiday
  async actionGetHolidayDetail({ dispatch, commit }, id) {
    try {
      const res = await jApiAttendance.getHolidayDetail(id);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setHolidayDetail', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateHoliday({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.createHoliday(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateHoliday({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.updateHoliday(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteHoliday({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.deleteHoliday(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // shift
  async actionGetShiftDetail({ dispatch, commit }, data) {
    try {
      const res = await jApiAttendance.getShiftDetail(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setShiftDetail', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.createShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.updateShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.deleteShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionExportShift({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.exportShift(rootId);
      window.location.href = res.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  // mapping shift
  async actionGetMappingShiftTemplate({ dispatch }, data) {
    try {
      const res = await jApiAttendance.getMappingShiftTemplate(data);
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionImportMappingShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.importMappingShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionGetDailyShift({ dispatch, commit }, data) {
    try {
      const res = await jApiAttendance.getDailyShift(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setDailyShift', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateDailyShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.createDailyShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateDailyShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.updateDailyShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionReassignDailyShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.reassignDailyShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteDailyShift({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.deleteDailyShift(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionGetEmployeeDetailById({ dispatch, commit }, data) {
    try {
      const res = await jApiEmployee.getDetailEmployeeById(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setEmployeeDetail', res);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // venue
  async actionGetVenueList({ dispatch }, { reqData, callback }) {
    try {
      const listVenue = await jApiAttendance.getVenueList(reqData);
      const res = listVenue.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async actionGetVenueDetail({ dispatch, commit }, id) {
    try {
      const res = await jApiAttendance.getVenueDetail(id);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setVenueDetail', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateVenue({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.createVenue(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionUpdateVenue({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.updateVenue(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteVenue({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.deleteVenue(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionExportVenue({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiAttendance.exportVenue(rootId);
      window.location.href = res.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },

  async actionSearchPlaceByAddress({ dispatch }, { searchQuery, callback }) {
    try {
      const listPlaces = await jApiAttendance.searchPlaceByAddress(searchQuery);
      const res = listPlaces.data.results;
      res.forEach((lab, index) => {
        res[index].id = lab.place_id;
        res[index].label = lab.formatted_address;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async actionSearchPlaceByLatLong({ dispatch }, { lat, lng }) {
    try {
      const place = await jApiAttendance.searchPlaceByLatLong(lat, lng);
      return place.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
      return err.response.data;
    }
  },

  // manager dashboard
  async actionGetAttendanceToday({ dispatch, commit }) {
    try {
      const res = await jApiAttendance.getAttendanceToday();
      commit('setAttendanceToday', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetAttendanceFivedays({ dispatch, commit }) {
    try {
      const res = await jApiAttendance.getAttendanceFivedays();
      commit('setAttendanceFivedays', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetAttendanceTendays({ dispatch, commit }) {
    try {
      const res = await jApiAttendance.getAttendanceTendays();
      commit('setAttendanceTendays', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetListEmploymentType({ dispatch, commit }) {
    try {
      const res = await jApiAttendance.getListEmploymentType();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setEmploymentList', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
