/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import router from '@/router';
import jApiDivisionFlow from '@/services/jApiDivisionFlow';

const state = {
  division_detail: null,
  division_created: false,
  division_flow_data: null,
  cost_center_list: null,
  current_cost_center: {},
};

const mutations = {
  divisionCreated(state) {
    state.division_created = !state.division_created;
    router.go(-1);
    // const isSetting = window.location.href.includes('pro/setting');
    // if (isSetting) {
    //   router.push('/company/division');
    // } else {
    //   router.push('/company/organization');
    // }
  },

  divisionUpdated(state) {
    state.division_created = !state.division_created;
    router.go(-1);
    // const isSetting = window.location.href.includes('pro/setting');
    // if (isSetting) {
    //   router.push('/company/division');
    // } else {
    //   router.push('/company/organization');
    // }
  },

  showDetailDivision(state, data) {
    state.division_detail = data.data;
  },

  dataFlowDivision(state, data) {
    state.division_flow_data = data;
  },

  costCenterListSuccess(state, data) {
    state.cost_center_list = data.data.data.map(val => (Object.assign(val, { name: val.name })));
  },

  costCenterByDivisionSuccess(state, data) {
    const { id, name, code } = data.data.data;
    state.current_cost_center = { id, name, label: `${code} - ${name}` };
  },
};

const actions = {
  async applyCostCenter({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiDivisionFlow.applyCostCenter(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async getCostCenterList({ dispatch, commit }, req) {
    try {
      const res = await jApiDivisionFlow.getCostCenterList(req);
      commit('costCenterListSuccess', res);
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  // async
  async getAsyncCostCenterList({ dispatch }, { reqData, callback }) {
    try {
      const listCost = await jApiDivisionFlow.getCostCenterList(reqData);
      const res = listCost.data.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.code} - ${lab.name}`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getDetailCostCenterByDivision({ dispatch, commit }, req) {
    try {
      const res = await jApiDivisionFlow.getDetailCostCenterByDivision(req);
      commit('costCenterByDivisionSuccess', res);
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async createUnit({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiDivisionFlow.createDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async createDivision({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createDivision = await jApiDivisionFlow.createDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createDivision.message, { root: true });
      commit('divisionCreated', createDivision.data);
      return createDivision;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async getDivisionDetail({ commit, dispatch }, divisionId) {
    try {
      const detailDivision = await jApiDivisionFlow.getDivisionDetail(divisionId);
      commit('showDetailDivision', detailDivision);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateDivision({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateDivision = await jApiDivisionFlow.updateDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateDivision.message, { root: true });
      commit('divisionUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteDivision({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const deleteDivision = await jApiDivisionFlow.deleteDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', deleteDivision.message, { root: true });
      commit('divisionUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createFlowDivision({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createFlowDivision = await jApiDivisionFlow.createFlowDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createFlowDivision.message, { root: true });
      commit('divisionUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateFlowDivision({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateFlowDivision = await jApiDivisionFlow.updateFlowDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateFlowDivision.message, { root: true });
      commit('divisionUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteFlowDivision({ dispatch, commit }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const deleteFlowDivision = await jApiDivisionFlow.deleteFlowDivision(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', deleteFlowDivision.message, { root: true });
      commit('divisionUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async createFlowDivisionInUpdate({ dispatch }, req) {
    try {
      await jApiDivisionFlow.createFlowDivision(req);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async updateFlowDivisionInUpdate({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await jApiDivisionFlow.updateFlowDivision(req);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteFlowDivisionInUpdate({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await jApiDivisionFlow.deleteFlowDivision(req);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async setDataFlowDivision({ commit }, req) {
    commit('dataFlowDivision', req);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
