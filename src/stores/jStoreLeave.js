/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiLeaveMonitor from '@/services/leave/jApiLeaveMonitor';

const state = {
  back_properties: {
    is_monitor: null,
    value: null,
  },
  leave_employee_list: [],
};

const mutations = {
  setBackProperties(state, data) {
    state.back_properties = {
      is_monitor: data.is_monitor,
      value: data.value,
    };
  },
  setLeaveEmployee(state, payload) {
    state.leave_employee_list = payload;
  },
};

const actions = {
  async actionApproval({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const id = req.id;
      const payload = {
        action: req.action,
        note: req.note,
      };
      const res = await apiLeaveMonitor.actionApproval(payload, id);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', `Success ${req.action === 'approve' ? 'approv' : req.action}ing leave${res.data.message ? '' : ''}`, { root: true });
      return res.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  onSetBackProperties({ commit }, req) {
    commit('setBackProperties', req);
  },
  async actionGetLeaveEmployee({ dispatch, commit }, data) {
    try {
      const res = await apiLeaveMonitor.getLeaveEmployee(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setLeaveEmployee', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetLeaveEmployeeByRestriction({ dispatch, commit }, data) {
    try {
      const res = await apiLeaveMonitor.getLeaveEmployeeByRestriction(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setLeaveEmployee', res.data);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
