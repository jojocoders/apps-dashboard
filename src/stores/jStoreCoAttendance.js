/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiCoAttendance from '@/services/jApiCoAttendance';
import jApiUser from '@/services/jApiUser';

const state = {
  user_restriction: [],
  user_restriction_report: [],
  coattendace_detail: null,
  coattendance_logs: [],
};

const mutations = {
  setUserRestric(state, data) {
    state.user_restriction = data.data.data;
  },
  setUserRestricReport(state, data) {
    state.user_restriction_report = data.data.data;
  },
  setCoAttendanceDetail(state, data) {
    state.coattendace_detail = data.data;
  },
  setCoattendanceLogs(state, data) {
    state.coattendance_logs = data.data;
  },
};

const actions = {
  async actionGetUserRestrictionMonitor({ commit, dispatch }, req) {
    try {
      const res = await jApiUser.getUserRestrictionMonitor(req);
      commit('setUserRestric', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetUserRestrictionReport({ commit, dispatch }, req) {
    try {
      const res = await jApiUser.getUserRestrictionReport(req);
      commit('setUserRestricReport', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetSummaryBlock({ dispatch }, { data }) {
    try {
      const res = await jApiCoAttendance.getSummaryBlock(data);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetCoattendanceDetail({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.getCoattendanceDetail(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      commit('setCoAttendanceDetail', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionDeleteAttendance({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.deleteAttendance(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  async actionDeleteAttendanceDraft({ dispatch }, params) {
    // dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.deleteAttendanceDraft(params);
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      // dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionCreateCoattendance({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.createCoattendance(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionUpdateCoattendance({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.updateCoattendance(params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionChangeStatusCoattendance({ dispatch }, { id, params }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.changeStatusCoattendance(id, params);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionExportCoattendanceMonitor({ dispatch }, params) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiCoAttendance.exportCoattendanceMonitor(params);
      window.location.href = res.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
  // LOGS
  async actionGetLogs({ dispatch, commit }, params) {
    try {
      const res = await jApiCoAttendance.getListLogs(params);
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setCoattendanceLogs', res.data);
      }
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
