/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import router from '@/router';
import apiRule from '@/services/jApiRule';

const state = {
  rules_table: [],
  is_rules_deleted: false,
  rules_detail: null,
  upload_rules_finished: false,
  all_rule_list: null,
  total_length_rule: null,
};

const mutations = {
  // list rules
  showListRules(state, data) {
    state.rules_table = data;
  },

  // rules was Deleted
  rulesDeleted(state) {
    state.is_rules_deleted = !state.is_rules_deleted;
  },

  // show detail rules
  showDetailRules(state, data) {
    state.rules_detail = data;
  },

  showUploadRuleFinished(state) {
    state.upload_rules_finished = !state.upload_rules_finished;
  },

  // set all rules
  setAllRules(state, payload) {
    state.total_length_rule = payload.length;
    state.all_rule_list = payload.data;
  },

  ruleCreatedOrUpdated() {
    setTimeout(() => {
      router.go(-1);
    }, 1000);
  },
};

const actions = {
  // get list table flow
  async getListRules({ commit, dispatch }, data) {
    try {
      const listRule = await apiRule.listRule(data);
      commit('showListRules', listRule);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create new rules
  async createNewRules({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait', { root: true });
    try {
      const createRule = await apiRule.createRule(data);
      commit('showListRules', createRule);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createRule.message, { root: true });
      commit('ruleCreatedOrUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // update rule
  async updateRule({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateRule = await apiRule.updateRule(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateRule.message, { root: true });
      commit('ruleCreatedOrUpdated');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // remove rules
  async removeRules({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const removeRule = await apiRule.removeRule(data);
      commit('rulesDeleted');
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', removeRule.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // detail rules
  async getDetailRules({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailRule = await apiRule.detailRule(data);
      commit('showDetailRules', detailRule);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getAllRules({ dispatch, commit }, data) {
    try {
      const getList = await apiRule.listRule(data);
      commit('setAllRules', getList);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
