// import router from '@/router';
/* eslint-disable no-shadow */
import jApiLeaveConfigMapping from './../../services/leave/jApiLeaveConfigMapping';

const state = {
  mapping_list: [],
  mapping_leave: null,
  mapping_detail: null,
  rule_list: [],
  user_company_id: null,
  user_name: null,
  leave_type: [],
  mapping_list_organigram: [],
};

const mutations = {
  setMappingList(state, data) {
    state.mapping_list = data;
  },
  setMappingOgranigramList(state, data) {
    state.mapping_list_organigram = data;
  },
  setMappingLeave(state, data) {
    state.mapping_leave = data;
  },
  setRuleList(state, data) {
    state.rule_list = data;
  },
  setMappingDetail(state, data) {
    state.mapping_detail = data;
  },
  setUserDataAsState(state, data) {
    state.user_company_id = data.user_company_id;
    state.user_name = data.name;
  },
  setLeaveType(state, data) {
    state.leave_type = data;
  },
};

const actions = {
  async getConfigMappingList({ commit, dispatch }, req) {
    try {
      const res = await jApiLeaveConfigMapping.getConfigMappingList(req);
      commit('setMappingList', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getConfigMappingOrganigramList({ commit, dispatch }, req) {
    try {
      const res = await jApiLeaveConfigMapping.getConfigMappingOrganigramList(req);
      commit('setMappingOgranigramList', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getMappingLeave({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveConfigMapping.getMappingLeave(state.user_company_id);
      commit('setMappingLeave', res.data);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getMappingDetail({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveConfigMapping.getMappingDetail(req.id, req.user_company_id);
      const data = res.data;
      data.id = req.id;
      commit('setMappingDetail', data);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getRuleList({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveConfigMapping.getRuleList(req);
      commit('setRuleList', res.data);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getLeaveType({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveConfigMapping.getLeaveType(req);
      commit('setLeaveType', res.data);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async reconfigureDate({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    let res;
    try {
      res = await jApiLeaveConfigMapping.reconfigureDate(req);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
    return res;
  },
  setUserData({ commit }, data) {
    commit('setUserDataAsState', data);
  },
  resetMappingList({ commit }) {
    commit('setMappingList', []);
  },

  async actionExportLeaveType({ dispatch }, rootId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveConfigMapping.exportLeaveType(rootId);
      window.location.href = res.data.url;
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
