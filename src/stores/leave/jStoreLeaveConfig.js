// import router from '@/router';
/* eslint-disable no-shadow */
import jApiProject from './../../services/leave/jApiProject';

const state = {
  isMappingTabSelected: false,
  isLeaveTypeTabSelected: true,
};

const mutations = {
  setShownTabsValue(state, data) {
    state.isLeaveTypeTabSelected = data.tab1;
    state.isMappingTabSelected = data.tab2;
  },
};

const actions = {
  showMappingTab({ commit }) {
    const data = {
      tab1: false,
      tab2: true,
    };
    commit('setShownTabsValue', data);
  },
  showLeaveTypeTab({ commit }) {
    const data = {
      tab1: true,
      tab2: false,
    };
    commit('setShownTabsValue', data);
  },
  async getDivision({ dispatch }, { reqData, callback }) {
    try {
      const listDiv = await jApiProject.listDivision(reqData);
      const res = listDiv.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
