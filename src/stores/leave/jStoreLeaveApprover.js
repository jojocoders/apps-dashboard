/* eslint-disable no-shadow */
import apiLeaveApprover from './../../services/leave/jApiLeaveApprover';

const state = {
  leave_detail: null,
  back_properties: {
    is_monitor: null,
    value: null,
  },
};

const mutations = {
  setLeaveDetail(state, data) {
    state.leave_detail = data.data.data;
  },
  setBackProperties(state, data) {
    state.back_properties = {
      is_monitor: data.is_monitor,
      value: data.value,
    };
  },
};

const actions = {
  async getLeaveDetail({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiLeaveApprover.getLeaveDetail(req);
      commit('setLeaveDetail', res);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionApproval({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const id = req.id;
      const payload = {
        action: req.action,
        note: req.note,
      };
      const res = await apiLeaveApprover.actionApproval(payload, id);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', `Success ${req.action === 'approve' ? 'approv' : req.action}ing leave${res.data.message ? '' : ''}`, { root: true });
      return res.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },
  onSetBackProperties({ commit }, req) {
    commit('setBackProperties', req);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
