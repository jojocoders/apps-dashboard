// import router from '@/router';
/* eslint-disable no-shadow */
import jApiLeaveClaimer from './../../services/leave/jApiLeaveClaimer';

const state = {
  user_restriction: [],
  dataDetailPolicy: null,
  leave_type: [],
  calculate_leave: '',
  leave_detail: null,
  period_to: null,
  leave_logs: [],
  role_setting: [],
};

const mutations = {

  setUserRestric(state, data) {
    state.user_restriction = data.data;
  },

  setLeaveType(state, data) {
    state.leave_type = data.data;
  },

  setCalculateLeave(state, data) {
    state.calculate_leave = data.data;
  },

  setPeriodTo(state, data) {
    state.period_to = data.data.end_date;
  },

  setLeaveDetail(state, data) {
    state.leave_detail = data.data.data;
  },

  setLeaveLogs(state, data) {
    state.leave_logs = data.data.data;
  },

  setRoleSetting(state, data) {
    state.role_setting = data.data;
  },
};

const actions = {

  async userRestriction({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.leaveUserRestric(req);
      commit('setUserRestric', res);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getUserTypeApproval({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.leaveUserType(req);
      commit('setLeaveType', res);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem',
        err.response.data.message, { root: true });
    }
  },

  async getCalculateLeave({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.calculateLeave(req);
      commit('setCalculateLeave', res);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem',
        err.response.data.message, { root: true });
    }
  },

  async saveDraft({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.saveDraft(req);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async submitLeave({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.submitLeave(req);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async createLeave({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.createLeaveV3(req);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async deleteLeave({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.deleteLeave(req);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async actionClaimer({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.actionClaimer(req.payload, req.id);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem',
        err.response.data.message, { root: true });
      return err.response.data;
    }
  },

  async getPeriodTo({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.getPeriodTo(req);
      commit('setPeriodTo', res.data);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message);
    }
  },

  async getDetailLeave({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.getDetailLeave(req.id);
      commit('setLeaveDetail', res);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem',
        err.response.data.message, { root: true });
    }
  },

  async getLeaveLogs({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await jApiLeaveClaimer.getLeaveLogs(req.id);
      commit('setLeaveLogs', res);
      dispatch('jStoreNotificationScreen/hideLoading', 'Please wait...', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem',
        err.response.data.message, { root: true });
    }
  },

  async actionGetRoleSetting({ dispatch, commit }) {
    try {
      const res = await jApiLeaveClaimer.getRoleSetting();
      if (res.error) {
        dispatch('jStoreNotificationScreen/toggleProblem', res.message, { root: true });
      } else {
        commit('setRoleSetting', res);
      }
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
