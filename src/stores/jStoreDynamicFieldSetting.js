/* eslint-disable no-shadow,consistent-return,no-param-reassign */
const state = {
  // Properties Field
  isBulk: false,
  isSearch: false,
  isDataset: false,
  isFieldGroup: false,
  isTableGroup: false,
  isDataPreview: false,
  isDataPreviewGroup: false,
  isComponent: false,
  propertiesOpened: false,
  propertiesGroup: false,
  // Modal Field in group
  isShowModalFieldFromGroup: false,
  showModalFieldLibrary: false,
  sourceModalFieldLibrary: null,
  // Modal External APi
  showModalExternalApi: false,
  sourceExternalApi: null,
  targetStackedProperties: null,
  // Modal Script Before & After
  showModalScriptBeforeAfter: false,
  sourceScriptBeforeAfter: null,
  // Modal Script Action Button
  showModalScriptActionBtn: false,
  sourceScriptActionBtn: null,
  // Modal Icon Action Button
  showModalIconActionBtn: false,
  sourceIconActionBtn: null,

  configurationOpened: false,
};

const getters = {

};

const mutations = {
  setIsBulk(state, data) {
    state.isBulk = data;
  },
  setIsSearch(state, data) {
    state.isSearch = data;
  },
  setIsDataSet(state, data) {
    state.isDataset = data;
  },
  setIsFieldGroup(state, data) {
    state.isFieldGroup = data;
  },
  setIsTableGroup(state, data) {
    state.isTableGroup = data;
  },
  setIsDataPreview(state, data) {
    state.isDataPreview = data;
  },
  setIsDataPreviewGroup(state, data) {
    state.setIsDataPreviewGroup = data;
  },
  setIsComponent(state, data) {
    state.isComponent = data;
  },
  setPropertiesOpened(state, data) {
    state.propertiesOpened = data;
  },
  setPropertiesGroup(state, data) {
    state.propertiesGroup = data;
  },
  setIsShowModalFieldFromGroup(state, data) {
    state.isShowModalFieldFromGroup = data;
  },
  setShowModalFieldLibrary(state, data) {
    state.showModalFieldLibrary = data;
  },
  setShowModalExternalApi(state, data) {
    state.showModalExternalApi = data;
  },
  setSourceExternalApi(state, data) {
    state.sourceExternalApi = data;
  },
  setTargetStackedProperties(state, data) {
    state.targetStackedProperties = data;
  },
  setSourceModalFieldLibrary(state, data) {
    state.sourceModalFieldLibrary = data;
  },
  setShowModalScriptBeforeAction(state, data) {
    state.showModalScriptBeforeAfter = data;
  },
  setSourceScriptBeforeAction(state, data) {
    state.sourceScriptBeforeAfter = data;
  },
  setShowModalScriptActionBtn(state, data) {
    state.showModalScriptActionBtn = data;
  },
  setSourceScriptActionBtn(state, data) {
    state.sourceScriptActionBtn = data;
  },
  setShowModalIconActionBtn(state, data) {
    state.showModalIconActionBtn = data;
  },
  setSourceIconActionBtn(state, data) {
    state.sourceIconActionBtn = data;
  },
  setConfigurationOpened(state, data) {
    state.configurationOpened = data;
  },
};

const actions = {
  toggleModalExternalApi({ commit }, { status, data }) {
    commit('setShowModalExternalApi', status);
    commit('setSourceExternalApi', data);
  },

  toggleModalScriptBeforeAfter({ commit }, { status, data }) {
    commit('setShowModalScriptBeforeAction', status);
    commit('setSourceScriptBeforeAction', data);
  },
  toggleModalScriptActionButton({ commit }, { status, data }) {
    commit('setShowModalScriptActionBtn', status);
    commit('setSourceScriptActionBtn', data);
  },
  toggleModalIconActionBtn({ commit }, { status, data }) {
    commit('setShowModalIconActionBtn', status);
    commit('setSourceIconActionBtn', data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
