/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import Vue from 'vue';
import router from '@/router';
import apiPosition from '../services/jApiPosition';
import apiUser from '../services/jApiUser';
import apiOvertimeConfig from '../services/overtime/jApiOvertimeConfig';

const state = {
  // Set default state(s) here
  position_detail: false,
  position_detail_sync: false,
  position_created: false,
  position_updated: false,
  position_deleted: false,

  divisions_detail: null,
  layer_detail: null,
  employee_detail: null,
  organigram_temp_detail: null,
  data_created_temp: null,
  list_location: null,
  email_id: false,
  related_rule: [],
  list_overtime_type: [],
  list_leave_type: [],
  list_rule: [],
  list_position: [],
  list_organigram_restriction: [],
  list_subordinate: [],
  list_assignee: [],
  detail_cost_center: null,
  detail_location: null,
  detail_tax_location: null,
};

const mutations = {
  // set detail position
  setDetailPosition(state, payload) {
    state.position_detail = payload.data;
  },

  mutateListLocation(state, payload) {
    state.list_location = payload.data;
  },

  setListOvertimeType(state, payload) {
    state.list_overtime_type = payload.data;
  },
  setListLeaveType(state, payload) {
    state.list_leave_type = payload.data;
  },
  setListRule(state, payload) {
    state.list_rule = payload.data;
  },

  // create position
  createPosition(state) {
    state.position_created = !state.position_created;
    router.go(-1);
  },

  // update position
  updatePosition(state) {
    state.position_updated = !state.position_updated;
    router.go(-1);
  },

  // position deleted
  deletePosition(state) {
    state.position_deleted = !state.position_deleted;
    router.go(-1);
  },

  // email id
  gotEmailID() {
    state.email_id = !state.email_id;
  },

  // ------------------------------------------------------------
  setDetailDivisions(state, payload) {
    state.divisions_detail = payload.data;
  },

  setDetailLayer(state, payload) {
    state.layer_detail = payload.data;
  },

  setDetailEmployee(state, payload) {
    state.employee_detail = payload.data;
  },

  setDetailOrganigramTemp(state, data) {
    state.organigram_temp_detail = data.data;
  },

  setResponseUpdateTemp(state, data) {
    state.data_created_temp = data.data;
  },

  setCoAttendanceMappingRule(state, data) {
    state.related_rule = data.data;
  },

  setListPosition(state, data) {
    state.list_position = data.data;
  },

  setListRestriction(state, data) {
    state.list_organigram_restriction = data;
  },

  setListSubordinate(state, data) {
    state.list_subordinate = data.childs || [];
  },

  setListAssignee(state, data) {
    state.list_assignee = data;
  },

  setDetailCostCenter(state, data) {
    state.detail_cost_center = data;
  },

  setDetailLocation(state, data) {
    state.detail_location = data;
  },

  setDetailTaxLocation(state, data) {
    state.detail_tax_location = data;
  },

};

const actions = {

  // get detail position
  async getDetailPosition({ dispatch, commit }, idPosition) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailPosition = await apiPosition.getDetailPosition(idPosition);
      commit('setDetailPosition', detailPosition);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create position
  async createPosition({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createPos = await apiPosition.createPosition(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createPos.message, { root: true });
      commit('createPosition');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // update position
  async updatePosition({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatePos = await apiPosition.updatePosition(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updatePos.message, { root: true });
      commit('updatePosition');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // delete position
  async deletePosition({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const removePos = await apiPosition.deletePosition(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', removePos.message, { root: true });
      commit('deletePosition');
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // import position
  async importPosition({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', '', { root: true });
    try {
      const res = await apiPosition.importPosition(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // -----------------------------------------------------------------------------
  // get list location
  async getListLocation({ dispatch }, { reqData, callback }) {
    try {
      const listLocation = await apiPosition.companyListLocation(reqData);
      const res = listLocation.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list parent
  async getListParent({ dispatch }, { reqData, callback }) {
    try {
      const listParent = await apiPosition.getListParent(reqData);
      const res = listParent.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.id} - ${lab.name} ${(lab.user && lab.user.profile) ? `- ${lab.user.profile.first_name} ${lab.user.profile.last_name}` : ''}`;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list Divisions
  async getListDivisions({ dispatch }, { reqData, callback }) {
    try {
      const listDivision = await apiPosition.getListDivisions(reqData);
      const res = listDivision.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create division
  async createDivision({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const newData = data;
      delete newData.route_id;
      const createDiv = await apiPosition.createDivision(newData);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createDiv.message, { root: true });
      commit('setDetailDivisions', createDiv);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list layer
  async getListLayer({ dispatch }, { reqData, callback }) {
    try {
      const listLayer = await apiPosition.getListLayer(reqData);
      const res = listLayer.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create layer
  async createLayer({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const newData = data;
      delete newData.route_id;
      const createLay = await apiPosition.createLayer(newData);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createLay.message, { root: true });
      commit('setDetailLayer', createLay);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list employee
  async getListEmployee({ dispatch }, { reqData, callback }) {
    try {
      const listEmployee = await apiPosition.getListEmployee(reqData);
      const res = listEmployee.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.id} - ${lab.name}`;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list employee temp
  async getListEmployeeTemp({ dispatch }, { reqData, callback }) {
    try {
      const listEmployee = await apiPosition.getListEmployeeTemp(reqData);
      if (listEmployee.data.length !== 0 && typeof listEmployee.data.id !== 'undefined') {
        const res = [listEmployee.data];
        res.forEach((lab, index) => {
          res[index].label = `${lab.profile.first_name} ${lab.profile.last_name} (${lab.email})`;
          res[index].name = `${lab.profile.first_name} ${lab.profile.last_name} (${lab.email})`;
        });
        callback(null, res);
      }
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, []);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get list rule
  async getListRule({ dispatch }, { reqData, callback }) {
    try {
      const listRule = await apiPosition.getListRule(reqData);
      const res = listRule.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // create employee
  async createEmployee({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const newData = data;
      delete newData.route_id;
      const createEmp = await apiPosition.createEmployee(newData);
      dispatch('jNotificatioenScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createEmp.message, { root: true });
      commit('setDetailEmployee', createEmp);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // get email id
  async getEmailId({ commit }, reqList) {
    try {
      const emailID = await apiUser.emailId(reqList);
      commit('gotEmailID');
      return emailID;
    } catch (err) {
      commit('gotEmailID');
      return null;
    }
  },

  async getDetailOrganigramTemp({ dispatch, commit }, idPosition) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailOrganigram = await apiPosition.getDetailOrganigramTemp(idPosition);
      commit('setDetailOrganigramTemp', detailOrganigram);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    }
  },

  async updateOrganigramTemp({ dispatch, commit }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updateOrganigramTemp = await apiPosition.createOrganigramTemp(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updateOrganigramTemp.message, { root: true });
      commit('setResponseUpdateTemp', updateOrganigramTemp);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async deleteOrganigramTemp({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const deleteOrganigramTemp = await apiPosition.deleteOrganigramTemp(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', deleteOrganigramTemp.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getListCompanyLocation({ dispatch, commit }, data) {
    try {
      const getListCompanyLocation = await apiPosition.companyListLocation(data);
      commit('mutateListLocation', getListCompanyLocation);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async getCoattendanceMappingRule({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getCoattendanceMappingRule(data);
      commit('setCoAttendanceMappingRule', res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createCoattendanceMappingRule({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.createCoattendanceMappingRule(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async updateCoattendanceMappingRule({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.updateCoattendanceMappingRule(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // MAPPING LEAVE, OVERTIME
  async actionCreateMappingCategory({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.createMappingCategory(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async actionUpdateMappingCategory({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.updateMappingCategory(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async actionDeleteMappingCategory({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.deleteMappingCategory(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },

  async actionGetListLeaveType({ dispatch }, { reqData, callback }) {
    try {
      const listLeaveType = await apiPosition.getListLeaveType(reqData);
      const res = listLeaveType.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetListOvertimeType({ dispatch }, { reqData, callback }) {
    try {
      const listOvertimeType = await apiPosition.getListOvertimeType(reqData);
      const res = listOvertimeType.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetListExpenseType({ dispatch }, { reqData, callback }) {
    try {
      const listExpenseType = await apiPosition.getListExpenseType(reqData);
      const res = listExpenseType.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionCreateMappingExpense({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.createMappingExpense(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },

  async actionDeleteMappingExpense({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.deleteMappingExpense(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },

  async actionGetListPosition({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getListPosition(data);
      commit('setListPosition', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGetListPositionAsync({ dispatch }, { reqData, callback }) {
    try {
      const listPosition = await apiPosition.getListPosition(reqData);
      const res = listPosition.data.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.id} - ${lab.name} ${(lab.user && lab.user.employee_id) ? `- ${lab.user.employee_id}` : ''}`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async actionGetListPositionFlow({ dispatch }, { reqData, callback }) {
    try {
      const listPosition = await apiPosition.getListPosition(reqData);
      const res = listPosition.data.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  async actionGetListFlow({ dispatch }, { reqData, callback }) {
    try {
      const listPosition = await apiPosition.getListFlow(reqData);
      const res = listPosition.data.data;
      res.forEach((lab, index) => {
        res[index].label = `${lab.id} - ${lab.name}`;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },

  // RESTRICTION
  async actionGetListOrganigramRestriction({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getListOrganigramRestriction(data);
      commit('setListRestriction', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateOrganigramRestriction({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.createOrganigramRestriction(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async actionDeleteOrganigramRestriction({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.deleteOrganigramRestriction(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },

  // SUBORDINATE
  async actionGetListSubordinate({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getListSubordinate(data);
      commit('setListSubordinate', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateSubordinate({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.createSubordinate(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async actionDeleteSubordinate({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPosition.deleteSubordinate(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },

  // ASSIGNEE
  async actionGetListAssignee({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getListAssignee(data);
      commit('setListAssignee', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // GENERAL
  async actionGetCostCenterByDivision({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getCostCenterByDivision(data);
      commit('setDetailCostCenter', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetTaxLocationByLocation({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getTaxLocationByLocation(data);
      commit('setDetailLocation', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.error, { root: true });
    }
  },
  async actionGetTaxLocation({ dispatch, commit }, data) {
    try {
      const res = await apiPosition.getTaxLocation(data);
      commit('setDetailTaxLocation', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionUpdatePosition({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatePos = await apiPosition.updatePosition(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', updatePos.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionGeListMappingRuleOvertime({ dispatch }, data) {
    try {
      const res = await apiOvertimeConfig.getListMappingRuleConfig(data);
      return res.data;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  async actionUpdateListMappingRuleOvertime({ dispatch }, data) {
    try {
      const res = await apiOvertimeConfig.updateListMappingRuleConfig(data);
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
