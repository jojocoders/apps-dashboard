/* eslint-disable no-shadow */
import jUserAPIManager from '@/services/payroll/jUserAPIManager';
import jConstant from '@/utils/constant';

const state = {
  responseAction: 0,
  counterLoadData: 0,
  errorMessage: '',
  successMessage: '',
  uiData: {
    selectedId: 0,
    payrollDetail: null,
  },
  dataForRequest: null,
  form: {
    user_id: 0,
    salary: '',
    salary_configuration: null,
    salary_type: null,
    overtime: false,
    payroll_loan: {
      product_loan: true,
      max_monthly_installment: 0,
    },
    note: '',
    tax: {
      id: 0,
      name: '',
      number: '',
      configuration: null,
      code: '',
      status: {
        id: null,
      },
    },
    bank: {
      name: null,
      account: '',
      account_holder: '',
    },
    allowances: [],
    benefits: [],
    deductions: [],
    bonus: [],
    overtimes: [],
  },
  selectedBenefit: {
    id: null,
    amount: '',
    is_deduct_from_salary: true,
  },
  selectedAllowance: {
    id: null,
    amount: '',
  },
  selectedDeduction: {
    id: null,
    amount: '',
  },
  selectedBonus: {
    id: null,
    amount: '',
  },
  selectedOvertime: {
    id: null,
    amount: '',
  },
  selectedBenefitItem: null,
  selectedAllowanceItem: null,
  selectedDeductionItem: null,
  selectedBonusItem: null,
  selectedOvertimeItem: null,
  selectedTax: null,
  benefitBpjsk: {
    id: 0,
    number: '',
    total_member: 0,
    type: 0,
    benefit_id: 0,
    payroll_benefit_id: 0,
  },
  benefitBpjskk: {
    id: 0,
    number: '',
    total_member: 0,
    type: 0,
    benefit_id: 0,
    payroll_benefit_id: 0,
  },
  benefitBpjskkJP: {
    id: 0,
    number: '',
    total_member: 0,
    type: 0,
    benefit_id: 0,
    payroll_benefit_id: 0,
  },
  defaultComponent: null,
  bankList: [],
  benefitList: [],
  selectedBenefitList: [],
  allowanceList: [],
  statusTaxList: [],
  selectedAllowanceList: [],
  deductionList: [],
  selectedDeductionList: [],
  bonusList: [],
  selectedBonusList: [],
  overtimeList: [],
  selectedOvertimeList: [],
  stateForLoadTaxList: 0,
  stateForLoadBankList: 0,
  stateForLoadBenefitList: 0,
  stateForLoadAllowanceList: 0,
  stateForLoadDeductionList: 0,
  stateForLoadBonusList: 0,
  stateForLoadOvertimeList: 0,
  stateForLoadDefaultPayrollComponent: 0,
  taxList: [],
  payrollId: -1,
  payrollBenefitBpjsk: 0,
  payrollBenefitBpjskk: 0,
  payrollBenefitBpjskkJP: 0,
};

const actions = {
  onResetResponseAction({ commit }) {
    commit('resetResponseAction');
  },
  requestReloadData({ commit }) {
    commit('resetFormData');
    jUserAPIManager.requestGetUserPayrollDetailByUserId(state.form.user_id)
      .then((response) => {
        commit('onRequestGetDataSuccess', response.data);
      })
      .catch((error) => {
        if (error.error && error.message) {
          commit('onRequestFailed', error.message);
        } else {
          commit('onRequestFailed', 'Something error, please try again later');
        }
      });
  },
  reloadTaxList({ commit }) {
    commit('updateStateForLoadTaxList', 1);
    jUserAPIManager.requestGetTaxList()
      .then((response) => {
        commit('updateStateForLoadTaxList', 0);
        commit('onRequestTaxListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadTaxList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadBankList({ commit }) {
    commit('updateStateForLoadBankList', 1);
    jUserAPIManager.requestGetBankList()
      .then((response) => {
        commit('updateStateForLoadBankList', 0);
        commit('onRequestBankListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadBankList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadBenefitList({ commit }) {
    commit('updateStateForLoadBenefitList', 1);
    jUserAPIManager.requestGetBenefitList()
      .then((response) => {
        commit('updateStateForLoadBenefitList', 0);
        commit('onRequestBenefitListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadBenefitList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadAllowanceList({ commit }) {
    commit('updateStateForLoadAllowanceList', 1);
    jUserAPIManager.requestGetAllowanceList()
      .then((response) => {
        commit('updateStateForLoadAllowanceList', 0);
        commit('onRequestAllowanceListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadAllowanceList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadDeductionList({ commit }) {
    commit('updateStateForLoadDeductionList', 1);
    jUserAPIManager.requestGetDeductionList()
      .then((response) => {
        commit('updateStateForLoadDeductionList', 0);
        commit('onRequestDeductionListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadDeductionList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadBonusList({ commit }) {
    commit('updateStateForLoadBonusList', 1);
    jUserAPIManager.requestGetBonusList()
      .then((response) => {
        commit('updateStateForLoadBonusList', 0);
        commit('onRequestBonusListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadBonusList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadOvertimeList({ commit }) {
    commit('updateStateForLoadOvertimeList', 1);
    jUserAPIManager.requestGetOvertime()
      .then((response) => {
        commit('updateStateForLoadOvertimeList', 0);
        commit('onRequestOvertimeListSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadOvertimeList', 99);
        commit('updateCounter', 1);
      });
  },
  reloadDefaultPayrollComponent({ commit }) {
    commit('updateStateForLoadDefaultPayrollComponent', 1);
    jUserAPIManager.requestGetDefaultPayrollComponent()
      .then((response) => {
        commit('updateStateForLoadDefaultPayrollComponent', 0);
        commit('onRequestDefaultPayrollComponentSuccess', response.data);
        commit('updateCounter', 1);
      })
      .catch(() => {
        commit('updateStateForLoadDefaultPayrollComponent', 99);
        commit('updateCounter', 1);
      });
  },
  requestUpdateData({ commit }) {
    commit('buildDataForm');
    jUserAPIManager.requestSaveUserPayrollDetail(state.dataForRequest)
      .then((response) => {
        commit('onRequestUpdateDataSuccess', response.data);
      })
      .catch((error) => {
        if (error.error && error.message) {
          commit('onRequestFailed', error.message);
        } else {
          commit('onRequestFailed', 'Something error, please try again later');
        }
      });
  },
  onShowBenefitDetail({ commit }, data) {
    commit('onChangeSelectedBenefit', data);
  },
  addBenefit({ commit }) {
    commit('onUpdateSelectedBenefitData');
    commit('onAddBenefit');
  },
  updateBenefit({ commit }) {
    commit('onUpdateSelectedBenefitData');
    commit('onUpdateBenefit');
  },
  onShowAllowanceDetail({ commit }, data) {
    commit('onChangeSelectedAllowance', data);
  },
  addAllowance({ commit }) {
    commit('onUpdateSelectedAllowanceData');
    commit('onAddAllowance');
  },
  updateAllowance({ commit }) {
    commit('onUpdateSelectedAllowanceData');
    commit('onUpdateAllowance');
  },
  onShowDeductionDetail({ commit }, data) {
    commit('onChangeSelectedDeduction', data);
  },
  addDeduction({ commit }) {
    commit('onUpdateSelectedDeductionData');
    commit('onAddDeduction');
  },
  updateDeduction({ commit }) {
    commit('onUpdateSelectedDeductionData');
    commit('onUpdateDeduction');
  },
  onShowBonusDetail({ commit }, data) {
    commit('onChangeSelectedBonus', data);
  },
  addBonus({ commit }) {
    commit('onUpdateSelectedBonusData');
    commit('onAddBonus');
  },
  updateBonus({ commit }) {
    commit('onUpdateSelectedBonusData');
    commit('onUpdateBonus');
  },
  onShowOvertimeDetail({ commit }, data) {
    commit('onChangeSelectedOvertime', data);
  },
  addOvertime({ commit }) {
    commit('onUpdateSelectedOvertimeData');
    commit('onAddOvertime');
  },
  updateOvertime({ commit }) {
    commit('onUpdateSelectedOvertimeData');
    commit('onUpdateOvertime');
  },
  resetData({ commit }) {
    commit('resetFormData');
  },
};

const mutations = {
  resetResponseAction() {
    state.responseAction = 0;
  },
  resetFormData() {
    state.form.user_id = state.uiData.selectedId;
    state.payrollId = 0;
    state.form.salary = '';
    state.form.salary_configuration = null;
    state.form.salary_type = null;
    state.form.overtime = false;
    state.form.payroll_loan.product_loan = true;
    state.form.payroll_loan.max_monthly_installment = 0;
    state.form.note = '';
    state.form.tax = {
      id: 0,
      name: '',
      number: '',
      configuration: null,
      status: {
        id: null,
      },
    };
    state.form.bank = {
      name: null,
      account: '',
      account_holder: '',
    };
    state.form.allowances = [];
    state.form.deductions = [];
    state.form.bonus = [];
    state.form.overtimes = [];
    state.form.benefits = [];
    state.selectedBenefitList = [];
    state.selectedAllowanceList = [];
    state.selectedDeductionList = [];
    state.selectedOvertimeList = [];
    state.benefitBpjsk.number = '';
    state.benefitBpjsk.total_member = 0;
    state.benefitBpjskk.type = 1;
    state.benefitBpjskk.number = '';
    state.benefitBpjskk.total_member = 0;
    state.benefitBpjskk.type = 0;

    state.selectedAllowance.id = null;
    state.selectedAllowance.amount = '';

    state.selectedBenefit.id = null;
    state.selectedBenefit.amount = '';

    state.selectedDeduction.id = null;
    state.selectedDeduction.amount = '';
    state.selectedBonus.id = null;
    state.selectedBonus.amount = '';
    state.selectedOvertime.id = null;
  },
  buildDataForm() {
    // Build Json From
    state.dataForRequest = {};
    state.dataForRequest.user_id = state.form.user_id;
    state.dataForRequest.salary = state.form.salary;
    state.dataForRequest.salary_configuration = state.form.salary_configuration;
    state.dataForRequest.salary_type = state.form.salary_type;
    state.dataForRequest.overtime = state.form.overtime;
    state.dataForRequest.tax = state.form.tax;
    state.dataForRequest.bank = state.form.bank;
    state.dataForRequest.note = state.form.note;
    state.dataForRequest.payroll_loan = {
      product_loan: state.form.payroll_loan.product_loan,
      max_monthly_installment: state.form.payroll_loan.max_monthly_installment,
    };
    state.dataForRequest.allowances = [];
    state.selectedAllowanceList.forEach((allowanceItem) => {
      const selectedAllowanceItem = allowanceItem;
      selectedAllowanceItem.model.is_delete = allowanceItem.is_delete;
      state.dataForRequest.allowances.push(selectedAllowanceItem.model);
    });

    state.dataForRequest.deductions = [];
    state.selectedDeductionList.forEach((deductionItem) => {
      const selectedDeductionItem = deductionItem;
      selectedDeductionItem.model.is_delete = deductionItem.is_delete;
      state.dataForRequest.deductions.push(selectedDeductionItem.model);
    });

    state.dataForRequest.bonus = [];
    state.selectedBonusList.forEach((bonusItem) => {
      const selectedBonusItem = bonusItem;
      selectedBonusItem.model.is_delete = bonusItem.is_delete;
      state.dataForRequest.bonus.push(bonusItem.model);
    });


    state.dataForRequest.overtimes = [];
    state.selectedOvertimeList.forEach((overtimeItem) => {
      const selectedOvertimeItem = overtimeItem;
      selectedOvertimeItem.model.is_delete = overtimeItem.is_delete;
      state.dataForRequest.overtimes.push(overtimeItem.model);
    });

    state.dataForRequest.benefits = [];
    state.selectedBenefitList.forEach((benefitItem) => {
      if (benefitItem.model.code !== jConstant.AllowanceCodeBPJSKesehatan &&
        benefitItem.model.code !== jConstant.AllowanceCodeBPJSKetenagakerjaan) {
        const selectedBenefitItem = benefitItem;
        selectedBenefitItem.model.is_delete = benefitItem.is_delete;
        selectedBenefitItem.model.id = benefitItem.model.benefit_id;
        state.dataForRequest.benefits.push(selectedBenefitItem.model);
      } else {
        let selectedBenefitItem;
        if (benefitItem.model.code === jConstant.AllowanceCodeBPJSKesehatan) {
          selectedBenefitItem = state.benefitBpjsk;
        } else {
          selectedBenefitItem = state.benefitBpjskk;
        }
        selectedBenefitItem.id = benefitItem.model.benefit_id;
        selectedBenefitItem.code = benefitItem.model.code;
        selectedBenefitItem.is_deduct_from_salary = benefitItem.model.is_deduct_from_salary;
        state.dataForRequest.benefits.push(selectedBenefitItem);
      }
    });
  },
  onRequestGetDataSuccess(mState, data) {
    state.payrollId = data.user_payroll.id;
    data.user_payroll.benefits.forEach((benefit) => {
      if (benefit.code === 'bpjskk') {
        state.payrollBenefitBpjskk = benefit.payroll_benefit_id;
      } else if (benefit.code === 'bpjsk') {
        state.payrollBenefitBpjsk = benefit.payroll_benefit_id;
      } else if (benefit.code === 'bpjskk-jp') {
        state.payrollBenefitBpjskkJP = benefit.payroll_benefit_id;
      }
    });

    state.responseAction = 1;
    if (data.user_payroll.salary !== null && data.user_payroll.salary !== '') {
      if ((data.user_payroll.salary_configuration === 0 ||
        data.user_payroll.salary_configuration === null) &&
        data.user_payroll.salary === 0 &&
        (data.user_payroll.salary_type === 0 || data.user_payroll.salary_type === null)) {
        // Data Bank
        state.form = data.user_payroll;
        // User Default Company Config
        state.selectedAllowanceList = [];
        const numberFormatter = new Intl.NumberFormat();
        state.defaultComponent.allowances.forEach((allowance) => {
          state.selectedAllowanceList.push({
            is_delete: false,
            id: allowance.allowance_id,
            model: {
              id: allowance.allowance_id,
              code: allowance.code,
              description: allowance.description,
              name: allowance.name,
              amount: allowance.amount,
              payment_method: jConstant.AllowancePaymentMethodPersonal,
            },
            type: allowance.name,
            name: numberFormatter.format(allowance.amount),
          });
        });

        state.selectedBenefitList = [];
        state.defaultComponent.benefits.forEach((benefit) => {
          state.selectedBenefitList.push({
            is_delete: false,
            id: benefit.config_id,
            model: {
              id: benefit.id,
              code: benefit.code,
              description: benefit.description,
              name: benefit.name,
              amount: benefit.amount,
              payment_method: jConstant.BenefitPaymentMethodPersonal,
              config_id: benefit.config_id,
              benefit_id: benefit.benefit_id,
              payroll_benefit_id: benefit.payroll_benefit_id,
            },
            type: benefit.name,
            name: benefit.code === jConstant.AllowanceCodeBPJSKesehatan ||
              benefit.code === jConstant.AllowanceCodeBPJSKetenagakerjaan ?
              '' : numberFormatter.format(state.selectedBenefit.amount),
            show_delete_button: !(benefit.code === jConstant.AllowanceCodeBPJSKesehatan ||
              benefit.code === jConstant.AllowanceCodeBPJSKetenagakerjaan),
          });
        });

        state.selectedDeductionList = [];
        state.defaultComponent.deductions.forEach((deduction) => {
          state.selectedDeductionList.push({
            is_delete: false,
            id: deduction.deduction_id,
            model: {
              id: deduction.deduction_id,
              description: deduction.description,
              name: deduction.name,
              amount: deduction.amount,
            },
            type: deduction.name,
            name: numberFormatter.format(deduction.amount),
          });
        });

        state.selectedBonusList = [];
        state.defaultComponent.bonus.forEach((bonus) => {
          state.selectedBonusList.push({
            is_delete: false,
            id: bonus.bonus_id,
            model: {
              id: bonus.bonus_id,
              description: bonus.description,
              name: bonus.name,
              amount: bonus.amount,
            },
            type: bonus.name,
            name: numberFormatter.format(bonus.amount),
          });
        });

        state.selectedOvertimeList = [];
        state.defaultComponent.overtimes.forEach((overtime) => {
          state.selectedOvertimeList.push({
            is_delete: false,
            id: overtime.overtime_id,
            model: {
              id: overtime.overtime_id,
              description: overtime.description,
              name: overtime.name,
            },
            type: overtime.name,
            name: '',
          });
        });
      } else {
        // User Which have configured
        state.form = data.user_payroll;

        if (typeof state.form.note !== 'undefined') {
          state.form.note = state.form.note.note;
        }

        const numberFormatter = new Intl.NumberFormat();
        state.selectedBenefitList = [];
        state.form.benefits.forEach((benefit) => {
          const selectedBenefit = benefit;
          if (selectedBenefit.type === 0) {
            selectedBenefit.type = null;
          }

          if (selectedBenefit.code === 'bpjskk') {
            state.benefitBpjskk.number = selectedBenefit.number;
            state.benefitBpjskk.total_member = selectedBenefit.total_member;
            state.benefitBpjskk.payment_method = selectedBenefit.payment_method;
            state.benefitBpjskk.type = selectedBenefit.type;
          } else if (selectedBenefit.code === 'bpjsk') {
            state.benefitBpjsk.number = selectedBenefit.number;
            state.benefitBpjsk.total_member = selectedBenefit.total_member;
            state.benefitBpjsk.payment_method = selectedBenefit.payment_method;
            state.benefitBpjsk.type = selectedBenefit.type;
          } else if (selectedBenefit.code === 'bpjskk-jp') {
            state.benefitBpjskkJP.number = selectedBenefit.number;
            state.benefitBpjskkJP.total_member = selectedBenefit.total_member;
            state.benefitBpjskkJP.payment_method = selectedBenefit.payment_method;
            state.benefitBpjskkJP.type = selectedBenefit.type;
          }

          state.selectedBenefitList.push({
            is_delete: false,
            id: benefit.id,
            benefit_id: benefit.benefit_id,
            payroll_benefit_id: benefit.payroll_benefit_id,
            model: benefit,
            type: benefit.name,
            config_id: benefit.config_id,
            name: benefit.code === jConstant.AllowanceCodeBPJSKesehatan ||
              benefit.code === jConstant.AllowanceCodeBPJSKetenagakerjaan ?
              '' : numberFormatter.format(selectedBenefit.amount),

            show_delete_button: !(benefit.code === jConstant.AllowanceCodeBPJSKesehatan ||
              benefit.code === jConstant.AllowanceCodeBPJSKetenagakerjaan),
          });
        });

        state.selectedAllowanceList = [];
        state.form.allowances.forEach((allowance) => {
          state.selectedAllowanceList.push({
            is_delete: false,
            id: allowance.id,
            model: allowance,
            type: allowance.name,
            name: numberFormatter.format(allowance.amount),
          });
        });

        state.selectedDeductionList = [];
        state.form.deductions.forEach((deduction) => {
          state.selectedDeductionList.push({
            is_delete: false,
            id: deduction.id,
            model: deduction,
            type: deduction.name,
            name: numberFormatter.format(deduction.amount),
          });
        });
        state.selectedBonusList = [];
        state.form.bonus.forEach((bonus) => {
          state.selectedBonusList.push({
            is_delete: false,
            id: bonus.id,
            model: bonus,
            type: bonus.name,
            name: numberFormatter.format(bonus.amount),
          });
        });
        state.selectedOvertimeList = [];
        state.form.overtimes.forEach((overtime) => {
          state.selectedOvertimeList.push({
            is_delete: false,
            id: overtime.id,
            model: overtime,
            type: overtime.name,
            name: '',
          });
        });
      }

      if (state.form.tax == null ||
        typeof state.form.tax === 'undefined') {
        state.form.tax = {
          id: null,
          name: '',
          number: '',
          configuration: null,
          status: {
            id: null,
          },
          code: 'pph21',
        };
      } else if (state.form.tax.status == null) {
        state.form.tax.status = {
          id: null,
        };
      }

      if (typeof state.form.bank === 'undefined') {
        state.form.bank = {
          name: null,
          account: '',
          account_holder: '',
        };
      }

      state.form.user_id = state.uiData.selectedId;

      if (state.form.tax.configuration === '0' || state.form.tax.configuration === '') {
        state.form.tax.configuration = null;
      }
    }
  },
  onRequestFailed(mState, errorMessage) {
    state.responseAction = 99;
    state.errorMessage = errorMessage;
  },
  onChangeSelectedBenefit(mState, data) {
    state.selectedBenefitItem = data;
    state.selectedBenefit = data.model;
    state.benefitList.forEach((element) => {
      if (state.selectedBenefit.benefit_id === element.benefit_id &&
        state.selectedBenefit.config_id === element.config_id) {
        state.selectedBenefit.id = element.id;
      }
    });
  },
  onChangeSelectedAllowance(mState, data) {
    state.selectedAllowanceItem = data;
    state.selectedAllowance = data.model;
  },
  onChangeSelectedDeduction(mState, data) {
    state.selectedDeductionItem = data;
    state.selectedDeduction = data.model;
  },
  onChangeSelectedBonus(mState, data) {
    state.selectedBonusItem = data;
    state.selectedBonus = data.model;
  },
  onChangeSelectedOvertime(mState, data) {
    state.selectedOvertimeItem = data;
    state.selectedOvertime = data.model;
  },
  onRequestTaxListSuccess(mState, data) {
    state.selectedTax = null;
    state.statusTaxList = [];
    state.taxList = [];
    if (data.taxes.length > 0) {
      data.taxes.forEach((tax) => {
        const taxObject = {
          id: tax.id,
          name: tax.name,
          text: tax.name,
          value: tax.id,
          code: tax.code,
        };
        state.taxList.push(taxObject);
        if (tax.code === 'pph21') {
          state.statusTaxList = [];
          tax.statues.forEach((taxStatus) => {
            state.statusTaxList.push({
              id: taxStatus.id,
              name: taxStatus.name,
              value: taxStatus.id,
              text: taxStatus.name,
            });
          });
        }
      });
    }
  },
  onRequestBankListSuccess(mState, data) {
    state.bankList = [];
    data.data.forEach((bank) => {
      state.bankList.push({
        value: bank.name,
        text: bank.name,
      });
    });
  },
  onRequestBenefitListSuccess(mState, data) {
    state.benefitList = [];
    data.benefits.forEach((benefit) => {
      if (benefit.code === 'bpjskk') {
        state.benefitBpjskk.id = benefit.id;
        state.benefitBpjskk.benefit_id = benefit.benefit_id;
        state.benefitBpjskk.payroll_benefit_id = benefit.payroll_benefit_id;
      } else if (benefit.code === 'bpjsk') {
        state.benefitBpjsk.id = benefit.id;
        state.benefitBpjsk.benefit_id = benefit.benefit_id;
        state.benefitBpjsk.payroll_benefit_id = benefit.payroll_benefit_id;
      } else if (benefit.code === 'bpjskk-jp') {
        state.benefitBpjskkJP.id = benefit.id;
        state.benefitBpjskkJP.benefit_id = benefit.benefit_id;
        state.benefitBpjskkJP.payroll_benefit_id = benefit.payroll_benefit_id;
      }
      state.benefitList.push({
        id: benefit.id,
        benefit_id: benefit.benefit_id,
        payroll_benefit_id: benefit.payroll_benefit_id,
        value: benefit.id,
        text: benefit.name,
        name: benefit.name,
        code: benefit.code,
        description: benefit.description,
        config_id: benefit.config_id,
      });
    });
  },
  onRequestAllowanceListSuccess(mState, data) {
    state.allowanceList = [];
    data.allowances.forEach((allowance) => {
      state.allowanceList.push({
        id: allowance.id,
        value: allowance.id,
        text: allowance.name,
        name: allowance.name,
        code: allowance.code,
        payment_method: jConstant.AllowancePaymentMethodPersonal,
        description: allowance.description,
      });
    });
  },
  onRequestDeductionListSuccess(mState, data) {
    state.deductionList = [];
    data.deductions.forEach((deduction) => {
      state.deductionList.push({
        id: deduction.id,
        value: deduction.id,
        text: deduction.name,
        name: deduction.name,
        description: deduction.description,
      });
    });
  },
  onRequestBonusListSuccess(mState, data) {
    state.bonusList = [];
    data.bonus.forEach((bonus) => {
      state.bonusList.push({
        id: bonus.id,
        value: bonus.id,
        text: bonus.name,
        name: bonus.name,
        description: bonus.description,
      });
    });
  },
  onRequestOvertimeListSuccess(mState, data) {
    state.overtimeList = [];
    data.overtimes.forEach((overtime) => {
      state.overtimeList.push({
        id: overtime.id,
        value: overtime.id,
        text: overtime.name,
        name: overtime.name,
        description: overtime.description,
      });
    });
  },
  onRequestDefaultPayrollComponentSuccess(mState, data) {
    state.defaultComponent = data.component;
    data.component.benefits.forEach((benefit) => {
      if (benefit.code === 'bpjskk') {
        state.benefitBpjskk.payment_method = benefit.payment_method;
      } else if (benefit.code === 'bpjsk') {
        state.benefitBpjsk.payment_method = benefit.payment_method;
      } else if (benefit.code === 'bpjskk-jp') {
        state.benefitBpjskkJP.payment_method = benefit.payment_method;
      }
    });
  },
  updateStateForLoadTaxList(mState, value) {
    state.stateForLoadTaxList = value;
  },
  updateCounter(mState, value) {
    state.counterLoadData += value;
  },
  updateStateForLoadBankList(mState, value) {
    state.stateForLoadBankList = value;
  },
  updateStateForLoadBenefitList(mState, value) {
    state.stateForLoadBenefitList = value;
  },
  updateStateForLoadAllowanceList(mState, value) {
    state.stateForLoadAllowanceList = value;
  },
  updateStateForLoadDeductionList(mState, value) {
    state.stateForLoadDeductionList = value;
  },
  updateStateForLoadBonusList(mState, value) {
    state.stateForLoadBonusList = value;
  },
  updateStateForLoadOvertimeList(mState, value) {
    state.stateForLoadOvertimeList = value;
  },
  updateStateForLoadDefaultPayrollComponent(mState, value) {
    state.stateForLoadDefaultPayrollComponent = value;
  },
  onRequestUpdateDataSuccess() {
    state.responseAction = 2;
    state.successMessage = 'Success Update Payroll Data';
  },
  onUpdateSelectedBenefitData() {
    state.benefitList.forEach((benefit) => {
      if (benefit.id === state.selectedBenefit.id) {
        state.selectedBenefit.benefit_id = benefit.benefit_id;
        state.selectedBenefit.payroll_benefit_id = benefit.payroll_benefit_id;
        state.selectedBenefit.name = benefit.name;
        state.selectedBenefit.config_id = benefit.config_id;
        state.selectedBenefit.code = benefit.code;
      }
    });
  },
  onUpdateSelectedAllowanceData() {
    state.allowanceList.forEach((allowance) => {
      if (allowance.id === state.selectedAllowance.id) {
        state.selectedAllowance.id = allowance.id;
        state.selectedAllowance.name = allowance.name;
      }
    });
  },
  onUpdateSelectedDeductionData() {
    state.deductionList.forEach((deduction) => {
      if (deduction.id === state.selectedDeduction.id) {
        state.selectedDeduction.id = deduction.id;
        state.selectedDeduction.name = deduction.name;
      }
    });
  },
  onUpdateSelectedBonusData() {
    state.bonusList.forEach((bonus) => {
      if (bonus.id === state.selectedBonus.id) {
        state.selectedBonus.id = bonus.id;
        state.selectedBonus.name = bonus.name;
      }
    });
  },
  onUpdateSelectedOvertimeData() {
    state.overtimeList.forEach((overtime) => {
      if (overtime.id === state.selectedOvertime.id) {
        state.selectedOvertime.id = overtime.id;
        state.selectedOvertime.name = overtime.name;
      }
    });
  },
  validateBenefit() {
    if (state.selectedBenefit.id === 0) {
      state.errorMessage = 'Please select your benefit type';
      state.responseAction = 99;
      return false;
    }

    if (state.selectedBenefit.amount === '' &&
      state.selectedBenefit.code !== jConstant.AllowanceCodeBPJSKesehatan &&
      state.selectedBenefit.code !== jConstant.AllowanceCodeBPJSKetenagakerjaan) {
      state.errorMessage = 'Please Type your benefit amount';
      state.responseAction = 99;
      return false;
    }
    return true;
  },
  validateAllowance() {
    if (state.selectedAllowance.id === 0) {
      state.errorMessage = 'Please select your allowance type';
      state.responseAction = 99;
      return false;
    }

    if (state.selectedAllowance.amount === '') {
      state.errorMessage = 'Please Type your allowance amount';
      state.responseAction = 99;
      return false;
    }
    return true;
  },
  validateDeduction() {
    if (state.selectedDeduction.id === 0) {
      state.errorMessage = 'Please select your deduction type';
      state.responseAction = 99;
      return false;
    }

    if (state.selectedDeduction.amount === '') {
      state.errorMessage = 'Please Type your deduction amount';
      state.responseAction = 99;
      return false;
    }
    return true;
  },
  validateBonus() {
    if (state.selectedBonus.id === 0) {
      state.errorMessage = 'Please select your bonus type';
      state.responseAction = 99;
      return false;
    }

    // if (state.selectedBonus.source === jConstant.BonusSourceManualInput &&
    //   state.selectedBonus.amount === '') {
    //   state.errorMessage = 'Please input your bonus amount';
    //   state.responseAction = 99;
    //   return false;
    // }

    return true;
  },
  validateOvertime() {
    if (state.selectedOvertime.id === 0) {
      state.errorMessage = 'Please select your overtime type';
      state.responseAction = 99;
      return false;
    }

    return true;
  },
  onAddBenefit() {
    if (!mutations.validateBenefit()) {
      return;
    }

    mutations.onUpdateSelectedBenefitData();
    let selectedData;
    const numberFormatter = new Intl.NumberFormat();
    state.selectedBenefitList.forEach((benefitItem) => {
      if (benefitItem.model.config_id === state.selectedBenefit.config_id &&
        benefitItem.model.benefit_id === state.selectedBenefit.benefit_id) {
        selectedData = benefitItem;
        selectedData.id = state.selectedBenefit.id;
        selectedData.model = state.selectedBenefit;
        selectedData.type = state.selectedBenefit.name;
        selectedData.name = state.selectedBenefit.code === jConstant.AllowanceCodeBPJSKesehatan ||
          state.selectedBenefit.code === jConstant.AllowanceCodeBPJSKetenagakerjaan ?
          '' : numberFormatter.format(state.selectedBenefit.amount);
        selectedData.is_delete = false;
      }
    });
    if (typeof selectedData === 'undefined') {
      selectedData = {
        is_delete: false,
        id: state.selectedBenefit.id,
        model: state.selectedBenefit,
        type: state.selectedBenefit.name,
        name: state.selectedBenefit.code === jConstant.AllowanceCodeBPJSKesehatan ||
          state.selectedBenefit.code === jConstant.AllowanceCodeBPJSKetenagakerjaan ?
          '' : numberFormatter.format(state.selectedBenefit.amount),
      };
      state.selectedBenefitList.push(selectedData);
    }
    mutations.onResetSelectedBenefit();
  },
  onUpdateBenefit() {
    if (!mutations.validateBenefit()) {
      return;
    }
    mutations.onUpdateSelectedBenefitData();
    const numberFormatter = new Intl.NumberFormat();
    let isAdded = false;
    if (state.selectedBenefitItem.id !== state.selectedBenefit.id) {
      state.selectedBenefitList.forEach((benefitItem) => {
        if (benefitItem.model.config_id === state.selectedBenefit.config_id &&
          benefitItem.model.benefit_id === state.selectedBenefit.benefit_id) {
          const selectedItem = benefitItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedBenefit;
          selectedItem.id = state.selectedBenefit.id;
          selectedItem.type = state.selectedBenefit.name;
          selectedItem.name = benefitItem.model.code === jConstant.AllowanceCodeBPJSKesehatan ||
            benefitItem.model.code === jConstant.AllowanceCodeBPJSKetenagakerjaan ?
            '' : numberFormatter.format(state.selectedBenefit.amount);
          isAdded = true;
        }
      });
    }

    if (isAdded === false) {
      state.selectedBenefitList.forEach((benefitItem) => {
        const selectedItem = benefitItem;
        if (benefitItem.id === state.selectedBenefitItem.id &&
          benefitItem.model.code === jConstant.AllowanceCodeBPJSKesehatan &&
          benefitItem.model.code === jConstant.AllowanceCodeBPJSKetenagakerjaan) {
          selectedItem.is_delete = false;
          selectedItem.model = benefitItem;
          selectedItem.id = state.selectedBenefit.id;
        }
        if (benefitItem.id === state.selectedBenefitItem.id) {
          selectedItem.name = benefitItem.model.code === jConstant.AllowanceCodeBPJSKesehatan ||
            benefitItem.model.code === jConstant.AllowanceCodeBPJSKetenagakerjaan ?
            '' : numberFormatter.format(state.selectedBenefit.amount);
        }
      });
    }
    mutations.onResetSelectedBenefit();
  },
  onResetSelectedBenefit() {
    state.selectedBenefitItem = null;
    state.selectedBenefit = {
      id: null,
      amount: '',
      is_deduct_from_salary: true,
    };
  },
  onAddAllowance() {
    if (!mutations.validateAllowance()) {
      return;
    }

    mutations.onUpdateSelectedAllowanceData();
    let selectedData;
    const numberFormatter = new Intl.NumberFormat();
    state.selectedAllowanceList.forEach((allowanceItem) => {
      if (allowanceItem.id === state.selectedAllowance.id) {
        selectedData = allowanceItem;
        selectedData.id = state.selectedAllowance.id;
        selectedData.model = state.selectedAllowance;
        selectedData.type = state.selectedAllowance.name;
        selectedData.name = numberFormatter.format(state.selectedAllowance.amount);
        selectedData.is_delete = false;
      }
    });

    if (selectedData == null) {
      selectedData = {
        is_delete: false,
        id: state.selectedAllowance.id,
        model: state.selectedAllowance,
        type: state.selectedAllowance.name,
        name: numberFormatter.format(state.selectedAllowance.amount),
      };
      state.selectedAllowanceList.push(selectedData);
    }
    mutations.onResetSelectedAllowance();
  },
  onUpdateAllowance() {
    if (!mutations.validateAllowance()) {
      return;
    }
    mutations.onUpdateSelectedAllowanceData();

    const numberFormatter = new Intl.NumberFormat();
    let isAdded = false;
    if (state.selectedAllowanceItem.id !== state.selectedAllowance.id) {
      state.selectedAllowanceList.forEach((allowanceItem) => {
        if (allowanceItem.id === state.selectedAllowance.id) {
          const selectedItem = allowanceItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedAllowance;
          selectedItem.id = state.selectedAllowance.id;
          selectedItem.type = state.selectedAllowance.name;
          selectedItem.name = numberFormatter.format(state.selectedAllowance.amount);
          isAdded = true;
        }
      });
    }

    if (isAdded === false) {
      state.selectedAllowanceList.forEach((allowanceItem) => {
        if (allowanceItem.id === state.selectedAllowanceItem.id) {
          const selectedItem = allowanceItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedAllowance;
          selectedItem.id = state.selectedAllowance.id;
          selectedItem.type = state.selectedAllowance.name;
          selectedItem.name = numberFormatter.format(state.selectedAllowance.amount);
        }
      });
    }
    mutations.onResetSelectedAllowance();
  },
  onResetSelectedAllowance() {
    state.selectedAllowanceItem = null;
    state.selectedAllowance = {
      id: null,
      amount: '',
    };
  },
  onAddDeduction() {
    if (!mutations.validateDeduction()) {
      return;
    }

    mutations.onUpdateSelectedDeductionData();
    let selectedData;
    const numberFormatter = new Intl.NumberFormat();
    state.selectedDeductionList.forEach((deductionItem) => {
      if (deductionItem.id === state.selectedDeduction.id) {
        selectedData = deductionItem;
        selectedData.id = state.selectedDeduction.id;
        selectedData.model = state.selectedDeduction;
        selectedData.type = state.selectedDeduction.name;
        selectedData.name = numberFormatter.format(state.selectedDeduction.amount);
        selectedData.is_delete = false;
      }
    });

    if (selectedData == null) {
      selectedData = {
        is_delete: false,
        id: state.selectedDeduction.id,
        model: state.selectedDeduction,
        type: state.selectedDeduction.name,
        name: numberFormatter.format(state.selectedDeduction.amount),
      };
      state.selectedDeductionList.push(selectedData);
    }
    mutations.onResetSelectedDeduction();
  },
  onUpdateDeduction() {
    if (!mutations.validateDeduction()) {
      return;
    }
    mutations.onUpdateSelectedDeductionData();

    const numberFormatter = new Intl.NumberFormat();
    let isAdded = false;
    if (state.selectedDeductionItem.id !== state.selectedDeduction.id) {
      state.selectedDeductionList.forEach((deductionItem) => {
        if (deductionItem.id === state.selectedDeduction.id) {
          const selectedItem = deductionItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedDeduction;
          selectedItem.id = state.selectedDeduction.id;
          selectedItem.type = state.selectedDeduction.name;
          selectedItem.name = numberFormatter.format(state.selectedDeduction.amount);
          isAdded = true;
        }
      });
    }

    if (isAdded === false) {
      state.selectedDeductionList.forEach((deductionItem) => {
        if (deductionItem.id === state.selectedDeductionItem.id) {
          const selectedItem = deductionItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedDeduction;
          selectedItem.id = state.selectedDeduction.id;
          selectedItem.type = state.selectedDeduction.name;
          selectedItem.name = numberFormatter.format(state.selectedDeduction.amount);
        }
      });
    }
    mutations.onResetSelectedDeduction();
  },
  onResetSelectedDeduction() {
    state.selectedDeductionItem = null;
    state.selectedDeduction = {
      id: null,
      amount: '',
    };
  },
  onAddBonus() {
    if (!mutations.validateBonus()) {
      return;
    }

    mutations.onUpdateSelectedBonusData();
    let selectedData;
    const numberFormatter = new Intl.NumberFormat();
    state.selectedBonusList.forEach((bonusItem) => {
      if (bonusItem.id === state.selectedBonus.id) {
        selectedData = bonusItem;
        selectedData.id = state.selectedBonus.id;
        selectedData.model = state.selectedBonus;
        selectedData.type = state.selectedBonus.name;
        selectedData.name = numberFormatter.format(state.selectedBonus.amount);
        selectedData.is_delete = false;
      }
    });

    if (selectedData == null) {
      selectedData = {
        is_delete: false,
        id: state.selectedBonus.id,
        model: state.selectedBonus,
        type: state.selectedBonus.name,
        name: numberFormatter.format(state.selectedBonus.amount),
      };
      state.selectedBonusList.push(selectedData);
    }
    mutations.onResetSelectedBonus();
  },
  onUpdateBonus() {
    if (!mutations.validateBonus()) {
      return;
    }
    mutations.onUpdateSelectedBonusData();

    const numberFormatter = new Intl.NumberFormat();
    let isAdded = false;
    if (state.selectedBonusItem.id !== state.selectedBonus.id) {
      state.selectedBonusList.forEach((bonusItem) => {
        if (bonusItem.id === state.selectedBonus.id) {
          const selectedItem = bonusItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedBonus;
          selectedItem.id = state.selectedBonus.id;
          selectedItem.type = state.selectedBonus.name;
          selectedItem.name = numberFormatter.format(state.selectedBonus.amount);
          isAdded = true;
        }
      });
    }

    if (isAdded === false) {
      state.selectedBonusList.forEach((bonusItem) => {
        if (bonusItem.id === state.selectedBonusItem.id) {
          const selectedItem = bonusItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedBonus;
          selectedItem.id = state.selectedBonus.id;
          selectedItem.type = state.selectedBonus.name;
          selectedItem.name = numberFormatter.format(state.selectedBonus.amount);
        }
      });
    }
    mutations.onResetSelectedBonus();
  },
  onResetSelectedBonus() {
    state.selectedBonusItem = null;
    state.selectedBonus = {
      id: null,
      amount: '',
    };
  },
  onAddOvertime() {
    if (!mutations.validateOvertime()) {
      return;
    }

    mutations.onUpdateSelectedOvertimeData();
    let selectedData;
    state.selectedOvertimeList.forEach((overtimeItem) => {
      if (overtimeItem.id === state.selectedOvertime.id) {
        selectedData = overtimeItem;
        selectedData.id = state.selectedOvertime.id;
        selectedData.model = state.selectedOvertime;
        selectedData.type = state.selectedOvertime.name;
        selectedData.is_delete = false;
      }
    });

    if (selectedData == null) {
      selectedData = {
        is_delete: false,
        id: state.selectedOvertime.id,
        model: state.selectedOvertime,
        type: state.selectedOvertime.name,
        name: '',
      };
      state.selectedOvertimeList.push(selectedData);
    }
    mutations.onResetSelectedOvertime();
  },
  onUpdateOvertime() {
    if (!mutations.validateOvertime()) {
      return;
    }
    mutations.onUpdateSelectedOvertimeData();

    let isAdded = false;
    if (state.selectedOvertimeItem.id !== state.selectedOvertime.id) {
      state.selectedOvertimeList.forEach((overtimeItem) => {
        if (overtimeItem.id === state.selectedOvertime.id) {
          const selectedItem = overtimeItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedOvertime;
          selectedItem.id = state.selectedOvertime.id;
          selectedItem.type = state.selectedOvertime.name;
          selectedItem.name = '';
          isAdded = true;
        }
      });
    }

    if (isAdded === false) {
      state.selectedOvertimeList.forEach((overtimeItem) => {
        if (overtimeItem.id === state.selectedOvertimeItem.id) {
          const selectedItem = overtimeItem;
          selectedItem.is_delete = false;
          selectedItem.model = state.selectedOvertime;
          selectedItem.id = state.selectedOvertime.id;
          selectedItem.type = state.selectedOvertime.name;
          selectedItem.name = '';
        }
      });
    }
    mutations.onResetSelectedOvertime();
  },
  onResetSelectedOvertime() {
    state.selectedOvertimeItem = null;
    state.selectedOvertime = {
      id: null,
      amount: '',
    };
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
