/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiPayrollArea from '../services/jApiPayrollArea';

const state = {
  // Set default state(s) here
  list_company_location: null,
  listTaxLocation: null,
  loading: true,
  list_payroll_area: null,
  relation_payroll_area: null,
};

const getters = {
  // Change value of state(s) with getter
};

const mutations = {
  mutateCreatePayrollArea(state, data) {
    state.error = data;
  },

  mutateListCompanyLocation(state, data) {
    state.list_company_location = data;
  },

  setPayrollAreaList(state, data) {
    state.list_payroll_area = data;
  },

  setRelationPayrollArea(state, data) {
    state.relation_payroll_area = data;
  },
};

const actions = {
  async createPayrollArea({ commit, dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await apiPayrollArea.createPayrollArea(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      commit('mutateCreatePayrollArea', res);
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
        throw err;
      } else {
        commit('mutateError');
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async deletePayrollArea({ dispatch }, req) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await apiPayrollArea.deletePayrollArea(req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async updatePayrollArea({ dispatch }, { id, req }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please Wait', { root: true });
    try {
      const res = await apiPayrollArea.updatePayrollArea(id, req);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
    } catch (err) {
      if (err.response.data.errors) {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.errors[0].message, { root: true });
      } else {
        dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
        dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      }
    }
  },

  async getListCompanyLocation({ dispatch, commit }, data) {
    try {
      const getListCompanyLocation = await apiPayrollArea.companyListLocation(data);
      commit('getListCompanyLocation', getListCompanyLocation);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },

  // MAPPING
  async actionGetListPayrollArea({ dispatch, commit }, data) {
    try {
      const res = await apiPayrollArea.payrollAreaList(data);
      commit('setPayrollAreaList', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  // get async
  async actionGetListMasterPayrollArea({ dispatch }, { reqData, callback }) {
    try {
      const listPayrollArea = await apiPayrollArea.payrollAreaList(reqData);
      const res = listPayrollArea.data;
      res.forEach((lab, index) => {
        res[index].label = lab.name;
      });
      callback(null, res);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionGetMappingPayrollAreaByOrganigram({ dispatch, commit }, data) {
    try {
      const res = await apiPayrollArea.getMappingPayrollAreaByOrganigram(data);
      commit('setRelationPayrollArea', res.data);
    } catch (err) {
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async actionCreateMappingPayrollArea({ dispatch }, data) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPayrollArea.createMappingPayrollArea(data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async actionUpdateMappingPayrollArea({ dispatch }, { id, data }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPayrollArea.updateMappingPayrollArea(id, data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
  async actionDeleteMappingPayrollArea({ dispatch }, id) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const res = await apiPayrollArea.deleteMappingPayrollArea(id);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', res.message, { root: true });
      return res;
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
      return err.response;
    }
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
