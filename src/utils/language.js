const global = {
  account: {
    id: 'Akun',
    en: 'Account',
  },
  profile: {
    id: 'Profil',
    en: 'Profile',
  },
  logout: {
    id: 'Keluar',
    en: 'Logout',
  },
  dashboard: {
    id: 'BERANDA',
    en: 'DASHBOARD',
  },
  company: {
    id: 'PERUSAHAAN',
    en: 'COMPANY',
  },
  employee: {
    id: 'KARYAWAN',
    en: 'EMPLOYEE',
  },
  report: {
    id: 'LAPORAN',
    en: 'REPORT',
  },
  config: {
    id: 'PENGATURAN',
    en: 'CONFIG',
  },
  classification: {
    id: 'GOLPER',
    en: 'CLASSIFICATION',
  },
  policy: {
    id: 'KEBIJAKAN',
    en: 'POLICY',
  },
  form: {
    id: 'FORMULIR',
    en: 'FORM',
  },
  transportation: {
    id: 'TRANSPORTASI',
    en: 'TRANSPORTATION',
  },
  journey_mode: {
    id: 'MODA TRANSPORTASI',
    en: 'TRANSPORTATION MODE',
  },
  category: {
    id: 'KATEGORI',
    en: 'CATEGORY',
  },
  approver: {
    id: 'PERSETUJUAN',
    en: 'APPROVER',
  },
  mapping_lumpsum: {
    id: 'PEMETAAN LUMPSUM',
    en: 'MAPPING LUMPSUM',
  },
  travel_order: {
    id: 'PESANAN PERJALANAN',
    en: 'TRAVEL ORDER',
  },
  open_error_data: {
    id: 'Buka Kesalahan Data',
    en: 'Open Error Data',
  },
  upload: {
    id: 'Unggah',
    en: 'Upload',
  },
  error_data: {
    id: 'Kesalahan Data',
    en: 'Error Data',
  },
  export_xls: {
    id: 'Kirim Report ke Email',
    en: 'Send Report to Email',
  },
  export_to_email: {
    id: 'Ekspor ke Email',
    en: 'Export to Email',
  },
  export_error: {
    id: 'Ekspor Kesalahan',
    en: 'Export Error',
  },
  updated_date: {
    id: 'Tanggal Diperbaharui',
    en: 'Updated Date',
  },
  created_date: {
    id: 'Tanggal Dibuat',
    en: 'Created Date',
  },
  search: {
    id: 'Cari',
    en: 'Search',
  },
  submit: {
    id: 'Simpan',
    en: 'Submit',
  },
  update: {
    id: 'Ubah',
    en: 'Update',
  },
  cancel: {
    id: 'Batal',
    en: 'Cancel',
  },
  delete: {
    id: 'Hapus',
    en: 'Delete',
  },
  created: {
    id: 'Dibuat',
    en: 'Created',
  },
  edited: {
    id: 'Diubah',
    en: 'Edited',
  },
  deleted: {
    id: 'Dihapus',
    en: 'Deleted',
  },
  row: {
    id: 'baris',
    en: 'row',
  },
  selected: {
    id: 'dipilih',
    en: 'selected',
  },
  select: {
    id: 'Pilih ',
    en: 'Select ',
  },
  sellect_all: {
    id: 'pilih semua',
    en: 'select all',
  },
  unsellect_all: {
    id: 'batalkan pilih semua',
    en: 'unselect all',
  },
  please_wait: {
    id: 'Mohon tunggu',
    en: 'Please wait',
  },
  you_want_to_delete: {
    id: 'Anda ingin menghapus ',
    en: 'You want to delete ',
  },
  are_you_sure: {
    id: 'Apakah anda yakin?',
    en: 'Are you sure?',
  },
  to_delete_this: {
    id: 'untuk menghapus ini ',
    en: 'to delete this ',
  },
  train: {
    id: 'Kereta',
    en: 'Train',
  },
  flight: {
    id: 'Pesawat',
    en: 'Flight',
  },
  something_went_wrong: {
    id: 'ada yang salah',
    en: 'something went wrong',
  },
  add: {
    id: 'tambah',
    en: 'add',
  },
  attachment: {
    id: 'LAMPIRAN',
    en: 'ATTACHMENT',
  },
  rows_per_page: {
    id: 'Baris per halaman',
    en: 'Rows per page',
  },
  minimum_amount: {
    id: 'Batas Harga',
    en: 'Limit Amount',
  },
};

const transportation = {
  title: {
    id: 'Transportasi',
    en: 'Transportation',
  },
  create: {
    id: 'Tambah Transportasi',
    en: 'Create Transportation',
  },
  edit: {
    id: 'Ubah Transportasi',
    en: 'Edit Transportation',
  },
  name: {
    id: 'Nama',
    en: 'Name',
  },
  description: {
    id: 'Deskripsi',
    en: 'Description',
  },
  travel_agent: {
    id: 'Agen Perjalanan',
    en: 'Travel Agent',
  },
  business_type: {
    id: 'Jenis Bisnis',
    en: 'Business type',
  },
  name_is_required: {
    id: 'nama wajib diisi',
    en: 'name is required',
  },
  type_is_not_listed: {
    id: 'jenis tidak terdaftar, silahkan coba yang lain',
    en: 'type is not listed, please try another',
  },
  cta_is_not_listed: {
    id: 'menghubungkan agen perjalanan tidak terdaftar, silahkan coba yang lain',
    en: 'connect travel agent is not listed, please try another',
  },
  bt_is_not_listed: {
    id: 'jenis bisnis tidak terdaftar, silahkan coba yang lain',
    en: 'bussines type is not listed, please try another',
  },
  select: {
    id: 'Pilih Transportasi',
    en: 'Select Transportation',
  },
  request_type: {
    id: 'Jenis Transportasi',
    en: 'Request Type',
  },
  connect_travel_agent: {
    id: 'Menghubungkan Agen Perjalanan',
    en: 'Connect Travel Agent',
  },
  public_transportation: {
    id: 'Transportasi Umum',
    en: 'Public Transportation',
  },
  private_transportation: {
    id: 'Transportasi Pribadi',
    en: 'Private Transportation',
  },
  official_transportation: {
    id: 'Transportasi Dinas',
    en: 'Official Transportation',
  },
  automatic: {
    id: 'Otomatis',
    en: 'Automatic',
  },
  domestic: {
    id: 'PD-DN',
    en: 'Domestic',
  },
  international: {
    id: 'PD-LN',
    en: 'International',
  },
  dom_int: {
    id: 'PD-DN-LN',
    en: 'Domestic - International',
  },
};

const transportationMap = {
  title: {
    id: 'Pemetaan Formulir',
    en: 'Form Mapping',
  },
  header: {
    id: 'Pemetaan Transportasi',
    en: 'Transportation Mapping',
  },
  create: {
    id: 'Tambah Pemetaan Formulir',
    en: 'Create Form Mapping',
  },
  edit: {
    id: 'Ubah Pemetaan Formulir',
    en: 'Edit Form Mapping',
  },
  form_plan: {
    id: 'Formulir Rencana',
    en: 'Form Plan',
  },
  select: {
    id: 'Pilih Formulir Rencana',
    en: 'Select Form Plan',
  },
  mapping: {
    id: 'Pemetaan',
    en: 'Mapping',
  },
};

const journeyMode = {
  title: {
    id: 'Moda Transportasi',
    en: 'Transportation Mode',
  },
  create: {
    id: 'Tambah Moda Transportasi',
    en: 'Create Transportation Mode',
  },
  edit: {
    id: 'Ubah Moda Transportasi',
    en: 'Edit Transportation Mode',
  },
  type: {
    id: 'Tipe',
    en: 'Type',
  },
  select_type: {
    id: 'Pilih Tipe',
    en: 'Select Type',
  },
  name_is_required: {
    id: 'nama wajib diisi',
    en: 'field name is required',
  },
  desc_is_required: {
    id: 'deskripsi wajib diisi',
    en: 'field description is required',
  },
  type_is_required: {
    id: 'tipe wajib diisi',
    en: 'field type is required',
  },
  type_is_not_listed: {
    id: 'jenis tidak terdaftar, silahkan coba yang lain',
    en: 'type is not listed, please try another',
  },
  transportation_is_required: {
    id: 'transportasi wajib diisi',
    en: 'field transportation is required',
  },
  transportation_is_not_listed: {
    id: 'transportasi tidak terdaftar, silahkan coba yang lain',
    en: 'transportation is not listed, please try another',
  },
  select_transportation_type: {
    id: 'Pilih Jenis Transportasi',
    en: 'Select Transportation Type',
  },
};

const journeyModeMap = {
  title: {
    id: 'Pemetaan Moda Transportasi',
    en: 'Transportation Mode Mapping',
  },
  create: {
    id: 'Tambah Pemetaan Moda Transportasi',
    en: 'Create Transportation Mode Mapping',
  },
  edit: {
    id: 'Ubah Pemetaan Moda Transportasi',
    en: 'Edit Transportation Mode',
  },
  form_journey: {
    id: 'Formulir Moda Transportasi',
    en: 'Form Transportation Mode',
  },
  form_trip_flight: {
    id: 'Formulir Trip-Pesawat',
    en: 'Form Trip-Flight',
  },
  form_trip_train: {
    id: 'Formulir Trip-Kereta',
    en: 'Form Trip-Train',
  },
  form_trip_hotel: {
    id: 'Formulir Trip-Hotel',
    en: 'Form Trip-Hotel',
  },
};

const category = {
  title: {
    id: 'Kategori',
    en: 'Category',
  },
  create: {
    id: 'Tambah Kategori',
    en: 'Create Category',
  },
  edit: {
    id: 'Ubah Kategori',
    en: 'Edit Category',
  },
  code: {
    id: 'kode',
    en: 'code',
  },
  icon: {
    id: 'simbol',
    en: 'icon',
  },
  code_is_required: {
    id: 'kode wajib diisi',
    en: 'code is required',
  },
  icon_is_not_listed: {
    id: 'simbol tidak terdaftar, silahkan coba yang lain',
    en: 'icon is not listed, please try another',
  },
};

const approver = {
  title: {
    id: 'Pemetaan Persetujuan',
    en: 'Mapping Approver',
  },
  create: {
    id: 'Membuat Pemetaan Persetujuan',
    en: 'Create Approver',
  },
  position: {
    id: 'Posisi',
    en: 'Position',
  },
  category: {
    id: 'Kategori',
    en: 'Category',
  },
  transportation_type: {
    id: 'Jenis Transportasi',
    en: 'Transportation Type',
  },
  mapping_rule_position: {
    id: 'Pemetaan Posisi Posisi',
    en: 'Mapping Rule Position',
  },
  position_name: {
    id: 'Nama Posisi',
    en: 'Position Name',
  },
  rule_approver: {
    id: 'Alur Persetujuan',
    en: 'Rule Approver',
  },
  rule_verificator: {
    id: 'Alur Verifikator',
    en: 'Rule Verificator',
  },
  rule_finance: {
    id: 'Alur Finance',
    en: 'Rule Finance',
  },
};

const home = {
  travel_destination: {
    id: 'Tujuan Perjalanan',
    en: 'Travel Destination',
  },
  cost_travel_by_team: {
    id: 'Biaya Perjalanan Berdasarkan Team',
    en: 'Cost Travel By Team',
  },
  cost_travel_by_month: {
    id: 'Biaya Perjalanan Berdasarkan Bulan',
    en: 'Cost Travel By Month',
  },
  most_visited_dm: {
    id: 'Tujuan Domestik Yang Sering Dikunjungi',
    en: 'Most Visited Domestic Destination',
  },
  most_visited_int: {
    id: 'Tujuan Internasional Yang Sering Dikunjungi',
    en: 'Most Visited International Destination',
  },
  travel_monitor: {
    id: 'Monitor Perjalanan',
    en: 'Travel Monitor',
  },
  destination: {
    id: ' Destinasi ',
    en: ' Destination ',
  },
  frequency: {
    id: 'Frekuensi',
    en: 'Frequency',
  },
  cost_total: {
    id: 'Total Biaya',
    en: 'Cost Total',
  },
  domestic_destination: {
    id: 'Tujuan Domestik',
    en: 'Domestic Destination',
  },
  international_destination: {
    id: 'Tujuan Internasional',
    en: 'International Destination',
  },
};

const report = {
  title: {
    id: 'Laporan Perjalanan Bisnis',
    en: 'Business Travel Report',
  },
  transaction_period: {
    id: 'Periode Transaksi',
    en: 'Transaction Period',
  },
  from: {
    id: 'Dari',
    en: 'From',
  },
  to: {
    id: 'Sampai',
    en: 'To',
  },
  requested_date: {
    id: 'Tanggal Pembuatan',
    en: 'Requested Date',
  },
  booked_date: {
    id: 'Tanggal Disetujui',
    en: 'Approved Date',
  },
  paid_date: {
    id: 'Tanggal Pembayaran',
    en: 'Paid Date',
  },
  canceled_date: {
    id: 'Tanggal Pembatalan',
    en: 'Rejected Date',
  },
  all: {
    id: 'Semua',
    en: 'All',
  },
  booked: {
    id: 'Dipesan',
    en: 'Booked',
  },
  paid: {
    id: 'Dibayar',
    en: 'Paid',
  },
  payment_canceled: {
    id: 'Pembayaran Dibatalkan',
    en: 'Payment Canceled',
  },
  booking_canceled: {
    id: 'Pemesanan Dibatalkan',
    en: 'Booking Canceled',
  },
  employee_id: {
    id: 'ID Pengguna',
    en: 'Employee ID',
  },
  employee_code: {
    id: 'Kode Pengguna',
    en: 'Employee Code',
  },
  user: {
    id: 'Pengguna',
    en: 'User',
  },
  number: {
    id: 'Nomor',
    en: 'Number',
  },
  project_code: {
    id: 'Kode Proyek',
    en: 'Project Code',
  },
  judul: {
    id: 'Judul',
    en: 'Title',
  },
  business_type: {
    id: 'Jenis Bisnis',
    en: 'Business type',
  },
  travel: {
    id: 'Perjalanan',
    en: 'Travel',
  },
  price: {
    id: 'Harga',
    en: 'Price',
  },
  transportation: {
    id: 'Transportasi',
    en: 'Transportation',
  },
  journey_mode: {
    id: 'Mode Perjalanan',
    en: 'Transportation Mode',
  },
  trip_status: {
    id: 'Status Perjalanan',
    en: 'Trip Status',
  },
  request_status: {
    id: 'Status Permintaan',
    en: 'Request Status',
  },
  sent: {
    id: 'Dikirim',
    en: 'Sent',
  },
  approved: {
    id: 'Disetujui',
    en: 'Approved',
  },
  closed: {
    id: 'Ditutup',
    en: 'Closed',
  },
  reject: {
    id: 'Ditolak',
    en: 'Rejected',
  },
};

const classification = {
  classification: {
    id: 'Golper',
    en: 'Classification',
  },
  classificationName: {
    id: 'Nama Golper',
    en: 'Classification Name',
  },
  create: {
    id: 'Buat Golper',
    en: 'Create Classification',
  },
  edit: {
    id: 'Ubah Golper',
    en: 'Edit Classification',
  },
  user: {
    id: 'Pengguna',
    en: 'User',
  },
  selectUser: {
    id: 'Pilih Pengguna*',
    en: 'Select User*',
  },
  users: {
    id: 'Pengguna',
    en: 'Users',
  },
  selectUsers: {
    id: 'Pilih Pengguna*',
    en: 'Select User(s)*',
  },
};

const policy = {
  policy: {
    id: 'Kebijakan',
    en: 'Policy',
  },
  golper_policy: {
    id: 'Kebijakan Golper',
    en: 'Classification Policy',
  },
  type: {
    id: 'tipe',
    en: 'type',
  },
  create: {
    id: 'Buat Kebijakan',
    en: 'Create Policy',
  },
  maximum_price: {
    id: 'Harga Tertinggi',
    en: 'Maximum Price',
  },
  minimum_price: {
    id: 'Harga Terendah',
    en: 'Minimum Price',
  },
  maximum_class: {
    id: 'Kelas Tertinggi',
    en: 'Maximum Class',
  },
  minimum_class: {
    id: 'Kelas Terendah',
    en: 'Minimum Class',
  },
  airlines: {
    id: 'Penerbangan',
    en: 'Airlines',
  },
  name_policy: {
    id: 'Nama Kebijakan',
    en: 'Name Policy',
  },
  if: {
    id: '*Jika anda tidak mengaktifkan harga atau kelas. Maka anda dianggap tidak mengisi form tersebut (masukan anda tidak akan disimpan)',
    en: '* If you do not activate the price or class. Then you are considered not to fill out the form (Your input will not be saved)',
  },
  activate_price: {
    id: 'Aktifkan Harga',
    en: 'Activate Price',
  },
  activate_class: {
    id: 'Aktifkan Kelas',
    en: 'Activate Class',
  },
  activate_airlines: {
    id: 'Aktifkan Penerbangan',
    en: 'Activate Airlines',
  },
  flight: {
    id: 'Pesawat',
    en: 'Flight',
  },
  train: {
    id: 'Kereta',
    en: 'Train',
  },
  executive: {
    id: 'Eksekutif',
    en: 'Executive',
  },
  business: {
    id: 'Bisnis',
    en: 'Business',
  },
  economy: {
    id: 'Ekonomi',
    en: 'Economy',
  },
  star: {
    id: 'Bintang',
    en: 'Star',
  },
};

const attachment = {
  title: {
    id: 'Laporan Perjalanan Bisnis',
    en: 'Business Travel Report',
  },
  transaction_period: {
    id: 'Periode Transaksi',
    en: 'Transaction Period',
  },
  from: {
    id: 'Dari',
    en: 'From',
  },
  to: {
    id: 'Sampai',
    en: 'To',
  },
  requested_date: {
    id: 'Tanggal Request',
    en: 'Requested Date',
  },
  booked_date: {
    id: 'Tanggal Pemesanan',
    en: 'Booked Date',
  },
  paid_date: {
    id: 'Tanggal Pembayaran',
    en: 'Paid Date',
  },
  canceled_date: {
    id: 'Tanggal Pembatalan',
    en: 'Canceled Date',
  },
  all: {
    id: 'Semua',
    en: 'All',
  },
  booked: {
    id: 'Sudah Dipesan',
    en: 'Booked',
  },
  paid: {
    id: 'Pembayaran',
    en: 'Paid',
  },
  payment_canceled: {
    id: 'Pembayaran Dibatalkan',
    en: 'Payment Canceled',
  },
  booking_canceled: {
    id: 'Pemesanan Dibatalkan',
    en: 'Booking Canceled',
  },
  ref_id: {
    id: 'ID REF',
    en: 'REF ID',
  },
  name: {
    id: 'Judul',
    en: 'Title',
  },
  request: {
    id: 'Permintaan',
    en: 'Request',
  },
  settlement: {
    id: 'Penyelesaian',
    en: 'Settlement',
  },
  created_date: {
    id: 'Tanggal Dibuat',
    en: 'Created Date',
  },
  request_status: {
    id: 'Status Permohonan',
    en: 'Request Status',
  },
};

const policyMapping = {
  golper_policy: {
    id: 'Kebijakan Golper',
    en: 'Classification Policy',
  },
  create: {
    id: 'Buat Pemetaan Golper',
    en: 'Create Classification Policy',
  },
  title: {
    id: 'Pemetaan Golper',
    en: 'Classification Policy',
  },
  classification: {
    id: 'Nama Klasifikasi',
    en: 'Classification Name',
  },
  placeholder_classification: {
    id: 'Pilih Klasifikasi',
    en: 'Select Classification',
  },
  flight: {
    id: 'Kebijakan Penerbangan',
    en: 'Flight Policy',
  },
  placeholders: {
    id: 'Pilih Kebijakan',
    en: 'Select Policy',
  },
  train: {
    id: 'Kebijakan Kereta',
    en: 'Train Policy',
  },
  hotel: {
    id: 'Kebijakan Hotel',
    en: 'Hotel Policy',
  },
  name: {
    id: 'Nama',
    en: 'Name',
  },
  policy: {
    id: 'Kebijakan',
    en: 'Policy',
  },
  classification_head: {
    id: 'Klasifikasi',
    en: 'Classification',
  },
  harga: {
    id: 'Harga',
    en: 'Price',
  },
  kelas: {
    id: 'Kelas',
    en: 'Class',
  },
  title_edit: {
    id: 'UBAH PEMETAAN KEBIJAKAN',
    en: 'EDIT MAPPING POLICY',
  },
};

function getLang() {
  const lang = localStorage.getItem('biztripLang');
  return lang;
}

export default {
  global: {
    account: global.account[getLang()],
    profile: global.profile[getLang()],
    logout: global.logout[getLang()],
    travel_order: global.travel_order[getLang()],
    mapping_lumpsum: global.mapping_lumpsum[getLang()],
    approver: global.approver[getLang()],
    category: global.category[getLang()],
    journey_mode: global.journey_mode[getLang()],
    transportation: global.transportation[getLang()],
    form: global.form[getLang()],
    policy: global.policy[getLang()],
    classification: global.classification[getLang()],
    config: global.config[getLang()],
    report: global.report[getLang()],
    employee: global.employee[getLang()],
    company: global.company[getLang()],
    dashboard: global.dashboard[getLang()],
    open_error_data: global.open_error_data[getLang()],
    upload: global.upload[getLang()],
    error_data: global.error_data[getLang()],
    export_xls: global.export_xls[getLang()],
    export_to_email: global.export_to_email[getLang()],
    export_error: global.export_error[getLang()],
    updated_date: global.updated_date[getLang()],
    created_date: global.created_date[getLang()],
    search: global.search[getLang()],
    submit: global.submit[getLang()],
    update: global.update[getLang()],
    cancel: global.cancel[getLang()],
    delete: global.delete[getLang()],
    created: global.created[getLang()],
    edited: global.edited[getLang()],
    deleted: global.deleted[getLang()],
    row: global.row[getLang()],
    selected: global.selected[getLang()],
    select: global.select[getLang()],
    sellect_all: global.sellect_all[getLang()],
    unsellect_all: global.unsellect_all[getLang()],
    please_wait: global.please_wait[getLang()],
    you_want_to_delete: global.you_want_to_delete[getLang()],
    are_you_sure: global.are_you_sure[getLang()],
    to_delete_this: global.to_delete_this[getLang()],
    train: global.train[getLang()],
    flight: global.flight[getLang()],
    something_went_wrong: global.something_went_wrong[getLang()],
    add: global.add[getLang()],
    attachment: global.attachment[getLang()],
    rows_per_page: global.rows_per_page[getLang()],
    minimum_amount: global.minimum_amount[getLang()],
  },
  transportation: {
    title: transportation.title[getLang()],
    create: transportation.create[getLang()],
    edit: transportation.edit[getLang()],
    name: transportation.name[getLang()],
    description: transportation.description[getLang()],
    travel_agent: transportation.travel_agent[getLang()],
    business_type: transportation.business_type[getLang()],
    name_is_required: transportation.name_is_required[getLang()],
    type_is_not_listed: transportation.type_is_not_listed[getLang()],
    cta_is_not_listed: transportation.cta_is_not_listed[getLang()],
    bt_is_not_listed: transportation.bt_is_not_listed[getLang()],
    select: transportation.select[getLang()],
    request_type: transportation.request_type[getLang()],
    connect_travel_agent: transportation.connect_travel_agent[getLang()],
    public_transportation: transportation.public_transportation[getLang()],
    private_transportation: transportation.private_transportation[getLang()],
    official_transportation: transportation.official_transportation[getLang()],
    automatic: transportation.automatic[getLang()],
    domestic: transportation.domestic[getLang()],
    international: transportation.international[getLang()],
    dom_int: transportation.dom_int[getLang()],
  },
  transportationMap: {
    title: transportationMap.title[getLang()],
    header: transportationMap.header[getLang()],
    create: transportationMap.create[getLang()],
    edit: transportationMap.edit[getLang()],
    form_plan: transportationMap.form_plan[getLang()],
    select: transportationMap.select[getLang()],
    mapping: transportationMap.mapping[getLang()],
  },
  journeyMode: {
    title: journeyMode.title[getLang()],
    create: journeyMode.create[getLang()],
    edit: journeyMode.edit[getLang()],
    type: journeyMode.type[getLang()],
    select_type: journeyMode.select_type[getLang()],
    name_is_required: journeyMode.name_is_required[getLang()],
    desc_is_required: journeyMode.desc_is_required[getLang()],
    type_is_required: journeyMode.type_is_required[getLang()],
    type_is_not_listed: journeyMode.type_is_not_listed[getLang()],
    transportation_is_required: journeyMode.transportation_is_required[getLang()],
    transportation_is_not_listed: journeyMode.transportation_is_not_listed[getLang()],
    select_transportation_type: journeyMode.select_transportation_type[getLang()],
  },
  journeyModeMap: {
    title: journeyModeMap.title[getLang()],
    create: journeyModeMap.create[getLang()],
    edit: journeyModeMap.edit[getLang()],
    form_journey: journeyModeMap.form_journey[getLang()],
    form_trip_flight: journeyModeMap.form_trip_flight[getLang()],
    form_trip_train: journeyModeMap.form_trip_train[getLang()],
    form_trip_hotel: journeyModeMap.form_trip_hotel[getLang()],
  },
  category: {
    title: category.title[getLang()],
    create: category.create[getLang()],
    edit: category.edit[getLang()],
    code: category.code[getLang()],
    icon: category.icon[getLang()],
    code_is_required: category.code_is_required[getLang()],
    icon_is_not_listed: category.icon_is_not_listed[getLang()],
  },
  approver: {
    title: approver.title[getLang()],
    create: approver.create[getLang()],
    position: approver.position[getLang()],
    category: approver.category[getLang()],
    transportation_type: approver.transportation_type[getLang()],
    mapping_rule_position: approver.mapping_rule_position[getLang()],
    position_name: approver.position_name[getLang()],
    rule_approver: approver.rule_approver[getLang()],
    rule_verificator: approver.rule_verificator[getLang()],
    rule_finance: approver.rule_finance[getLang()],
  },
  home: {
    travel_destination: home.travel_destination[getLang()],
    cost_travel_by_team: home.cost_travel_by_team[getLang()],
    most_visited_dm: home.most_visited_dm[getLang()],
    cost_travel_by_month: home.cost_travel_by_month[getLang()],
    most_visited_int: home.most_visited_int[getLang()],
    travel_monitor: home.travel_monitor[getLang()],
    destination: home.destination[getLang()],
    frequency: home.frequency[getLang()],
    cost_total: home.cost_total[getLang()],
    domestic_destination: home.domestic_destination[getLang()],
    international_destination: home.international_destination[getLang()],
  },
  report: {
    title: report.title[getLang()],
    transaction_period: report.transaction_period[getLang()],
    from: report.from[getLang()],
    to: report.to[getLang()],
    requested_date: report.requested_date[getLang()],
    booked_date: report.booked_date[getLang()],
    paid_date: report.paid_date[getLang()],
    canceled_date: report.canceled_date[getLang()],
    all: report.all[getLang()],
    booked: report.booked[getLang()],
    paid: report.paid[getLang()],
    payment_canceled: report.payment_canceled[getLang()],
    booking_canceled: report.booking_canceled[getLang()],
    employee_id: report.employee_id[getLang()],
    employee_code: report.employee_code[getLang()],
    user: report.user[getLang()],
    number: report.number[getLang()],
    project_code: report.project_code[getLang()],
    judul: report.judul[getLang()],
    business_type: report.business_type[getLang()],
    travel: report.travel[getLang()],
    price: report.price[getLang()],
    transportation: report.transportation[getLang()],
    journey_mode: report.journey_mode[getLang()],
    trip_status: report.trip_status[getLang()],
    request_status: report.request_status[getLang()],
    sent: report.sent[getLang()],
    approved: report.approved[getLang()],
    closed: report.closed[getLang()],
    reject: report.reject[getLang()],
  },
  classification: {
    classification: classification.classification[getLang()],
    classificationName: classification.classificationName[getLang()],
    create: classification.create[getLang()],
    edit: classification.edit[getLang()],
    user: classification.user[getLang()],
    selectUser: classification.selectUser[getLang()],
    users: classification.users[getLang()],
    selectUsers: classification.selectUsers[getLang()],
  },
  policy: {
    policy: policy.policy[getLang()],
    golper_policy: policy.golper_policy[getLang()],
    type: policy.type[getLang()],
    create: policy.create[getLang()],
    maximum_price: policy.maximum_price[getLang()],
    minimum_price: policy.minimum_price[getLang()],
    maximum_class: policy.maximum_class[getLang()],
    minimum_class: policy.minimum_class[getLang()],
    airlines: policy.airlines[getLang()],
    name_policy: policy.name_policy[getLang()],
    if: policy.if[getLang()],
    activate_price: policy.activate_price[getLang()],
    activate_class: policy.activate_class[getLang()],
    activate_airlines: policy.activate_airlines[getLang()],
    flight: policy.flight[getLang()],
    train: policy.train[getLang()],
    executive: policy.executive[getLang()],
    business: policy.business[getLang()],
    economy: policy.economy[getLang()],
    star: policy.star[getLang()],
  },
  attachment: {
    title: attachment.title[getLang()],
    transaction_period: attachment.transaction_period[getLang()],
    from: attachment.from[getLang()],
    to: attachment.to[getLang()],
    requested_date: attachment.requested_date[getLang()],
    booked_date: attachment.booked_date[getLang()],
    paid_date: attachment.paid_date[getLang()],
    canceled_date: attachment.canceled_date[getLang()],
    all: attachment.all[getLang()],
    booked: attachment.booked[getLang()],
    paid: attachment.paid[getLang()],
    payment_canceled: attachment.payment_canceled[getLang()],
    booking_canceled: attachment.booking_canceled[getLang()],
    ref_id: attachment.ref_id[getLang()],
    name: attachment.name[getLang()],
    request: attachment.request[getLang()],
    settlement: attachment.settlement[getLang()],
    created_date: attachment.created_date[getLang()],
    request_status: attachment.request_status[getLang()],
  },
  policyMapping: {
    golper_policy: policyMapping.golper_policy[getLang()],
    create: policyMapping.create[getLang()],
    title: policyMapping.title[getLang()],
    classification: policyMapping.classification[getLang()],
    placeholder_classification: policyMapping.placeholder_classification[getLang()],
    flight: policyMapping.flight[getLang()],
    placeholders: policyMapping.placeholders[getLang()],
    train: policyMapping.train[getLang()],
    hotel: policyMapping.hotel[getLang()],
    name: policyMapping.name[getLang()],
    policy: policyMapping.policy[getLang()],
    classification_head: policyMapping.classification_head[getLang()],
    harga: policyMapping.harga[getLang()],
    kelas: policyMapping.kelas[getLang()],
    title_edit: policyMapping.title_edit[getLang()],
  },
};
