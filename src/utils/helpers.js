export default function trimDecimalValue(numericString, limitDecimal) {
  if (numericString === '') return '';

  const [integerPart, decimalPart] = numericString.split('.');

  if (decimalPart && decimalPart.length > limitDecimal) {
    const trimmedDecimalPart = decimalPart.substring(0, limitDecimal);
    return `${integerPart}.${trimmedDecimalPart}`;
  }
  return numericString;
}
