const EXAMPLE = 1;

export default {
  example: EXAMPLE,
  STATUS_DRAFT: 1,
  STATUS_SENT: 3,
  STATUS_APPROVED: 4,
  STATUS_REJECTED: 6,
  STATUS_CLOSED: 9,
  STATUS_CANCEL: 11,

  WORKFLOW_ROLE_REQUESTOR: 1,
  WORKFLOW_ROLE_APPROVER: 2,

  REIMBURSEMENT: 1,
  CASH_ADVANCE: 2,
  BIZTRIP: 3,
  PETTY_CASH: 4,
  DIRECT_PAYMENT: 5,
  PREPAID_EXPENSE: 6,
  PROJECT: 7,
  RECURRING_LOG: 9,
  RECURRING_ALLOWANCE_COMPONENT_TYPE: 3,
  RECURRING_BONUS_COMPONENT_TYPE: 5,
  RECURRING_DEDUCTION_COMPONENT_TYPE: 4,
  RECURRING_IS_TEMPORARY: 0,
  RECURRING_BASIC_COMPONENT_TYPE: 1,

  // STATUS TRANSACTION
  TIMES_STATUS_SENT: 2,
  TIMES_STATUS_DRAFT: 1,
  TIMES_STATUS_PROCESS: 3,
  TIMES_STATUS_APPROVED: 4,
  TIMES_STATUS_REIMBURSED: 5,
  TIMES_STATUS_REJECTED: 6,

  // ATTENDANCE LEAVE TYPE POLICY
  POLICY_TYPE_FALSE: 0,
  POLICY_TYPE_TRUE: 1,
  POLICY_TYPE_MONTH: 2,
  POLICY_TYPE_YEAR: 3,

  POLICY_TYPE_UNPAID: 0,
  POLICY_TYPE_PAID: 1,
  POLICY_TYPE_DAILY_PAY: 2,

  // ATTENDANCE LEAVE TYPE POLICY NAME
  POLICY_NAME_REPETITION: 1,
  POLICY_NAME_CARRY_FORWARD: 2,
  POLICY_NAME_EXPIRED_CARRY_FORWARD: 3,
  POLICY_NAME_HOLIDAY_SUBTITUTE: 4,
  POLICY_NAME_PRORATE: 5,
  POLICY_NAME_MAX_DEBIT_CREDIT: 6,
  POLICY_NAME_BACKDATE: 7,
  POLICY_NAME_MINIMUM_DAY_SUBMIT: 8,
  POLICY_NAME_REVISION: 9,
  POLICY_NAME_ADDED_QUOTA: 10,
  POLICY_NAME_EXCLUDE_HOLIDAY_FOR_LEAVE: 11,
  POLICY_NAME_PAID_LEAVE: 12,
  POLICY_NAME_HAVE_HALF_DAY: 13,
  POLICY_NAME_ONE_TIME_SUBMISSION: 14,
  POLICY_NAME_MANDATORY_DOCUMENT: 15,
  POLICY_NAME_MINIMUM_SUBMISSION: 16,
  POLICY_NAME_SPECIAL_LEAVE_LENGTH_WORKING_START: 17,
  POLICY_NAME_SPECIAL_LEAVE_QUOTA: 18,
  POLICY_NAME_SPECIAL_LEAVE_LENGTH_WORKING_END: 19,
  POLICY_NAME_MAXIMUM_SUBMISSION: 20,
  POLICY_NAME_LOCK_SAME_LEAVE: 21,
  POLICY_NAME_GENDER: 22,
  POLICY_NAME_MARITAL_STATUS: 23,
  POLICY_NAME_LOCK_MONTH: 24,
  POLICY_NAME_RELATED_TYPES: 25,
  POLICY_NAME_AUTO_MAPPING_QUOTA: 26,
  POLICY_NAME_ONCE_DURING_ONE_PERIOD: 27,
  POLICY_NAME_REPETITION_CALCULATION: 28,
  POLICY_NAME_AUTO_BLOCK: 30,

  // ATTENDANCE LEAVE TYPE PERIOD TYPE
  PERIOD_TYPE_DAILY: 1,
  PERIOD_TYPE_MONTHLY: 2,

  // ADMINISTRATOR USER ROLE
  ROLE_ADMIN: 1,
  ROLE_IMPLEMENTOR: 2,
  ROLE_USER: 3,
};
