<template>
  <div
    v-if="isLoading"
    style="text-align: center;">
    <div class="lds-ellipsis"><div/><div/><div/><div/></div>
  </div>
  <div v-else>
    <j-form-base-dynamic
      v-if="isDynamic && fieldList.length > 0"
      ref="jFormCreate"
      :data-fields="fieldList"
      :is-detail="true"
      :is-disabled="!editableByWorkflow"/>
    <div
      v-if="isDynamic && fieldList.length === 0"
      class="container-dynamic-field-no-available">No Available</div>
    <j-form-base-auto
      v-if="!isDynamic && fieldList.length > 0"
      ref="jFormCreate"
      :data-fields="fieldList"
      :is-detail="true"
      :is-disabled="true"/>
  </div>
</template>

<script>
/* eslint-disable no-nested-ternary, max-len, no-unused-vars, no-eval, no-console, no-underscore-dangle, no-plusplus */
import axios from 'axios';
import Vue from 'vue';
import jwtDecode from 'jwt-decode';
import { mapActions } from 'vuex';
import jFormBaseAuto from '@/views/application/application-component/form-ui/form-base/jFormBaseAuto';
import jFormBaseDynamic from '@/views/application/application-component/form-ui/form-base/jFormBaseDynamic';

export default {
  name: 'JDetailAutoField',
  components: {
    jFormBaseAuto,
    jFormBaseDynamic,
  },
  props: {
    formData: {
      type: Object,
      default: () => {},
    },
    recordId: {
      type: String,
      default: '',
    },
    setDefaultValue: {
      type: String,
      default: '',
    },
    columns: {
      type: Array,
      default: () => [],
    },
    externalUrl: {
      type: Object,
      default: () => {},
    },
    isExternal: {
      type: Boolean,
      default: false,
    },
    dataFields: {
      type: Array,
      default: () => [],
    },
    dataForm: {
      type: Object,
      default: () => {},
    },
    fieldKey: {
      type: Object,
      default: () => {},
    },
    isDynamic: {
      type: Boolean,
      default: false,
    },
    fieldMaster: {
      type: Object,
      default: () => {},
    },
    isDisabled: {
      type: Boolean,
      default: () => false,
    },
  },
  data() {
    return {
      fieldList: [],
      masterListFiled: [],
      isLoading: true,
      session: jwtDecode(JSON.parse(localStorage.getItem('Token')).value),
    };
  },
  computed: {
    queryParamsParent() {
      if (this.$route.query.queryParamsParent) {
        try {
          const docodeQueryParams = JSON.parse(atob(String(this.$route.query.queryParamsParent).replaceAll('%3D', /=/g)));
          return docodeQueryParams;
        } catch (err) {
          return null;
        }
      }
      return null;
    },
    editableByWorkflow() {
      let editable = true;
      let stop = false;
      // if (Number(localStorage.getItem(`layer_${this.$route.params.pageId}`)) > 1) {
      //   for (let i = 0; i < Number(localStorage.getItem(`layer_${this.$route.params.pageId}`)); i++) {
      //     if (localStorage.getItem(`dataWorkflowLayer_${this.$route.params.pageId}_${i}`)) {
      //       const dataWorkflow = JSON.parse(localStorage.getItem(`dataWorkflowLayer_${this.$route.params.pageId}_${i}`));
      //       if (!stop && dataWorkflow.status !== 'draft') {
      //         editable = false;
      //         stop = true;
      //       }
      //     }
      //   }
      // }
      if (this.queryParamsParent && Number(JSON.parse(this.queryParamsParent.breadcrumbsParent).layer) > 1) {
        for (let i = 0; i < Number(JSON.parse(this.queryParamsParent.breadcrumbsParent).layer); i++) {
          if (JSON.parse(this.queryParamsParent.breadcrumbsParent)[`dataWorkflowLayer_${i}`]) {
            const dataWorkflow = JSON.parse(this.queryParamsParent.breadcrumbsParent)[`dataWorkflowLayer_${i}`];
            if (!stop && dataWorkflow.status !== 'draft') {
              editable = false;
              stop = true;
            }
          }
        }
      }
      return (!this.$route.params.status || this.$route.params.status === 'draft') && editable && !this.isDisabled;
    },
  },
  watch: {
    recordId(val) {
      this.isLoading = true;
      this.fieldList = [];
      if (this.isExternal) {
        this.getDataExternalApi();
      } else {
        this.getRecordData();
      }
    },
  },
  created() {
    if (this.isDynamic) {
      this.getRecordDataDynamic();
    } else if (this.isExternal) {
      this.getDataExternalApi();
    } else {
      this.getRecordData();
    }
  },
  methods: {
    ...mapActions('jStoreFormUi', ['actionGetRecordData']),

    getDataExternalApi() {
      const data = this.externalUrl;
      const token = Vue.ls.get('Token');
      const method = data.method.toLowerCase();
      const _field = this.dataForm;
      const _user = this.session.user;
      const variable = { query: this.recordId };

      let fieldkey = {};
      if (this.fieldKey) {
        if (Object.keys(this.fieldKey).length > 0) {
          fieldkey = this.fieldKey;
        } else {
          const fieldRelation = this.dataFields.filter(f => f.type === 'relation_record' || f.type === 'relation_record_external_api');
          fieldRelation.forEach((r) => {
            fieldkey[r.field_ui_key] = this.dataForm[r.field_ui_key] ? this.dataForm[r.field_ui_key].id : 0;
          });
        }
      } else {
        const fieldRelation = this.dataFields.filter(f => f.type === 'relation_record' || f.type === 'relation_record_external_api');
        fieldRelation.forEach((r) => {
          fieldkey[r.field_ui_key] = this.dataForm[r.field_ui_key] ? this.dataForm[r.field_ui_key].id : 0;
        });
      }

      let url = '';
      if (method === 'get') {
        const queryParams = Object.keys(data.query_params);
        const queryParamsValue = Object.values(data.query_params);

        let urlParams = '';
        queryParams.forEach((e, index) => {
          let y;
          const andMore = queryParams.length - 1 > index ? '&' : '';
          urlParams += `${e}=${eval(`y=${queryParamsValue[index]}`)}${andMore}`;
        });

        url = `${data.url}?${urlParams}`;
        axios.get(url, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }).then((res) => {
          const forms = res.data[data.source];
          if (forms.length > 0) {
            let y;
            const getData = forms[0];
            this.setDataDetailFieldsExternal(getData);
          } else {
            this.isLoading = false;
          }
        });
      } else {
        let y;
        const params = eval(`y=${data.body}`);

        url = data.url;
        axios[method](url, params, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }).then((res) => {
          const forms = res.data[data.source];
          if (forms.length > 0) {
            let x;
            const getData = forms[0];
            this.setDataDetailFieldsExternal(getData);
          } else {
            this.isLoading = false;
          }
        });
      }
    },

    setDataDetailFieldsExternal(data) {
      let y;
      this.fieldList = this.columns.map((el, index) => ({
        id: Number(`${Math.floor(Math.random() * 100)}${Math.floor(Math.random() * 100)}`),
        name: el.name,
        type: el.type,
        placeholder: el.placeholder,
        value: eval(`y=data.${el.field_data_key}`),
        value_id: data[el.field_data_key] ? data[el.field_data_key] : 0,
        value_form: null,
        grid: el.grid_size ? el.grid_size : '12',
        available_list: el.config ? (el.config.list_options ? el.config.list_options.map(val => ({
          id: val,
          name: val,
        })) : []) : [],
        available_list_checkbox: el.config ? (el.config.list_options ? el.config.list_options.map(val => ({
          value: val,
          text: val,
        })) : []) : [],
        available_list_tags: el.config ? (el.config.list_tags ? el.config.list_tags.map(val => ({
          name: val.name,
          background: val.background,
          font_color: val.font_color,
        })) : []) : [],
        external_url: el.config ? (el.config.data_source_external ? el.config.data_source_external : null) : null,
        external_url_variable: el.config ? (el.config.data_source_external_variable ? el.config.data_source_external_variable : '') : '',
        external_url_set_value: el.config ? (el.config.data_source_external_set_value ? el.config.data_source_external_set_value : '') : '',
        is_delete: false,
        is_editable: el.is_disable,
        is_mandatory: el.is_mandatory,
        map_type: el.config ? (el.config.map_type ? el.config.map_type : 1) : 1,
        relation_record_data: el.config ? (el.config.data_source ? el.config.data_source : null) : null,
        accepted_file_type: el.config ? (el.config.accepted_file_type ? el.config.accepted_file_type : []) : [],
        multiple_value: [],
        person: {},
        is_external_source: false,
        time: '00:00:00',
        stopTime: true,
        field_ui_key: el.field_ui_key,
        field_data_key: el.field_data_key,
        config: el.config,
      }));
      this.isLoading = false;
    },

    getRecordData() {
      const dataFilter = {};
      dataFilter[this.setDefaultValue] = this.recordId;

      const param = {
        form_data_id: this.formData.id,
        page: 1,
        limit: 5,
        filter: dataFilter,
      };
      this.actionGetRecordData(param).then((res) => {
        this.setDataDetailFields(res.data[0]);
      });
    },

    setDataDetailFields(data) {
      this.fieldList = this.columns.map((el, index) => ({
        id: Number(`${Math.floor(Math.random() * 100)}${Math.floor(Math.random() * 100)}`),
        name: el.name,
        type: el.type,
        placeholder: el.placeholder,
        value: data[el.field_data_key],
        // value: this.cekTypeData(el.field_data_key) === 'timestamp' ?
        //   (el.type === 'time' ? this.convertTime(data[el.field_data_key]) : this.convertTimestamp(data[el.field_data_key]))
        //   : data[el.field_data_key],
        value_id: data[el.field_data_key] ? data[el.field_data_key] : 0,
        value_form: null,
        grid: el.grid_size ? el.grid_size : '12',
        available_list: el.config ? (el.config.list_options ? el.config.list_options.map(val => ({
          id: val,
          name: val,
        })) : []) : [],
        available_list_checkbox: el.config ? (el.config.list_options ? el.config.list_options.map(val => ({
          value: val,
          text: val,
        })) : []) : [],
        available_list_tags: el.config ? (el.config.list_tags ? el.config.list_tags.map(val => ({
          name: val.name,
          background: val.background,
          font_color: val.font_color,
        })) : []) : [],
        external_url: el.config ? (el.config.data_source_external ? el.config.data_source_external : null) : null,
        external_url_variable: el.config ? (el.config.data_source_external_variable ? el.config.data_source_external_variable : '') : '',
        external_url_set_value: el.config ? (el.config.data_source_external_set_value ? el.config.data_source_external_set_value : '') : '',
        is_delete: false,
        is_editable: el.is_disable || this.formData.primary_key === el.field_data_key,
        is_mandatory: el.is_mandatory,
        map_type: el.config ? (el.config.map_type ? el.config.map_type : 1) : 1,
        relation_record_data: el.config ? (el.config.data_source ? el.config.data_source : null) : null,
        accepted_file_type: el.config ? (el.config.accepted_file_type ? el.config.accepted_file_type : []) : [],
        multiple_value: this.setDataMultipleValue(data, el),
        person: this.setDataPersonValue(data, el),
        is_external_source: false,
        time: '00:00:00',
        stopTime: true,
        field_ui_key: el.field_ui_key,
        field_data_key: el.field_data_key,
        html: el.config ? (el.config.html ? el.config.html : '') : '',
        allow_minus_value: el.config && el.config.allow_minus_value ? el.config.allow_minus_value : null,
        config: el.config,
      }));
      this.isLoading = false;
    },

    getRecordDataDynamic() {
      let x;
      const _field = this.dataForm;
      const _user = this.session.user;
      const dataFilter = {};
      const filterKeys = Object.keys(this.fieldMaster.config.filter);
      const filterValues = Object.values(this.fieldMaster.config.filter);

      if (filterValues.length > 0) {
        filterValues.forEach((e, index) => {
          if (e.includes('_user') || e.includes('_field')) {
            dataFilter[filterKeys[index]] = eval(`x=${e}`);
          } else if (filterKeys[index].includes('.company_user_id')) {
            dataFilter[filterKeys[index]] = Number(e);
          } else if (e.includes('.today')) {
            const date = new Date();
            const startOfDay = new Date(Date.parse(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 00:00:00`)).getTime();
            const endOfDay = new Date(Date.parse(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 23:59:59`)).getTime();
            // eslint-disable-next-line
            dataFilter[filterKeys[index]] = { '$between': [startOfDay, endOfDay] };
          } else {
            // eslint-disable-next-line
            if (this.fieldMaster.config.data_source.form.columns[filterKeys[index]].type === 'number') {
              dataFilter[filterKeys[index]] = Number(e);
            } else {
              dataFilter[filterKeys[index]] = e.slice(1, -1);
            }
          }
        });
      }

      const param = {
        form_data_id: this.fieldMaster.config.data_source.form.id,
        page: 1,
        limit: 5,
        filter: dataFilter,
        filter_operator: this.fieldMaster.config.filter_operator,
      };
      this.actionGetRecordData(param).then((res) => {
        const dataField = res.data.length > 0 ? (res.data[0][this.fieldMaster.config.data_source.data_selection] || []) : [];
        this.masterListFiled = dataField;
        this.setFiledDynamic(dataField);
      });
    },

    setFiledDynamic(data) {
      let filedFiltered = [];
      if (this.fieldMaster.value && !this.dataForm[`${this.fieldMaster.field_ui_key}_hasChange`]) {
        filedFiltered = data.filter(f => typeof this.fieldMaster.value[f.field_ui_key] !== 'undefined');
      } else {
        filedFiltered = data.filter(f => !f.is_delete);
      }
      this.fieldList = filedFiltered.map((el, index) => ({
        id: Number(`${Math.floor(Math.random() * 100)}${Math.floor(Math.random() * 100)}`),
        name: el.name,
        type: el.type,
        placeholder: el.placeholder,
        value: this.fieldMaster.value ? this.fieldMaster.value[el.field_ui_key] : null,
        value_id: this.fieldMaster.value ? this.fieldMaster.value[el.field_ui_key] : 0,
        value_form: null,
        config: el.config,
        grid: el.grid_size ? el.grid_size : '12',
        available_list: el.config ? (el.config.list_options ? el.config.list_options.map(val => ({
          id: val,
          name: val,
        })) : []) : [],
        available_list_checkbox: el.config ? (el.config.list_options ? el.config.list_options.map(val => ({
          value: val,
          text: val,
        })) : []) : [],
        available_list_tags: el.config ? (el.config.list_tags ? el.config.list_tags.map(val => ({
          name: val.name,
          background: val.background,
          font_color: val.font_color,
        })) : []) : [],
        external_url: el.config ? (el.config.data_source_external ? el.config.data_source_external : null) : null,
        external_url_variable: el.config ? (el.config.data_source_external_variable ? el.config.data_source_external_variable : '') : '',
        external_url_set_value: el.config ? (el.config.data_source_external_set_value ? el.config.data_source_external_set_value : '') : '',
        is_delete: this.fieldMaster.value && !this.dataForm[`${this.fieldMaster.field_ui_key}_hasChange`] ? false : el.is_delete,
        is_editable: el.is_disable,
        is_mandatory: el.is_mandatory,
        is_hidden: el.is_hidden,
        map_type: el.config ? (el.config.map_type ? el.config.map_type : 1) : 1,
        relation_record_data: el.config ? (el.config.data_source ? el.config.data_source : null) : null,
        relation_record_auto_fill: el.config ? (el.config.data_field_auto ? el.config.data_field_auto : []) : [],
        is_auto_fill: el.config ? (el.config.is_auto_fill ? el.config.is_auto_fill : false) : false,
        is_auto_show: el.config ? (el.config.is_auto_show ? el.config.is_auto_show : false) : false,
        accepted_file_type: el.config ? (el.config.accepted_file_type ? el.config.accepted_file_type : []) : [],
        multiple_value: this.setDataMultipleValue(data, el),
        person: this.setDataPersonValue(data, el),
        is_external_source: false,
        time: '00:00:00',
        is_with_param: el.config ? (el.config.is_with_param ? el.config.is_with_param : false) : false,
        filter_column: el.config ? (el.config.filter_column ? el.config.filter_column : null) : null,
        field_key: el.config ? (el.config.field_key ? el.config.field_key : null) : null,
        is_detail_page_screen: true,
        field_ui_key: el.field_ui_key,
        field_data_key: el.field_data_key,
        html: el.config ? (el.config.html ? el.config.html : '') : '',
        groups: el.config ? (el.config.data_field_group ? this.configFieldGroup(el.config.data_field_group, data[el.field_data_key], el.field_data_key, el.is_disable) : []) : [],
        master_groups: el.config ? (el.config.data_field_group ? el.config.data_field_group : []) : [],
        error_input: false,
        error_message: '',
        is_delete_disable: el.is_disable,
      }));
      this.isLoading = false;
    },

    setDataMultipleValue(data, el) {
      let dataMultiple = [];
      if (this.cekTypeData(el.field_data_key) === '[]text' || this.cekTypeData(el.field_data_key) === '[]number' || this.cekTypeData(el.field_data_key) === '[]attachment') {
        if (el.type === 'checkbox') {
          dataMultiple = data[el.field_data_key];
        } else if (el.type === 'attachment' || el.type === 'multiple_attachment_dynamic') {
          if (data[el.field_data_key]) {
            dataMultiple = data[el.field_data_key].map((val, indexVal) => ({
              id: indexVal + 1,
              name: val.name,
              url: val.url,
            }));
          } else {
            dataMultiple = el.config ? (el.config.list_options ? el.config.list_options.map((val, indexVal) => ({
              id: indexVal + 1,
              name: val,
            })) : []) : [];
          }
        } else {
          // eslint-disable-next-line
          if (data[el.field_data_key]) {
            dataMultiple = data[el.field_data_key].map(val => ({
              id: val,
              name: val,
            }));
          }
        }
      }
      return dataMultiple;
    },

    setDataPersonValue(data, el) {
      let dataPerson = {};
      if (this.cekTypeData(el.field_data_key) === 'person') {
        dataPerson = data[el.field_data_key];
      }
      return dataPerson;
    },

    convertTimestamp(data) {
      if (data) {
        // const date = new Date(data.toString().length === 13 || data.toString().length === 12 ? data : data * 1000);
        const date = new Date(data);
        const years = date.getFullYear();
        const mounth = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        const dates = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        return `${years}-${mounth}-${dates}`;
      }
      return null;
    },

    convertTime(data) {
      if (data) {
        // const date = new Date(data.toString().length === 13 || data.toString().length === 12 ? data : data * 1000);
        const date = new Date(data);
        const hours = date.getHours();
        const minutes = date.getMinutes();
        return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
      }
      return null;
    },

    cekTypeData(key) {
      if (key === '') { return ''; }
      if (key === null) { return ''; }
      const findType = this.formData.columns[key];
      return findType.type;
    },

    submitData() {
      return new Promise((resolve, reject) => {
        if (this.fieldList.length > 0) {
          this.$refs.jFormCreate.getData().then((data) => {
            const param = {
              master: this.masterListFiled,
              value: data,
            };
            resolve(param);
          });
        } else resolve(null);
      });
    },

    submitDataNoMandatory() {
      return new Promise((resolve, reject) => {
        if (this.fieldList.length > 0) {
          this.$refs.jFormCreate.getDataNoMandatory().then((data) => {
            const param = {
              master: this.masterListFiled,
              value: data,
            };
            resolve(param);
          });
        } else resolve(null);
      });
    },
  },
};
</script>
