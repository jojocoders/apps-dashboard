/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jPolicyServices = {
  created() {
    this.getDetailPolicy();
  },
  methods: {
    ...mapActions('jStoreTimesheetConfigTaskPolicy', ['getPolicyDetail', 'createTaskPolicy']),

    getDetailPolicy() {
      this.getPolicyDetail();
    },

    sendDataSetting() {
      this.createTaskPolicy(this.generateDataSetting);
    },
  },

  computed: {
    ...mapState('jStoreTimesheetConfigTaskPolicy', ['policy_data']),

    generateDataSetting() {
      if (this.task_setting !== null) {
        const genData = {
          id: this.task_setting.id,
          name: this.task_setting.name,
          description: this.task_setting.description,
          use_attendance: this.task_setting.use_attendance,
          auto_approve: this.task_setting.auto_approved,
          period_setting: this.task_setting.periode,
          minimum_day_submit: this.task_setting.minimum_day,
          maximum_day_submit: this.task_setting.maximum_day,
        };
        return genData;
      }
      return null;
    },
  },

  watch: {
    'policy_data.detail_policy'(newVal) {
      if (newVal) {
        this.task_setting = {
          id: newVal.id,
          name: newVal.name,
          description: newVal.description,
          use_attendance: newVal.use_attendance,
          auto_approved: newVal.is_auto_approve,
          periode: newVal.period_setting,
          minimum_day: newVal.minimum_day_submit,
          maximum_day: newVal.maximum_day_submit,
        };
      }
    },
  },
};
