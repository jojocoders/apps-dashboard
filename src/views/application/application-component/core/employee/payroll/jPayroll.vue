<template>
  <div>
    <b-row>
      <b-col
        v-if="!isLoading"
        sm="12"
        md="9">
        <div v-if="payroll_id !== 0">
          <j-title-header :options="salaryOption"/>
          <j-salary
            @refresh-options="refreshOptions"/>
        </div>
        <div v-if="payroll_id !== 0">
          <j-title-header :options="taxOption"/>
          <j-tax
            @refresh-options="refreshOptions"/>
        </div>
        <!-- <div>
          <j-title-header :options="bankOption"/>
          <j-bank
            @refresh-options="refreshOptions"/>
        </div> -->
        <div v-if="payroll_id !== 0">
          <j-title-header :options="insuranceOption"/>
          <j-insurance
            :payroll-id="payrollId"
            @refresh-options="refreshOptions"/>
        </div>
        <!-- <div>
          <j-title-header :options="groupingOption"/>
          <j-grouping
            @refresh-options="refreshOptions"/>
        </div> -->
        <div>
          <j-title-header :options="stopPayment"/>
          <j-normal-stop
            @refresh-options="refreshOptions"/>
        </div>
        <!-- <div>
          <j-title-header :options="extendsRun"/>
          <j-extends-run-payroll
            @refresh-options="refreshOptions"/>
        </div> -->
      </b-col>

      <!-- <b-col
        sm="12"
        md="3">
        <div class="jHeaderTitle">
          <b-row>
            <b-col>
              <i class="fa fa-clock-o"/>
              <h4>Recent Activity</h4>
            </b-col>
          </b-row>
          <hr>
          <div class="w-100 text-center">
            <b-button
              class="btn blue"
              @click="openLogModal">
              Show Log
            </b-button>
          </div>
        </div>
      </b-col> -->

      <!-- <b-modal
        ref="logModal"
        class="mod-form modal-activity-log"
        centered>
        <div>
          <j-modal-activity-log
            v-if="isOpen"
            :data-log="activity_logs"
            :employee-id="data_employee.employee_id"
            :join-date="data_employee.join_date"
            @close-modal="closeLogModal"/>
        </div>
      </b-modal> -->
    </b-row>
  </div>
</template>

<script>
import { mapActions, mapState } from 'vuex';
import jApiEmployee from '@/services/jApiEmployee';
import jUserAPIManager from '@/services/payroll/jUserAPIManager';
import jSalary from './salary/jSalary';
import jTax from './tax/jTax';
import jBank from './bank/jBank';
import jInsurance from './insurance/jInsurance';
import jGrouping from './grouping/jGrouping';
import jNormalStop from './normal-stop/jNormalStop';
import jExtendsRunPayroll from './extends-run/jExtendsRunPayroll';
import jModalActivityLog from '../jModalActivityLog';

export default {
  name: 'JPayroll',
  components: {
    jSalary,
    jTax,
    jBank,
    jInsurance,
    jGrouping,
    jNormalStop,
    jExtendsRunPayroll,
    jModalActivityLog,
  },
  data() {
    return {
      isOpen: false,
      isLoading: true,
      data_employee: {
        employee_id: '',
        join_date: null,
      },
      refresh_relations: false,
      salaryOption: {
        main_title: 'Salary',
        opt_title: '',
      },
      taxOption: {
        main_title: 'Tax',
        opt_title: '',
      },
      bankOption: {
        main_title: 'Bank',
        opt_title: '',
      },
      insuranceOption: {
        main_title: 'Insurance',
        opt_title: '',
      },
      groupingOption: {
        main_title: 'Payroll Grouping',
        opt_title: '',
      },
      stopPayment: {
        main_title: 'Normal Stop Payment',
        opt_title: '',
      },
      extendsRun: {
        main_title: 'Extends Run Payroll',
        opt_title: '',
      },
      payroll_id: 0,
    };
  },
  computed: {
    ...mapState('jStoreEmployee', ['activity_logs', 'employee_detail']),
    ...mapState('jStoreAddPayroll', ['uiData', 'form', 'payrollId']),
  },
  watch: {
    employee_detail(newVal) {
      const employeeForm = {
        employee_id: newVal.employee_id,
        join_date: newVal.company.join_date && newVal.company.join_date !== '0000-00-00' ? newVal.company.join_date : null,
      };
      this.data_employee = employeeForm;
    },
    payrollId(val) {
      this.payroll_id = val;
    },
  },
  created() {
    const userCompanyId = {
      id: Number(this.$route.params.id),
    };
    jApiEmployee.detailEmployee(userCompanyId)
      .then((res) => {
        jUserAPIManager.requestGetUserDetailByUserId(res.user.user_id).then((val) => {
          this.uiData.payrollDetail = val.data.user;
          this.onLoadData(res.user.user_id);
        });
      });
  },
  methods: {
    ...mapActions('jStoreAddPayroll',
      [
        'onResetResponseAction',
        'requestReloadData',
        'reloadTaxList',
        'reloadBankList',
        'reloadBenefitList',
        'reloadAllowanceList',
        'reloadDeductionList',
        'reloadBonusList',
        'reloadOvertimeList',
        'reloadDefaultPayrollComponent',
        'requestUpdateData',
        'onShowBenefitDetail',
        'onShowAllowanceDetail',
        'onShowDeductionDetail',
        'onShowBonusDetail',
        'onShowOvertimeDetail',
        'addBenefit',
        'addAllowance',
        'addDeduction',
        'addBonus',
        'addOvertime',
        'updateBenefit',
        'updateAllowance',
        'updateDeduction',
        'updateBonus',
        'updateOvertime',
      ],
    ),

    refreshOptions(val) {
      this.refresh_relations = val;
    },
    async onLoadData(selectedId) {
      // eslint-disable-next-line
      this.uiData.selectedId = selectedId;
      this.form.user_id = selectedId;
      this.requestReloadData();
      this.reloadTaxList();
      // this.reloadBankList();
      this.reloadBenefitList();
      this.reloadAllowanceList();
      this.reloadDeductionList();
      this.reloadBonusList();
      this.reloadOvertimeList();
      this.reloadDefaultPayrollComponent();

      this.isLoading = false;
    },
    closeLogModal() {
      this.$refs.logModal.hide();
      this.isOpen = false;
    },
    openLogModal() {
      this.isOpen = true;
      this.$refs.logModal.show();
    },
  },
};
</script>
