/* eslint-disable import/no-unresolved,import/first */
import * as all from '@jojocoders/jojonomic-ui';
import { layout } from 'vue-extend-layout';
import Vue from 'vue';
import moment from 'moment';
import stores from './stores/index';
import Storage from 'vue-ls';
import router from './router';
import JsonExcel from 'vue-json-excel';
import jPageHeader from '@/components/page-header/jPageHeader';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import UUID from 'vue-uuid';
import VueViewer from 'v-viewer';
import VueSignature from 'vue-signature-pad';
import Editor from 'vue-editor-js/src/index';
import DatadogConfig from '@/configs/datadog';

// axios global
window.axios = require('axios');

// vendor
const keys = Object.keys(all);
keys.forEach((data) => {
  if (Array.isArray(all[data])) {
    Vue.use(...all[data]);
  } else {
    Vue.use(all[data]);
  }
});

DatadogConfig();

Vue.use(VueGoogleMaps, {
  load: {
    // apikey dev
    // key: 'AIzaSyDG-03nQ019lqTbZvBpdq7btPpaicwcUV8',
    key: process.env.GMAPS_KEY,
    libraries: '',
  },
});

Vue.use(VueLodash, { lodash });
Vue.use(UUID);
Vue.config.productionTip = false;

Vue.use(Storage);
Vue.use(VueSignature);
Vue.use(Editor);
Vue.component('downloadExcel', JsonExcel);
Vue.component('j-page-header', jPageHeader);

Vue.use(VueViewer);

Vue.filter('toCurrency', (value) => {
  if (typeof value !== 'number') {
    return value;
  }
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'IDR',
    minimumFractionDigits: 2,
  });
  return formatter.format(value);
});

Vue.filter('formatFullDateTime', value => moment(String(value)).format('MMMM DD, YYYY hh:mm:ss'));
Vue.filter('formatFullDate', value => moment(String(value)).format('MMMM DD, YYYY'));

// import this project scss
import './sass/main.scss';

// register global component here
// ex: Vue.component('header', Header)

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: stores,
  ...layout,
});
