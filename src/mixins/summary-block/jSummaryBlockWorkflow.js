/* eslint-disable max-len, no-param-reassign, no-plusplus, next-line, no-eval, no-unused-vars, no-lonely-if, quote-props, prefer-const, no-underscore-dangle, no-shadow */
export default {
  data() {
    return {
      isFirstLoad: true,
      showSummaryBlock: false,
      summaryBlockClass: '',
      summaryBlockNative: [
        'draft',
        'sent',
        'approved',
        'closed',
        'rejected',
        'canceled',
        'approved_by_me',
      ],
      defaultSummaryBlockRequester: [
        {
          key: 'draft',
          name: 'Draft',
          color: '#808285',
          type: 'first_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
        {
          key: 'sent',
          name: 'Sent',
          color: '#F36F21',
          type: 'middle_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
        {
          key: 'approved',
          name: 'Approved',
          color: '#42A5CE',
          type: 'middle_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
        {
          key: 'closed',
          name: 'Closed',
          color: '#0C9C68',
          type: 'last_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
        {
          key: 'rejected',
          name: 'Rejected',
          color: '#8A0D4F',
          type: 'block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
        {
          key: 'canceled',
          name: 'Canceled',
          color: '#4D3923',
          type: 'block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
      ],
      defaultSummaryBlockApprover: [
        {
          key: 'sent',
          name: 'Sent',
          color: '#F36F21',
          type: 'first_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        }, {
          key: 'approved_by_me',
          name: 'Approved By Me',
          color: '#42A5CE',
          type: 'middle_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        }, {
          key: 'approved',
          name: 'Approved',
          color: '#42A5CE',
          type: 'last_block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        }, {
          key: 'rejected',
          name: 'rejected',
          color: '#8A0D4F',
          type: 'block',
          is_hidden: false,
          filter: {},
          filter_operator: '',
          filter_function: {},
        },
      ],
      indexBlockSelected: 0,
      paramsFilter: {},
      paramsFilterFunction: [],
    };
  },

  methods: {
    setClassSummaryBlock(type) {
      switch (type) {
        case 'first_block':
          this.summaryBlockClass = 'sum-start';
          break;
        case 'middle_block':
          this.summaryBlockClass = 'sum-middle';
          break;
        case 'last_block':
          this.summaryBlockClass = 'sum-last';
          break;
        case 'block':
          this.summaryBlockClass = 'sum-alone-last';
          break;
        default:
          this.summaryBlockClass = 'sum-alone-last';
          break;
      }
      return this.summaryBlockClass;
    },

    setStyleSummary(data, color) {
      const styleColor = `--color:${color};`;
      const styleMargin = this.setStyleMargin(data);

      return `${styleColor}${styleMargin}`;
    },

    setStyleMargin(data) {
      let style = 'margin-left: 0px';
      if (this.index > 0) {
        const type = data[this.index].type;
        const blockBefore = data[this.index - 1].type;

        if (type === 'first_block') {
          if (blockBefore === 'first_block' || blockBefore === 'middle_block') {
            style = 'margin-left: 24px';
          }
        }
        if (type === 'block') {
          if (blockBefore === 'first_block' || blockBefore === 'middle_block') {
            style = 'margin-left: 36px';
          }
        }
      }

      return style;
    },

    isDefaultBlock(key) {
      let status = '';
      if (
        key === 'draft' || key === 'sent' ||
        key === 'approved' || key === 'approved_by_me' ||
        key === 'closed' || key === 'canceled' || key === 'rejected'
      ) {
        status = true;
      } else {
        status = false;
      }
      return status;
    },

    createMasterSummaryBlock(type) {
      let block = [];

      if (type === 'table-requester') {
        block = this.formUi.table_requester_view.summary_block ?
          this.formUi.table_requester_view.summary_block.map((e) => { e.count = 0; return e; }) : this.defaultSummaryBlockRequester;
      }
      if (type === 'table-approver') {
        block = this.formUi.table_approver_view.summary_block ?
          this.formUi.table_approver_view.summary_block.map((e) => { e.count = 0; return e; }) : this.defaultSummaryBlockApprover;
      }

      return block;
    },

    getDataSummary() {
      this.showSummaryBlock = false;
      this.getFilterFunction();
    },

    getFilterFunction() {
      this.masterSummaryBlock.forEach((summary) => {
        const filterFunction = {};
        if (summary.filter_function) {
          Object.entries(summary.filter_function).forEach((val) => {
            this.actionGetFunctionData(val[1]).then((res) => {
              filterFunction[val[0]] = res;
            });
          });
          this.paramsFilterFunction.push(filterFunction);
        } else {
          this.paramsFilterFunction.push(summary.filter_function);
        }
      });
    },

    getFilterParent() {
      let dataFilter = {};

      if (JSON.parse(this.element.info).filter_column) {
        let x;
        let keyFilter = '';
        const dataRecord = this.data_record;
        if (this.cekTypeData(JSON.parse(this.element.info).filter_column) === 'object') {
          keyFilter = `"${JSON.parse(this.element.info).filter_column}.id"`;
        } else {
          keyFilter = JSON.parse(this.element.info).filter_column;
        }
        dataFilter = eval(`x={ ${keyFilter}: dataRecord.${JSON.parse(this.element.info).filter_variable} }`);
      }

      const filterFunctionKeys = Object.keys(this.filterFunction);
      const filterFunctionValues = Object.values(this.filterFunction);

      filterFunctionKeys.forEach((e, index) => {
        dataFilter[e] = filterFunctionValues[index];
      });

      return dataFilter;
    },

    getCountSummary(type, dataFilter) {
      // const filterParent = this.getFilterParent();
      const params = {
        form_data_id: this.formDataId,
        filter: dataFilter,
        filter_operator: this.dataTable.filter_operator ? this.dataTable.filter_operator : 'and',
      };
      // if (Object.keys(filterParent).length > 0) {
      //   params.filter = filterParent;
      //   params.filter_operator = this.dataTable.filter_operator ? this.dataTable.filter_operator : 'and';
      // }

      const summaryBlock = this.masterSummaryBlock.filter(block => this.summaryBlockNative.includes(block.key));
      const func = type === 'requester' ? this.getDataSummaryBlock : this.getDataSummaryBlockApp;

      func(params).then((res) => {
        summaryBlock.forEach((summary, index) => {
          const getIndex = this.masterSummaryBlock.findIndex(f => f.key === summary.key);
          if (getIndex > -1) {
            this.masterSummaryBlock[getIndex].count = res.data[summary.key];
          }

          if (index === summaryBlock.length - 1) {
            this.getCountCustomSummary(type, dataFilter);
          }
        });
      });
    },

    getCountCustomSummary(type, dataFilter) {
      const summaryBlock = this.masterSummaryBlock.filter(block => !this.summaryBlockNative.includes(block.key));
      const func = type === 'requester' ? this.actionGetRecordRequesterCountV2 : this.actionGetRecordApproverCountV2;

      if (summaryBlock && summaryBlock.length > 0) {
        summaryBlock.forEach((summary, index) => {
          const getIndex = this.masterSummaryBlock.findIndex(f => f.key === summary.key);
          if (getIndex > -1) {
            this.clearDataFilterV2();
            this.setFilterBlockCustom(getIndex);
            const params = {
              form_data_id: this.formDataId,
              page: 1,
              limit: 1,
              // filter: combineFilter,
              sort: this.dataTable.sort[0],
              // filter_operator: this.setFilterOperatorBlock(summary.filter_operator),
              filter_app: this.setFilterAppV2(summary.filter_operator),
              filter_setup: this.setFilterSetupV2(summary.filter_operator),
              filter_setup_operator: summary.filter_operator ? summary.filter_operator : 'and',
              page_id: Number(this.$route.params.pageId),
            };

            func(params).then((res) => {
              this.masterSummaryBlock[getIndex].count = res;

              if (index === (summaryBlock.length - 1)) {
                setTimeout(() => {
                  this.showSummaryBlock = true;
                  this.isFirstLoad = false;
                }, 500);
              }
            });
          }
        });
      } else {
        setTimeout(() => {
          this.showSummaryBlock = true;
          this.isFirstLoad = false;
        }, 500);
      }
    },

    getFilterSummaryBlock(filters) {
      let x;
      let dataFilter = {};
      const _user = this.session.user;
      const _recordParent = this.data_record;
      const filterKeys = Object.keys(filters);
      const filterValues = Object.values(filters);

      if (filterValues.length > 0) {
        filterValues.forEach((e, index) => {
          if (e.includes('_user') || e.includes('_recordParent')) {
            dataFilter[filterKeys[index]] = eval(`x=${e}`);
          } else if (filterKeys[index].includes('.company_user_id')) {
            dataFilter[filterKeys[index]] = Number(e);
          } else if (e.includes('.today')) {
            const date = new Date();
            const startOfDay = new Date(Date.parse(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 00:00:00`)).getTime();
            const endOfDay = new Date(Date.parse(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 23:59:59`)).getTime();
            dataFilter[filterKeys[index]] = { '$between': [startOfDay, endOfDay] };
          } else {
            if (this.cekTypeData(filterKeys[index]) === 'number') {
              dataFilter[filterKeys[index]] = Number(e);
            } else {
              dataFilter[filterKeys[index]] = e.slice(1, -1);
            }
          }
        });
      }

      return dataFilter;
    },

    getData(index) {
      this.load_first = false;
      this.indexBlockSelected = index;
      this.status = this.masterSummaryBlock[index].key;
      this.getInitialHeaderData();
      this.cekIsUseFilterFunction();
    },

    setParams(dataFilter, a) {
      let param = {};
      const filterOperator = this.masterSummaryBlock[this.indexBlockSelected].filter_operator;

      if (this.isDefaultBlock(this.status)) {
        param = {
          form_data_id: this.formDataId,
          flow_status: this.status,
          page: this.isServerTable ? a.pagination.page : 1,
          limit: this.isServerTable ? a.pagination.limit : 1000000000,
          sort: this.columnSort ? this.columnSort : this.dataTable.sort[0],
          // filter: dataFilter,
          // filter_operator: this.dataTable.filter_operator ? this.dataTable.filter_operator : 'and',
          filter_app: this.setFilterAppV2(this.dataTable.filter_operator),
          filter_setup: this.setFilterSetupV2(this.dataTable.filter_operator),
          filter_setup_operator: this.dataTable.filter_operator ? this.dataTable.filter_operator : 'and',
          page_id: Number(this.$route.params.pageId),
        };
      } else {
        this.setFilterBlockCustom();
        param = {
          form_data_id: this.formDataId,
          page: this.isServerTable ? a.pagination.page : 1,
          limit: this.isServerTable ? a.pagination.limit : 1000000000,
          sort: this.columnSort ? this.columnSort : this.dataTable.sort[0],
          // filter: this.setFilterBlock(dataFilter),
          // filter_operator: this.setFilterOperatorBlock(filterOperator || 'and'),
          filter_app: this.setFilterAppV2(this.dataTable.filter_operator),
          filter_setup: this.setFilterSetupV2(this.dataTable.filter_operator),
          filter_setup_operator: this.dataTable.filter_operator ? this.dataTable.filter_operator : 'and',
          page_id: Number(this.$route.params.pageId),
        };
      }
      return param;
    },

    setFilterBlockCustom(index) {
      const selectedIndex = index || this.indexBlockSelected;
      let x;
      const _user = this.session.user;
      const _recordParent = this.data_record;

      const filterBlockKeys = Object.keys(this.masterSummaryBlock[selectedIndex].filter);
      const filterBlockValues = Object.values(this.masterSummaryBlock[selectedIndex].filter);
      if (filterBlockValues.length > 0) {
        filterBlockValues.forEach((e, index) => {
          if (e.includes('_user') || e.includes('_recordParent')) {
            this.combineFilterV2('Setup', filterBlockKeys[index], eval(`x=${e}`));
          } else if (filterBlockKeys[index].includes('.company_user_id')) {
            this.combineFilterV2('Setup', filterBlockKeys[index], Number(e));
          } else if (e.includes('.today')) {
            const date = new Date();
            const startOfDay = new Date(Date.parse(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 00:00:00`)).getTime();
            const endOfDay = new Date(Date.parse(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 23:59:59`)).getTime();
            this.combineFilterV2('Setup', filterBlockKeys[index], [startOfDay, endOfDay]);
          } else {
            if (this.cekTypeData(filterBlockKeys[index]) === 'number') {
              this.combineFilterV2('Setup', filterBlockKeys[index], Number(e));
            } else {
              this.combineFilterV2('Setup', filterBlockKeys[index], e.slice(1, -1));
            }
          }
        });
      }

      const filterFuncKeys = Object.keys(this.paramsFilterFunction[selectedIndex]);
      const filterFuncValues = Object.values(this.paramsFilterFunction[selectedIndex]);
      if (filterFuncKeys.length > 0) {
        filterFuncKeys.forEach((e, index) => {
          this.combineFilterV2('Setup', e, filterFuncValues[index]);
        });
      }
    },

    setFilterBlock(dataFilter) {
      const baseFilter = dataFilter;
      const filter = this.masterSummaryBlock[this.indexBlockSelected].filter;
      const filterFunction = this.paramsFilterFunction[this.indexBlockSelected];
      const combineFilter = {
        ...baseFilter,
        ...this.getFilterSummaryBlock(filter),
        ...filterFunction,
      };
      return combineFilter;
    },

    setFilterOperatorBlock(filterOperator) {
      let filter = '';

      if (filterOperator) {
        filter = filterOperator;
      } else if (!filterOperator) {
        filter = this.dataTable.filter_operator ? this.dataTable.filter_operator : 'and';
      }

      return filter;
    },

    updateCountBlock(count) {
      if (!this.isFirstLoad && this.masterSummaryBlock.length > 0) {
        this.masterSummaryBlock[this.indexBlockSelected].count = count;
        this.showSummaryBlock = false;

        this.$nextTick(() => {
          this.showSummaryBlock = true;
        });
      }
    },
  },
};

