/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export default {
  computed: {
    ...mapState('jFlow', ['detail_sla']),
  },
  created() {
    if ((this.detail_sla.sla && this.detail_sla.sla.length > 0) ||
      (this.detail_sla.duration && String(this.detail_sla.duration).length > 0)) {
      this.mode = 'edit';
    }

    if (this.mode === 'edit') {
      const slaData = this.detail_sla.sla ? [...this.detail_sla.sla] : [];
      const mapSLA = this.flow_sla.sla.map((data) => {
        let store = data;
        slaData.forEach((sla) => {
          if (sla.type === data.type) {
            store = {
              id: sla.id,
              is_check: true,
              value: sla.value,
              type: sla.type,
              is_delete: false,
            };
          }
        });
        return store;
      });

      const dataEdit = {
        duration: this.detail_sla.duration,
        sla: mapSLA,
      };

      this.flow_sla = dataEdit;
    }
  },
};
