/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModeCreateFlowType2 = {
  methods: {
    ...mapActions('jOrganigram', ['getAllDivisions']),
    modeCreate() {
      const getAll = {
        pagination: {
          limit: 0,
          page: 0,
          column: 'id',
          ascending: false,
          query: '',
        },
      };

      // get list divisions
      this.getAllDivisions(getAll);
    },
  },
  computed: {
    ...mapState('jOrganigram', ['all_division_list']),
  },
};
