/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModeEditFlowType2 = {
  methods: {
    ...mapActions('jOrganigram', ['getAllDivisions']),
    ...mapActions('jFlows', ['getDetailFlow']),

    modeEdit() {
      const getAll = {
        pagination: {
          limit: 0,
          page: 0,
          column: 'id',
          ascending: false,
          query: '',
        },
      };

      // get list divisions
      this.getAllDivisions(getAll);

      // get detail Flow
      this.getDetailFlow({ id: Number(this.$route.params.id) });
    },
  },
  computed: {
    ...mapState('jFlows', ['detail_flow']),
    default_division() {
      if (this.detail_flow !== null) {
        return this.detail_flow.division;
      }
      return null;
    },
  },
  watch: {
    detail_flow(newVal) {
      const detailFlow = {
        id: newVal.id,
        name: newVal.name,
        division: newVal.division_id ? { id: newVal.division_id } : null,
      };
      this.flow = detailFlow;
    },
  },
};
