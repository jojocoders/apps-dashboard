import NodeRSA from 'node-rsa';
import { mapActions } from 'vuex';

export default {
  data() {
    return {
      publicKey: '',
      privateKey: '',
      nsRSA: new NodeRSA(),
      decryptData: '',
      encryptData: '',

    };
  },

  methods: {
    ...mapActions('jStoreLogin', ['getKeyPublicRSA']),

    async setKeyPublicRSA() {
      const res = await this.getKeyPublicRSA();

      if (res && !res.error) {
        this.publicKey = atob(res.data.key);
        this.nsRSA.importKey(this.publicKey, 'pkcs8-public-pem');
        this.nsRSA.setOptions({ encryptionScheme: 'pkcs1' });
      }
    },

    async encryption(str) {
      const result = await this.nsRSA.encrypt(str, 'base64');
      return result;
    },

    async decryption(str) {
      const result = await this.nsRSA.decrypt(str);
      return result;
    },
  },

};
