export default {
  data() {
    return {
      initialHeight: 0,
      container: '',
      isGetValue: false,
    };
  },

  computed: {
    containerHeight: {
      get() {
        return this.initialHeight;
      },
      set(newValue) {
        this.initialHeight = newValue;
      },
    },
  },

  watch: {
    formData(val) {
      if (val) {
        setTimeout(() => {
          this.containerHeight = this.$refs.containerFormData.offsetHeight - 23;
        }, 500);
      }
    },
    coattendace_detail(val) {
      if (val) {
        setTimeout(() => {
          this.containerHeight = this.$refs.containerFormData.offsetHeight - 23;
        }, 500);
      }
    },
  },
};
