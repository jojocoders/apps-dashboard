/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState } from 'vuex';

export const jMixOptionsFormFlowType2 = {
  created() {
    if (this.mode === 'create') {
      this.modeCreate();
    } else if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
};
