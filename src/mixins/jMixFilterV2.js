/* eslint-disable max-len, no-param-reassign, no-plusplus, no-console, no-eval, no-unused-vars, no-lonely-if, quote-props, prefer-const, no-underscore-dangle, no-shadow */
export default {
  data() {
    return {
      dataFilterSetup: {
        whereIs: {},
        whereLike: {},
        whereIn: {},
        whereBetween: {},
        whereGt: {},
        whereLt: {},
      },
      dataFilterApp: {
        whereIs: {},
        whereLike: {},
        whereIn: {},
        whereBetween: {},
        whereGt: {},
        whereLt: {},
      },
    };
  },

  methods: {
    combineFilterV2(type, key, value) {
      // const keys = key.replace('.id', '');
      if (this.cekTypeData(key) === 'timestamp') {
        this[`dataFilter${type}`].whereBetween[key.replaceAll('"', '')] = value;
      } else if (Array.isArray(value)) {
        this[`dataFilter${type}`].whereIn[key.replaceAll('"', '')] = value;
      } else if (key.includes('.id')) {
        this[`dataFilter${type}`].whereIs[key.replaceAll('"', '')] = value;
      } else if (key.includes('.name')) {
        this[`dataFilter${type}`].whereLike[key.replaceAll('"', '')] = value;
      } else if (this.cekIsRegex(key)) {
        this[`dataFilter${type}`].whereLike[key.replaceAll('"', '')] = value;
      } else {
        this[`dataFilter${type}`].whereIs[key.replaceAll('"', '')] = value;
      }
    },

    combineFilterGT(type, key, value) {
      this[`dataFilter${type}`].whereGt[key.replaceAll('"', '')] = value;
    },

    combineFilterLT(type, key, value) {
      this[`dataFilter${type}`].whereLt[key.replaceAll('"', '')] = value;
    },

    setFilterAppV2(operator) {
      const filterApp = {};
      if (Object.keys(this.dataFilterApp.whereIs).length > 0) {
        filterApp[`where_is_${operator || 'and'}`] = this.dataFilterApp.whereIs;
      }
      if (Object.keys(this.dataFilterApp.whereLike).length > 0) {
        filterApp[`where_like_${operator || 'and'}`] = this.dataFilterApp.whereLike;
      }
      if (Object.keys(this.dataFilterApp.whereIn).length > 0) {
        filterApp[`where_in_${operator || 'and'}`] = this.dataFilterApp.whereIn;
      }
      if (Object.keys(this.dataFilterApp.whereBetween).length > 0) {
        filterApp[`where_between_${operator || 'and'}`] = this.dataFilterApp.whereBetween;
      }
      if (Object.keys(this.dataFilterApp.whereGt).length > 0) {
        filterApp.where_gt_and = this.dataFilterApp.whereGt;
      }
      if (Object.keys(this.dataFilterApp.whereLt).length > 0) {
        filterApp.where_lt_and = this.dataFilterApp.whereLt;
      }
      return filterApp;
    },

    setFilterSetupV2(operator) {
      const filterSetup = {};
      if (Object.keys(this.dataFilterSetup.whereIs).length > 0) {
        filterSetup[`where_is_${operator || 'and'}`] = this.dataFilterSetup.whereIs;
      }
      if (Object.keys(this.dataFilterSetup.whereLike).length > 0) {
        filterSetup[`where_like_${operator || 'and'}`] = this.dataFilterSetup.whereLike;
      }
      if (Object.keys(this.dataFilterSetup.whereIn).length > 0) {
        filterSetup[`where_in_${operator || 'and'}`] = this.dataFilterSetup.whereIn;
      }
      if (Object.keys(this.dataFilterSetup.whereBetween).length > 0) {
        filterSetup[`where_between_${operator || 'and'}`] = this.dataFilterSetup.whereBetween;
      }
      if (Object.keys(this.dataFilterSetup.whereGt).length > 0) {
        filterSetup.where_gt_and = this.dataFilterSetup.whereGt;
      }
      if (Object.keys(this.dataFilterSetup.whereLt).length > 0) {
        filterSetup.where_lt_and = this.dataFilterSetup.whereLt;
      }
      return filterSetup;
    },

    clearDataFilterV2() {
      this.dataFilterSetup = {
        whereIs: {},
        whereLike: {},
        whereIn: {},
        whereBetween: {},
        whereGt: {},
        whereLt: {},
      };
      this.dataFilterApp = {
        whereIs: {},
        whereLike: {},
        whereIn: {},
        whereBetween: {},
        whereGt: {},
        whereLt: {},
      };
    },
  },
};
