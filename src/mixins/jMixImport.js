/* eslint-disable no-console, no-unused-vars, max-len, no-param-reassign */
import axios from 'axios';
import Vue from 'vue';
import { mapActions } from 'vuex';

export default {
  data() {
    return {
      onProgressImport: false,
      popoverImportShow: false,
      rerenderInputFile: true,
      advanceReportSetting: false,
      isOpenModalProgressImport: false,
      isOpenModalActivityImport: false,
      fileImport: null,
      fileNameImport: null,
      cancelProgressImport: null,
      fileProgressImport: 0,
      dataActivityImport: [],
    };
  },
  watch: {
    isShowImportPopover(val) {
      if (!val) this.toggleModalActivity(false);
    },
  },
  computed: {
    isUseAdvanceReport() {
      return this.formUi.import_view ? this.advanceReportSetting && this.formUi.import_view.is_use_advance_report : false;
    },
  },
  methods: {
    ...mapActions('jStoreFormUi', ['actionCancelImportActivity']),

    async getDataAdvanceReport() {
      const { data, error } = await this.actionGetUserCompanySetting(false);
      if (!error) this.advanceReportSetting = !!data.advance_report;
    },

    downloadTemplate() {
      this.$store.dispatch('jStoreNotificationScreen/showLoading', 'Please wait');

      const token = Vue.ls.get('Token');
      const gateUrl = process.env.GATE_URL;
      const dataColumn = [];

      if (this.formUi.import_view && this.formUi.import_view.columns) {
        this.formUi.import_view.columns.forEach((e) => {
          dataColumn.push({ [e.key]: e.name });
        });
      }
      const dataColumnExport = { header: dataColumn };

      if (this.isMode === 'external_url') {
        const url = this.formUi.data_source.external_url.export.url;
        axios.get(url, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          responseType: 'blob',
        }).then((res) => {
          this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');

          const a = document.createElement('a');
          const urlBlob = window.URL.createObjectURL(res.data);
          const typeFile = this.formUi.import_view.format_file || 'xls';
          const fileName = `${this.formUi.import_view.title}.${typeFile}`;

          a.href = urlBlob;
          a.download = fileName;
          a.click();
        });
      } else if (this.isUseAdvanceReport) {
        const typeForm = this.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id';
        let url = `${gateUrl}nocode-import-multipart/import/${typeForm}/${this.formUi.id}/template`;
        if (this.delimiterSelected) url = `${url}?delimiter=${this.delimiterSelected.id}`;

        axios.get(url, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          responseType: 'blob',
        }).then((res) => {
          this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');

          const a = document.createElement('a');
          const urlBlob = window.URL.createObjectURL(res.data);
          const typeFile = this.formUi.import_view.format_file || 'xls';
          const fileName = `${this.formUi.import_view.title}.${typeFile}`;

          a.href = urlBlob;
          a.download = fileName;
          a.click();
        });
      } else {
        const url = `${gateUrl}nocode/record/import/template/excel`;
        const data = {
          form_data_id: this.formDataId,
          page: 1,
          limit: 0,
        };
        if (this.isMultiple) data.multiple_form_ui_id = this.formUi.id;
        else data.form_ui_id = this.formUi.id;

        axios.post(url, data, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          responseType: 'blob',
        }).then((res) => {
          this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');

          const a = document.createElement('a');
          const urlBlob = window.URL.createObjectURL(res.data);
          const typeFile = this.formUi.import_view.format_file || 'xls';
          const fileName = `${this.formUi.import_view.title}.${typeFile}`;

          a.href = urlBlob;
          a.download = fileName;
          a.click();
        });
      }
    },

    handleFileUpload(file) {
      if (this.isUseAdvanceReport) this.importDataAdvance(file);
      else this.importData(file);
    },

    importData(file) {
      this.$store.dispatch('jStoreNotificationScreen/showLoading', 'Please wait');

      const token = Vue.ls.get('Token');
      const gateUrl = process.env.GATE_URL;
      const formData = new FormData();
      formData.append('fileupload', file[0]);

      let url;
      if (this.isMode === 'external_url') {
        url = this.formUi.data_source.external_url.import.url;
      } else {
        const typeForm = this.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id';
        url = `${gateUrl}nocode/record/import?form_data_id=${this.formDataId}&${typeForm}=${this.formUi.id}`;
      }

      axios.post(url, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`,
        },
      },
      ).then(() => {
        this.popoverImportShow = false;
        this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');
        this.$store.dispatch('jStoreNotificationScreen/toggleSuccess', 'Success Import Data');
        this.successImport();
      }).catch(() => {
        this.popoverImportShow = false;
        this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');
        this.$store.dispatch('jStoreNotificationScreen/toggleProblem', 'Error Import Data');
      });
    },

    importDataAdvance(file) {
      const sizeFile = file[0].size;
      const nameFile = file[0].name;
      const dotPosition = nameFile.lastIndexOf('.') + 1;
      const extension = nameFile.substring(dotPosition);

      if (extension !== 'csv') {
        this.$store.dispatch('jStoreNotificationScreen/toggleProblem', 'Format file must be csv');
      } else if (sizeFile > 104857487) {
        this.$store.dispatch('jStoreNotificationScreen/toggleProblem', `${nameFile} is too large`);
      } else {
        this.toggleModalProgress(true);
        this.onProgressImport = true;
        this.popoverImportShow = false;
        this.fileNameImport = nameFile.replace(`.${extension}`, '');

        // eslint-disable-next-line
        const _this = this
        const token = Vue.ls.get('Token');
        const formData = new FormData();
        const axiosSource = axios.CancelToken.source();
        const typeForm = this.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id';
        const url = `/nocode-import-multipart/import/${typeForm}/${this.formUi.id}`;

        this.cancelProgressImport = { cancel: axiosSource.cancel };
        formData.append('fileupload', file[0]);
        if (this.delimiterSelected) formData.append('delimiter', this.delimiterSelected.id);

        const config = {
          // eslint-disable-next-line
          onUploadProgress: function(progressEvent) {
            const percentCompleted = Math.round((progressEvent.loaded / progressEvent.total) * 100);
            _this.fileProgressImport = percentCompleted;
          },
          cancelToken: axiosSource.token,
        };

        const api = axios.create({
          baseURL: process.env.GATE_URL,
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`,
          },
        });

        api.post(url, formData, config).then(() => {
          this.onProgressImport = false;
          setTimeout(() => {
            this.toggleModalProgress(false);
            this.successImport();
          }, 2000);
        }).catch((error) => {
          this.$store.dispatch('jStoreNotificationScreen/toggleProblem', error.response.data.message);
          this.onProgressImport = false;
          setTimeout(() => { this.toggleModalProgress(false); }, 1500);
        });
      }
    },

    seeActivityImport() {
      const token = Vue.ls.get('Token');
      const gateUrl = process.env.GATE_URL;
      const typeForm = this.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id';
      const url = `${gateUrl}nocode-import-multipart/import/${typeForm}/${this.formUi.id}/monitoring`;

      axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
      ).then((res) => {
        this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');
        this.toggleModalActivity(true);
        this.popoverImportShow = false;
        this.dataActivityImport = res.data.data;
      }).catch((error) => {
        this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');
        this.$store.dispatch('jStoreNotificationScreen/toggleProblem', error.response.data.message);
      });
    },

    handleCancelImport(id) {
      this.$swal({
        title: 'Confirmation',
        text: 'Are you sure want to cancel this activity?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          const params = {
            formId: this.formUi.id,
            importId: id,
            isMultiple: this.isMultiple,
          };
          this.actionCancelImportActivity(params);
          this.isShowImportPopover = false;
        } else {
          // eslint-disable-next-line no-throw-literal
          throw {};
        }
      });
    },

    actionCancelProgressImport() {
      this.onProgressImport = false;
      this.cancelProgressImport.cancel();
      setTimeout(() => { this.toggleModalProgress(false); }, 1500);
    },

    downloadDataFails(data) {
      const token = Vue.ls.get('Token');
      const gateUrl = process.env.GATE_URL;
      const typeForm = this.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id';
      const url = `${gateUrl}nocode-import-multipart/import/${typeForm}/${this.formUi.id}/download-error-records/${data.import_id}`;

      axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        responseType: 'blob',
      },
      ).then((res) => {
        this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');

        const a = document.createElement('a');
        const urlBlob = window.URL.createObjectURL(res.data);
        const fileName = data.file_name;

        a.href = urlBlob;
        a.download = fileName;
        a.click();
      }).catch((error) => {
        if (
          error.request.responseType === 'blob' &&
          error.response.data instanceof Blob &&
          error.response.data.type &&
          error.response.data.type.toLowerCase().indexOf('json') !== -1
        ) {
          new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => {
              error.response.data = JSON.parse(reader.result);
              this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');
              this.$store.dispatch('jStoreNotificationScreen/toggleProblem', error.response.data.message);
              resolve(Promise.reject(error));
            };
            reader.onerror = () => { reject(error); };
            reader.readAsText(error.response.data);
          }).then((err) => {
            this.$store.dispatch('jStoreNotificationScreen/hideLoading', '');
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', err.response.data);
          });
        }
      });
    },

    toggleModalProgress(status) {
      if (status) {
        this.isOpenModalProgressImport = true;
        setTimeout(() => {
          const element = document.getElementById('modalProgressImport');
          if (element) element.classList.add('show');
        }, 200);
      } else {
        const element = document.getElementById('modalProgressImport');
        if (element) element.classList.remove('show');

        setTimeout(() => {
          this.isOpenModalProgressImport = false;
        }, 200);
      }
    },

    toggleModalActivity(status) {
      if (status) {
        this.isOpenModalActivityImport = true;
        setTimeout(() => {
          const element = document.getElementById('modalActivityImport');
          if (element) element.classList.add('show');
        }, 200);
      } else {
        const element = document.getElementById('modalActivityImport');
        if (element) element.classList.remove('show');

        setTimeout(() => {
          this.isOpenModalActivityImport = false;
        }, 200);
      }
    },

    clearInputFile() {
      this.rerenderInputFile = false;
      setTimeout(() => { this.rerenderInputFile = true; }, 500);
    },
  },
};
