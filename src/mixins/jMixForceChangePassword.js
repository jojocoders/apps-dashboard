export default {
  computed: {
    reminderPassImg() {
      // eslint-disable-next-line
      return require('@/assets/img/ico-reminder-pass.svg');
    },
  },
  methods: {
    openReminderPass() {
      const vm = this;
      this.$swal({
        title: '<strong style="font-size:16px; color:#606060">Hello there!</strong>',
        html: `<span style="font-size:14px">Looks like you never change your password,
        <br>let’s change the password to keep your account safe!</span>`,
        showCancelButton: true,
        imageUrl: this.reminderPassImg,
        confirmButtonColor: '#1A85E4',
        reverseButtons: true,
        confirmButtonText: 'Change Password',
        cancelButtonText: 'Later',
        allowOutsideClick: false,
        allowEscapeKey: false,
        cancelButtonClass: 'button-cancel-reminder-pass',
        confirmButtonClass: 'button-confirm-reminder-pass',
      }).then(async (result) => {
        if (result.value || !result.value) {
          if (!result.dismiss) {
            vm.$router.push('/profile?action=change-password');
          }
        }
      });
    },
  },
};
