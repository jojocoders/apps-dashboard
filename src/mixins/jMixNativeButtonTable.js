/* eslint-disable max-len */
export default {
  computed: {
    addNativeButtonLabel() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.add ? this.dataTable.config_native_button_table.add.label : 'Add';
    },

    importNativeButtonLabel() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.import ? this.dataTable.config_native_button_table.import.label : 'Import';
    },

    exportNativeButtonLabel() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.export ? this.dataTable.config_native_button_table.export.label : 'Export';
    },

    filterNativeButtonLabel() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.filter ? this.dataTable.config_native_button_table.filter.label : 'Filter';
    },

    groupNativeButtonLabel() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.group ? this.dataTable.config_native_button_table.group.label : 'Group';
    },

    isShowAddNativeButton() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.add ? !this.dataTable.config_native_button_table.add.is_hidden : true;
    },

    isShowExportNativeButton() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.export ? !this.dataTable.config_native_button_table.export.is_hidden : true;
    },

    isShowImportNativeButton() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.import ? !this.dataTable.config_native_button_table.import.is_hidden : true;
    },

    isShowFilterNativeButton() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.filter ? !this.dataTable.config_native_button_table.filter.is_hidden : true;
    },

    isShowGroupNativeButton() {
      return this.dataTable && this.dataTable.config_native_button_table && this.dataTable.config_native_button_table.group ? !this.dataTable.config_native_button_table.group.is_hidden : true;
    },
  },
};
