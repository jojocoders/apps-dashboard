/* eslint-disable max-len, no-unused-vars, no-eval, no-underscore-dangle */
import Vue from 'vue';
import axios from 'axios';

export default {
  data() {
    return {
      showTagFilter: false,
      masterTagFilter: [],
      indexTagActive: 0,
      styleTagFilter: '',
    };
  },

  methods: {
    actionGetMasterTagFilter(functionName, payload) {
      const token = Vue.ls.get('Token');
      const gateUrl = localStorage.getItem('gateUrl');

      const url = `${gateUrl}nocode/function?function_name=${functionName}`;
      const body = payload;

      return axios.post(url, body, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }).then(res => res.data);
    },
  },
};
