/* eslint-disable max-len */
export default {
  data() {
    return {
      summaryData: null,
      summaryList: [],
      summaryDataFormat: 'text',
      isOpenViewDetails: false,
      selected_rows_summary_list: [],
      show_blank_footer: false,
      selectedColumnSummaryData: null,
    };
  },

  computed: {
    isSummaryListIncludeTimerField() {
      return this.summaryList.map(e => this.cekTypeData(e.key) === 'timer').includes(true);
    },
  },

  watch: {
    isOpenViewDetails(val) {
      if (val) {
        const floatingElement = document.getElementsByClassName('show-floating')[0];
        if (floatingElement) {
          floatingElement.classList.add('show-detail');
        }
      } else {
        this.selected_rows = this.selected_rows_summary_list;
        const floatingElement = document.getElementsByClassName('show-floating')[0];
        if (floatingElement) {
          floatingElement.classList.remove('show-detail');
        }
      }
    },

    selected_rows_summary_list(val) {
      if (val && val.length) {
        if (this.isSummaryListIncludeTimerField) {
          this.selected_rows_summary_list.forEach((row, index) => {
            const targetedRow = this.data_table.find(data => data.id === row.id);

            if (targetedRow) {
              this.selected_rows_summary_list[index] = targetedRow;
            }
          });
        }
        this.summaryData = this.calculateSummaryData(this.selected_rows_summary_list);
      }
      if (val && val.length === 0) {
        this.summaryData = 0;
      }
    },
  },

  methods: {
    setPreviewData(config, tableColumns) {
      // Set Summary Data
      if (this.isShowSummaryData) {
        this.selectedColumnSummaryData = config.summary_data;

        if (Object.hasOwn(this.formData.columns, this.selectedColumnSummaryData) && this.formData.columns[this.selectedColumnSummaryData].type === 'number') {
          const targetColumn = tableColumns.find(i => i.key === this.selectedColumnSummaryData);
          if (targetColumn && targetColumn.type === 'currency') this.summaryDataFormat = 'currency';

          this.summaryData = this.calculateSummaryData(this.selected_rows);
        }
      }

      // Set Summary List
      if (this.isShowSummaryList) {
        if (this.summaryListType === 'custom') {
          this.summaryList = this.dataTable.config_preview_data.summary_list;
        } else {
          this.summaryList = tableColumns;
        }
      }
    },

    openViewDetail() {
      this.isOpenViewDetails = !this.isOpenViewDetails;
    },

    updateSelectedRows(val) {
      this.selected_rows_summary_list = val;
    },

    closeFloating() {
      this.isOpenViewDetails = false;
      this.selected_rows_summary_list = [];
      this.selected_rows = [];
    },

    calculateSummaryData(data) {
      return data.reduce((prev, current) => {
        if (current[this.selectedColumnSummaryData] === undefined) {
          return prev + 0;
        }
        return prev + current[this.selectedColumnSummaryData];
      }, 0);
    },
  },
};
