/* eslint-disable func-names */
export default {
  data() {
    return {
      useEditorCode: false,
      typeUseToggleEditorCode: [
        'text', 'number', 'number_decimal', 'date',
        'time', 'date_time', 'single_selection',
        'relation_record', 'currency', 'single_attachment',
        'radio', 'checkbox', 'toggle', 'password',
        'relation_record_external_api',
      ],
    };
  },

  computed: {
    isUseToggleEditorCode() {
      return this.typeUseToggleEditorCode.includes(this.dataSetup.type.id);
    },
  },

  watch: {
    'dataSetup.isValueChange': function (val) {
      if (val) {
        if (this.isUseToggleEditorCode) this.useEditorCode = true;
      } else {
        this.useEditorCode = false;
      }
    },
  },
};

