/* eslint-disable max-len, no-unused-vars, no-eval, no-underscore-dangle */
import Vue from 'vue';
import axios from 'axios';

export default {
  data() {
    return {
      selectedNativeButton: null,
      nativeButtonType: '',
    };
  },

  methods: {
    async checkConfirmationMessage(data, isApproveUpdate, params) {
      if (this.selectedNativeButton && this.selectedNativeButton.is_use_confirmation_message) {
        if (this.selectedNativeButton.dynamic_confirmation_message) {
          let payload;
          if (this.nativeButtonType === 'bulk_update' || this.nativeButtonType === 'bulk_delete' || this.nativeButtonType === 'submit_multiple') {
            payload = data;
          } else {
            payload = {
              form_data_id: this.nativeButtonType === 'create' ? '' : data.form_data_id,
              record_id: this.nativeButtonType === 'create' ? '' : data.data.ids,
              data: data.data || null,
              form_type: data.multiple_form_ui_id ? 'multiple_form_ui_id' : 'form_ui_id',
              form_type_id: data.multiple_form_ui_id || data.form_ui_id,
              type: this.nativeButtonType === 'update_req' ? 'update' : this.nativeButtonType,
            };
          }

          const responseFunction = await this.getValueFunction(this.selectedNativeButton.dynamic_confirmation_message, payload);

          if (responseFunction && responseFunction.confirmation_message) {
            this.handleConfirmationMessage(responseFunction.message, data, isApproveUpdate, params);
          } else {
            this.handleNativeButton(data, isApproveUpdate, params);
          }
        } else {
          const message = this.selectedNativeButton.custom_confirmation_message || 'Are you sure want to continue this action?';

          this.handleConfirmationMessage(message, data, isApproveUpdate, params);
        }
      } else {
        this.handleNativeButton(data, isApproveUpdate, params);
      }
    },

    setSubmitNativeButtonMultiple(data) {
      this.selectedNativeButton = this.nativeButtonsSubmit;
      this.nativeButtonType = 'submit_multiple';
      this.checkConfirmationMessage(data);
    },

    getValueFunction(functionName, payload) {
      const token = Vue.ls.get('Token');
      const gateUrl = localStorage.getItem('gateUrl');

      let url;
      let body;
      if (payload.type) {
        url = `${gateUrl}nocode/function?form_type=${payload.form_type}&form_type_id=${payload.form_type_id}&record_id=${payload.record_id}&button=${payload.type}`;
        body = payload.data || {};
      } else {
        url = `${gateUrl}nocode/function?function_name=${functionName}`;
        body = payload;
      }

      return axios.post(url, body, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }).then(res => res.data);
    },

    handleConfirmationMessage(message, data, isApproveUpdate, params) {
      this.$swal({
        title: 'Confirmation',
        text: message,
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value && this.nativeButtonType === 'create') {
          this.submitedRecord(data);
        } else if (result.value && this.nativeButtonType === 'draft') {
          this.savedDraftReqRecord(data);
        } else if (result.value && this.nativeButtonType === 'update') {
          this.submitedAddRecord(data, isApproveUpdate);
        } else if (result.value && this.nativeButtonType === 'submit') {
          this.submitedReqRecord(data, params);
        } else if (result.value && this.nativeButtonType === 'submit_manual') {
          this.submitedReqRecordDynamic(data, params);
        } else if (result.value && this.nativeButtonType === 'submit_multiple') {
          this.submitReqRecord();
        } else if (result.value && this.nativeButtonType === 'update_req') {
          this.savedDraftRecord(data);
        } else if (result.value && this.nativeButtonType === 'approve') {
          this.approveRecordApp(data);
        } else if (result.value && (this.nativeButtonType === 'bulk_update' || this.nativeButtonType === 'bulk_delete')) {
          this.$refs.progressUpdate.toggleIsVisible();
          this.isOpenViewDetails = false;
        } else {
          // eslint-disable-next-line no-throw-literal
          throw {};
        }
      });
    },

    handleNativeButton(data, isApproveUpdate, params) {
      if (this.nativeButtonType === 'create') {
        this.submitedRecord(data);
      } else if (this.nativeButtonType === 'draft') {
        this.savedDraftReqRecord(data);
      } else if (this.nativeButtonType === 'update') {
        this.submitedAddRecord(data, isApproveUpdate);
      } else if (this.nativeButtonType === 'submit') {
        this.submitedReqRecord(data, params);
      } else if (this.nativeButtonType === 'submit_manual') {
        this.submitedReqRecordDynamic(data, params);
      } else if (this.nativeButtonType === 'submit_multiple') {
        this.submitReqRecord();
      } else if (this.nativeButtonType === 'update_req') {
        this.savedDraftRecord(data);
      } else if (this.nativeButtonType === 'approve') {
        this.approveRecordApp(data);
      } else if (this.nativeButtonType === 'bulk_update') {
        this.$swal({
          title: 'Submit Record',
          text: 'Are you sure want to update this record?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.dismiss) {
            // eslint-disable-next-line
            throw {};
          } else {
            this.$refs.progressUpdate.toggleIsVisible();
            this.isOpenViewDetails = false;
          }
        });
      } else if (this.nativeButtonType === 'bulk_delete') {
        this.$swal({
          title: 'Delete Record',
          text: 'Are you sure want to delete this record?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.dismiss) {
            // eslint-disable-next-line
              throw {};
          } else {
            this.$refs.progressUpdate.toggleIsVisible();
            this.isOpenViewDetails = false;
          }
        });
      }
    },

    async checkConfirmationMessageExternalApi(method, url, params, token, primaryKey, formUi, dataRecord) {
      if (this.selectedNativeButton && this.selectedNativeButton.is_use_confirmation_message) {
        if (this.selectedNativeButton.dynamic_confirmation_message) {
          const payload = {
            record_id: this.nativeButtonType === 'create' ? '' : primaryKey,
            data: dataRecord || null,
            form_type: typeof formUi.tabs !== 'undefined' ? 'multiple_form_ui_id' : 'form_ui_id',
            form_type_id: formUi.id,
            type: this.nativeButtonType,
          };
          const responseFunction = await this.getValueFunction(this.selectedNativeButton.dynamic_confirmation_message, payload);

          if (responseFunction && responseFunction.confirmation_message) {
            this.handleConfirmationMessageExternalApi(responseFunction.message, method, url, params, token);
          } else {
            this.handleNativeButtonExternalApi(method, url, params, token);
          }
        } else {
          const message = this.selectedNativeButton.custom_confirmation_message || 'Are you sure want to continue this action?';

          this.handleConfirmationMessageExternalApi(message, method, url, params, token);
        }
      } else {
        this.handleNativeButtonExternalApi(method, url, params, token);
      }
    },

    handleConfirmationMessageExternalApi(message, method, url, params, token) {
      this.$swal({
        title: 'Confirmation',
        text: message,
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value && (this.nativeButtonType === 'create' || this.nativeButtonType === 'update')) {
          this.submitAddRecordExternalApi(method, url, params, token);
        }
      });
    },

    handleNativeButtonExternalApi(method, url, params, token) {
      if (this.nativeButtonType === 'create' || this.nativeButtonType === 'update') {
        this.submitAddRecordExternalApi(method, url, params, token);
      }
    },
  },
};
