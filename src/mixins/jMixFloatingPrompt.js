export default {
  data() {
    return {
      show_blank_footer: false,
      show_detail: false,
      height_floating: 0,
    };
  },

  computed: {
    floatingHeight: {
      get() {
        return this.height_floating;
      },
      set(newValue) {
        this.height_floating = newValue;
      },
    },
    // eslint-disable-next-line consistent-return
    floatingPage() {
      if (this.show_blank_footer) {
        return this.$refs.detailTable.page;
      }
    },
  },

  watch: {
    selected_rows(val) {
      if (val.length > 0) {
        this.show_blank_footer = true;
        this.$refs.floating.show();
        setTimeout(() => {
          this.floatingHeight = this.$refs.divFloating.offsetHeight + 60;
        }, 300);
        document.querySelectorAll('.jback').forEach((elem) => {
          elem.remove();
        });
      } else {
        this.show_blank_footer = false;
        this.$refs.floating.hide();
      }
    },
    show_blank_footer(val) {
      if (val) {
        setTimeout(() => {
          this.floatingHeight = this.$refs.divFloating.offsetHeight + 60;
        }, 300);
      } else {
        setTimeout(() => {
          this.floatingHeight = 0;
        }, 300);
      }
    },
    show_detail() {
      setTimeout(() => {
        this.floatingHeight = this.$refs.divFloating.offsetHeight + 60;
      }, 300);
    },
    floatingPage() {
      setTimeout(() => {
        this.floatingHeight = this.$refs.divFloating.offsetHeight + 60;
      }, 300);
    },
  },

  methods: {
    headerID() {
      return <input type="checkbox" checked disabled class="header-id" />;
    },

    floatingPromptAction() {
      this.$refs.totalSelectedRow.textContent = this.selected_rows.length;
    },

    closeAction() {
      this.show_blank_footer = false;
      this.selected_rows = [];
    },
  },
};
