export default {
  computed: {
    logo_tab() {
      const logoTab = process.env.LOGO_TAB;
      // eslint-disable-next-line global-require
      const officelessLogo = require('@/assets/logo/favicon.png');
      if (logoTab && logoTab !== '@LOGO_TAB@') {
        return process.env.LOGO_TAB;
      }
      return officelessLogo;
    },
    title_tab() {
      const titleTab = process.env.TITLE_TAB;
      if (titleTab && titleTab !== '@TITLE_TAB@') {
        return process.env.TITLE_TAB;
      }
      return this.$route.meta.title;
    },
  },
};
