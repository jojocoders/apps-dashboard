export default {
  methods: {
    // this function for create timestamp by offset timezone
    createTimestampByTimezone(date, offset) {
      const timezoneOffset = date.getTimezoneOffset() * -1;
      const timestampLocal = date.getTime() + (timezoneOffset * 60 * 1000);
      return timestampLocal - (offset * 60 * 60 * 1000);
    },

    // this function for show timestamp by offset timezone
    showTimestampByTimezone(date, offset) {
      const timezoneOffset = date.getTimezoneOffset();
      const timestampLocal = date.getTime() + (timezoneOffset * 60 * 1000);
      return timestampLocal + (offset * 60 * 60 * 1000);
    },

    convertDateFormat(data, format, offset) {
      if (data) {
        const date = new Date(
          offset !== null ?
            this.showTimestampByTimezone(new Date(data), offset) :
            data,
        );
        if (format === 'dd-mmm-yyyy') return this.$moment(date).format('DD-MMM-YYYY');
        if (format === 'mmmm dd, yyyy') return this.$moment(date).format('MMMM DD, YYYY');
      }
      return null;
    },

    convertDateTime(data, offset) {
      if (data) {
        const date = new Date(
          offset !== null ?
            this.showTimestampByTimezone(new Date(data), offset) :
            data,
        );
        const years = date.getFullYear();
        const mounth = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        const dates = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();

        const hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
        const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();

        return `${years}-${mounth}-${dates} ${hours}:${minutes}`;
      }
      return null;
    },

    convertTimestamp(data, offset) {
      if (data) {
        const date = new Date(
          offset !== null ?
            this.showTimestampByTimezone(new Date(data), offset) :
            data,
        );
        const years = date.getFullYear();
        const mounth = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        const dates = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        return `${years}-${mounth}-${dates}`;
      }
      return null;
    },

    convertTime(data, offset) {
      if (data) {
        const date = new Date(
          offset !== null ?
            this.showTimestampByTimezone(new Date(data), offset) :
            data,
        );
        const hours = date.getHours();
        const minutes = date.getMinutes();
        return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
      }
      return null;
    },
  },
};
