/* eslint-disable import/no-extraneous-dependencies, no-nested-ternary, max-len, no-console, no-unused-vars, no-eval, no-plusplus */
export default {
  props: {
    timeframeActive: {
      type: String,
      default: '',
    },
    timeframeDateRange: {
      type: Object,
      default: () => { },
    },
  },

  data() {
    return {
      date: new Date(),
      startNow: null,
      endNow: null,
      timeRange: null,
    };
  },

  watch: {
    timeframeActive(val) {
      if (val) {
        this.filterRangeDate(val);
        this.combineFilterV2('App', 'created_at', this.timeRange);

        this.setupFilterTimeframe();
      } else if (!val && !this.timeframeDateRange) {
        this.resetTimeframe();
      }
    },

    timeframeDateRange: {
      handler(val) {
        if (val.from && val.to) {
          this.clearDataFilterV2();
          this.isLoading = true;
          const from = new Date(Date.parse(`${val.from.getFullYear()}-${val.from.getMonth() + 1}-${val.from.getDate()} 00:00:00`)).getTime();
          const to = new Date(Date.parse(`${val.to.getFullYear()}-${val.to.getMonth() + 1}-${val.to.getDate()} 00:00:00`)).getTime();
          this.timeRange = [from, to];

          this.combineFilterV2('App', 'created_at', this.timeRange);
          this.setupFilterTimeframe();
        } else if (!val.from && !val.to) {
          this.resetTimeframe();
        }
      },
      deep: true,
    },
  },

  computed: {
    useSourceDataset() {
      return !!this[`data${this.titleText(this.type)}`].dataset;
    },
  },

  methods: {
    filterRangeDate(timeframe) {
      this.clearDataFilterV2();
      this.isLoading = true;

      const svnDays = 7;
      const trtDays = 30;
      const oneYear = 365;
      const now = Date.now();
      const lastSvn = new Date(this.date.getTime() - (svnDays * 24 * 60 * 60 * 1000)).getTime();
      const lastTrt = new Date(this.date.getTime() - (trtDays * 24 * 60 * 60 * 1000)).getTime();
      const lastYears = new Date(this.date.getTime() - (oneYear * 24 * 60 * 60 * 1000)).getTime();
      const threeYears = new Date(this.date.getTime() - ((oneYear * 3) * 24 * 60 * 60 * 1000)).getTime();
      const fiveYears = new Date(this.date.getTime() - ((oneYear * 5) * 24 * 60 * 60 * 1000)).getTime();
      const thisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getTime();
      const thisYear = new Date(new Date().getFullYear(), 0, 1).getTime();
      this.startNow = new Date(Date.parse(`${this.date.getFullYear()}-${this.date.getMonth() + 1}-${this.date.getDate()} 00:00:00`)).getTime();
      this.endNow = new Date(Date.parse(`${this.date.getFullYear()}-${this.date.getMonth() + 1}-${this.date.getDate()} 23:59:59`)).getTime();

      const getSunday = (d) => {
        const day = new Date(d).getDay();
        const diff = (new Date(d).getDate() - day) + (day === 0 ? -7 : 0);
        return new Date(new Date(d).setDate(diff));
      };
      const thisWeek = getSunday(new Date()).setHours(0, 0, 0, 0);

      switch (timeframe) {
        case '7 days ago':
          this.timeRange = [lastSvn, now];
          break;

        case '30 days ago':
          this.timeRange = [lastTrt, now];
          break;

        case '365 days ago':
          this.timeRange = [lastYears, now];
          break;

        case '3 years ago':
          this.timeRange = [threeYears, now];
          break;

        case '5 years ago':
          this.timeRange = [fiveYears, now];
          break;

        case 'This Week':
          this.timeRange = [thisWeek, now];
          break;

        case 'This Month':
          this.timeRange = [thisMonth, now];
          break;

        case 'This Year':
          this.timeRange = [thisYear, now];
          break;

        default:
          this.timeRange = [this.startNow, this.endNow];
      }
    },

    setupFilter(params, sourceData, filter) {
      const dataFilter = filter;
      let x;
      let columnNotFound = 0;
      let filterHaveValue = 0;

      if (params) {
        params.forEach((e) => {
          if (e.column && e.column.field_data_key) {
            const findColumn = sourceData.columns.find(f => f.name === e.column.field_data_key);
            if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'timestamp' && e.column.value.start_date && e.column.value.end_date) {
              const startOfDay = new Date(Date.parse(`${this.$moment(e.column.value.start_date).format('YYYY-MM-DD')} 00:00:00`)).getTime();
              const endOfDay = new Date(Date.parse(`${this.$moment(e.column.value.end_date).format('YYYY-MM-DD')} 23:59:59`)).getTime();
              dataFilter[e.column.field_data_key] = [startOfDay, endOfDay];
              this.combineFilterV2('App', e.column.field_data_key, [startOfDay, endOfDay]);
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'object' && e.column.value) {
              dataFilter[`${e.column.field_data_key}.id`] = e.column.value;
              this.combineFilterV2('App', `${e.column.field_data_key}.id`, e.column.value);
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'tags' && e.column.value) {
              dataFilter[`${e.column.field_data_key}.name`] = e.column.value.name;
              this.combineFilterV2('App', `${e.column.field_data_key}.name`, e.column.value.name);
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === '[]tags' && e.column.value.length > 0) {
              dataFilter[`${e.column.field_data_key}.name`] = e.column.value.map(el => (el.name));
              this.combineFilterV2('App', `${e.column.field_data_key}.name`, e.column.value.map(el => (el.name)));
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'person' && e.column.value) {
              dataFilter[`${e.column.field_data_key}.company_user_id`] = e.column.value.id;
              this.combineFilterV2('App', `${e.column.field_data_key}.company_user_id`, e.column.value.id);
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'number' && e.column.value) {
              dataFilter[e.column.field_data_key] = Number(e.column.value);
              this.combineFilterV2('App', e.column.field_data_key, Number(e.column.value));
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'text' && e.column.value) {
              dataFilter[e.column.field_data_key] = e.column.value;
              this.combineFilterV2('App', e.column.field_data_key, e.column.value);
              filterHaveValue++;
            }
            if (!findColumn) {
              columnNotFound++;
            }
          } else {
            const findColumn = sourceData.columns.find(f => f.name === e.column.name);
            if (e.column && e.column.type === 'timestamp' && findColumn) {
              if (e.start_date && e.end_date) {
                const startOfDay = new Date(Date.parse(`${this.$moment(e.start_date).format('YYYY-MM-DD')} 00:00:00`)).getTime();
                const endOfDay = new Date(Date.parse(`${this.$moment(e.end_date).format('YYYY-MM-DD')} 23:59:59`)).getTime();
                dataFilter[e.column.id] = [startOfDay, endOfDay];
                this.combineFilterV2('App', e.column.id, [startOfDay, endOfDay]);
              }
            } else if (e.column && e.column.type !== 'timestamp' && findColumn) {
              if (e.value) {
                if (sourceData.form_data.columns[e.column.id] && sourceData.form_data.columns[e.column.id].type === 'number') {
                  dataFilter[e.column.id] = Number(e.value);
                  this.combineFilterV2('App', e.column.id, Number(e.value));
                } else {
                  dataFilter[e.column.id] = e.value;
                  this.combineFilterV2('App', e.column.id, e.value);
                }
              }
            }
            if (!findColumn) {
              columnNotFound++;
            }
          }
        });
      }

      const data = this.type === 'table' ? this.formUi.table_view : sourceData;
      if (data.filter && (!params || params.length === 0 || params.length === columnNotFound || filterHaveValue === 0)) {
        // eslint-disable-next-line
        const _user = this.session.user;
        const filterKeys = Object.keys(data.filter);
        const filterValues = Object.values(data.filter);

        if (this.type === 'table') {
          if (filterValues.length > 0) {
            filterValues.forEach((e, index) => {
              if (e.includes('_user')) {
                dataFilter[filterKeys[index]] = eval(`x=${e}`);
                this.combineFilterV2('Setup', filterKeys[index], eval(`x=${e}`));
              } else if (filterKeys[index].includes('.company_user_id')) {
                dataFilter[filterKeys[index]] = Number(e);
                this.combineFilterV2('Setup', filterKeys[index], Number(e));
              } else if (e.includes('.today')) {
                dataFilter[filterKeys[index]] = [this.startNow, this.endNow];
                this.combineFilterV2('Setup', filterKeys[index], [this.startNow, this.endNow]);
              } else {
                // eslint-disable-next-line
                if (this.cekTypeData(filterKeys[index]) === 'number') {
                  dataFilter[filterKeys[index]] = Number(e);
                  this.combineFilterV2('Setup', filterKeys[index], Number(e));
                } else {
                  dataFilter[filterKeys[index]] = e.slice(1, -1);
                  this.combineFilterV2('Setup', filterKeys[index], e.slice(1, -1));
                }
              }
            });
          }
        } else if (this.type !== 'table') {
          if (filterValues.length > 0) {
            filterValues.forEach((e, index) => {
              if (typeof e === 'object') {
                dataFilter[filterKeys[index]] = [e.start, e.end];
                this.combineFilterV2('Setup', filterKeys[index], [e.start, e.end]);
              } else {
                // eslint-disable-next-line
                if (e.includes('_user')) {
                  dataFilter[filterKeys[index]] = eval(`x=${e}`);
                  this.combineFilterV2('Setup', filterKeys[index], eval(`x=${e}`));
                } else if (filterKeys[index].includes('.company_user_id')) {
                  dataFilter[filterKeys[index]] = Number(e);
                  this.combineFilterV2('Setup', filterKeys[index], Number(e));
                } else if (e.includes('.today')) {
                  dataFilter[filterKeys[index]] = [this.startNow, this.endNow];
                  this.combineFilterV2('Setup', filterKeys[index], [this.startNow, this.endNow]);
                } else {
                  // eslint-disable-next-line
                  if (sourceData.form_data.columns[filterKeys[index]] && sourceData.form_data.columns[filterKeys[index]].type === 'number') {
                    dataFilter[filterKeys[index]] = Number(e);
                    this.combineFilterV2('Setup', filterKeys[index], Number(e));
                  } else {
                    dataFilter[filterKeys[index]] = e.slice(1, -1);
                    this.combineFilterV2('Setup', filterKeys[index], e.slice(1, -1));
                  }
                }
              }
            });
          }
        }
      }

      const filterFunctionData = this.type === 'table' ? this.filterFunction : sourceData.filter_function_value;
      if (filterFunctionData) {
        const filterFunctionKeys = Object.keys(filterFunctionData);
        const filterFunctionValues = Object.values(filterFunctionData);

        if (filterFunctionValues && filterFunctionValues.length > 0) {
          filterFunctionKeys.forEach((e, index) => {
            dataFilter[e] = filterFunctionValues[index];
            this.combineFilterV2('Setup', e, filterFunctionValues[index]);
          });
        }
      }

      let param;
      const paramData = this.type === 'table' ? this.formUi.table_view : sourceData;

      if (this.useSourceDataset) {
        param = {
          sort: paramData.sort ? paramData.sort[0] : {},
          dataset_id: paramData.dataset.dataset_id,
          filter: dataFilter,
          filter_operator: paramData.filter_operator || 'and',
          limit: 1000,
          page: 1,
        };
      } else {
        param = {
          sort: paramData.sort ? paramData.sort[0] : {},
          form_data_id: this.type === 'table' ? this.formUi.form_data_id : paramData.form_data_id,
          limit: 1000000000,
          page: 1,
          filter_app: this.setFilterAppV2(paramData.filter_operator),
          filter_setup: this.setFilterSetupV2(paramData.filter_operator),
          filter_setup_operator: paramData.filter_operator || 'and',
        };
      }

      if (this.type === 'table') {
        if (this.formUi.table_view.filter_operator && (!params || params.length === 0 || params.length === columnNotFound || filterHaveValue === 0)) {
          param.filter_operator = this.formUi.table_view.filter_operator;
        }
      }

      return param;
    },

    setupFilterExternalUrl(params, sourceData, filter) {
      const dataFilter = filter;
      let x;
      let columnNotFound = 0;
      let filterHaveValue = 0;

      if (params) {
        params.forEach((e) => {
          if (e.column && e.column.field_data_key) {
            const findColumn = sourceData.columns.find(f => f.name === e.column.field_data_key);
            if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'timestamp' && e.column.value.start_date && e.column.value.end_date) {
              const startOfDay = new Date(Date.parse(`${this.$moment(e.column.value.start_date).format('YYYY-MM-DD')} 00:00:00`)).getTime();
              const endOfDay = new Date(Date.parse(`${this.$moment(e.column.value.end_date).format('YYYY-MM-DD')} 23:59:59`)).getTime();
              // eslint-disable-next-line
              dataFilter[e.column.field_data_key] = { '$between': [startOfDay, endOfDay] };
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'object' && e.column.value) {
              dataFilter[`${e.column.field_data_key}.id`] = e.column.value;
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'tags' && e.column.value) {
              dataFilter[`${e.column.field_data_key}.name`] = e.column.value.name;
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === '[]tags' && e.column.value.length > 0) {
              dataFilter[`${e.column.field_data_key}.name`] = e.column.value.map(el => (el.name));
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'person' && e.column.value) {
              dataFilter[`${e.column.field_data_key}.company_user_id`] = e.column.value.id;
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'number' && e.column.value) {
              dataFilter[e.column.field_data_key] = Number(e.column.value);
              filterHaveValue++;
            } else if (findColumn && sourceData.form_data.columns[e.column.field_data_key].type === 'text' && e.column.value) {
              dataFilter[e.column.field_data_key] = e.column.value;
              filterHaveValue++;
            }
            if (!findColumn) {
              columnNotFound++;
            }
          } else {
            const findColumn = sourceData.columns.find(f => f.name === e.column.name);
            if (e.column && e.column.type === 'timestamp' && findColumn) {
              if (e.start_date && e.end_date) {
                const startOfDay = new Date(Date.parse(`${this.$moment(e.start_date).format('YYYY-MM-DD')} 00:00:00`)).getTime();
                const endOfDay = new Date(Date.parse(`${this.$moment(e.end_date).format('YYYY-MM-DD')} 23:59:59`)).getTime();
                // eslint-disable-next-line
                dataFilter[e.column.id] = { '$between': [startOfDay, endOfDay] };
              }
            } else if (e.column && e.column.type !== 'timestamp' && findColumn) {
              if (e.value) {
                if (sourceData.form_data.columns[e.column.id] && sourceData.form_data.columns[e.column.id].type === 'number') {
                  dataFilter[e.column.id] = Number(e.value);
                } else {
                  dataFilter[e.column.id] = e.value;
                }
              }
            }
            if (!findColumn) {
              columnNotFound++;
            }
          }
        });
      }
    },

    setupFilterTimeframe() {
      let param;
      const paramData = this.type === 'table' ? this.formUi.table_view : this[`data${this.titleText(this.type)}`];

      if (this.useSourceDataset) {
        const dataFilter = {
          created_at: this.timeRange,
        };

        param = {
          sort: paramData.sort ? paramData.sort[0] : {},
          dataset_id: paramData.dataset.dataset_id,
          filter: dataFilter,
          filter_operator: paramData.filter_operator || 'and',
          limit: 1000,
          page: 1,
        };
      } else {
        param = {
          sort: paramData.sort ? paramData.sort[0] : {},
          form_data_id: this.type === 'table' ? this.formUi.form_data_id : paramData.form_data_id,
          limit: 1000000000,
          page: 1,
          filter_app: this.setFilterAppV2(paramData.filter_operator),
          filter_setup: this.setFilterSetupV2(paramData.filter_operator),
          filter_setup_operator: paramData.filter_operator || 'and',
        };
      }

      if (this.type === 'map') {
        this.resetData();
      }
      if (this.type === 'table') {
        if (this.isMode !== 'form') {
          delete param.form_data_id;
          param.dataset_id = this.formUi.data_source.dataset.dataset_id;
          param.limit = 1000;
        }
      } else if (this.type === 'chart') {
        if (paramData.type === 'line_chart') {
          this.initLine(param);
        }
        if (paramData.type === 'bar_chart') {
          this.initBar(param);
        }
        if (paramData.type === 'pie_chart') {
          this.initPie(param);
        }
      } else {
        this.getDataCanvas(param);
      }
    },

    resetTimeframe() {
      if (this.type === 'chart') {
        this.initChart();
      } else if (this.type === 'text') {
        this.initText();
      } else if (this.type === 'map') {
        this.initMap();
      } else if (this.type === 'table') {
        if (this.isMode === 'form' || this.isMode === 'dataset') {
          this.getTableViewData();
        } else {
          this.getTableViewDataExternal();
        }
      }
    },

    titleText(text) {
      return text.toLowerCase().split(' ').map(word => (word.charAt(0).toUpperCase() + word.slice(1))).join(' ');
    },
  },
};
