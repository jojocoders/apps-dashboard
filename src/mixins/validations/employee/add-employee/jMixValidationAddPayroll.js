/* eslint-disable import/prefer-default-export */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationAddPayroll = {
  validations() {
    return {
      form_salary: {
        amount: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_salary_config: {
        salary_config: {
          required,
        },
        salary_type: {
          required,
        },
      },
      form_tax: {
        tax_id: {
          required,
        },
        number: {
          required,
        },
        registration_date: {
          required,
        },
      },
      form_tax_config: {
        configuration: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_tax_status: {
        tax_status_id: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_tax_cost: {
        cost_center_id: {
          required,
        },
        company_region_id: {
          required,
        },
        tax_location_id: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_bpjskk: {
        number: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_pansion: {
        number: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_bpjsk: {
        number: {
          required,
        },
        type: {
          required,
        },
        total_member: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
    };
  },
};
