/* eslint-disable import/prefer-default-export */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationAddCompanyDetail = {
  validations() {
    return {
      form_payroll_area: {
        payroll_area_id: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
      form_payroll_scale: {
        pay_scale_id: {
          required,
        },
        effective_begin: {
          required,
        },
        effective_end: {
          required,
        },
      },
    };
  },
};
