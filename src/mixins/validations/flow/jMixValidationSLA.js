/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required, requiredIf } from 'vuelidate/lib/validators';

export default {
  validations() {
    return {
      flow_sla: {
        duration: {
          required,
        },
        sla: {
          $each: {
            value: {
              required: requiredIf(data => data.is_check),
            },
            is_check: { },
            type: { },
          },
        },
      },
    };
  },
};
