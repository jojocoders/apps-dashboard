/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationFormFlowType2 = {
  validations: {
    flow: {
      name: { required },
      division: { required },
    },
  },
};
