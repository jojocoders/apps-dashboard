/* eslint-disable import/prefer-default-export */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationReimbursement = {
  validations() {
    return {
      form_reimbursement_info: {
        code: {
          required,
        },
        employee: {
          required,
        },
        start_date: {
          required,
        },
        limit: {
          required,
        },
      },
      form_reimbursement_payment: {
        payment_date: {
          required,
        },
        amount: {
          required,
        },
      },
    };
  },
};
