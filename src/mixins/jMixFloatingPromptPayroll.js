export default {
  data() {
    return {
      show_blank_footer: false,
      height_floating: 0,
    };
  },

  computed: {
    floatingHeight: {
      get() {
        return this.height_floating;
      },
      set(newValue) {
        this.height_floating = newValue;
      },
    },
  },

  watch: {
    show_blank_footer(val) {
      if (val) {
        this.setFloatingHeight();
      } else {
        setTimeout(() => {
          this.floatingHeight = 0;
        }, 300);
      }
    },
  },

  methods: {
    setFloatingHeight() {
      setTimeout(() => {
        this.floatingHeight = this.$refs.divFloating.offsetHeight + 40;
      }, 300);
    },
  },
};
