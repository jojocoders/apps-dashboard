/* eslint-disable max-len */
import { jMixWindowResizing } from '@/mixins/components/jMixWindowResizing';

export default {
  mixins: [
    jMixWindowResizing,
  ],

  data() {
    return {
      screenOptions: {
        small: {
          maxCol: 4,
        },
        medium: {
          maxCol: 5,
        },
        large: {
          maxCol: 8,
        },
        extra_large: {
          maxCol: 15,
        },
      },
      allColumns: [],
      expandRow: [],
      device: null,
      isTableHeaderReady: false,
    };
  },

  watch: {
    window_width() {
      if (this.isRowExpansion && this.isTableHeaderReady) {
        this.device = this.getScreenWidth();
      }
    },

    device() {
      if (this.isRowExpansion) {
        this.getRowExpansion();
      }
    },

    isTableHeaderReady(val) {
      if (val && this.isRowExpansion) {
        this.device = this.getScreenWidth();
      }
    },
  },

  methods: {
    getRowExpansion() {
      const columns = this.allColumns.map(val => val.key);
      const targetedIndex = this.screenOptions[this.device] ? this.screenOptions[this.device].maxCol : null;

      if (targetedIndex) {
        this.expandRow = this.allColumns.slice(targetedIndex);

        if (!this.expandRow.length) {
          this.table_options.showChildRowToggler = false;
          this.table_options_server.showChildRowToggler = false;

          if (this.$refs.applicationTableComponent) {
            this.$refs.applicationTableComponent.openChildRows = [];
          }
        } else {
          this.table_options.showChildRowToggler = true;
          this.table_options_server.showChildRowToggler = true;
        }

        this.columns = columns.slice(0, targetedIndex);
        this.columns_server = columns.slice(0, targetedIndex);
      }
    },

    getScreenWidth() {
      if (this.window_width >= 2560) {
        return 'extra_large';
      } else if (this.window_width < 2560 && this.window_width >= 1440) {
        return 'large';
      } else if (this.window_width < 1440 && this.window_width >= 1024) {
        return 'medium';
      }
      return 'small';
    },
  },
};
