/* eslint-disable no-useless-escape, max-len */
import DOMPurify from 'dompurify';

export default {
  methods: {
    /**
     * Checks whether a given string is a valid image URL.
     *
     * A valid image URL must meet the following criteria:
     * 1. Starts with "http://" or "https://".
     * 2. Contains a valid domain name with letters, numbers, hyphens (-), and dots (.).
     * 3. Includes a valid file path with alphanumeric characters, hyphens (-), slashes (/), and dots (.).
     * 4. Ends with an allowed image file extension: jpg, jpeg, png, gif, bmp, webp, or svg.
     * 5. The validation is case-insensitive (e.g., ".JPG" and ".jpg" are both valid).
     *
     * Examples of valid URLs:
     * ✅ https://example.com/image.jpg
     * ✅ http://cdn.example.com/path/to/image.png
     * ✅ https://sub.domain-example.com/folder/image.webp
     *
     * Examples of invalid URLs:
     * ❌ https://example.com/image (missing file extension)
     * ❌ ftp://example.com/image.png (not using http/https)
     * ❌ javascript:alert(1) (XSS attempt)
     */
    sanitizeUrl(rawUrl) {
      const urlRegex = /^https?:\/\/[\w\-]+(\.[\w\-]+)+\/[\w\-./&%]+\.(jpg|jpeg|png|gif|bmp|webp|svg|pdf|csv|txt|xls|xlsx|docx|doc|ppt|pptx|mp4)(\?.*)?$/i;
      const url = rawUrl ? rawUrl.trim() : '';
      return urlRegex.test(url) ? DOMPurify.sanitize(url) : '';
    },
  },
};
