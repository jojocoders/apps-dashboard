export default {
  methods: {
    getUserEmail(user) {
      return user.email || '';
    },
    getUserInitial(user) {
      const firstName = user.first_name ? user.first_name[0] : '';
      const lastName = user.last_name ? user.last_name[0] : '';
      const initial = `${firstName}${lastName}`;
      return initial.toUpperCase();
    },
    getUserName(user) {
      return `${user.first_name} ${user.last_name}` || '';
    },
    getUserTitle(user) {
      if (user.organigrams) {
        return user.organigrams[0].name;
      } else if (user.title) {
        return user.title;
      }
      return user.position;
    },
    getUserPhoto(user) {
      return user.photo_url || '';
    },
  },
};
