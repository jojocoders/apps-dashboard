/* eslint-disable no-unused-vars, no-eval, no-console, no-plusplus, max-len, no-nested-ternary, no-underscore-dangle */
import { mapActions } from 'vuex';
import Vue from 'vue';
import axios from 'axios';

export default {
  data() {
    return {
      finishUpdateDataAction: false,
    };
  },
  computed: {
    showActionButton() {
      return this.dataFormUi && this.dataFormUi.detail_view.action_buttons && this.data_record_child && this.finishUpdateDataAction;
    },
    showActionButtonOnMultiple() {
      return this.formUi && this.formUi.detail_view.action_buttons && this.recordData && this.finishUpdateDataAction;
    },
  },
  methods: {
    ...mapActions('jStoreFormUi', ['actionGetFunctionData']),

    async confirmActionButton(data) {
      if (data.is_use_confirmation_message && data.dynamic_confirmation_message) {
        const responseFunction = await this.getValueFunctionAction(data);
        const text = responseFunction && responseFunction.confirmation_message ? responseFunction.message : 'Are you sure want to continue this action?';

        this.$swal({
          title: 'Confirmation',
          text,
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.dismiss) {
            // eslint-disable-next-line
            throw {};
          } else {
            this.actionButtonsSet(data);
          }
        });
      } else if (data.is_use_confirmation_message) {
        this.$swal({
          title: 'Confirmation',
          text: data.custom_confirmation_message || 'Are you sure want to continue this action?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.dismiss) {
            // eslint-disable-next-line
            throw {};
          } else {
            this.actionButtonsSet(data);
          }
        });
      } else {
        this.actionButtonsSet(data);
      }
    },

    async confirmActionButtonTable(item, recordData, isComponent, sourceView) {
      if (item.is_use_confirmation_message && item.dynamic_confirmation_message) {
        const payload = {
          record_id: recordData.ids,
          data: recordData,
          key: item.key,
          source_view: sourceView,
        };
        if (this.isMultiple) payload.multiple_form_ui_id = this.formUi.id;
        else payload.form_ui_id = this.formUi.id;

        const responseFunction = await this.getValueFunctionAction(payload);
        const text = responseFunction && responseFunction.confirmation_message ? responseFunction.message : 'Are you sure want to continue this action?';

        this.$swal({
          title: 'Confirmation',
          text,
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.dismiss) {
            // eslint-disable-next-line
            throw {}
          } else if (isComponent) {
            this.$emit('action-button', item, recordData);
          } else {
            this.actionButtons(item, recordData);
          }
        });
      } else if (item.is_use_confirmation_message) {
        this.$swal({
          title: 'Confirmation',
          text: item.custom_confirmation_message || 'Are you sure want to continue this action?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.dismiss) {
            // eslint-disable-next-line
            throw {}
          } else if (isComponent) {
            this.$emit('action-button', item, recordData);
          } else {
            this.actionButtons(item, recordData);
          }
        });
      } else if (isComponent) {
        this.$emit('action-button', item, recordData);
      } else {
        this.actionButtons(item, recordData);
      }
    },

    setVisibilityActionButtonDetail(onMultiple) {
      this.finishUpdateDataAction = false;
      let lengthProgressActionButton = 0;
      const dataFormUi = onMultiple ? this.formUi : this.dataFormUi;
      const dataRecord = onMultiple ? this.recordData : this.data_record_child;
      const actionButtons = dataFormUi.detail_view.action_buttons && dataFormUi.detail_view.action_buttons.length > 0 ?
        dataFormUi.detail_view.action_buttons : [];
      const lengthActionButton = actionButtons.length;

      const _user = this.session.user;
      const _field = dataRecord;
      // eslint-disable-next-line
      async function _hitFunction(functionName, payload) {
        let x;
        const token = Vue.ls.get('Token');
        const gateUrl = localStorage.getItem('gateUrl');
        const url = `${gateUrl}nocode/function?function_name=${functionName}`;
        const body = eval(`x=${JSON.stringify(payload)}`);

        const hitApi = await axios.post(url, body, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        });
        return hitApi.data || null;
      }

      actionButtons.forEach((e, index) => {
        let y;
        let textCondition = '';
        let lengthProgressVisibility = 0;
        const dataOperator = e.visibility_condition ? e.visibility_condition.operator : 'and';
        const dataVariables = e.visibility_condition ? e.visibility_condition.variables : [];
        const lengthConditions = dataVariables.length;

        if (dataVariables.length > 0) {
          dataVariables.forEach(async (v, indexVar) => {
            const andMore = lengthConditions - 1 > indexVar ? `${dataOperator === 'and' ? '&&' : '||'}` : '';
            if (v.value_type === 'value') {
              if (v.value.includes('_hitFunction')) {
                const resEval = await eval(`y=${v.value}`);
                if (Array.isArray(resEval)) {
                  textCondition += `${resEval.includes(dataRecord[v.key])} ${andMore} `;
                } else {
                  textCondition += `"${dataRecord[v.key]}" === "${resEval}" ${andMore} `;
                }
              } else {
                const value = v.value ? (v.value[0] === "'" || v.value[0] === '"' ? v.value.slice(1, -1) : v.value) : '';
                textCondition += `"${dataRecord[v.key]}" === "${value}" ${andMore} `;
              }
              lengthProgressVisibility++;
              if (lengthConditions === lengthProgressVisibility) {
                console.log(e.label, textCondition);
                dataFormUi.detail_view.action_buttons[index].visibility = eval(`y=${textCondition}`);
                lengthProgressActionButton++;
                if (lengthActionButton === lengthProgressActionButton) this.finishUpdateDataAction = true;
              }
            } else {
              textCondition += `"${dataRecord[v.key]}" === "${v.value}" ${andMore} `;
              this.actionGetFunctionData(v.value).then((res) => {
                if (Array.isArray(res)) {
                  textCondition = textCondition.replace(`"${dataRecord[v.key]}" === "${v.value}"`, `${res.includes(dataRecord[v.key])}`);
                } else {
                  textCondition = textCondition.replace(v.value, res);
                }
                lengthProgressVisibility++;
                if (lengthConditions === lengthProgressVisibility) {
                  console.log(e.label, textCondition);
                  dataFormUi.detail_view.action_buttons[index].visibility = eval(`y=${textCondition}`);
                  lengthProgressActionButton++;
                  if (lengthActionButton === lengthProgressActionButton) this.finishUpdateDataAction = true;
                }
              });
            }
          });
        } else {
          dataFormUi.detail_view.action_buttons[index].visibility = true;
          lengthProgressActionButton++;
          if (lengthActionButton === lengthProgressActionButton) this.finishUpdateDataAction = true;
        }
      });
    },

    setVisibilityActionButtonTable(item, dataRecord) {
      let y;
      let textCondition = '';
      const dataOperator = item.visibility_condition ? item.visibility_condition.operator : 'and';
      const dataVariables = item.visibility_condition ? item.visibility_condition.variables : [];
      const lengthConditions = dataVariables.length;

      if (dataVariables.length > 0) {
        dataVariables.forEach((v, indexVar) => {
          const andMore = lengthConditions - 1 > indexVar ? `${dataOperator === 'and' ? '&&' : '||'}` : '';
          const value = v.value ? (v.value[0] === "'" || v.value[0] === '"' ? v.value.slice(1, -1) : v.value) : '';
          if (v.value_type === 'value') textCondition += `'${dataRecord[v.key]}' === '${value}' ${andMore} `;
        });
        return eval(`y=${textCondition}`);
      }
      return true;
    },

    getValueFunctionAction(payload) {
      const token = Vue.ls.get('Token');
      const gateUrl = localStorage.getItem('gateUrl');
      const formType = payload.multiple_form_ui_id ? 'multiple_form_ui_id' : 'form_ui_id';
      const formTypeId = payload.multiple_form_ui_id || payload.form_ui_id;
      const url = `${gateUrl}nocode/function?form_type=${formType}&form_type_id=${formTypeId}&record_id=${payload.record_id}&button=${payload.key}&source_view=${payload.source_view}`;
      const body = payload.data || {};

      return axios.post(url, body, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }).then(res => res.data);
    },
  },
};
