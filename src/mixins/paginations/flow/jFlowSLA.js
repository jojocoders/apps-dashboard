/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';
import jApi from '@/services/jApi';

export const jFlowSLA = {
  data() {
    return {
      filter_data: '',
      select_type: 'name',
      options_search: [
        {
          text: 'Id',
          value: 'id',
        },
        {
          text: 'Name',
          value: 'name',
        },
      ],
      env: 'jojoflow/flow/list',
      load_first: false,
      length_table: '',
      columns: ['id', 'name', 'duration', 'sla_policy'],
      options: {
        headings: {
          id: 'ID',
          name: 'FLOW NAME',
          duration: 'DURATION',
          sla_policy: 'SLA POLICY',
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        filterable: true,
        sortable: ['name'],
        perPage: 10,
        perPageValues: [10, 20, 50, 100],
        rowClassCallback(row) {
          return `row-${row.id}-sla`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
              query_type: data.query_type ? data.query_type : '',
            },
          };
        },
        requestFunction(data) {
          const api = jApi.generateApi();
          return api.post(this.url, data).catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/getProblemMessage', e.message);
          });
        },
        responseAdapter({ data }) {
          if (data.length) {
            this.length_table = data.length;
          }
          return {
            data: data.data ? data.data : [],
            count: data.length,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jFlows', ['sla_created', 'sla_updated']),
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.flowServer.setFilter('');
      }
    },

    sla_created() {
      this.refreshTable();
    },

    sla_updated() {
      this.refreshTable();
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['hideLoading', 'showLoading', 'toggleProblem', 'toggleLoading']),

    refreshTable() {
      this.$refs.flowServer.getData();
    },

    filter(val) {
      this.removeRowBackground();
      this.$refs.flowServer.setFilter(val);
    },

    loading(val) {
      const a = val;
      a.pagination.query_type = this.select_type;
      if (this.load_first) {
        this.toggleLoading();
      }
    },
    loaded() {
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
  },
};
