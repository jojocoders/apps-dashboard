/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';
import apiRequestor from '@/services/jApiRequestor';

export const jMixPaginationRequestor = {
  data() {
    return {
      filter_data: '',
      select_type: 'name',
      options_search: [
        {
          text: 'Name',
          value: 'name',
        },
      ],
      data_table: [],
      selected_rows: [],
      load_first: false,
      columns: ['name', 'description', 'action'],
      options: {
        headings: {
          name: 'FORM',
          description: 'DESCRIPTION',
          action: 'ACTION',
        },
        texts: {
          filterPlaceholder: 'Search...',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        sortable: [],
        debounce: 1000,
        filterable: true,
        perPage: 5,
        orderBy: {
          ascending: true,
          column: 'created_date',
        },
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !data.ascending,
              query: data.query ? data.query : '',
              query_type: data.query_type ? data.query_type : '',
            },
          };
        },
        requestFunction(data) {
          const getList = apiRequestor.listMyForm(data);
          return getList.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length,
          };
        },
      },
    };
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.myFormList.setFilter('');
      }
    },
    created_res() {
      this.refreshTable();
    },
    updated_res() {
      this.refreshTable();
    },
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    loading(val) {
      const a = val;
      a.pagination.query_type = this.select_type;
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.myFormList.getData();
    },

    filter(val) {
      this.$refs.myFormList.setFilter(val);
    },
  },
};
