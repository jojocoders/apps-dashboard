/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

import { mapActions } from 'vuex';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';
import axios from 'axios';
import Vue from 'vue';

export const jMixRequestor = {
  components: {
    jReusableFunction,
  },
  data() {
    return {
      filter_data: '',
      select_type: 'name',
      options_search: [
        {
          text: 'RECORD NAME',
          value: 'name',
        },
        // {
        //   text: 'FORM',
        //   value: 'form',
        // },
      ],
      load_first: false,
      length_table: '',
      data_table: [],
      selected_rows: [],
      columns: ['checkbox', 'id', 'name', 'form', 'created_date', 'updated_date', 'action'],
      options: {
        headings: {
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
          id: 'ID',
          name: 'RECORD NAME',
          form: 'FORM',
          created_date: 'CREATED DATE',
          updated_date: 'UPDATED DATE',
          action: 'ACTION',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        dateFormat: 'MMM D, YYYY',
        filterable: true,
        sortable: ['created_date', 'updated_date'],
        perPage: 5,
        orderBy: {
          ascending: false,
          column: 'created_date',
        },
        perPageValues: [5, 10, 20, 50, 100],
        rowClassCallback(row) {
          return `row-${row.organigram_id}`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
            status: 1,
          };
        },
        requestFunction(data) {
          const token = Vue.ls.get('Token');

          const api = axios.create({
            baseURL: process.env.GATE_URL,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          return api.post(this.url, data).catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/getProblemMessage', e.message);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    env() {
      return '/jojoform-transaction/summary-block/claimer/data-table/by-record';
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleLoading', 'toggleProblem']),
    loading(data) {
      const a = data;
      a.pagination.query_type = this.select_type;
      // eslint-disable-next-line
      data.status = this.status;
      if (data.status !== 1 && data.status !== 4) {
        this.columns = ['id', 'name', 'form', 'created_date', 'updated_date', 'action'];
      }
      if (this.load_first) {
        this.toggleLoading();
      }
    },
    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
    filter(val) {
      this.$refs.requestorServer.setFilter(val);
    },
    rowClick(data) {
      const id = data.row.id;
      this.$router.replace(`/detail/${this.status}/${id}`);
    },
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.requestorServer.setFilter('');
      }
    },
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
  },
};
