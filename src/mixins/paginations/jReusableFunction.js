export default {
  checkbox(h, objThis) {
    const thisParams = objThis;

    function checkedAll() {
      thisParams.selected_rows = thisParams.data_table;
    }

    function unCheckedAll() {
      thisParams.selected_rows = [];
    }

    if (thisParams.selected_rows.length === thisParams.data_table.length) {
      return (
        <i
          id= 'select-all-unchecked'
          class= 'fa  fa-check-square i--center-block pointer--cursor'
          style= {{ display: 'block', fontSize: '20px' }}
          onClick= { unCheckedAll }>
        </i>
      );
    }
    return (
      <i
        id= 'select-all-unchecked'
        class= 'fa fa-square-o i--center-block pointer--cursor'
        style= {{ display: 'block', fontSize: '20px' }}
        onClick= { checkedAll }>
      </i>
    );
  },
};
