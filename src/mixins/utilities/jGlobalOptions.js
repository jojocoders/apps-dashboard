import { mapState } from 'vuex';

const jGlobalOptions = {
  computed: {
    ...mapState('jStoreGlobalOptionsPayroll',
      [
        'salaryConfigList',
        'salaryTypeList',
        'taxConfigList',
        'idTypeList',
        'nationalityList',
        'genderList',
        'maritalStatusList',
        'religionList',
        'allowance_type_list',
        'monthList',
      ],
    ),
  },
};

export default jGlobalOptions;
