export default {
  methods: {
    scrollToErrorField(className) {
      setTimeout(() => {
        const elem = document.getElementsByClassName(className)[0];
        elem.scrollIntoView({ behavior: 'smooth', block: 'center' });
      }, 200);
    },
  },
};
