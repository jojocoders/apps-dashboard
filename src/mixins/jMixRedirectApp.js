/* eslint-disable max-len */
import { mapState, mapMutations } from 'vuex';

export default {
  watch: {
    reload_data_component(val) {
      if (val) {
        this.getDataComponent();

        setTimeout(() => {
          this.setReloadDataComponent(false);
        }, 5000);
      }
    },
  },

  computed: {
    ...mapState('jStoreFormUi', ['reload_data_component']),
  },

  methods: {
    ...mapMutations('jStoreFormUi', ['setReloadDataComponent']),

    redirectNativeButton(res) {
      // Untuk aplikasi dengan id-hash
      // const applicationId = res.action.redirect.application_id ? res.action.redirect.application_id.replace(/=/g, '%3D') : '';

      // Untuk aplikasi dengan id
      const applicationId = res.action.redirect.application_id || '';
      const applicationName = res.action.redirect.application_name ? res.action.redirect.application_name.replace(/\s/g, '-') : '';
      const pageName = res.action.redirect.pages_name ? res.action.redirect.pages_name.replace(/\s/g, '-') : '';
      const pageId = res.action.redirect.pages_id || '';

      const formUiId = res.action.redirect.form_ui_id || '';
      const recordId = res.action.redirect.record_id || '';

      const mode = res.action.redirect.mode || '';
      const status = res.action.redirect.status || '';

      const tabName = res.action.redirect.tab_name ? res.action.redirect.tab_name.replace(/\s/g, '-') : '';

      let path = '';
      let reloadPage = false;

      if (applicationName && applicationId && pageName && pageId) {
        if (tabName && !status) {
          path = `/app/${applicationName}/${applicationId}/${pageName}/${pageId}/detail-multiple/${formUiId}/${recordId}/${tabName}`;
          if (Number(pageId) === Number(this.$route.params.pageId) &&
          formUiId === this.$route.params.formUiId &&
          recordId === this.$route.params.id &&
          tabName.toLowerCase() === this.$route.params.tabName.toLowerCase()) {
            reloadPage = true;
          }
        } else if (tabName && status) {
          path = `/app/${applicationName}/${applicationId}/${pageName}/${pageId}/detail-multiple/${formUiId}/${recordId}/${tabName}/${status}/${mode}`;
          if (Number(pageId) === Number(this.$route.params.pageId) &&
          formUiId === this.$route.params.formUiId &&
          recordId === this.$route.params.id &&
          tabName.toLowerCase() === this.$route.params.tabName.toLowerCase() &&
          status === this.$route.params.status &&
          mode === this.$route.params.mode) {
            reloadPage = true;
          }
        } else if (status) {
          path = `/app/${applicationName}/${applicationId}/${pageName}/${pageId}/detail-record/${formUiId}/${recordId}/${status}/${mode}`;
        } else if (formUiId) {
          path = `/app/${applicationName}/${applicationId}/${pageName}/${pageId}/detail-record/${formUiId}/${recordId}`;
        } else {
          path = `/app/${applicationName}/${applicationId}/${pageName}/${pageId}`;
        }
        if (res.action.redirect.param_embed_url) localStorage.setItem('paramEmbedUrl', res.action.redirect.param_embed_url);

        if (reloadPage) {
          if (res.action.redirect.type === 'page' && res.action.redirect.target === 'new_tab') {
            window.open(this.$route.fullPath, '_blank');
          } else {
            setTimeout(() => {
              window.location.reload();
            }, 2500);
          }
        } else if (res.action.redirect.is_use_query_params && res.action.redirect.query_params) {
          const router = this.$router.resolve({
            path,
            query: {
              queryParamsParent: res.action.redirect.query_params && res.action.redirect.query_params.is_multiple ? this.encodeQueryParamsButton(res.action.redirect.query_params) : null,
              queryParams: res.action.redirect.query_params && !res.action.redirect.query_params.is_multiple ? this.encodeQueryParamsButton(res.action.redirect.query_params) : null,
            },
          });
          this.redirectPage(res, router);
        } else if (res.action.redirect.is_use_query_params) {
          const router = this.$router.resolve({
            path,
            query: {
              queryParamsParent: this.$route.query.queryParamsParent || null,
              queryParams: this.$route.query.queryParams || null,
            },
          });
          this.redirectPage(res, router);
        } else if (res.action.redirect.data_layer_multiple_ui) {
          const layerDestination = res.action.redirect.data_layer_multiple_ui.layer;
          const formUiIdParent = res.action.redirect.data_layer_multiple_ui.form_ui_id_parent;
          const recordIdParent = res.action.redirect.data_layer_multiple_ui.record_id_parent;
          const tabNameCurrent = res.action.redirect.data_layer_multiple_ui.current_tab_name ?
            res.action.redirect.data_layer_multiple_ui.current_tab_name.replace(/\s/g, '-') : '';
          const statusCurrent = res.action.redirect.data_layer_multiple_ui.status_parent;
          const modeCurrent = res.action.redirect.data_layer_multiple_ui.mode_parent;
          const pathWorkFlow = statusCurrent && modeCurrent ? `/${statusCurrent}/${modeCurrent}` : '';
          const pathLayer = `/app/${applicationName}/${applicationId}/${pageName}/${pageId}/detail-multiple/${formUiIdParent}/${recordIdParent}/${tabNameCurrent}${pathWorkFlow}`;

          // localStorage.setItem(`pathLayer_${pageId}_${layerDestination}`, pathLayer);
          // localStorage.setItem(`layer_${pageId}`, layerDestination);

          const breadcrumbsParent = {};
          breadcrumbsParent.layer = layerDestination;
          breadcrumbsParent[`pathLayer_${breadcrumbsParent.layer}`] = pathLayer;
          breadcrumbsParent[`dataWorkflowLayer_${breadcrumbsParent.layer}`] = { status, mode };

          const queryParams = {
            filterApp: JSON.stringify({}),
            filterSetup: JSON.stringify({}),
            filterDefault: JSON.stringify({}),
            filterSearchOptions: JSON.stringify({}),
            originalComponent: JSON.stringify({}),
            breadcrumbsParent: JSON.stringify(breadcrumbsParent),
            layer: breadcrumbsParent.layer || 0,
            page: 1,
            status,
          };

          setTimeout(() => {
            // const router = this.$router.resolve(path);
            const router = this.$router.resolve({
              path,
              query: {
                queryParamsParent: btoa(JSON.stringify(queryParams)),
                queryParams: null,
              },
            });
            this.redirectPage(res, router);
          }, 1000);
        } else {
          const router = this.$router.resolve(path);
          this.redirectPage(res, router);
        }
      }
    },

    encodeQueryParamsButton(data) {
      const dataFilterDefault = {
        filter_data: data.filter_default_data || null,
        filter_data_start_date: data.filter_default_data_start_date || null,
        filter_data_end_date: data.filter_data_default_end_date || null,
        select_type: data.filter_default_column_data,
      };
      const queryParams = {
        filter: data.all_filter ? JSON.stringify(data.all_filter) : null,
        filterDefault: JSON.stringify(dataFilterDefault),
        filterSearchOptions: data.filter_search_options_data ? JSON.stringify(data.filter_search_options_data) : null,
        page: data.page || 1,
        indexBlock: data.index_summary_block || 0,
        layer: data.layer_parent || 0,
        status: data.status,
      };
      return btoa(JSON.stringify(queryParams));
    },

    redirectPage(res, router) {
      if (res.action.redirect.type === 'page' && res.action.redirect.target === 'new_tab') {
        window.open(router.href, '_blank');
      } else {
        window.open(router.href, '_self');
      }
    },

    handleFuncReloadActionButton(redirect) {
      if (redirect.reload_data) this.getDataComponent();
      if (redirect.reload_component) this.setReloadDataComponent(true);
      if (redirect.reload_page) window.location.reload();
    },
  },
};
