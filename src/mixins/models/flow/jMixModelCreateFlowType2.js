/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelCreateFlowType2 = {
  computed: {
    generateData() {
      if (this.flow !== null) {
        const genData = {
          name: this.flow.name,
          type: 1,
          use_division_chart: true,
          division_id: this.flow.division.id,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jFlows', ['createFlowByDivision']),
    sendData() {
      if (this.generateData !== null) {
        this.createFlowByDivision(this.generateData);
      }
    },
  },
};
