/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelEditFlowType2 = {
  computed: {
    generateData() {
      if (this.flow !== null) {
        const genData = {
          id: this.flow.id,
          name: this.flow.name,
          type: 1,
          use_division_chart: true,
          division_id: this.flow.division.id,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jFlows', ['updateFlowByDivision']),
    sendData() {
      this.$refs.formFlowType2.validateForm();
      if (this.generateData !== null) {
        this.updateFlowByDivision(this.generateData);
      }
    },
  },
};
