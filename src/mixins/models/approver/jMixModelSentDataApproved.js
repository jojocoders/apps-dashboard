/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelSentDataApproved = {
  computed: {},
  methods: {
    ...mapActions('jStoreApprover', ['sentDataDraftApprover', 'hideModal', 'showModal']),
    sentDataApproved() {
      let dataName = '';
      let params = [];
      this.selectedRows.forEach((e, i) => {
        if (e.reference_ids) {
          params = params.concat(e.reference_ids);
          dataName += `${e.first_name} ${e.last_name}`;
          dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName}, `;
        } else {
          params.push(e.id);
          dataName += e.name;
          dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName}, `;
        }
      });
      if (this.selectedRows !== null) {
        this.hideModal();
        this.$swal({
          title: `Are you sure want to approve ${dataName}`,
          input: 'text',
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          inputValidator: (value) => {
            if (!value) {
              return 'You need to write something!';
            }
            return '';
          },
        }).then((result) => {
          if (result.value) {
            const data = [];
            params.forEach((id) => {
              data.push({
                id: id,
                status: 'approved',
                note: result.value,
              });
            });
            this.sentDataDraftApprover({ action: data });
          } else {
            this.showModal();
            // eslint-disable-next-line
            throw {};
          }
        });
      }
    },
    sentDataRejected() {
      let dataName = '';
      let params = [];
      this.selectedRows.forEach((e, i) => {
        if (e.reference_ids) {
          params = params.concat(e.reference_ids);
          dataName += `${e.first_name} ${e.last_name}`;
          dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName}, `;
        } else {
          params.push(e.id);
          dataName += e.name;
          dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName}, `;
        }
      });
      if (this.selectedRows !== null) {
        this.hideModal();
        this.$swal({
          title: `Are you sure want to reject ${dataName}`,
          input: 'text',
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          inputValidator: (value) => {
            if (!value) {
              return 'You need to write something!';
            }
            return '';
          },
        }).then((result) => {
          if (result.value) {
            const data = [];
            params.forEach((id) => {
              data.push({
                id: id,
                status: 'rejected',
                note: result.value,
              });
            });
            this.sentDataDraftApprover({ action: data });
          } else {
            this.showModal();
            // eslint-disable-next-line
            throw {};
          }
        });
      }
    },
  },

};
