/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelSentDataDrafts = {
  computed: {},

  methods: {
    ...mapActions('jStoreRequestor', ['sentDataDraftRequestor', 'closedDataApproveRequestor', 'deleteDataDraftRequestor', 'cancelDataApproveRequestor']),

    sentDataDrafts() {
      let dataName = '';
      const params = [];
      this.selectedRows.forEach((e, i) => {
        params.push(e.id);
        dataName += e.name;
        dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName},`;
      });
      if (this.selectedRows !== null) {
        const text = `want to sent ${dataName}`;

        this.$swal({
          title: 'Are you sure',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.sentDataDraftRequestor({ ids: params });
          }
        });
      }
    },

    deleteDataDrafts() {
      let dataName = '';
      const params = [];
      this.selectedRows.forEach((e, i) => {
        params.push(e.id);
        dataName += e.name;
        dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName},`;
      });
      if (this.selectedRows !== null) {
        const text = `want to delete ${dataName}`;

        this.$swal({
          title: 'Are you sure',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.deleteDataDraftRequestor({ ids: params });
          }
        });
      }
    },

    closedDataApprove() {
      let dataName = '';
      const params = [];
      this.selectedRows.forEach((e, i) => {
        params.push(e.id);
        dataName += e.name;
        dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName},`;
      });
      if (this.selectedRows !== null) {
        const text = `want to closed ${dataName}`;

        this.$swal({
          title: 'Are you sure',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.closedDataApproveRequestor({ ids: params });
          }
        });
      }
    },

    cancelDataApprove() {
      let dataName = '';
      const params = [];
      this.selectedRows.forEach((e, i) => {
        params.push(e.id);
        dataName += e.name;
        dataName = i + 1 === this.selectedRows.length ? dataName : `${dataName},`;
      });
      if (this.selectedRows !== null) {
        const text = `want to cancel ${dataName}`;

        this.$swal({
          title: 'Are you sure',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.cancelDataApproveRequestor({ ids: params });
          }
        });
      }
    },
  },

};
