/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

import { mapActions } from 'vuex';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';
import axios from 'axios';
import Vue from 'vue';

export const jApproverUserDataTable = {
  data() {
    return {
      filter_data: '',
      select_type: 'staff',
      options_search: [
        {
          text: 'STAFF',
          value: 'staff',
        },
      ],
      load_first: false,
      length_table: '',
      selected_rows: [],
      data_table: [],
      columns: ['checkbox', 'more', 'staff', 'reference_ids_storage', 'total_records', 'created_date', 'updated_date'],
      options: {
        headings: {
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
          staff: 'STAFF',
          reference_ids_storage: 'REFERENCE ID',
          total_records: 'RECORD',
          created_date: 'CREATED DATE',
          updated_date: 'UPDATED DATE',
          more: '',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        dateFormat: 'MMM D, YYYY',
        filterable: true,
        sortable: ['total_records', 'created_date', 'updated_date'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        orderBy: {
          ascending: false,
          column: '',
        },
        rowClassCallback(row) {
          return `row-${row.organigram_id}`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy === 'staff' ? 'name' : data.orderBy,
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
            status: 2,
          };
        },
        requestFunction(data) {
          const token = Vue.ls.get('Token');

          const api = axios.create({
            baseURL: process.env.GATE_URL,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          return api.post(this.url, data).catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/getProblemMessage', e.message);
          });
        },
        responseAdapter({ data }) {
          data.data.forEach((e) => {
            const referenceId = e.reference_ids ? e.reference_ids : [];
            // eslint-disable-next-line
            e.reference_ids_storage = [...referenceId];
            e.reference_ids_storage.splice(2);
          });
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
        templates: {
          staff(h, row) {
            return <div on-click={ () => this.$router.replace(`/approver/record/byuser/${row.user_company_id}`)}>{row.first_name} {row.last_name}</div>;
          },
          reference_id(h, row) {
            return <div on-click={ () => this.$router.replace(`/approver/record/byuser/${row.user_company_id}`)}>{row.reference_ids.length}</div>;
          },
        },
      },
    };
  },
  computed: {
    env() {
      return this.table_api_url;
    },
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.approverUserServer.setFilter('');
      }
    },

    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleLoading', 'toggleProblem']),

    loading(data) {
      const a = data;
      a.pagination.query_type = this.select_type === 'staff' ? 'name' : this.select_type;

      // eslint-disable-next-line no-param-reassign
      data.status = this.status;
      if (data.status !== 3) {
        this.columns = ['satff', 'reference_id', 'record', 'created_date', 'updated_date'];
      }
      if (this.load_first) {
        this.toggleLoading();
      }
    },

    loaded(data) {
      data.data.data.forEach((e) => {
        const referenceId = e.reference_ids ? e.reference_ids : [];
        // eslint-disable-next-line
        e.reference_ids_storage = [...referenceId];
        e.reference_ids_storage.splice(2);
      });
      this.data_table = data.data.data;
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.approverUserServer.getData();
    },

    filter(val) {
      this.$refs.approverUserServer.setFilter(val);
    },
  },
};
