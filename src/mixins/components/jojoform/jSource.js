/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import jApi from '@/services/jApi';
import { mapActions } from 'vuex';

export const jSource = {
  data() {
    return {
      filter_data: '',
      select_type: 'name',
      options_search: [
        {
          text: 'Name',
          value: 'name',
        },
      ],
      load_first: false,
      length_table: '',
      columns: ['name', 'description', 'total_value', 'created_date', 'updated_date'],
      options: {
        headings: {
          name: 'NAME',
          description: 'DESCRIPTION',
          total_value: 'TOTAL VALUE',
          created_date: 'CREATED DATE',
          updated_date: 'UPDATED DATE',

        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        dateFormat: 'MMM D, YYYY',
        filterable: true,
        texts: {
          filterPlaceholder: 'Search...',
          limit: 'Rows per page',
        },
        sortable: ['name', 'created_date', 'updated_date'],
        perPage: 5,
        orderBy: {
          ascending: true,
          column: 'created_date',
        },
        perPageValues: [5, 10, 20, 50, 100],
        rowClassCallback(row) {
          return `row-${row.id}`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const api = jApi.generateApi();

          return api.post(this.url, data).catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/toggleProblem', e.message);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.recordsFiltered ? data.recordsFiltered : 0,
          };
        },
      },
    };
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.sourceServer.setFilter('');
      }
    },
  },
  computed: {
    env() {
      return '/jojoform/additional-info/source/data-table';
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleLoading', 'toggleProblem']),
    loading() {
      if (this.load_first) {
        this.toggleLoading();
      }
    },
    loaded() {
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
    filter(val) {
      this.$refs.sourceServer.setFilter(val);
    },
  },
};
