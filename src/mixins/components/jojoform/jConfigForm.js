/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

import { mapState, mapActions } from 'vuex';
import { required } from 'vuelidate/lib/validators';

export const jConfigForm = {
  data() {
    return {
      name: '',
      isDetail: false,
      columns: ['option', 'action'],
      tableData: [],
      relations: [
        {
          id: 0,
          form_id: null,
          rule_id: null,
          categories: [
            {
              id: 0,
              rule_id: null,
              category_id: null,
            },
          ],
        },
      ],
      is_relation:false,
      form: null,
      data_position_name: null,
      data_delete: [],
      data_delete_category: [],
    };
  },
  computed: {
    ...mapState('jStoreConfig', ['list_position', 'config_detail', 'list_rule', 'list_form']),
    JPConfig() {
      let data = this.relations.map(e => ({ id:e.id,
        form_id: e.form_id.id,
        rule_id: e.form_id.is_use_category === true ? 0 : e.rule_id.id,
        categories: e.form_id.is_use_category === true ? this.getCategories(e.categories) : [] }));
      data = data.map((e) => { e.is_delete = false; return e; });
      data = this.isDetail && this.data_delete.length !== 0 ? data.concat(this.data_delete) : data;
      return {
        id: this.isDetail ? this.$route.params.id : 0,
        organigram_id: this.data_position_name.id,
        mapping: data,
      };
    },
    listForm() {
      return this.list_form;
    },
    listPosition() {
      return this.list_position;
    },
  },
  validations: {
    data_position_name: { required },
    relations: {
      $each: {
        id: {
          required,
        },
        form_id: {
          required,
        },
        rule_id: {

        },
        categories: {
          $each: {
            id: {

            },
            rule_id: {

            },
            category_id: {

            },
          },
        },
      },
    },
  },
  watch: {
    config_detail(data) {
      this.data_position_name = { id: data.data[0].organigram_id, label: data.data[0].organigram_name.length > 0 ? data.data[0].organigram_name : '-' };
      this.relations = data.data.map(e => (
        {
          id: e.id,
          categories: e.category,
          form_id:{ id:e.form_id },
          rule_id:{ id: e.rule_id, label: e.rule_name.length > 0 ? e.form_name : '-' },
        }
      ));
    },
  },
  async created() {
    const params = {
      pagination: {
        limit: 0,
        page: 0,
        column: '',
        ascending: false,
      },
    };
    try {
      await Promise.all([
        this.getDataPosition(params),
      ]);
      if (this.$route.params.id) {
        this.isDetail = true;
        this.detailConfig({ organigram_id: this.$route.params.id });
      } else {
        this.data_position_name = { id: this.$route.params.root };
      }
    } catch (err) {
      throw err;
    }
  },
  methods: {
    ...mapActions('jStoreConfig', ['createConfig', 'detailConfig', 'updateConfig', 'deleteConfig', 'getDataPosition', 'getDataRule', 'getDataForm']),
    addRelation() {
      this.relations.push({
        id: 0,
        form_id: null,
        rule_id: null,
        categories: [
          {
            id: 0,
            rule_id: null,
            category_id: null,
          },
        ],
      });
    },
    addCategories(data) {
      this.relations[data].categories.push({ id: 0, category_id: null, rule_id: null });
    },
    submitConfig() {
      this.$v.data_position_name.$touch();
      this.$v.relations.$touch();
      if (!this.$v.relations.$error) {
        this.isDetail ? this.updateConfig(this.JPConfig) : this.createConfig(this.JPConfig);
      }
    },
    deleteRelation(data) {
      this.relations = this.relations.filter((e, i) => {
        if (Number(i) === Number(data) && e.id !== 0) {
          e.is_delete = true;
          e.form_id = e.form_id.id;
          e.rule_id = e.rule_id.id;
          this.data_delete.push(e);
        }
        return Number(i) !== Number(data);
      });
    },
    deleteCategories(data1, data2) {
      this.relations[data1].categories = this.relations[data1].categories.filter((e, i) => {
        if (Number(i) === Number(data2) && e.id !== 0) {
          e.is_delete = true;
          e.category_id = e.category_id.id;
          e.rule_id = e.rule_id.id;
          this.data_delete_category.push(e);
        }
        return Number(i) !== Number(data2);
      });
    },
    popDeleteConfirm() {
      this.$swal({
        title: 'Delete Form',
        text: 'Are you sure want to delete this data?',
        showCancelButton: true,
        confirmButtonText: 'Delete',
      }).then((result) => {
        if (result.value) {
          // req api to delete
          const params = {
            ids: [this.$route.params.id],
          };
          this.deleteConfig(params);
        }
      });
    },
    getValue(data) {
      // console.log(data);
    },
    getCategories(val) {
      let data = val.map(e => ({ id:e.id,
        category_id: e.category_id.id,
        rule_id: e.rule_id.id }));
      data = data.map((e) => { e.is_delete = false; return e; });
      data = this.isDetail && this.data_delete_category.length !== 0 ?
        data.concat(this.data_delete_category) : data;
      return data;
    },
  },
};
