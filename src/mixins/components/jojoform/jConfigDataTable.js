/* eslint-disable no-else-return */
/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

import { mapActions } from 'vuex';
import apiConfig from '@/services/jApiConfig';

import axios from 'axios';
import Vue from 'vue';

export const jConfigDataTable = {
  data() {
    return {
      load_first: false,
      length_table: '',
      columns: ['organigram_id', 'organigram_name', 'rule_name', 'form_name', 'created_date', 'updated_date', 'action'],
      options: {
        headings: {
          organigram_id: 'POSITION ID',
          organigram_name: 'POSITION NAME',
          rule_name: 'RULE NAME',
          form_name: 'FORM',
          approver: 'APPROVER',
          created_date: 'CREATED DATE',
          updated_date: 'UPDATED DATE',
          action: 'ACTION',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        dateFormat: 'MMM D, YYYY',
        filterable: true,
        sortable: ['created_date', 'updated_date'],
        perPage: 5,
        orderBy: {
          ascending: true,
          column: 'organigram_id',
        },
        perPageValues: [5, 10, 20, 50, 100],
        rowClassCallback(row) {
          return `row-${row.organigram_id}`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListConfig = apiConfig.listDataTableConfig(data);
          return getListConfig.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
        templates: {
          action(h, row) {
            if (row.organigram_id !== 0 && row.rule_id !== 0) {
              return <b-button class="btn blue" on-click={ () => this.$router.replace(`/config/mapping/edit/${row.organigram_id}`)}>Edit</b-button>;
            } else {
              return <b-button class="btn green" on-click={ () => this.$router.replace(`/config/mapping/create/${row.organigram_id}`)}>Add</b-button>;
            }
          },
        },
      },
    };
  },
  computed: {
    env() {
      return '/jojoform-transaction/setup/organigram-form-rule/data-table';
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleLoading', 'toggleProblem']),
    loading() {
      if (this.load_first) {
        this.toggleLoading();
      }
    },
    loaded() {
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
  },
};
