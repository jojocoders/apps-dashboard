/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

import { mapActions } from 'vuex';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';
import jConstant from '@/utils/constant';
import axios from 'axios';
import Vue from 'vue';

export const jApproverRecordByUserDataTable = {
  components: {
    jReusableFunction,
  },
  data() {
    return {
      load_first: false,
      length_table: '',
      filter_data: '',
      select_type: 'name',
      options_search: [
        {
          text: 'RECORD TITLE',
          value: 'name',
        },
      ],
      data_table: [],
      columns: ['checkbox', 'record_id', 'name', 'form', 'created_date', 'updated_date', 'action'],
      options: {
        headings: {
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
          record_id: 'REFERENCE ID',
          name: 'RECORD TITLE',
          form: 'FORM',
          created_date: 'CREATED DATE',
          updated_date: 'UPDATED DATE',
          action: 'ACTION',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        dateFormat: 'MMM D, YYYY',
        filterable: true,
        sortable: [],
        perPage: 5,
        orderBy: {
          ascending: false,
          column: '',
        },
        perPageValues: [5, 10, 20, 50, 100],
        rowClassCallback(row) {
          return `row-${row.organigram_id}`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
            status: jConstant.STATUS_SENT,
            user_company_id: 0,
          };
        },
        requestFunction(data) {
          const token = Vue.ls.get('Token');

          const api = axios.create({
            baseURL: process.env.GATE_URL,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          return api.post(this.url, data).catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/getProblemMessage', e.message);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    env() {
      return this.table_api_url;
    },
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.approverRecordByUser.setFilter('');
      }
    },

    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleLoading', 'toggleProblem']),
    loading(data) {
      // eslint-disable-next-line
      data.pagination.query_type = this.select_type;
      // eslint-disable-next-line
      data.user_company_id = Number(this.user_company_id);
      if (this.load_first) {
        this.toggleLoading();
      }
    },
    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
    refreshTable() {
      this.$refs.approverRecordByUser.getData();
    },
    filter(val) {
      this.$refs.approverRecordByUser.setFilter(val);
    },
  },
};
