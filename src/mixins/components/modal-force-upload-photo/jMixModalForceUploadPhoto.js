import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState('jStoreHome', ['modal_upload_photo']),
  },

  watch: {
    modal_upload_photo(val) {
      this.photo = localStorage.getItem('photo');
      const body = document.getElementsByTagName('body')[0];
      if (val) {
        body.style.paddingRight = '8px';
        body.style.overflow = 'hidden';
      } else {
        body.style.paddingRight = null;
        body.style.overflow = null;
      }
    },
  },

  methods: {
    toggleModalUploadPhoto() {
      const modal = this.$parent.$refs.modalForceUploadPhoto;
      modal.toggleModal();
    },
  },
};
