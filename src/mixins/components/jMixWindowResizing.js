/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
export const jMixWindowResizing = {
  data() {
    return {
      window_height: 0,
      window_width: 0,
    };
  },
  created() {
    window.addEventListener('resize', this.resizeWindow);
    this.resizeWindow();
  },
  destroyed() {
    window.removeEventListener('resize', this.resizeWindow);
  },
  methods: {
    resizeWindow() {
      this.window_height = window.innerHeight;
      this.window_width = window.innerWidth;
    },
  },
};
