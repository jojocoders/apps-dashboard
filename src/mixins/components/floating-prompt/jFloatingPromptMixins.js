/* eslint-disable import/no-duplicates,import/prefer-default-export */
import { mapGetters } from 'vuex';
import { mapActions } from 'vuex';

/**
 * Created by Arga Ghulam Ahmad - arga@jojonomic.com
 * Last update on 9 August 2018
 */
export const jFloatingPromptMixins = {
  /**
     * active: is floating prompt active?
     * @returns {{active: boolean}}
     */
  data() {
    return {
      active: false,
    };
  },
  /**
     * getDataId: get array of data ids
     * countChosenIds: count chosen ids
     * countChosenSubIds: count chosen sub ids
     * sumTotalAmount: sum total amount
     * sumTotalRecord:  sum total record
     * toggleActive:  toggleActive
     * closeFloatingPrompt: close floating prompt
     */
  methods: {
    ...mapGetters(['getCheckedId', 'getDataContainer', 'getResult', 'getCurrentTable']),
    ...mapActions(['actionCurrentTable']),
    getDataId() {
      return this.getDataContainer().map(data => data.dataId);
    },
    countChosenIds() {
      return this.getCheckedId().length;
    },
    countChosenSubIds() {
      let subIdsTotal = 0;
      this.getResult().forEach((data) => {
        subIdsTotal += data.subIds.length;
      });
      return subIdsTotal;
    },
    sumTotalAmount() {
      let totalAmount = 0;
      this.getResult().forEach((data) => {
        totalAmount += data.amount;
      });
      return totalAmount;
    },
    sumTotalRecord() {
      let totalRecord = 0;
      this.getResult().forEach((data) => {
        totalRecord += data.records;
      });
      return totalRecord;
    },
    toggleActive() {
      this.active = !this.active;
    },
    closeFloatingPrompt() {
      this.actionCurrentTable('');
    },
  },
  /**
     * getTotalAmountsString: get total amount as string
     * getFloatingPromptClass: get floating prompt class when active and inactive
     * getDetailsArrowIcon: get arrow icon for active and inactive floating prompt
     */
  computed: {
    getTotalAmountsString() {
      return `Rp${this.sumTotalAmount()}`;
    },
    getFloatingPromptClass() {
      if (this.active) {
        return 'floating-prompt details-active';
      }
      return 'floating-prompt';
    },
    getDetailsArrowIcon() {
      if (!this.active) {
        return 'fa fa-angle-up';
      }
      return 'fa fa-angle-down';
    },
  },
};
