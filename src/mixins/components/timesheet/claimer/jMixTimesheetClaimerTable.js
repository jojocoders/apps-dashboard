import { mapActions } from 'vuex';
import jApiGetListClaimer from '../../../../services/timesheet/claimer/jApiTimesheetClaimerList';
import jLoadingTable from '../../../../components/skeleton-loading/jLoadingTable';
import jReusableFunction from '../../../paginations/jReusableFunction';
import constant from '../../../../utils/constant';

const jMixTimesheetClaimerTable = {
  components: {
    jLoadingTable,
    jReusableFunction,
    jApiGetListClaimer,
  },
  data() {
    return {
      status: '',
      load_first: false,
      length_table: '',
      selected_rows: [],
      data_table: [],
      env: 'times/timesheet/list',
      columns: ['checkbox', 'id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'],
      options: {
        headings: {
          checkbox() {
            // const objThis = this;
            // console.log(h);
            // return jReusableFunction.checkbox(h, objThis);
          },
          id: 'ID',
          date: 'DATE',
          total_task: 'TASK',
          duration: 'DURATION',
          created_date: 'CREATED DATE',
          updated_date: 'UPDATED DATE',
        },
        debounce: 1000,
        sortable: ['id'],
        sortIcon: { base: 'fa', up: 'fa-chevron-up', down: 'fa-chevron-down', is: 'fa-sort' },
        orderBy: {
          ascending: true,
          column: 'id',
        },
        filterable: true,
        texts: {
          filterPlaceholder: 'Search...',
          limit: 'Rows per page',
        },
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          if (window.location.href.includes('reported')) {
            this.status = constant.TIMES_STATUS_APPROVED;
          } else if (window.location.href.includes('sent')) {
            this.status = constant.TIMES_STATUS_SENT;
          } else {
            this.status = constant.TIMES_STATUS_DRAFT;
          }
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
            status: [this.status],
          };
        },
        requestFunction(data) {
          const listClaimer = jApiGetListClaimer.getListSummary(data);
          return listClaimer.catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/toggleStateProblem', e.message);
          });
        },
        responseAdapter(data) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['showLoading', 'hideLoading', 'toggleProblem']),
    loading() {
      if (this.load_first) {
        this.showLoading();
      }
    },
    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
    // table row
    rowClick(data) {
      const date = new Date(data.row.date).getTime();
      localStorage.setItem('timesheet_active_id', data.row.id);
      this.$router.push(`/timesheet/claimer/detail/${date}`);
    },
    filter(val) {
      this.$refs.listForm.setFilter(val);
    },
  },
  created() {
    if (window.location.href.includes('claimer')) {
      switch (this.statusTimesheet) {
        case 'draft':
          this.columns = ['checkbox', 'id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
          break;
        case 'sent':
          this.columns = ['id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
          break;
        case 'reported':
          this.columns = ['id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
          break;
        default:
          this.columns = ['checkbox', 'id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
      }
    } else {
      switch (this.statusTimesheet) {
        case 'sent':
          this.columns = ['checkbox', 'id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
          break;
        case 'reported':
          this.columns = ['id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
          break;
        default:
          this.columns = ['checkbox', 'id', 'date', 'total_task', 'duration', 'created_date', 'updated_date'];
      }
    }
  },
};

export default jMixTimesheetClaimerTable;
