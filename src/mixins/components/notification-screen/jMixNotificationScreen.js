import { mapState, mapActions } from 'vuex';

const jMixNotificationScreen = {
  computed: {
    ...mapState('jStoreNotificationScreen', ['info_screen', 'warning_screen', 'problem_screen', 'success_screen', 'loading_screen', 'maintenance_screen']),
    ...mapState('jStoreNotificationScreen', ['info_message', 'warning_message', 'problem_message', 'problem_code', 'success_message', 'loading_message', 'maintenance_message']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleInfo', 'toggleWarning', 'toggleProblem', 'toggleSuccess', 'toggleLoading', 'hideLoading', 'showLoading']),
  },
  watch: {
    // loading when move to another path
    // $route() {
    //   this.$store.dispatch('jStoreLogin/getProfileAndRole');
    // },
    // trigger info_screen
    info_screen() {
      const info = this.$refs.infoScreen;
      if (this.info_screen === true) {
        info.toggleInfoScreen();
        this.toggleInfo();
      }
    },
    // trigger warning_screen
    warning_screen() {
      const warning = this.$refs.warningScreen;
      if (this.warning_screen === true) {
        warning.toggleWarningScreen();
        this.toggleWarning();
      }
    },
    // trigger problem_screen
    problem_screen() {
      const problem = this.$refs.problemScreen;
      if (this.problem_screen === true) {
        if (!problem.isVisible) problem.toggleProblemScreen();
        this.toggleProblem();
      }
    },
    // trigger success_screen
    success_screen() {
      const success = this.$refs.successScreen;
      if (this.success_screen === true) {
        success.toggleSuccessScreen();
        this.toggleSuccess();
      }
    },
    // tigger loading_screen
    loading_screen() {
      const loading = this.$refs.loadingScreen;
      if (this.loading_screen === true) {
        loading.showLoadingScreen();
      } else {
        loading.closeLoadingScreen();
      }
    },
    // tigger cover loading_screen
    cover_loading_screen() {
      const loading = this.$refs.loadingScreen;
      if (this.loading_screen === true) {
        loading.showCoverLoadingScreen();
      } else {
        loading.closeLoadingScreen();
      }
    },
    // trigger maintenance_screen
    maintenance_screen() {
      const maintenance = this.$refs.maintenanceScreen;
      maintenance.toggleMaintenanceScreen();
    },
  },
};

export default jMixNotificationScreen;
