import { mapState, mapActions } from 'vuex';

const jStoreNotificationScreen = {
  computed: {
    ...mapState('jStoreNotificationScreen', ['info_screen', 'warning_screen', 'problem_screen', 'success_screen', 'loading_screen']),
    ...mapState('jStoreNotificationScreen', ['info_message', 'warning_message', 'problem_message', 'success_message', 'loading_message']),
    ...mapState('jStoreNotificationScreen', ['info_action', 'warning_action', 'problem_action', 'success_action', 'loading_action']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleInfo', 'toggleWarning', 'toggleProblem', 'toggleSuccess', 'toggleLoading']),
  },
  watch: {
    problem_action() {
      const problem = this.$refs.problemScreen;
      if (this.problem_screen === true) {
        problem.isVisible = true;
      } else {
        problem.isVisible = false;
      }
    },
    info_action() {
      const info = this.$refs.infoScreen;
      if (this.info_screen === true) {
        info.isVisible = true;
      } else {
        info.isVisible = false;
      }
    },
    warning_action() {
      const warning = this.$refs.warningScreen;
      if (this.warning_screen === true) {
        warning.isVisible = true;
      } else {
        warning.isVisible = false;
      }
    },
    success_action() {
      const success = this.$refs.successScreen;
      if (this.success_screen === true) {
        success.isVisible = true;
      } else {
        success.isVisible = false;
      }
    },
    loading_action() {
      const loading = this.$refs.loadingScreen;
      if (this.loading_screen === true) {
        loading.showLoadingScreen();
      } else {
        loading.closeLoadingScreen();
      }
    },
    // loading when move to another path
    $route() {
      this.toggleLoading('Please wait...');
      setTimeout(() => {
        this.toggleLoading('');
      }, 500);
    },
  },
};

export default jStoreNotificationScreen;
