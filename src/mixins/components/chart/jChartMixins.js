const jChartMixins = {
  methods: {
    setData(data) {
      this.rawData = data;
      this.chartData = this.convertData(this.rawData);
    },
    convertData(rawData) {
      let chartData = [];
      let prevChildrenCount = 0;
      const spacing = 0;
      rawData.forEach((e) => {
        chartData.push(this.convertElement(e, null, spacing, prevChildrenCount));
        if (e.children.length !== 0) {
          const children = this.convertChildren(e.children, e.id, spacing + 1);
          prevChildrenCount = children.length;
          chartData = chartData.concat(children);
        }
      });
      return chartData;
    },
    convertChildren(list, parent, spacing) {
      let data = [];
      let prevChildrenCount = 0;
      let isLastChild;
      list.forEach((e, i) => {
        isLastChild = (i === list.length - 1);
        data.push(this.convertElement(e, parent, spacing, isLastChild, prevChildrenCount));
        if (e.children.length !== 0) {
          const children = this.convertChildren(e.children, e.id, spacing + 1);
          prevChildrenCount = children.length;
          data = data.concat(children);
        }
      });
      return data;
    },
    convertElement(data, parent, spacing, isLastChild, childrenCount) {
      let pattern;
      if (parent === null) pattern = 1;
      else if (isLastChild) pattern = 3;
      else pattern = 2;
      this.showFlag[data.id] = true;

      return {
        id: data.id,
        name: data.name,
        description: data.description,
        parent_id: parent,
        prev_children_count: childrenCount,
        pattern: this.createPattern(pattern),
        spacing,
        is_show_child: true,
      };
    },
    createPattern(type) {
      switch (type) {
        case 1:
          return this.patternOne();
        case 2:
          return this.patternTwo();
        case 3:
          return this.patternThree();
        default:
          return this.patternOne();
      }
    },
    patternOne() {
      return {
        is_have_sibling_up: false,
        is_have_sibling_down: false,
      };
    },
    patternTwo() {
      return {
        is_have_sibling_up: true,
        is_have_sibling_down: true,
      };
    },
    patternThree() {
      return {
        is_have_sibling_up: true,
        is_have_sibling_down: false,
      };
    },
    addData(parentId, data) {
      this.editElement(this.rawData, parentId, data, 2);
    },
    editData(parentId, data) {
      this.editElement(this.rawData, parentId, data, 1);
    },
    deleteData(parentId) {
      this.editElement(this.rawData, parentId, null, 3);
    },
    editElement(tree, parentId, data, edit) {
      tree.forEach((e) => {
        e.is_delete = false;
        if (e.id === parentId) {
          if (edit === 1) {
            e.name = data.name;
            e.description = data.description;
          } else if (edit === 2) {
            const child = {
              id: 0,
              name: data.name,
              description: data.description,
              children: [],
            };
            e.children.push(child);
          } else if (edit === 3) {
            e.is_delete = true;
          }
        }
        if (e.children.length !== 0) this.editElement(e.children, parentId, data, edit);
      });
    },
    addRoot(data) {
      this.editElement(this.rawData, null, null, 0);
      this.rawData.push(data);
    },
  },
};

export default jChartMixins;
