/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';
import jApi from '@/services/jApi';

export const jRules = {
  data() {
    return {
      filter_data: '',
      select_type: 'name',
      options_search: [
        {
          text: 'Id',
          value: 'id',
        },
        {
          text: 'Name',
          value: 'name',
        },
      ],
      env: 'jojoflow/rule/list',
      load_first: false,
      columns: ['id', 'name', 'created_date', 'flow'],
      options: {
        // see the options API
        headings: {
          id: 'ID',
          name: 'RULES NAME',
          created_date: 'CREATED DATE',
          flow: 'SET OF FLOW',
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        filterable: true,
        sortable: ['name', 'created_date'],
        perPage: 10,
        perPageValues: [10, 20, 50, 100],
        rowClassCallback(row) {
          return `row-${row.id}`;
        },
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : '',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
              query_type: data.query_type ? data.query_type : '',
            },
          };
        },
        requestFunction(data) {
          const api = jApi.generateApi();
          return api.post(this.url, data).catch((e) => {
            this.dispatch('error', e);
            this.$store.commit('jStoreNotificationScreen/getProblemMessage', e.message);
          });
        },
        responseAdapter({ data }) {
          if (data.length) {
            this.length_table = data.length;
          }
          return {
            data: data.data ? data.data : [],
            count: data.length,
          };
        },
      },
    };
  },
  watch: {
    filter_data(val) {
      if (val === '') {
        this.$refs.rulesServer.setFilter('');
      }
    },
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleLoading', 'toggleProblem']),

    filter(val) {
      this.removeRowBackground();
      this.$refs.rulesServer.setFilter(val);
    },

    loading(val) {
      const a = val;
      a.pagination.query_type = this.select_type;
      if (this.load_first) {
        this.toggleLoading();
      }
    },
    loaded() {
      if (this.load_first) {
        this.toggleLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },
  },
};
