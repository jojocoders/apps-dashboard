/* eslint-disable no-param-reassign,no-restricted-syntax,guard-for-in,
import/prefer-default-export */
const _ = require('lodash');

export const jSelectMixins = {
  data() {
    return {
      selected: null,
      defaultOpt: null,

      dataId: null,
      data: null,
      dataRelation: null,
      changedData: null,
      changedDataRelation: null,
    };
  },
  created() {
    this.selected = _.cloneDeep(this.prevSelected);
    this.defaultOpt = _.cloneDeep(this.prevDefault);
  },
  watch: {
    selected() {
      this.mGetDataId();
      this.mGetData();
      this.mGetDataRelation();
      this.mGetChangedData();
      this.mGetChangedDataRelation();
      this.$emit('selectedChange', this.selected);
      this.$emit('eDataId', this.dataId);
      this.$emit('eData', this.data);
      this.$emit('eChangedData', this.changedData);
      this.$emit('eDataRelation', this.dataRelation);
      this.$emit('eChangedDataRelation', this.changedDataRelation);
    },
    defaultOpt() {
      this.$emit('defaultChange', this.defaultOpt);
    },
  },
  methods: {
    passDataId() {
      return this.dataId;
    },
    passData() {
      return this.data;
    },
    passDataRelation() {
      return this.dataRelation;
    },
    passChangedData() {
      return this.changedData;
    },
    passChangedDataRelation() {
      return this.changedDataRelation;
    },
    mSelectAll() {
      this.selected = this.options.slice(0);
    },
    mUnSelectAll() {
      this.selected = [];
    },
    mSetDefaultOpt() {
      this.defaultOpt = [];
      for (const j in this.defaultOptRelation) {
        const i = this.defaultOptRelation[j];
        const res = {
          relation_id: i.relation_id,
          id: i.id,
          title: i.title,
          icon: i.icon,
          isDeleted: i.isDeleted,
        };
        this.defaultOpt.push(res);
      }
      this.selected = this.defaultOpt.slice();
    },
    mSetDefaultEmpty() {
      this.defaultOpt = [];
      this.selected = [];
    },
    mGetDataId() {
      const arr = [];
      if (this.selected) {
        for (const s of this.selected) {
          arr.push(s.id);
        }
        this.dataId = arr;
      } else {
        this.dataId = [];
      }
    },
    mGetData() {
      this.data = [];
      if (this.defaultOpt) {
        if (this.selected) {
          // masukkin sema yg diselect terlebih dahulu
          this.data = this.selected.map(select => ({
            id: select.id, title: select.title, icon: select.icon, isDeleted: select.isDeleted,
          }));
          this.defaultOpt.forEach((i) => {
            let isExists = false;
            this.data.forEach((j) => {
              if (i.id === j.id) {
                isExists = true;
              }
            });
            if (!isExists) {
              const curr = {
                id: i.id, title: i.title, icon: i.icon, isDeleted: true,
              };
              this.data.push(curr);
            }
          });
        } else {
          this.data = this.defaultOpt.map(select => ({
            id: select.id, title: select.title, icon: select.icon, isDeleted: select.isDeleted,
          }));
        }
      } else if (this.selected) {
        this.data = this.selected.map(select => ({
          id: select.id, title: select.title, icon: select.icon, isDeleted: select.isDeleted,
        }));
      }
    },
    mGetChangedData() {
      this.changedData = [];
      if (this.defaultOpt) {
        if (this.selected) {
          // masukin yg defaultOpt dulu (buat yg keapus)
          this.defaultOpt.forEach((i) => {
            let isExist = false;
            this.selected.forEach((j) => {
              if (i.id === j.id) {
                isExist = true;
              }
            });
            if (!isExist) {
              const curr = {
                id: i.id, title: i.title, icon: i.icon, isDeleted: true,
              };
              this.changedData.push(curr);
            }
          });
          // masukin semua yg diselect kecuali yg udh ada atau defaultnya
          this.selected.forEach((i) => {
            let isExist = false;
            this.changedData.forEach((j) => {
              if (i.id === j.id) {
                isExist = true;
              }
            });
            this.defaultOpt.forEach((j) => {
              if (i.id === j.id) {
                isExist = true;
              }
            });
            if (!isExist) {
              this.changedData.push(i);
            }
          });
        }
      } else if (this.selected) {
        this.changedData = this.selected.map(select => ({
          id: select.id, title: select.title, icon: select.icon, isDeleted: select.isDeleted,
        }));
      }
    },
    mGetDataRelation() {
      this.dataRelation = [];
      let val;
      if (this.defaultOpt) {
        if (this.selected) {
          // dapetin semua yg diselect tambahin relation is nya
          val = this.selected.map(select => ({
            relation_id: 0,
            id: select.id,
            title: select.title,
            icon: select.icon,
            isDeleted: select.isDeleted,
          }));

          val.forEach((i) => {
            this.dataRelation.push(i);
          });

          // dicek defaultnya dgn yg d select udh ada atau blom,
          // klo ada, ganti relation_id nya ke yg seharusnya,
          // klo ga ada tambahin dengan isdeleted true
          this.defaultOpt.forEach((i) => {
            let isExists = false;
            this.dataRelation.forEach((j) => {
              if (i.id === j.id) {
                // eslint-disable-next-line no-param-reassign
                isExists = true;
                j.relation_id = i.relation_id;
              }
            });
            if (!isExists) {
              const curr = {
                relation_id: i.relation_id, id: i.id, title: i.title, icon: i.icon, isDeleted: true,
              };
              this.dataRelation.push(curr);
            }
          });
        }
      } else if (this.selected) {
        this.dataRelation = this.selected.map(select => ({
          relation_id: 0,
          id: select.id,
          title: select.title,
          icon: select.icon,
          isDeleted: select.isDeleted,
        }));
      }
    },
    mGetChangedDataRelation() {
      this.changedDataRelation = [];
      if (this.defaultOpt) {
        this.defaultOpt.forEach((i) => {
          let isExist = false;
          this.selected.forEach((j) => {
            if (i.id === j.id) {
              isExist = true;
            }
          });
          if (!isExist) {
            const curr = {};
            curr.relation_id = i.relation_id;
            curr.id = i.id;
            curr.title = i.title;
            curr.icon = i.icon;
            curr.isDeleted = true;
            this.changedDataRelation.push(curr);
          }
        });
        this.selected.forEach((i) => {
          let isExist = false;
          this.changedDataRelation.forEach((j) => {
            if (i.id === j.id) {
              isExist = true;
            }
          });
          this.defaultOpt.forEach((j) => {
            if (i.id === j.id) {
              isExist = true;
            }
          });
          if (!isExist) {
            const curr = {};
            curr.relation_id = 0;
            curr.id = i.id;
            curr.title = i.title;
            curr.icon = i.icon;
            curr.isDeleted = false;
            this.changedDataRelation.push(curr);
          }
        });
      } else if (this.selected) {
        this.changedDataRelation = this.selected.map(select => ({
          relation_id: 0,
          id: select.id,
          title: select.title,
          icon: select.icon,
          isDeleted: select.isDeleted,
        }));
      }
    },

  },
};
