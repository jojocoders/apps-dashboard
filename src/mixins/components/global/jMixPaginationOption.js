/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
// eslint-disable-next-line
import { ASYNC_SEARCH } from '@riophae/vue-treeselect';

export default {
  methods: {
    loadOptionsDivision({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
          },
        };
        this.$store.dispatch('jFlows/getListDivisions', { reqData, callback });
      }
    },

    loadOptionsPositionByDivision({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          division_id: this.v_addApprover.organigram_id.id,
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
            query_type: 'position',
          },
        };
        this.$store.dispatch('jFlows/getListByDivisions', { reqData, callback });
      }
    },

    loadOptionsPosition({ action, searchQuery, callback }) {
      let queryType;
      switch (this.filter ? this.filter : '') {
        case 'email':
          queryType = 'email';
          break;
        case 'user_name':
          queryType = 'name';
          break;
        default:
          queryType = 'position';
          break;
      }
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
            query_type: queryType,
          },
        };
        this.$store.dispatch('jFlows/getListPosition', { reqData, callback, label: (this.filter ? this.filter : 'name') });
      }
    },

    loadOptionForm({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
          },
        };
        this.$store.dispatch('jStorePagination/getListForm', { reqData, callback });
      }
    },

    loadOptionFormTransaction({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
          },
        };
        this.$store.dispatch('jStorePagination/getListFormTransaction', { reqData, callback });
      }
    },

    loadOptionCategory({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
            query_type: '',
          },
        };
        this.$store.dispatch('jStorePagination/getListCategory', { reqData, callback });
      }
    },

    loadOptionCategoryTransaction({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
            query_type: 'name',
          },
        };
        this.$store.dispatch('jStorePagination/getListCategorytransaction', { reqData, callback });
      }
    },

    loadOptionRule({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 50,
            page: 1,
            column: '',
            ascending: true,
            query: searchQuery,
          },
        };
        this.$store.dispatch('jStorePagination/getListRule', { reqData, callback });
      }
    },
  },
};
