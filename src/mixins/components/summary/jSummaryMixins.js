const jSummaryMixins = {
  props: {
    status: String,
    name: String,
    amounts: Number,
    dateSummary: String,
    color: String,
    active: Boolean,
    index: Number,
  },
};

export default jSummaryMixins;
