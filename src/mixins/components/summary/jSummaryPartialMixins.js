/* eslint-disable import/prefer-default-export */
/**
 * Created by Arga Ghulam Ahmad - arga@jojonomic.com
 * Last update on 9 August 2018
 *
 * jSummaryPartial mixins, used on all partial component of jSummary
 */
export const jSummaryPartialMixins = {
  props: {
    status: String,
    amounts: Number,
    summaryDate: String,
    color: String,
    totalData: Number,
    active: Boolean,
  },
  computed: {
    /**
     * determine path style depend on color
     * @returns {string} style
     */
    computedPathStyle() {
      if (this.active) {
        return `summary-block--${this.color}--active`;
      }
      return `summary-block--${this.color}`;
    },
    /**
     * determine g element style
      * @returns {string}
     */
    computedStyleG() {
      if (this.active) {
        return 'summary-block__item--active';
      }
      return 'summary-block__item';
    },
    /**
     * determine rect style depend on color
     * @returns {string} style
     */
    computedRectStyle() {
      return `summary-block__line--${this.color}`;
    },
    /**
     * determine scale for svg depend on total data
     * @returns {string} style
     */
    computedScale() {
      if (this.totalData === 6) {
        return 'scale(0.5)';
      } if (this.totalData === 5) {
        return 'scale(0.6)';
      } if (this.totalData === 4) {
        return 'scale(0.7)';
      }
      return 'scale(1)';
    },
  },
};
