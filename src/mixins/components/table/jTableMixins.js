/* eslint-disable import/no-duplicates,
import/prefer-default-export,consistent-return,no-underscore-dangle */
import { mapActions } from 'vuex';
import { mapGetters } from 'vuex';

/**
 * Created by Arga Ghulam Ahmad - arga@jojonomic.com
 * Last update on 9 August 2018
 *
 * Mixins use by client side table (jTable and jTableUser) component.
 * This mixins use for reduce redundancy on many client side table, with share same used function.
 */
export const jTableMixins = {
  /**
   * tableData: array contains table data
   */
  props: {
    tableData: Array,
  },
  /**
   * checkedId: array contains checked ids
   * rows: array contains rows of table
   * options: see documentation at https://github.com/matfish2/vue-tables-2#options
   * result: array contains checked data
   */
  data() {
    return {
      checkedId: [],
      rows: [],
      options: {
        perPageValues: [5, 10, 25, 50, 100],
        sortIcon: {
          base: 'fa', up: 'fa-sort-up', down: 'fa-sort-down', is: 'fa-sort',
        },
        headings: {
          checkbox(h) {
            const _this = this;
            if (this.checkedId.length === this.rows.length) {
              return h(
                'i',
                {
                  attrs: {
                    id: 'select-all-unchecked',
                    class: 'fa fa-square-o i--center-block',
                    style: 'display: block; text-align: center; font-size: 20px;',
                  },
                  on: {
                    click() {
                      _this.checkedId = [];
                    },
                  },
                },
                [''],
              );
            } if (this.checkedId.length !== this.rows.length) {
              return h(
                'i',
                {
                  attrs: {
                    id: 'select-all-checked',
                    class: 'fa fa-check-square i--center-block',
                    style: 'display: block; text-align: center; font-size: 20px;',
                  },
                  on: {
                    click() {
                      _this.checkedId = _this.getDataId;
                    },
                  },
                },
                [''],
              );
            }
          },
        },
      },
      result: [],
    };
  },
  /**
   * map store's actions and getters
   * check: function for check feature debug purpose
   */
  methods: {
    ...mapActions(['actionCheckedId', 'actionDataContainer', 'actionResult', 'actionCurrentTable']),
    ...mapGetters(['getCurrentTable']),
    check() {
      // if (e.target.checkedId) {
      // }
    },
  },
  /**
   * getDataId: return arrays of data ids
   * countChosenIds: return number of chosen ids
   * countChosenSubIds: return number of chosen sub ids
   * sumTotalAmount: return total amount
   * sumTotalRecord: return total record
   */
  computed: {
    getDataId() {
      return this.rows.map(data => data.dataId);
    },
    countChosenIds() {
      return this.checkedId.length;
    },
    countChosenSubIds() {
      let subIdsTotal = 0;
      this.result.forEach((data) => {
        subIdsTotal += data.subIds.length;
      });
      return subIdsTotal;
    },
    sumTotalAmount() {
      let totalAmount = 0;
      this.result.forEach((data) => {
        totalAmount += data.amount;
      });
      return totalAmount;
    },
    sumTotalRecord() {
      let totalRecord = 0;
      this.result.forEach((data) => {
        totalRecord += data.records;
      });
      return totalRecord;
    },
  },
  watch: {
    /**
     * checkedId: watch for checkedId changes, communicate with floating prompt purpose
     */
    checkedId() {
      const result = [];

      // add checked data to result array
      this.checkedId.forEach((id) => {
        result.push(this.rows.find(data => data.dataId === id));
      });

      this.result = result;

      // update vuex to communicate with floating prompt
      this.actionCurrentTable(this.$options.name);
      this.actionCheckedId(this.checkedId);
      this.actionResult(this.result);
    },
  },
  created() {
    this.getUsersData();
  },
};
