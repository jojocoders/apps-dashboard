/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState } from 'vuex';
import mainConstant from '@jojocoders/jojonomic-ui/src/Utils/constant';

export const jPrivRoles = {
  computed: {
    ...mapState('jStoreLogin', ['roles']),
    privRoleAdmin() {
      return this.roles.includes(mainConstant.RoleAdmin);
    },
    privRoleSysadmin() {
      return this.roles.includes(mainConstant.RoleSysadmin);
    },
    privRoleApprover() {
      return this.roles.includes(mainConstant.RoleApprover);
    },
    privRoleFinance() {
      return this.roles.includes(mainConstant.RoleFinance);
    },
    privRoleManagement() {
      return this.roles.includes(mainConstant.RoleManagement);
    },
    privRoleClaimer() {
      return this.roles.includes(mainConstant.RoleClaimer);
    },
    privRoleProcurePurchaser() {
      return this.roles.includes(mainConstant.RoleProcurePurchaser);
    },
    privRoleProcureAdmin() {
      return this.roles.includes(mainConstant.RoleProcureAdmin);
    },
    privRoleAdminFinance() {
      return this.roles.includes(mainConstant.RoleAdminFinance);
    },
    privRoleProcureManagement() {
      return this.roles.includes(mainConstant.RoleProcureManagement);
    },
    privRolePayrollHRD() {
      return this.roles.includes(mainConstant.RolePayrollHRD);
    },
    privRolePayrollStaff() {
      return this.roles.includes(mainConstant.RolePayrollStaff);
    },
    privRoleProcureClaimer() {
      return this.roles.includes(mainConstant.RoleProcureClaimer);
    },
    privRoleProcureApprover() {
      return this.roles.includes(mainConstant.RoleProcureApprover);
    },
    privRoleProcureFinance() {
      return this.roles.includes(mainConstant.RoleProcureFinance);
    },
    privRoleTimesHR() {
      return this.roles.includes(mainConstant.RoleTimesHR);
    },
    privRoleAdminBudget() {
      return this.roles.includes(mainConstant.RoleAdminBudget);
    },
    privRoleBudgetCustody() {
      return this.roles.includes(mainConstant.RoleBudgetCustody);
    },
  },
};
