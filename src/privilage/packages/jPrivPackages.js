/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState } from 'vuex';
import mainConstant from '@jojocoders/jojonomic-ui/src/Utils/constant';

export const jPrivPackages = {
  computed: {
    ...mapState('jStoreLogin', ['packages']),
    privPackageReimbursement() {
      return this.packages.includes(mainConstant.Reimbursement);
    },
    privPackageAttendance() {
      return this.packages.includes(mainConstant.Attendance);
    },
    privPackageProcure() {
      return this.packages.includes(mainConstant.Procure);
    },
    privPackageTrip() {
      return this.packages.includes(mainConstant.Trip);
    },
    privPackagePayroll() {
      return this.packages.includes(mainConstant.Payroll);
    },
    privPackageBiztrav() {
      return this.packages.includes(mainConstant.Biztrav);
    },
    privPackageBiztrip() {
      return this.packages.includes(mainConstant.Biztrip);
    },
    privPackageCostcenter() {
      return this.packages.includes(mainConstant.Costcenter);
    },
    privPackageForm() {
      return this.packages.includes(mainConstant.Form);
    },
    privPackageTravelonomic() {
      return this.packages.includes(mainConstant.Travelonomic);
    },
  },
  methods: {
    privPackage(productPackage) {
      return this.packages.includes(productPackage);
    },
  },
};
