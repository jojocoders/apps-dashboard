/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState } from 'vuex';
import constant from '@/utils/constant';

export const jPrivCompanySettings = {
  computed: {
    ...mapState('jStoreLogin', ['features']),
  },
};
