import stores from '@/stores';

export default {
  getErrorMessage(val) {
    if (val.errors !== undefined) {
      const errorMessage = [];
      const errorCode = [];
      val.errors.forEach((e) => {
        errorMessage.push(e.message);
        errorCode.push(e.code);
      });
      const errorJoin = errorCode.join(', ');
      const error = {
        error_message: errorMessage,
        error_code: errorJoin,
      };
      stores.dispatch('jStoreNotificationScreen/hideLoading', '');
      stores.dispatch('jStoreNotificationScreen/toggleProblem', error);
    } else {
      stores.dispatch('jStoreNotificationScreen/hideLoading', '');
      stores.dispatch('jStoreNotificationScreen/toggleProblem', val.message);
    }
  },
};
