import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';
import jCreateTimesheet from '@/views/application/application-component/times/timesheet/jCreateTimesheet';
import jCreateTask from '@/views/application/application-component/times/timesheet/jCreateTask';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/claimer/draft',
    name: 'List Draft Timesheet',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/claimer/sent',
    name: 'List Sent Timesheet Claimer',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/claimer/reported',
    name: 'List Reported Timesheet Claimer',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/approver/sent',
    name: 'List Sent Timesheet Approver',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/approver/reported',
    name: 'List Reported Timesheet Approver',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/approver/rejected',
    name: 'List Reported Timesheet Approver',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/timesheet/claimer/create',
    name: 'Create Timesheet',
    component: jCreateTimesheet,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'default',
    },
  },
  {
    path: '/timesheet/claimer/detail/:date',
    name: 'Detail Timesheet',
    component: jCreateTimesheet,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'default',
    },
  },
  {
    path: '/timesheet/claimer/create/task',
    name: 'Create Task',
    component: jCreateTask,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'default',
    },
  },
  {
    path: '/timesheet/claimer/detail/task/:days/:id',
    name: 'Detail Task',
    component: jCreateTask,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'default',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/timesheet/summary',
    name: 'Report Timesheet Summary',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/timesheet/summary/detail/:id/:startDate/:endDate',
    name: 'Report Timesheet Detail',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/timesheet/form-project',
    name: 'Report Form Project',
    component: jApplication,
    meta: {
      title: 'Timesheet',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
