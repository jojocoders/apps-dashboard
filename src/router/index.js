import Vue from 'vue';
import Router from 'vue-router';
import jRouterLogin from './jRouterLogin';
import jRouterLogout from './jRouterLogout';
import jRouterHome from './jRouterHome';
import jRouterApplication from './jRouterApplication';
import jRouterPosition from './jRouterPosition';

import jRouterAttendance from './jRouterAttendance';
import jRouterCoAttendance from './jRouterCoAttendance';
import jRouterCore from './jRouterCore';
import jRouterFlowRules from './jRouterFlowRules';
import jRouterLeaveApprover from './jRouterLeaveApprover';
import jRouterLeaveClaimer from './jRouterLeaveClaimer';
import jRouterLeaveConfig from './jRouterLeaveConfig';
import jRouterLeaveReport from './jRouterLeaveReport';
import jRouterLeaveMonitor from './jRouterLeaveMonitor';
import jRouterOvertimeApprover from './overtime/jRouterOvertimeApprover';
import jRouterOvertimeConfig from './overtime/jRouterOvertimeConfig';
import jRouterOvertimeMonitor from './overtime/jRouterOvertimeMonitor';
import jRouterOvertimeReport from './overtime/jRouterOvertimeReport';
import jRouterOvertimeRequestor from './overtime/jRouterOvertimeRequestor';
import jRouterTimesheet from './jRouterTimesheet';
import jRouterTimesheetConfig from './jRouterTimesheetConfig';

Vue.use(Router);

const moduleRoutes = [
  jRouterLogin,
  jRouterLogout,
  jRouterHome,
  jRouterApplication,
  jRouterAttendance,
  jRouterCoAttendance,
  jRouterCore,
  jRouterPosition,
  jRouterFlowRules,
  jRouterLeaveApprover,
  jRouterLeaveClaimer,
  jRouterLeaveConfig,
  jRouterLeaveReport,
  jRouterLeaveMonitor,
  jRouterOvertimeApprover,
  jRouterOvertimeConfig,
  jRouterOvertimeMonitor,
  jRouterOvertimeReport,
  jRouterOvertimeRequestor,
  jRouterTimesheet,
  jRouterTimesheetConfig,
];

const baseRoutes = [
  {
    path: '*',
    redirect: '/',
  },
];

const routes = baseRoutes.concat(...moduleRoutes);

const router = new Router({
  mode: 'history',
  routes,
});


// validate route by middleware
function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];

  if (!subsequentMiddleware) return context.next;

  return () => {
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];

    const context = {
      from,
      next,
      router,
      to,
    };

    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({ ...context, next: nextMiddleware });
  }
  return next();
});

export default router;
