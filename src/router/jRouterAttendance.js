import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  // ATTENDANCE MY LOG
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/claimer',
    name: 'Attendance',
    component: jApplication,
    meta: {
      title: 'Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/claimer/detail/:attendedId',
    name: 'Attendance Detail',
    component: jApplication,
    meta: {
      title: 'Attendance Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/group/detail/:attendedId',
    name: 'Attendance Group Detail',
    component: jApplication,
    meta: {
      title: 'Attendance Group Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // ATTENDANCE MONITOR
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/monitor/attended/detail/:attendedId',
    name: 'Monitor Attended Detail',
    component: jApplication,
    meta: {
      title: 'Monitor Attendance Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // ATTENDANCE REPORT
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/attendance/summary/detail/:id/:start_date/:end_date',
    name: 'Report Attendance Detail',
    component: jApplication,
    meta: {
      title: 'Report Attendance Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // ATTENDANCE CONFIG
  // code
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/code/list',
    name: 'Code List',
    component: jApplication,
    meta: {
      title: 'Code List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // exceed notification
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/exceed-notification/list',
    name: 'Exceed Notification List',
    component: jApplication,
    meta: {
      title: 'Exceed Notification List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // automatic attendance
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/automatic-attendance/list',
    name: 'Automatic Attendance List',
    component: jApplication,
    meta: {
      title: 'Automatic Attendance List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // reminder
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/reminder/list',
    name: 'Reminder List',
    component: jApplication,
    meta: {
      title: 'Reminder List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/reminder/create',
    name: 'Create Reminder',
    component: jApplication,
    meta: {
      title: 'Create Reminder',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/reminder/edit/:id',
    name: 'Update Reminder',
    component: jApplication,
    meta: {
      title: 'Update Reminder',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // holiday
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/holiday/list',
    name: 'Holiday List',
    component: jApplication,
    meta: {
      title: 'Holiday List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/holiday/create',
    name: 'Create Holiday',
    component: jApplication,
    meta: {
      title: 'Create Holiday',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/holiday/edit/:id',
    name: 'Update Holiday',
    component: jApplication,
    meta: {
      title: 'Update Holiday',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // shift
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/shift/list',
    name: 'Shift List',
    component: jApplication,
    meta: {
      title: 'Shift List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/shift/create',
    name: 'Create Shift',
    component: jApplication,
    meta: {
      title: 'Create Shift',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/shift/edit/:id',
    name: 'Update Shift',
    component: jApplication,
    meta: {
      title: 'Update Shift',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // mapping shift
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/mapping-shift/list',
    name: 'Mapping Shift List',
    component: jApplication,
    meta: {
      title: 'Mapping Shift List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/mapping-shift/create',
    name: 'Create Mapping Shift',
    component: jApplication,
    meta: {
      title: 'Create Mapping Shift',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/mapping-shift/edit/:id',
    name: 'Update Mapping Shift',
    component: jApplication,
    meta: {
      title: 'Update Mapping Shift',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // venue
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/venue/list',
    name: 'Venue List',
    component: jApplication,
    meta: {
      title: 'Venue List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/venue/create',
    name: 'Create Venue',
    component: jApplication,
    meta: {
      title: 'Create Venue',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/attendance/config/venue/edit/:id',
    name: 'Update Venue',
    component: jApplication,
    meta: {
      title: 'Update Venue',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
