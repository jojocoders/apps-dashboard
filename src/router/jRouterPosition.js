import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/list',
    name: 'List Position',
    component: jApplication,
    meta: {
      title: 'List Position',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
    children: [
      {
        path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/create',
        name: 'Create Position',
        component: jApplication,
        meta: {
          title: 'Create Position',
        },
      },
    ],
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/update/:id',
    name: 'Position Detail',
    component: jApplication,
    meta: {
      title: 'Position Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
