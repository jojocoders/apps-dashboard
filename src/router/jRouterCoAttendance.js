import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  // REQUESTOR
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/create',
    name: 'Create Co-Attendance',
    component: jApplication,
    meta: {
      title: 'Create Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/detail/:id',
    name: 'Detail Co-Attendance',
    component: jApplication,
    meta: {
      title: 'Detail Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/draft/detail/:id/:startDate/:endDate',
    name: 'Draft Detail Co-Attendance',
    component: jApplication,
    meta: {
      title: 'Draft Detail Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/draft',
    name: 'List Draft Co-Attendance',
    component: jApplication,
    meta: {
      title: 'List Draft Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/sent',
    name: 'List Sent Co-Attendance',
    component: jApplication,
    meta: {
      title: 'List Sent Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/approved',
    name: 'List Approved Co-Attendance',
    component: jApplication,
    meta: {
      title: 'List Approved Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/rejected',
    name: 'List Rejected Co-Attendance',
    component: jApplication,
    meta: {
      title: 'List Rejected Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/requestor/canceled',
    name: 'List Canceled Co-Attendance',
    component: jApplication,
    meta: {
      title: 'List Canceled Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // APPROVER
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/approver/:statusApprover',
    name: 'List Co-Attendance',
    component: jApplication,
    meta: {
      title: 'List Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/co-attendance/approver/detail/:id',
    name: 'Detail Co-Attendance',
    component: jApplication,
    meta: {
      title: 'Detail Co-Attendance',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
