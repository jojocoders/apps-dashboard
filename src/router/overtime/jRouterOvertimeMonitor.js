import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/monitor/:statusMonitor',
    name: 'Overtime Monitor',
    component: jApplication,
    meta: {
      title: 'Overtime Monitor',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/monitor/detail/:id',
    name: 'Detail Overtime',
    component: jApplication,
    meta: {
      title: 'Detail Overtime',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
