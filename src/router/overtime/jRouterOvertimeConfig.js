import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  // OVERTIME TYPE
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/type/list',
    name: 'Overtime Type List',
    component: jApplication,
    meta: {
      title: 'Overtime Type List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/type/create',
    name: 'Create Overtime Type',
    component: jApplication,
    meta: {
      title: 'Create Overtime Type',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/type/edit/:id',
    name: 'Update Overtime Type',
    component: jApplication,
    meta: {
      title: 'Update Overtime Type',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // FORMULA
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/formula/list',
    name: 'Formula List',
    component: jApplication,
    meta: {
      title: 'Formula List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/formula/create',
    name: 'Create Formula',
    component: jApplication,
    meta: {
      title: 'Create Formula',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/formula/edit/:id',
    name: 'Update Formula',
    component: jApplication,
    meta: {
      title: 'Update Formula',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // POLICY
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/policy/list',
    name: 'Policy List',
    component: jApplication,
    meta: {
      title: 'Policy List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/policy/create',
    name: 'Create Policy',
    component: jApplication,
    meta: {
      title: 'Create Policy',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/config/policy/edit/:id',
    name: 'Update Policy',
    component: jApplication,
    meta: {
      title: 'Update Policy',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
