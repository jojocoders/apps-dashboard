import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/approver/:statusApprover',
    name: 'List Overtime Approver',
    component: jApplication,
    meta: {
      title: 'List Overtime Approver',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/approver/detail/:id',
    name: 'Detail Overtime',
    component: jApplication,
    meta: {
      title: 'Detail Overtime',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
