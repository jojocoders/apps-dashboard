import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/requestor/:statusApprover',
    name: 'List Overtime Requestor',
    component: jApplication,
    meta: {
      title: 'List Overtime Requestor',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/requestor/detail/:id',
    name: 'Detail Overtime',
    component: jApplication,
    meta: {
      title: 'Detail Overtime',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/overtime/requestor/create',
    name: 'Create Overtime',
    component: jApplication,
    meta: {
      title: 'Create Overtime',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
