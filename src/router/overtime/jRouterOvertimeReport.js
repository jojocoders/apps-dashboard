import auth from '@/middleware/auth';
import log from '@/middleware/log';
import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/overtime/list',
    name: 'Report Overtime List',
    component: jApplication,
    meta: {
      title: 'Report Overtime List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/overtime/detail/:id/:start_date/:end_date',
    name: 'Report Overtime Detail',
    component: jApplication,
    meta: {
      title: 'Report Overtime Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
