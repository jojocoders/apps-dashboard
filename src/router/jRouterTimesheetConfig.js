import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/project/list',
    name: 'jProjectList',
    component: jApplication,
    meta: {
      title: 'Task Project',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/project/create',
    name: 'jProjectCreate',
    component: jApplication,
    meta: {
      title: 'Create Task Project',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/project/edit/:id',
    name: 'jProjectEdit',
    component: jApplication,
    meta: {
      title: 'Edit Task Project',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/assignment/list',
    name: 'jAssignmentList',
    component: jApplication,
    meta: {
      title: 'Task Assignment',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/assignment/create',
    name: 'jAssignmentCreate',
    component: jApplication,
    meta: {
      title: 'Create Task Assignment',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/timesheet/assignment/edit/:id',
    name: 'jAssignmentEdit',
    component: jApplication,
    meta: {
      title: 'Edit Task Assignment',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
