import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/leave/summary',
    name: 'Report Leave Summary',
    component: jApplication,
    meta: {
      title: 'Report Leave Summary',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/leave/list',
    name: 'Report Leave',
    component: jApplication,
    meta: {
      title: 'Report Leave',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/report/leave/detail/:id/:start_date/:end_date/:status/:type',
    name: 'Report Leave Detail',
    component: jApplication,
    meta: {
      title: 'Report Leave Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
