import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  // EMPLOYEE
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/employee',
    name: 'Employee List',
    component: jApplication,
    meta: {
      title: 'Employee List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
    children: [
      {
        path: '/app/:applicationName/:applicationId/:pageName/:pageId/employee/create',
        name: 'Create Employee',
        component: jApplication,
        meta: {
          title: 'Create Employee',
          middleware: [auth, log],
        },
      },
    ],
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/employee/detail/:id',
    name: 'Employee Detail',
    component: jApplication,
    meta: {
      title: 'Employee Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // ORG UNIT
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/unit',
    name: 'Org Unit List',
    component: jApplication,
    meta: {
      title: 'Org Unit List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/unit/update/:id',
    name: 'Update Org Unit',
    component: jApplication,
    meta: {
      title: 'Update Org Unit',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // POSITION
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position',
    name: 'Position List',
    component: jApplication,
    meta: {
      title: 'Position List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/create',
    name: 'Create Position',
    component: jApplication,
    meta: {
      title: 'Create Position',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/update/:id',
    name: 'Update Position',
    component: jApplication,
    meta: {
      title: 'Update Position',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/update/create-division/:id',
    name: 'Create New Division',
    component: jApplication,
    meta: {
      title: 'Create New Division',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/update/create-employee/:id',
    name: 'Create New Employee',
    component: jApplication,
    meta: {
      title: 'Create New Employee',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/position/update/create-layer/:id',
    name: 'Create New Layer',
    component: jApplication,
    meta: {
      title: 'Create New Layer',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // COST CENTER
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/cost-center/list',
    name: 'Cost Center List',
    component: jApplication,
    meta: {
      title: 'Cost Center List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/cost-center/detail/:id',
    name: 'Detail Cost Center',
    component: jApplication,
    meta: {
      title: 'Detail Cost Center',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
