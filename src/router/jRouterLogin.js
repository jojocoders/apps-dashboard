import jLogin from '@/views/login/jLogin';
import jForgotPassword from '@/views/login/jForgotPassword';
import jAuthLogin from '@/views/login/jAuthLogin';
import jLoginLdap from '@/views/login/jLoginLdap';
import jCustomLogin from '@/views/login/jCustomLogin';
import Vue from 'vue';

export default [
  {
    path: '/',
    name: 'jLogin',
    component: jLogin,
    beforeEnter(to, from, next) {
      if (Vue.ls.get('Token')) {
        const middlewareRole = localStorage.getItem('userRole');
        if (middlewareRole && (Number(middlewareRole) === 1
        || Number(middlewareRole) === 2
        || Number(middlewareRole) === 3)) {
          next('/home');
        } else {
          localStorage.clear();
          next('/denied');
        }
      } else {
        next();
      }
    },
    meta: {
      title: 'Login',
    },
  },
  {
    path: '/forgot',
    name: 'jForgotPassword',
    component: jForgotPassword,
    meta: {
      title: 'Forgot Password',
    },
  },
  {
    path: '/auth/callback',
    name: 'jAuthLogin',
    component: jAuthLogin,
    meta: {
      title: 'Auth Login',
    },
  },
  {
    path: '/auth/google/callback',
    name: 'jAuthLogin',
    component: jAuthLogin,
    meta: {
      title: 'Auth Login',
    },
  },
  {
    path: '/oauth-callback',
    name: 'jAuthLogin',
    component: jAuthLogin,
    meta: {
      title: 'Auth Login',
    },
  },
  {
    path: '/signin-with-ldap',
    name: 'jLoginLdap',
    component: jLoginLdap,
    meta: {
      title: 'Login With Active Directory',
    },
  },
  {
    path: '/:path/login',
    name: 'jCustomLogin',
    component: jCustomLogin,
    beforeEnter: (to, from, next) => {
      const hasToken = Vue.ls.get('Token');
      const middlewareRole = localStorage.getItem('userRole');
      const roleHasAccess = middlewareRole && (Number(middlewareRole) === 1
      || Number(middlewareRole) === 2
      || Number(middlewareRole) === 3);

      if (hasToken && roleHasAccess) {
        next('/home');
      } else if (hasToken && !roleHasAccess) {
        next('/denied');
      } else {
        const isCustomLoginEnabled = process.env.SIGNIN_WITH_CUSTOM_LOGIN === 'true';
        const customLoginPath = process.env.SIGNIN_WITH_CUSTOM_LOGIN_API || '';

        if (!isCustomLoginEnabled || to.params.path !== customLoginPath.split('/')[0]) {
          next({ name: 'jLogin' });
        } else {
          next();
        }
      }
    },
  },
];
