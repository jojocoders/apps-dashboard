import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';
import jCreateLeave from '@/views/application/application-component/times/leave/jCreateLeave';
import jDraftLeave from '@/views/application/application-component/times/leave/jDraftLeave';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/claimer/:statusClaimer',
    name: 'Claimer',
    component: jApplication,
    meta: {
      title: 'Claimer',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/claimer/create/leave',
    name: 'Create Leave',
    component: jCreateLeave,
    meta: {
      title: 'Create Leave',
      middleware: [auth, log],
      layout: 'default',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/claimer/draft/:idLeave',
    name: 'Draft Leave',
    component: jDraftLeave,
    meta: {
      title: 'Draft Leave',
      middleware: [auth, log],
      layout: 'default',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/claimer/detail/:idLeave',
    name: 'Detail Leave',
    component: jApplication,
    meta: {
      title: 'Detail Leave',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
