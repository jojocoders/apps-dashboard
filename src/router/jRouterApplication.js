/* eslint-disable max-len */
import jApplication from '@/views/application/jApplication';

// Form UI Component
import jExportBase from '@/views/application/application-component/form-ui/form-base/jExportBase';
import jDetailRecord from '@/views/application/application-component/form-ui/form-base/jDetailRecord';

// Multiple Form UI Component
import jDetailViewComponent from '@/views/application/application-component/multiple-form-ui/jDetailViewComponent';

import auth from '@/middleware/auth';
import log from '@/middleware/log';

export default [
  // MAIN APPLICATION
  {
    path: '/app/:applicationId/:pageId',
    name: 'jApplication',
    component: jApplication,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId',
    name: 'jApplication',
    component: jApplication,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // FORM UI COMPONENT
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/export-form-ui/:formUiId',
    name: 'jExportBase',
    component: jExportBase,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/export-form-ui/:formUiId/:mode',
    name: 'jExportBase',
    component: jExportBase,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // Router DS-001 (v)
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-record/:formUiId/:id',
    name: 'jDetailRecord',
    component: jDetailRecord,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // Router DS-002 (v)
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-record/:formUiId/:id/:status/:mode',
    name: 'jDetailRecord',
    component: jDetailRecord,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // Router DS-003
  // {
  //   path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-record/:formUiId/:id/:formUiIdParent/:idParent/:filterColumn/:filterVariable',
  //   name: 'jDetailRecord',
  //   component: jDetailRecord,
  //   meta: {
  //     title: 'Officeless',
  //     middleware: [auth, log],
  //     layout: 'jDynamicWrapper',
  //   },
  // },
  // Router DS-004 (v)
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-record/:formUiId/:id/:formUiIdParent/:idParent/:filterColumn/:filterVariable/:tabName',
    name: 'jDetailRecord',
    component: jDetailRecord,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // Router DS-005 (v)
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-record/:formUiId/:id/:formUiIdParent/:idParent/:filterColumn/:filterVariable/:tabName/:status',
    name: 'jDetailRecord',
    component: jDetailRecord,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // MULTIPLE FORM UI COMPONENT
  // Router DM-001
  // {
  //   path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-multiple/:formUiId/:id',
  //   name: 'jDetailViewComponent',
  //   component: jDetailViewComponent,
  //   meta: {
  //     title: 'Officeless',
  //     middleware: [auth, log],
  //     layout: 'jDynamicWrapper',
  //   },
  // },
  // Router DM-002 (v)
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-multiple/:formUiId/:id/:tabName',
    name: 'jDetailViewComponent',
    component: jDetailViewComponent,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  // Router DM-004
  // {
  //   path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-multiple/:formUiId/:id/:status/:mode',
  //   name: 'jDetailViewComponent',
  //   component: jDetailViewComponent,
  //   meta: {
  //     title: 'Officeless',
  //     middleware: [auth, log],
  //     layout: 'jDynamicWrapper',
  //   },
  // },
  // Router DM-005 (v)
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/detail-multiple/:formUiId/:id/:tabName/:status/:mode',
    name: 'jDetailViewComponent',
    component: jDetailViewComponent,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },

  // ACCESS MANAGEMENT DETAIL
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/access-detail/:accessApplicationId',
    name: 'jAccessManagementDetail',
    component: jApplication,
    meta: {
      title: 'Detail Access Management',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
