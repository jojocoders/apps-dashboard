import jHome from '@/views/home/jHome';
import jProfile from '@/views/profile/jProfile';
import jDenied from '@/views/login/jDenied';
import auth from '@/middleware/auth';
import log from '@/middleware/log';

export default [
  {
    path: '/home',
    name: 'jHome',
    component: jHome,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jOfficelessHomeWrapper',
    },
  },
  {
    path: '/profile',
    name: 'jProfile',
    component: jProfile,
    meta: {
      title: 'Officeless',
      middleware: [auth, log],
      layout: 'jOfficelessHomeWrapper',
    },
  },
  {
    path: '/denied',
    name: 'denied',
    component: jDenied,
    meta: {
      title: 'Access Denied',
    },
  },
];
