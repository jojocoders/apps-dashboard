import jLogin from '@/views/login/jLogin';

export default [
  {
    path: '/logout',
    name: 'jLogout',
    component: jLogin,
    beforeEnter(to, from, next) {
      localStorage.clear();
      next('/');
    },
  },
];
