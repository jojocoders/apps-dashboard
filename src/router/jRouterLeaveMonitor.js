import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/monitor/:statusMonitor',
    name: 'Leave Monitor',
    component: jApplication,
    meta: {
      title: 'Leave Monitor',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
