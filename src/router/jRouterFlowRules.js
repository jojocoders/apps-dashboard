import auth from '@/middleware/auth';
import admin from '@/middleware/admin';

import jApplication from '@/views/application/jApplication';
import jFlowUpdate from '@/views/application/application-component/core/flow-and-rules/flow/jFlowUpdate';
import jFlowNew from '@/views/application/application-component/core/flow-and-rules/flow/jFlowNew';
import jFlowCreateType from '@/views/application/application-component/core/flow-and-rules/flow/create/jFlowCreateType2';
import jFlowEditType from '@/views/application/application-component/core/flow-and-rules/flow/edit/jFlowEditType2';
import jFlowSlaCreate from '@/views/application/application-component/core/flow-and-rules/flow/flow-sla/create/jCreateFlowSLA';
import jFlowSlaEdit from '@/views/application/application-component/core/flow-and-rules/flow/flow-sla/detail/jDetailFlowSLA';
import jRuleNew from '@/views/application/application-component/core/flow-and-rules/rules/jRulesNew';
import jRuleUpdate from '@/views/application/application-component/core/flow-and-rules/rules/jRulesUpdate';

const jojoflows = [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flows',
    name: 'Flows',
    component: jApplication,
    meta: {
      title: 'Flows and Rules',
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flow/update-by-division/:id',
    name: 'Edit Flow Type',
    component: jFlowEditType,
    meta: {
      title: 'Edit Flow Type',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flow/new-by-division',
    name: 'New Flow Type',
    component: jFlowCreateType,
    meta: {
      title: 'Create Flow By Division',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flow/new',
    name: 'New Flow',
    component: jFlowNew,
    meta: {
      title: 'Create Flow',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flow/update/:id',
    name: 'Update Flow',
    component: jFlowUpdate,
    meta: {
      title: 'Update Flow',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/rules/new',
    name: 'New Rules',
    component: jRuleNew,
    meta: {
      title: 'Create Rule',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/rules/update/:id',
    name: 'Update Rules',
    component: jRuleUpdate,
    meta: {
      title: 'Update Rule',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flows/sla/edit/:id',
    name: 'Edit Flow SLA',
    component: jFlowSlaEdit,
    meta: {
      title: 'Edit Flow SLA',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/flows/sla/create',
    name: 'Create Flow SLA',
    component: jFlowSlaCreate,
    meta: {
      title: 'Create Flow SLA',
    },
  },
];

jojoflows.map((dataPath) => {
  const eachData = dataPath;
  eachData.meta.middleware = [auth, admin];
  return eachData;
});


export default jojoflows;
