import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/approver/detail/:idLeave',
    name: 'Leave Detail',
    component: jApplication,
    meta: {
      title: 'Leave Detail',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/approver/:statusApprover',
    name: 'Approver List',
    component: jApplication,
    meta: {
      title: 'Approver List',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
