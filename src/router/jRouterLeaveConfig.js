import auth from '@/middleware/auth';
import log from '@/middleware/log';

import jApplication from '@/views/application/jApplication';

export default [
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/config',
    name: 'Leave Config',
    component: jApplication,
    meta: {
      title: 'Configuration',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/config/leave-type/create',
    name: 'Leave Tyoe Create',
    component: jApplication,
    meta: {
      title: 'Configuration',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/config/leave-type/detail/:idLeaveType',
    name: 'Leave Type Detail',
    component: jApplication,
    meta: {
      title: 'Configuration',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/config/mapping/create',
    name: 'Create Mapping',
    component: jApplication,
    meta: {
      title: 'Configuration',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
  {
    path: '/app/:applicationName/:applicationId/:pageName/:pageId/leave/config/mapping/detail/:userCompanyId',
    name: 'Update Mapping',
    component: jApplication,
    meta: {
      title: 'Configuration',
      middleware: [auth, log],
      layout: 'jDynamicWrapper',
    },
  },
];
