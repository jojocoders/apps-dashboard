// import Vue from 'vue';

export default function admin({ next, router }) {
  const role = JSON.parse(localStorage.getItem('role'));
  if (role.includes(1) || role.includes(2)) {
    return next();
  } else if (role.includes(17) || role.includes(18)) {
    window.location = '/costcenter';
  }
  // return next();
  return router.push('/logout');
}
