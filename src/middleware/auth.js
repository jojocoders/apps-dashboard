import Vue from 'vue';

export default function auth({ next, router }) {
  if (!Vue.ls.get('Token')) {
    return router.push('/');
  }
  return next();
}
