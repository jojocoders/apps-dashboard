import jApi from './jApi';

export default {
  getTaxLocationList(params) {
    const api = jApi.generateApi();
    return api.get('payroll/tax-locations', { params })
      .then(res => res);
  },
  createTaxLocation(params) {
    const api = jApi.generateApi();
    return api.post('payroll/tax-locations', params)
      .then(res => res.data);
  },
  updateTaxLocation(params, gradeId) {
    const api = jApi.generateApi();
    return api.put(`payroll/tax-locations/${gradeId}`, params)
      .then(res => res.data);
  },
  deleteTaxLocation(gradeId) {
    const api = jApi.generateApi();
    return api.delete(`payroll/tax-locations/${gradeId}`)
      .then(res => res.data);
  },
};
