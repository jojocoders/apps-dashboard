import jApi from './jApi';

export default {
  // BASIC SALARY
  getBasicSalary(params) {
    const api = jApi.generateApi();
    return api.post('/payroll/admin/payroll-recurring/list', params).then(res => res);
  },
  saveBasicSalary(params) {
    const api = jApi.generateApi();
    return api.post('/payroll/admin/payroll-recurring/save', params).then(res => res.data);
  },
  deleteBasicSalary(params) {
    const api = jApi.generateApi();
    return api.post('/payroll/admin/payroll-recurring/recurring-delete', params).then(res => res);
  },
  saveMappingSalaryConfig(payrollId, recurringId, params) {
    const api = jApi.generateApi();
    return api.put(`/payroll/payrolls/${payrollId}/recurrings/${recurringId}/salary-configs`, params).then(res => res.data);
  },

  // COST CENTER MASTER
  getMasterCostCenter(params) {
    const api = jApi.generateApi();
    return api.get('/payroll/cost-centers?includes=tax_locations', { params }).then(res => res);
  },

  // COST CENTER
  async getCostCenter(payrollId, params) {
    const api = jApi.generateApi();
    // return api.get(`/payroll/payrolls/${payrollId}/cost-centers`, { params }).then(res => res);
    try {
      const res = await api.get(`/payroll/payrolls/${payrollId}/cost-centers`, { params });
      return res;
    } catch (err) {
      return err.response;
    }
  },
  createCostCenter(payrollId, params) {
    const api = jApi.generateApi();
    return api.post(`/payroll/payrolls/${payrollId}/cost-centers`, params).then(res => res.data);
  },
  updateCostCenter(payrollId, params, costId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/payrolls/${payrollId}/cost-centers/${costId}`, params).then(res => res.data);
  },
  deleteCostCenter(payrollId, costId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/payrolls/${payrollId}/cost-centers/${costId}`).then(res => res.data);
  },

  // TAX INFORMATION
  getTaxInformation(payrollId) {
    const api = jApi.generateApi();
    return api.get(`/payroll/payrolls/${payrollId}/tax`).then(res => res.data);
  },
  saveTaxInformation(payrollId, params) {
    const api = jApi.generateApi();
    return api.post(`/payroll/payrolls/${payrollId}/tax`, params).then(res => res.data);
  },

  // TAX CONFIG
  async getTaxConfig(payrollId, params) {
    const api = jApi.generateApi();
    // return api.get(`/payroll/payrolls/${payrollId}/tax-configs`, { params }).then(res => res);
    try {
      const res = await api.get(`/payroll/payrolls/${payrollId}/tax-configs`, { params });
      return res;
    } catch (err) {
      return err.response;
    }
  },
  createTaxConfig(payrollId, params) {
    const api = jApi.generateApi();
    return api.post(`/payroll/payrolls/${payrollId}/tax-configs`, params).then(res => res.data);
  },
  updateTaxConfig(payrollId, params, taxId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/payrolls/${payrollId}/tax-configs/${taxId}`, params).then(res => res.data);
  },
  deleteTaxConfig(payrollId, taxId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/payrolls/${payrollId}/tax-configs/${taxId}`).then(res => res.data);
  },

  // TAX LOCATION
  getTaxLocation(params) {
    const api = jApi.generateApi();
    return api.get('/tax-locations', params).then(res => res);
  },
  createTaxLocation(params) {
    const api = jApi.generateApi();
    return api.post('/tax-locations', params).then(res => res.data);
  },
  updateTaxLocation(params, taxId) {
    const api = jApi.generateApi();
    return api.put(`/tax-locations/${taxId}`, params).then(res => res.data);
  },
  deleteTaxLocation(taxId) {
    const api = jApi.generateApi();
    return api.delete(`/tax-locations/${taxId}`).then(res => res.data);
  },

  // TAX STATUS
  async getTaxStatus(payrollId, params) {
    const api = jApi.generateApi();
    // return api.get(`/payroll/payrolls/${payrollId}/tax-status`, { params }).then(res => res);
    try {
      const res = await api.get(`/payroll/payrolls/${payrollId}/tax-status`, { params });
      return res;
    } catch (err) {
      return err.response;
    }
  },
  createTaxStatus(payrollId, params) {
    const api = jApi.generateApi();
    return api.post(`/payroll/payrolls/${payrollId}/tax-status`, params).then(res => res.data);
  },
  updateTaxStatus(payrollId, params, taxId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/payrolls/${payrollId}/tax-status/${taxId}`, params).then(res => res.data);
  },
  deleteTaxStatus(payrollId, taxId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/payrolls/${payrollId}/tax-status/${taxId}`).then(res => res.data);
  },

  // BENEFITS
  getBenefits(payrollId) {
    const api = jApi.generateApi();
    return api.get(`/payroll/payrolls/${payrollId}/benefits`).then(res => res.data);
  },
  createBenefits(payrollId, params) {
    const api = jApi.generateApi();
    return api.post(`/payroll/payrolls/${payrollId}/benefits`, params).then(res => res.data);
  },
  updateBenefits(payrollId, params, benefitId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/payrolls/${payrollId}/benefits/${benefitId}`, params).then(res => res.data);
  },
  deleteBenefits(payrollId, benefitId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/payrolls/${payrollId}/benefits/${benefitId}`).then(res => res.data);
  },

  // COMPONENT NUMBER
  async getComponentNumber(componentId) {
    const componentType = 'payroll_benefit';
    const api = jApi.generateApi();
    try {
      const res = await api.get(`/payroll/payroll-components/${componentType}/${componentId}/numbers`);
      return res;
    } catch (err) {
      return err.response;
    }
  },
  createComponentNumber(params, componentId) {
    const componentType = 'payroll_benefit';
    const api = jApi.generateApi();
    return api.post(`/payroll/payroll-components/${componentType}/${componentId}/numbers`, params).then(res => res.data);
  },
  setComponentNumber(params, componentId) {
    const componentType = 'payroll_benefit';
    const api = jApi.generateApi();
    return api.put(`/payroll/payroll-components/${componentType}/${componentId}/numbers`, params).then(res => res.data);
  },
  updateComponentNumber(params, numberId, componentId) {
    const componentType = 'payroll_benefit';
    const api = jApi.generateApi();
    return api.put(`/payroll/payroll-components/${componentType}/${componentId}/numbers/${numberId}`, params).then(res => res.data);
  },
  deleteComponentNumber(numberId, componentId) {
    const componentType = 'payroll_benefit';
    const api = jApi.generateApi();
    return api.delete(`/payroll/payroll-components/${componentType}/${componentId}/numbers/${numberId}`).then(res => res.data);
  },

  // PAYROLL AREA MASTER
  getMasterPayrollArea() {
    const api = jApi.generateApi();
    return api.get('/payroll/payroll-area').then(res => res.data);
  },

  // PAYROLL AREA
  async getPayrollArea(userCompanyId, params) {
    const api = jApi.generateApi();
    // return api.get(`/payroll/employees/${userCompanyId}/payroll-areas?includes=payroll_area`,
    //   { params }).then(res => res);
    try {
      const res = await api.get(`/payroll/employees/${userCompanyId}/payroll-areas?includes=payroll_area`, { params });
      return res;
    } catch (err) {
      return err.response;
    }
  },
  createPayrollArea(userCompanyId, params) {
    const api = jApi.generateApi();
    return api.post(`/payroll/employees/${userCompanyId}/payroll-areas`, params).then(res => res.data);
  },
  updatePayrollArea(userCompanyId, params, payrollId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/employees/${userCompanyId}/payroll-areas/${payrollId}`, params).then(res => res.data);
  },
  deletePayrollArea(userCompanyId, payrollId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/employees/${userCompanyId}/payroll-areas/${payrollId}`).then(res => res.data);
  },

  // PAYSCALE MASTER
  getMasterPayscale(params) {
    const api = jApi.generateApi();
    return api.get('/payroll/pay-scales', { params }).then(res => res.data);
  },
  createMasterPayscale(params, payscaleId) {
    const api = jApi.generateApi();
    return api.post(`/payroll/pay-scales/${payscaleId}`, params).then(res => res.data);
  },
  updateMasterPayscale(params, payscaleId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/pay-scales/${payscaleId}`, params).then(res => res.data);
  },
  deleteMasterPayscale(payscaleId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/pay-scales/${payscaleId}`).then(res => res.data);
  },

  // PAYSCALE EMPLOYEE
  getPayscale(userCompanyId) {
    const api = jApi.generateApi();
    return api.get(`/payroll/pay-scales/company-users/${userCompanyId}`).then(res => res);
  },
  createPayscale(userCompanyId, params, payscaleId) {
    const api = jApi.generateApi();
    return api.post(`/payroll/pay-scales/${payscaleId}/company-users/${userCompanyId}`, params).then(res => res.data);
  },
  updatePayscale(userCompanyId, params, payscaleId, payscaleEmployeeId) {
    const api = jApi.generateApi();
    return api.put(`/payroll/pay-scales/${payscaleId}/company-users/${userCompanyId}/pay-scale-company-users/${payscaleEmployeeId}`, params).then(res => res.data);
  },
  deletePayscale(userCompanyId, payscaleId, payscaleEmployeeId) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/pay-scales/${payscaleId}/company-users/${userCompanyId}/pay-scale-company-users/${payscaleEmployeeId}`).then(res => res.data);
  },


  // COMPANY REGION
  getCompanyRegion(params) {
    const api = jApi.generateApi();
    return api.get('/payroll/company-regions', { params }).then(res => res.data);
  },

  // NORMAL STOP PAYMENT
  getNormalStopPayment(userCompanyId) {
    const api = jApi.generateApi();
    return api.get(`/payroll/stop-payments/company-user/${userCompanyId}`).then(res => res.data);
  },
  getListNormalStopPayment(userCompanyId) {
    const api = jApi.generateApi();
    return api.get(`/payroll/stop-payments/user-company/${userCompanyId}`).then(res => res.data);
  },
  setNormalStopPayment(userCompanyId, params) {
    const api = jApi.generateApi();
    return api.put(`/payroll/stop-payments/company-user/${userCompanyId}`, params).then(res => res.data);
  },

  createNormalStopPayment(userCompanyId, params) {
    const api = jApi.generateApi();
    return api.put(`/payroll/stop-payments/company-user/${userCompanyId}`, params).then(res => res.data);
  },

  // EXTENDS RUN PAYROLL
  getExtendsRunPayroll(userCompanyId) {
    const api = jApi.generateApi();
    return api.get(`/payroll/run-payroll-extensions/company-user/${userCompanyId}`).then(res => res.data);
  },
  setExtendsRunPayroll(userCompanyId, params) {
    const api = jApi.generateApi();
    return api.put(`/payroll/run-payroll-extensions/company-user/${userCompanyId}`, params).then(res => res.data);
  },

  // CHECK AVAILABILITY UPDATE EMPLOYEE
  checkAvailability(params) {
    const api = jApi.generateApi();
    return api.post('/payroll/update-employee-data/availability-check', params).then(res => res.data);
  },

  checkAccessPayrollInfo(params) {
    const api = jApi.generateApi();
    return api.post('/user/authorize-payroll-info-grade', params).then(res => res.data);
  },
};
