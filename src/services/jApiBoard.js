import jApi from './jApi';

export default {
  // ------------------------------------------------------------------

  getBoards() {
    const api = jApi.generateApi();
    return api.get('board-service/boards')
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getBoardAssignees(boardId) {
    const api = jApi.generateApi();
    return api.get(`board-service/boards/${boardId}/assignees`)
      .then(res => res.data);
  },
  updateBoardAssignees(req) {
    const api = jApi.generateApi();
    return api.patch(`board-service/boards/${req.board_id}/assignees`, {
      operation: req.operation,
      user_company_id: req.user_company_id,
    })
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getBoardStages(req) {
    const api = jApi.generateApi();
    return api.get(`board-service/boards/${req}/stages`)
      .then(res => res.data);
  },
  createBoardStages(req) {
    const api = jApi.generateApi();
    return api.post('board-service/stages', req)
      .then(res => res.data);
  },
  updateBoardStages(req) {
    const api = jApi.generateApi();
    return api.patch(`board-service/stages/${req.stage_id}`, req)
      .then(res => res.data);
  },
  deleteBoardStages(req) {
    const api = jApi.generateApi();
    return api.delete(`board-service/stages/${req.stageId}`)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getStageAmount(req) {
    const api = jApi.generateApi();
    return api.get(`board-service/stages/${req}/count`)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getCards(req, params) {
    const api = jApi.generateApi();
    return api.get(`board-service/stages/${req}/cards`, { params })
      .then(res => res.data);
  },

  getCardsByStage(req) {
    const api = jApi.generateApi();
    return api.post('board-service/stages/cards/search', req)
      .then(res => res.data);
  },

  getMoreCardsByStage(req) {
    const api = jApi.generateApi();
    return api.post('board-service/stages/cards/search', req)
      .then(res => res.data);
  },


  // ------------------------------------------------------------------

  getLabelList(req) {
    const api = jApi.generateApi();
    return api.get(`board-service/label/board/${req}`)
      .then(res => res.data);
  },
  updateCardLabel(req) {
    const api = jApi.generateApi();
    return api.put(`/board-service/label/${req.label.id}`, req.label)
      .then(res => res.data);
  },
  mappingCardLabel(req) {
    const api = jApi.generateApi();
    return api.put(`board-service/cards/${req.boardCardId}/label`, {
      label_id: req.labelId,
    }).then(res => res.data);
  },
  deleteMappingCardLabel(req) {
    const api = jApi.generateApi();
    return api.delete(`board-service/cards/${req.cardId}/label/${req.labelId}`)
      .then(res => res.data);
  },
  createCardDiscussion(req) {
    const api = jApi.generateApi();
    return api.post(`board-service/cards/${req}/discussion`, {})
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getRelationRecordStageName(req) {
    const arrToString = req.join();
    const api = jApi.generateApi();
    return api.get(`board-service/cards/values?value_ids=${arrToString}`)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  moveCard(req) {
    const api = jApi.generateApi();
    return api.patch(`board-service/stages/${req.stageFromId}/cards/${req.cardId}`, {
      assigned_to: req.assignedTo,
      assigned_multiple: req.assignedMultiple,
      board_stage_id: req.stageToId,
    }).then(res => res.data);
  },

  // ------------------------------------------------------------------

  changeOrderCard(cardId, req) {
    const api = jApi.generateApi();
    return api.patch(`board-service/cards/${cardId}/vertical_move_order`, req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  changeAssignee(req) {
    const api = jApi.generateApi();
    return api.patch(`board-service/stages/${req.stageId}/cards/${req.cardId}`, {
      assigned_to: req.assignedTo,
      assigned_multiple: req.assignedMultiple,
    }).then(res => res.data);
  },

  // ------------------------------------------------------------------

  getCardsActivityLog(cardId) {
    const api = jApi.generateApi();
    return api.get(`board-service/cards/${cardId}/trails`)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getFormList(req) {
    const api = jApi.generateApi();
    return api.post('jojoform-transaction/list/by-form', req)
      .then(res => res);
  },

  getFormListBatchCustomThree(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/list-batch-custom3', req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------


  createOrganigramFormRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoform-transaction/setup/organigram-form-rule/create', req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  getCardById(req) {
    const api = jApi.generateApi();
    return api.get(`board-service/cards/${req}`)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  createCard(req) {
    const api = jApi.generateApi();
    return api.post('board-service/cards/by-value', req)
      .then(res => res.data);
  },
  createCardNew(req, id) {
    const api = jApi.generateApi();
    return api.post(`board-service/boards/${id}`, req)
      .then(res => res.data);
  },
  updateCard(req) {
    const api = jApi.generateApi();
    return api.patch(`board-service/cards/${req.id}`, {
      name: req.name,
      due_date: req.due_date,
      assigned_to: req.assignedTo,
      assigned_multiple: req.assignedMultiple,
    }).then(res => res.data);
  },
  archiveCard(cardId) {
    const api = jApi.generateApi();
    return api.patch(`board-service/cards/${cardId}/archive`)
      .then(res => res.data);
  },
  deleteCard(cardId) {
    const api = jApi.generateApi();
    return api.delete(`board-service/cards/${cardId}`)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  createForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform/additional-info/form/create', req)
      .then(res => res.data);
  },
  updateForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform/additional-info/form/update', req)
      .then(res => res.data);
  },
  createBoards(req) {
    const api = jApi.generateApi();
    return api.post('board-service/boards', req)
      .then(res => res.data);
  },
  detailForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform/additional-info/form/detail', req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  updateVisibilityPrivilege(boardId, req) {
    const api = jApi.generateApi();
    return api.put(`/board-service/privelege/visibility/${boardId}`, req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  updateEmailReminder(boardId, req) {
    const api = jApi.generateApi();
    return api.patch(`/board-service/boards/${boardId}`, req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  sendExportBoard(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/generate-excel-board', req)
      .then(res => res.data);
  },

  getExportMonitoringList(req) {
    const api = jApi.generateApi();
    return api.get('jojoform-transaction/get-list-generate-excel-board', req)
      .then(res => res);
  },

  generateExcelExportMonitoringDetail(req) {
    const api = jApi.generateApi();
    return api.post('jojoform-transaction/get-generate-excel-board', req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

  searchCard(req) {
    const api = jApi.generateApi();
    return api.post('board-service/stages/cards/search', req)
      .then(res => res.data);
  },

  // ------------------------------------------------------------------

};
