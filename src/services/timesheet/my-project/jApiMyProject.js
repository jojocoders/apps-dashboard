import jApi from '../../jApi';

export default {
  getListTaskAssign() {
    const api = jApi.generateApi();
    return api.get('/times/timesheet/config/task-assign/list')
      .then(res => res.data);
  },
  getListTaskByProject(projectId) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/config/task-project/detail-by-project', projectId)
      .then(res => res.data);
  },
  getListUser(projectId) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/config/task-assign/list-user-by-project-id', projectId)
      .then(res => res.data);
  },
  getDueDate(id) {
    const api = jApi.generateApi();
    return api.post('/project/detail', id)
      .then(res => res.data);
  },
};
