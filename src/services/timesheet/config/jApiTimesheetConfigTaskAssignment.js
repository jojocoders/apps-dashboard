import jApi from '../../jApi';

export default {
  getOrgDetail(req) {
    const api = jApi.generateApi();
    return api.post('/jojoflow/organigram/detail', req)
      .then(res => res.data);
  },

  getPositionFormList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res);
  },

  getDivisionFormList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res);
  },

  getTaskFormList(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/list', req)
      .then(res => res);
  },

  getAssignmentList(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-assign/list', req)
      .then(res => res);
  },

  createTaskAssignment(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-assign/create', req)
      .then(res => res.data);
  },

  getAssignmentDetail(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/config/task-assign/detail', req)
      .then(res => res.data);
  },

  deleteTaskAssignment(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-assign/delete', req)
      .then(res => res.data);
  },
};
