import jApi from '../../jApi';

export default {

  getFormList(req) {
    const api = jApi.generateApi();
    return api.post('jojoform/setup/form/list', req)
      .then(res => res);
  },

  getProjectFormList(req) {
    const api = jApi.generateApi();
    return api.post('project/list', req)
      .then(res => res);
  },

  getProjectList(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/list', req)
      .then(res => res);
  },

  createTaskProject(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/create', req)
      .then(res => res.data);
  },

  deleteTaskProject(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/delete', req)
      .then(res => res.data);
  },

  updateTaskProjectDetail(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/update', req)
      .then(res => res.data);
  },

  getProjectDetail(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/detail-by-project', req)
      .then(res => res.data);
  },

  exportTaskProject(req) {
    const api = jApi.generateApi();
    return api.get('times/timesheet-report/task-project/export', req)
      .then(res => res.data);
  },

};
