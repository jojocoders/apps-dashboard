import jApi from '../../jApi';

export default {

  getPolicyDetail(req) {
    const api = jApi.generateApi();
    return api.get('times/timesheet/config/policy/detail-per-company', req)
      .then(res => res.data);
  },

  createTaskPolicy(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/policy/update', req)
      .then(res => res.data);
  },

};
