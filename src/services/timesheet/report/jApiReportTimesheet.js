import jApi from '../../jApi';

export default {
  getEmployeeList(req) {
    const api = jApi.generateApi();
    return api.post('/times/user-restriction/report', req)
      .then(res => res);
  },
  getProjectList(req) {
    const api = jApi.generateApi();
    return api.post('/project/list', req)
      .then(res => res);
  },
  getTaskList(req) {
    const api = jApi.generateApi();
    return api.post('times/timesheet/config/task-project/list', req)
      .then(res => res);
  },
  // Summary
  getSummaryReport(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet-report/summary/list', req)
      .then(res => res);
  },
  getDetailSummaryReport(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet-report/summary/detail', req)
      .then(res => res);
  },
  generateSummaryXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times-timesheet-go-service/report/export', req)
      .then(res => res);
  },
  generateDetailXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet-report/detail/export', req)
      .then(res => res);
  },
  // Form-Project
  getFormByTaskId(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/config/task-project/get-form', req)
      .then(res => res);
  },

  generateFormXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet-report/form-task/export', req)
      .then(res => res);
  },

  getModuleSetting(req) {
    const api = jApi.generateApi();
    return api.get('/times/module-setting', req)
      .then(res => res.data);
  },
};
