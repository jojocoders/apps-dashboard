import jApi from '../../jApi';

export default {
  getListTaskByDate(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/list-by-date', req)
      .then(res => res.data);
  },
  getListTask(req) {
    const api = jApi.generateApi();
    return api.get('/times/timesheet/config/task-assign/list', req)
      .then(res => res.data);
  },
  updatedTime(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/update', req)
      .then(res => res.data);
  },
  updateNotes(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/update-notes', req)
      .then(res => res.data);
  },
  getListTaskDetailById(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/list', req)
      .then(res => res.data);
  },
  getRunningTask(req) {
    const api = jApi.generateApi();
    return api.get('/times/timesheet/check-running-task', req)
      .then(res => res.data);
  },
  getFormByTaskId(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/config/task-project/get-form', req)
      .then(res => res.data);
  },
  getForm(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/get-form', req)
      .then(res => res.data);
  },
  createAdditionalData(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/add-form', req)
      .then(res => res.data);
  },
  getTaskLog(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/log', req)
      .then(res => res.data);
  },
  getProjectSwimlane(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/config/project-swimlane/list-by-project', req)
      .then(res => res.data);
  },

  getEmployee(req) {
    const api = jApi.generateApi();
    return api.post('/user/employee/list-with-pagination', req)
      .then(res => res.data);
  },

  getRelationRecord(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/list/by-form', req)
      .then(res => res.data);
  },

  getOrgId(req) {
    const api = jApi.generateApi();
    return api.post('/jojoflow/organigram/detail-by-user-company', req)
      .then(res => res.data);
  },

  getLogApproval(req) {
    const api = jApi.generateApi();
    return api.get(`/times-timesheet-go-service/approval/${req.id}/log`, req)
      .then(res => res.data);
  },
};
