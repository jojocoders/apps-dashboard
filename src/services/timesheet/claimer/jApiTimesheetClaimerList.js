import jApi from '../../jApi';

export default {
  // reportDataDraft(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/times/timesheet/action/submit', req)
  //     .then(res => res.data);
  // },
  deleteDataDraft(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/action/delete', req)
      .then(res => res.data);
  },

  // Golang
  reportDataDraft(req) {
    const api = jApi.generateApi();
    return api.post('/times-timesheet-go-service/action/submit', req)
      .then(res => res.data);
  },
  // deleteDataDraft(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/times-timesheet-go-service/action/delete', req)
  //     .then(res => res.data);
  // },
  getSummaryBlock() {
    const api = jApi.generateApi();
    return api.get('/times/timesheet/summary-block')
      .then(res => res.data);
  },
  getSummaryBlockApprover() {
    const api = jApi.generateApi();
    return api.get('/times-timesheet-go-service/approver/summary-block')
      .then(res => res.data);
  },
  getListSummary(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/list', req)
      .then(res => res.data);
  },
  getListSummaryApprover(req) {
    const api = jApi.generateApi();
    return api.post('/times-timesheet-go-service/approver/list', req)
      .then(res => res.data);
  },

  // Golang
  // getListSummary(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/times-timesheet-go-service/approver/list', req)
  //     .then(res => res.data);
  // },

  approveDataDraft(req) {
    const api = jApi.generateApi();
    return api.post('/times-timesheet-go-service/action/approve', req)
      .then(res => res.data);
  },

  rejectDataDraft(req) {
    const api = jApi.generateApi();
    return api.post('/times-timesheet-go-service/action/reject', req)
      .then(res => res.data);
  },
};
