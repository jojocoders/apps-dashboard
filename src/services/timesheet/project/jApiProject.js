import jApi from '../../jApi';

const request = {
  pagination: {
    limit: 0,
    page: 0,
    column: '',
    ascending: false,
  },
};

export default {
  listProject(req) {
    const api = jApi.generateApi();
    return api.post('project/list', req)
      .then(res => res);
  },

  exportProject(req) {
    const api = jApi.generateApi();
    return api.post('project/export', req)
      .then(res => res.data);
  },

  createProject(req) {
    const api = jApi.generateApi();
    return api.post('project/create', req)
      .then(res => res.data);
  },

  removeProject(req) {
    const api = jApi.generateApi();
    return api.post('project/delete', req)
      .then(res => res.data);
  },

  detailProject(req) {
    const api = jApi.generateApi();
    return api.post('project/detail', req)
      .then(res => res.data);
  },

  updateProject(req) {
    const api = jApi.generateApi();
    return api.post('project/update', req)
      .then(res => res.data);
  },

  divisionUpdateProject(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/project/update', req)
      .then(res => res.data);
  },

  createRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/create', req)
      .then(res => res.data);
  },

  removeRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/delete', req)
      .then(res => res.data);
  },

  detailRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/detail', req)
      .then(res => res.data.data);
  },

  updateRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/update', req)
      .then(res => res.data);
  },

  listRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/list', req)
      .then(res => res.data);
  },

  listOrganigramByDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list-by-division/pagination', req)
      .then(res => res.data);
  },

  createOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/create', req)
      .then(res => res.data.data);
  },

  removeOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/delete', req)
      .then(res => res.data);
  },

  updateOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/update', req)
      .then(res => res.data);
  },

  listOrganigram(req) {
    const api = jApi.generateApi();
    if (req) {
      return api.post('jojoflow/organigram/list', req)
        .then(res => res.data);
    }
    return api.post('jojoflow/organigram/list', request)
      .then(res => res.data);
  },

  getOrganigramChart(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list-chart', req)
      .then(res => res.data);
  },

  exportPositionToEmail(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/export', req)
      .then(res => res.data);
  },

  exportDivisionToEmail(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/export', req)
      .then(res => res.data);
  },

  getDetailPositionSync(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/integration/organigram-buffer/detail', req)
      .then(res => res.data);
  },

  getDetailPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram/detail', req)
      .then(res => res.data);
  },

  updatePositionSync(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/integration/organigram-buffer/sync', req)
      .then(res => res.data);
  },

  getDetailPositionSyncLog(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/integration/organigram-buffer/log', req)
      .then(res => res);
  },

  listPeriod(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/config/period/list', req)
      .then(res => res.data);
  },

  createPeriod(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/config/period/create', req)
      .then(res => res.data);
  },

  detailPeriod(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/config/period/detail', req)
      .then(res => res.data);
  },

  updatePeriod(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/config/period/update', req)
      .then(res => res.data);
  },

  listDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', (req || request))
      .then(res => res.data);
  },

  createDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/create', req)
      .then(res => res.data.data);
  },

  removeDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/delete', req)
      .then(res => res.data);
  },

  updateDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/update', req)
      .then(res => res.data);
  },

  detailDivision(idDivision) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/detail', idDivision)
      .then(res => res.data);
  },

  listPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res.data);
  },
};
