import axios from 'axios';
import Vue from 'vue';

function generateApi() {
  const token = Vue.ls.get('Token');
  return axios.create({
    baseURL: process.env.GATE_URL,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export default {
  listFieldForm(req) {
    const api = generateApi();
    return api.post('/jojoform/additional-info/form/detail', req)
      .then(res => res.data);
  },

  getDetailRecord(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/detail', req)
      .then(res => res.data);
  },

  getRelationRecord(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/list-relation-records', req)
      .then(res => res.data);
  },

  listCategoryinRecord(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/category/list/by-user', req)
      .then(res => res.data);
  },

  createAdditionalData(req) {
    const api = generateApi();
    return api.post('jojoform-transaction/create-submit', req)
      .then(res => res.data);
  },

  createAdditionalDatas(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/create', req)
      .then(res => res.data);
  },

  updateAdditionalData(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/update', req)
      .then(res => res.data);
  },

  deleteDataRequestor(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/delete', req)
      .then(res => res.data);
  },

  getLogs(req) {
    const api = generateApi();
    return api.post('/jojoform-transaction/log', req)
      .then(res => res.data);
  },

  getListBatchRecord(req, dataForward) {
    const api = generateApi();
    return api.post('/jojoform-transaction/list-batch-record', req)
      .then(res => ({
        data: res.data,
        dataForward,
      }));
  },

  getLayerByChild(req) {
    const api = generateApi();
    return api.post('/expense-setup/expense-layer/relation/hierarchy-by-child', req)
      .then(res => res.data);
  },
};
