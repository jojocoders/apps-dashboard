import jBaseAPIManager from './jBaseAPIManager';

export default class jAllowanceCustomDailyManager extends jBaseAPIManager {
  /**
   * Request Get Allowance Custom Daily
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetAllowanceCustomDaily(form) {
    const body = {
      allowance_id: form.selectedAllowance,
      type: form.selectedAllowanceCustomDailyType,
      month: form.selectedMonth,
      year: form.selectedYear,
    };
    const path = 'payroll/allowance/custom-daily/company-data';
    return jBaseAPIManager.requestPost(path, body);
  }

  static requestGetPercentageAmounts(form) {
    const body = {
      allowance_id: form.selectedAllowance,
      month: form.selectedMonth,
      year: form.selectedYear,
    };
    const path = 'payroll/allowance/custom-daily/percentage-amounts';
    return jBaseAPIManager.requestPost(path, body);
  }

  static requestSavePercentageAmounts(form) {
    const body = {
      allowance_id: form.allowance_id,
      percentage_amounts: form.percentage_amounts,
    };
    const path = 'payroll/allowance/custom-daily/save-amounts';
    return jBaseAPIManager.requestPost(path, body);
  }

  static requestSaveAllowanceCustomDailyBatch(form) {
    const body = {
      allowance_id: form.allowance_id,
      allowance_custom_daily: form.allowance_custom_daily,
    };
    const path = 'payroll/allowance/custom-daily/save-batch';
    return jBaseAPIManager.requestPost(path, body);
  }

  static requestGetImportAllowanceCustomFormat(form) {
    const body = {
      year: form.selectedYear,
      month: form.selectedMonth,
      allowance_id: form.selectedAllowance,
      type: form.selectedAllowanceCustomDailyType,
    };
    const path = 'payroll/admin/download/custom-daily-allowance-format';
    return jBaseAPIManager.requestPost(path, body);
  }
}
