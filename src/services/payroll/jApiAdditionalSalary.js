import jApi from '@/services/jApi';

export default {
  getListAdditionalSalary() {
    const api = jApi.generateApi();
    return api.get('payroll/additional-salaries')
      .then(res => res.data);
  },

  createAdditionalSalary(req) {
    const api = jApi.generateApi();
    return api.post('payroll/additional-salaries', req)
      .then(res => res.data);
  },

  updateAdditionalSalary(req) {
    const api = jApi.generateApi();
    return api.put(`payroll/additional-salaries/${req.id}`, req.data)
      .then(res => res.data);
  },

  deleteAdditionalSalary(id) {
    const api = jApi.generateApi();
    return api.delete(`payroll/additional-salaries/${id}`)
      .then(res => res.data);
  },

  filterListAdditionalSalary(params) {
    const api = jApi.generateApi();
    return api.get(`payroll/additional-salaries?${params}`)
      .then(res => res.data);
  },
};
