import axios from 'axios';
// import Vue from 'vue';
// import stores from '@/stores';
import router from '@/router';


export default class jBaseAPIManager {
  static createRequest() {
    let authToken = 'Bearer ';
    if (localStorage.getItem('Token')) {
      const token = JSON.parse(localStorage.getItem('Token')).value;
      authToken = `Bearer ${token}`;
    }
    const instance = axios.create({
      baseURL: process.env.GATE_URL,
      headers: { Authorization: authToken },
    });

    instance.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          localStorage.setItem('refreshToken', response.headers.refresh_token);
          // eslint-disable-next-line
          function parseJwt(token) {
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            return JSON.parse(window.atob(base64));
          }

          const dataExpire = parseJwt(response.headers.token);

          const tokenData = {
            value: response.headers.token,
            expire: dataExpire.exp * 1000,
          };

          localStorage.setItem('Token', JSON.stringify(tokenData));
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        }
        // handle error
        return Promise.reject(error);
      },
    );

    return instance;
  }


  static createRequestLogin() {
    let authToken = 'Bearer ';
    if (localStorage.getItem('Token')) {
      const token = JSON.parse(localStorage.getItem('Token')).value;
      authToken = `Bearer ${token}`;
    }

    const instance = axios.create({
      baseURL: process.env.GATE_URL,
      headers: { Authorization: authToken },
    });

    return instance;
  }

  static requestGet(url, isRetry = true, params) {
    return new Promise((resolve, reject) => {
      jBaseAPIManager.createRequest()
        .get(url, params)
        .then((response) => {
          resolve(response);
        })
        .catch((errorGet) => {
          if (errorGet.response) {
            if (errorGet.response.status === 401 && isRetry) {
              // jBaseAPIManager.requestRefreshToken()
              //   .then(() => {
              //     jBaseAPIManager.requestGet(url, false)
              //       .then((response) => {
              //         resolve(response);
              //       })
              //       .catch((errorAgain) => {
              //         reject(errorAgain.response.data);
              //       });
              //   })
              //   .catch((errorRefreshToken) => {
              //     reject(errorRefreshToken.response.data);
              //   });
            } else {
              reject(errorGet.response.data);
            }
          } else {
            reject({
              error: true,
              message: 'Something wrong, Try again later',
            });
          }
        });
    });
  }

  static requestPost(url, body, isRetry = true) {
    return new Promise((resolve, reject) => {
      jBaseAPIManager.createRequest()
        .post(url, body)
        .then((response) => {
          resolve(response);
        })
        .catch((errorPost) => {
          if (errorPost.response) {
            if (errorPost.response.status === 401 && isRetry) {
              // jBaseAPIManager.requestRefreshToken()
              //   .then(() => {
              //     jBaseAPIManager.requestPost(url, body, false)
              //       .then((response) => {
              //         resolve(response);
              //       })
              //       .catch((errorAgain) => {
              //         reject(errorAgain.response.data);
              //       });
              //   })
              //   .catch((errorRefreshToken) => {
              //     reject(errorRefreshToken.response.data);
              //   });
            } else {
              reject(errorPost.response.data);
            }
          } else {
            reject({
              error: true,
              message: 'Something wrong, Try again later',
            });
          }
        });
    });
  }

  static requestPut(url, body) {
    return new Promise((resolve, reject) => {
      jBaseAPIManager.createRequest()
        .put(url, body)
        .then((response) => {
          resolve(response);
        })
        .catch((errorPost) => {
          if (errorPost.response) {
            reject(errorPost.response.data);
          } else {
            reject({
              error: true,
              message: 'Something wrong, Try again later',
            });
          }
        });
    });
  }

  static requestDelete(url, body, isRetry = true) {
    return new Promise((resolve, reject) => {
      jBaseAPIManager.createRequest()
        .delete(url, { data: body })
        .then((response) => {
          resolve(response);
        })
        .catch((errorGet) => {
          if (errorGet.response) {
            if (errorGet.response.status === 401 && isRetry) {
              // jBaseAPIManager.requestRefreshToken()
              //   .then(() => {
              //     jBaseAPIManager.requestGet(url, false)
              //       .then((response) => {
              //         resolve(response);
              //       })
              //       .catch((errorAgain) => {
              //         reject(errorAgain.response.data);
              //       });
              //   })
              //   .catch((errorRefreshToken) => {
              //     reject(errorRefreshToken.response.data);
              //   });
            } else {
              reject(errorGet.response.data);
            }
          } else {
            reject({
              error: true,
              message: 'Something wrong, Try again later',
            });
          }
        });
    });
  }

  static requestPostLogin(url, body, isRetry = true) {
    return new Promise((resolve, reject) => {
      jBaseAPIManager.createRequestLogin()
        .post(url, body)
        .then((response) => {
          resolve(response);
        })
        .catch((errorPost) => {
          if (errorPost.response) {
            if (errorPost.response.status === 401 && isRetry) {
              // jBaseAPIManager.requestRefreshToken()
              //   .then(() => {
              //     jBaseAPIManager.requestPost(url, body, false)
              //       .then((response) => {
              //         resolve(response);
              //       })
              //       .catch((errorAgain) => {
              //         reject(errorAgain.response.data);
              //       });
              //   })
              //   .catch((errorRefreshToken) => {
              //     reject(errorRefreshToken.response.data);
              //   });
            } else {
              reject(errorPost.response.data);
            }
          } else {
            reject({
              error: true,
              message: 'Something wrong, Try again later',
            });
          }
        });
    });
  }

  static requestRefreshToken() {
    return new Promise((resolve, reject) => {
      if (localStorage.getItem('refreshToken')) {
        // const url = 'auth/refresh';
        // const body = {
        //   refresh_token: localStorage.getItem('refreshToken'),
        // };
        // jBaseAPIManager.requestPost(url, body, false)
        //   .then((response) => {
        //     Vue.ls.set('Token', response.data.token, 60 * 60 * 1000);
        //     localStorage.setItem('refreshToken', response.data.refresh_token);
        //     resolve(response);
        //   })
        //   .catch((error) => {
        //     reject(error.response.data);
        //   });
      } else {
        reject({
          error: true,
          message: 'Something wrong, Try again later',
        });
      }
    });
  }
}
