import jApi from '@/services/jApi';

export default {
  getListCurrencyRule(req) {
    const api = jApi.generateApi();
    return api.get('payroll-component-currency-rule', req)
      .then(res => res);
  },
  createCurrencyRule(req) {
    const api = jApi.generateApi();
    return api.post('payroll/payroll-component-currency-rule', req)
      .then(res => res);
  },
  updateCurrencyRule(req) {
    const api = jApi.generateApi();
    return api.put(`payroll/payroll-component-currency-rule/${req.id}`, req.data)
      .then(res => res);
  },
  deleteCurrencyRule(id) {
    const api = jApi.generateApi();
    return api.delete(`payroll/payroll-component-currency-rule/${id}`)
      .then(res => res);
  },
  getListCurrencyMaster() {
    const api = jApi.generateApi();
    return api.get('nocode/api/currency-master')
      .then(res => res.data);
  },

  // GET COMPONENT NAME AND ID
  getListBenefit() {
    const api = jApi.generateApi();
    return api.get('payroll/benefit')
      .then(res => res.data);
  },
  getListAllowance() {
    const api = jApi.generateApi();
    return api.get('payroll/allowance')
      .then(res => res.data);
  },
  getListDeduction() {
    const api = jApi.generateApi();
    return api.get('payroll/deduction')
      .then(res => res.data);
  },
  getListBonus() {
    const api = jApi.generateApi();
    return api.get('payroll/bonus')
      .then(res => res.data);
  },
  getListOvertime() {
    const api = jApi.generateApi();
    return api.get('payroll/overtime')
      .then(res => res.data);
  },
  getListSeverancePay() {
    const api = jApi.generateApi();
    return api.get('payroll/severance-pay')
      .then(res => res.data);
  },
  getListAdditionalSalary() {
    const api = jApi.generateApi();
    return api.get('payroll/additional-salaries')
      .then(res => res.data);
  },
};
