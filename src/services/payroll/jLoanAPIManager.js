import jBaseAPIManager from './jBaseAPIManager';

export default class jLoanAPIManager extends jBaseAPIManager {
  static requestGetLoanList(data) {
    const path = 'payroll/loan/list';

    return jBaseAPIManager.requestPost(path, data);
  }
  static requestSaveLoan(body) {
    const path = 'payroll/loan/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetLoanPaymentList(data) {
    const path = 'payroll/loan/payment/list';
    const body = {
      loan_id: data.loan_id,
      pagination: data.pagination,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestSaveLoanPayment(body) {
    const path = 'payroll/loan/payment/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetLoan(id) {
    const path = 'payroll/loan/get';
    const body = {
      id,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
}
