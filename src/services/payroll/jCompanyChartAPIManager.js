import jBaseAPIManager from './jBaseAPIManager';

export default class jCompanyChartAPIManager extends jBaseAPIManager {
  /**
   * Request Get Organization Chart.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetOrganizationChart() {
    const path = 'payroll/chart/organization';
    return jBaseAPIManager.requestGet(path);
  }

  /**
   * Request Save Organization Chart.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveOrganizationChart(body, onSuccess, onFailed) {
    const path = 'payroll/chart/organization';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }

  /**
   * Request Get Job Chart.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetJobChart() {
    const path = 'payroll/chart/job';
    return jBaseAPIManager.requestGet(path);
  }

  /**
   * Request Save Job Chart.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveJobChart(body, onSuccess, onFailed) {
    const path = 'payroll/chart/job';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }

  /**
   * Request Get Position Chart.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetPositionChart() {
    const path = 'payroll/chart/position';
    return jBaseAPIManager.requestGet(path);
  }

  /**
   * Request Save Position Chart.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSavePositionChart(body, onSuccess, onFailed) {
    const path = 'payroll/chart/position';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
}
