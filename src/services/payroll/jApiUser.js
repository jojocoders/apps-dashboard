import jApi from './jApi';

// temporary data
const request = {
  pagination: {
    limit: 0,
    page: 0,
    column: '',
    ascending: false,
  },
};

export default {
  listUser(req) {
    const api = jApi.generateApi();
    if (req) {
      return api.post('user/employee/list', req)
        .then(res => res.data.data);
    }
    return api.post('user/employee/list', request)
      .then(res => res.data.data);
  },

  emailId(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/detail-by-email', req)
      .then(res => res.data);
  },

  userAuthorize() {
    const api = jApi.generateApi();
    return api.get('user/authorize')
      .then(res => res.data);
  },
};
