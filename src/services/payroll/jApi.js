import axios from 'axios';
// import stores from '../stores';
import router from '../router';

export default {
  generateApi() {
    const token = JSON.parse(localStorage.getItem('Token')).value;// Vue.ls.get('Token');

    const api = axios.create({
      baseURL: process.env.GATE_URL,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    api.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          localStorage.setItem('refreshToken', response.headers.refresh_token);
          // eslint-disable-next-line
          function parseJwt(token) {
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            return JSON.parse(window.atob(base64));
          }

          const dataExpire = parseJwt(response.headers.token);

          const tokenData = {
            value: response.headers.token,
            expire: dataExpire.exp * 1000,
          };

          localStorage.setItem('Token', JSON.stringify(tokenData));
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        }
        // handle error
        return error;
      });

    return api;
  },
  loginApi() {
    const api = axios.create({
      baseURL: process.env.GATE_URL,
    });
    return api;
  },
};
