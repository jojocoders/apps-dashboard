import jBaseAPIManager from './jBaseAPIManager';

export default class jAttendanceAPIManager extends jBaseAPIManager {
  static requestGetManualAttendanceCodeMapping(id) {
    const body = { id };
    const path = 'payroll/admin/manual-attendance/mapping/get';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestSaveManualAttendanceCodeMapping(id, manualAttendanceId, leaveId, isDelete) {
    const body = {
      id,
      manual_attendance_id: manualAttendanceId,
      leave_id: leaveId,
      is_delete: isDelete };
    const path = 'payroll/admin/manual-attendance/mapping/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetManualAttendanceCodeMappingList() {
    const path = 'payroll/admin/manual-attendance/mapping/list';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetLeaveConfigLeaveType() {
    const path = 'payroll/admin/manual-attendance/leave/config/leave-type';
    return jBaseAPIManager.requestGet(path);
  }
}
