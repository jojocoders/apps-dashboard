import jApi from '../jApi';
import jBaseAPIManager from './jBaseAPIManager';

export default class jUserAPIManager extends jBaseAPIManager {
  /**
   * Request Login method.
   * @param {string} email User email
   * @param {string} password User password
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestLogin(email, password) {
    const path = 'auth/login';
    const body = { email, password };
    return jBaseAPIManager.requestPostLogin(path, body);
  }
  /**
   * Request Change Password method.
   * @param {string} old password
   * @param {string} new password
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSavePassword(oldPassword, newPassword) {
    const path = 'user/change-password';
    const body = {
      old_password: oldPassword,
      new_password: newPassword,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Get User Detail.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUsers(path) {
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Get User Detail.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetAttendance(path, form) {
    const body = {
      month: form.selectedMonth,
      year: form.selectedYear,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Get User Detail by Token. Token is taken from localStorage.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUserDetailByToken(onSuccess, onFailed) {
    const path = 'payroll/user';
    return jBaseAPIManager.requestGet(path, onSuccess, onFailed);
  }
  /**
   * Request User Detail by User ID.
   * @param {number} userId
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUserDetailByUserId(userId) {
    const path = 'payroll/user';
    const body = { user_id: userId };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Save User Detail.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveUserDetail(body, onSuccess, onFailed) {
    const path = 'payroll/user/save';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request Get User Company Detail by Token. Token is taken from localStorage.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUserCompanyDetailByToken(onSuccess, onFailed) {
    const path = 'payroll/user/company';
    return jBaseAPIManager.requestGet(path, onSuccess, onFailed);
  }
  /**
   * Request Get User Company Detail by User ID.
   * @param {number} userId User ID
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUserCompanyDetailByUserId(userId) {
    const path = 'payroll/user/company';
    const body = { user_id: userId };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Save User Company Detail.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveUserCompanyDetail(body) {
    const path = 'payroll/user/company/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Get User Payroll Detail by Token. Token is taken from localStorage.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUserPayrollDetailByToken() {
    const path = 'payroll/user/payroll';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Get User Payroll Detail by User ID
   * @param {number} userId User ID
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetUserPayrollDetailByUserId(userId) {
    const path = 'payroll/user/payroll';
    const body = { user_id: userId };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Save User Payroll Detail
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveUserPayrollDetail(body) {
    const path = 'payroll/user/payroll/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestCreatePayrollComponentNumbersOnPayroll(componentType, componentId, payload) {
    const path = `payroll/payroll-components/${componentType}/${componentId}/numbers`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestUpdatePayrollComponentNumbers(componentType, componentId, numbersId, payload) {
    const path = `payroll/payroll-components/${componentType}/${componentId}/numbers/${numbersId}`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestGetPayrollComponentsHistory(componentType, componentId) {
    const path = `payroll/payroll-components/${componentType}/${componentId}/numbers`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreatePayrollComponentHistory(componentType, componentId, payload) {
    const path = `payroll/payroll-components/${componentType}/${componentId}/numbers`;
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestDeletePayrollComponentHistory(componentType, componentId, numbersId) {
    const path = `payroll/payroll-components/${componentType}/${componentId}/numbers/${numbersId}`;
    return jBaseAPIManager.requestDelete(path);
  }
  /**
   * Request Get Allowance List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetAllowanceList(isTemporary) {
    let filter = '';
    if (typeof isTemporary !== 'undefined') {
      filter = `?filter[is_temporary]=${isTemporary}`;
    }
    const path = `payroll/allowance${filter}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetAllowanceMinimumWorkingHours(id) {
    const path = `payroll/allowances/${id}/minimum-working-hours`;
    return jBaseAPIManager.requestGet(path);
  }

  static requestGetAllowanceByOvertime(id) {
    const path = `payroll/allowances/${id}/overtime-daily`;
    return jBaseAPIManager.requestGet(path);
  }
  static filterAllowanceList(params) {
    const path = `payroll/allowance?${params}`;
    return jBaseAPIManager.requestGet(path);
  }


  /**
   * Request Get Allowance List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveAllowance(allowance) {
    const api = jApi.generateApi();
    return api.post('payroll/allowance', allowance)
      .then(res => res);
  }

  static requestSaveAllowanceOvertimeDuration(id, payload) {
    const path = `payroll/allowances/${id}/overtime-daily`;
    return jBaseAPIManager.requestPut(path, payload);
  }

  static requestSaveAllowanceMinimumWorkingHours(id, payload) {
    const path = `payroll/allowances/${id}/minimum-working-hours`;
    return jBaseAPIManager.requestPut(path, payload);
  }

  static requestSaveAllowanceMixRule(id, payload) {
    const path = `payroll/allowances/${id}/composites`;
    return jBaseAPIManager.requestPut(path, payload);
  }

  /**
   * Request Get Allowance Custom Daily
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetAllowanceCustomDaily(form) {
    const body = {
      allowance_id: form.selectedAllowance,
      type: form.selectedAllowanceCustomDailyType,
      company_id: form.company_id,
      month: form.selectedMonth,
      year: form.selectedYear,
    };
    const path = 'payroll/allowance/custom-daily/company-data';
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Get Additional Salary List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetAdditionalSalaryList() {
    const path = 'payroll/additional-salaries';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetAdditionalSalaryMapping(id) {
    const path = `payroll/payroll/${id}/additional-salaries`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetDetailAdditionalSalaryMapping(payrollId, payrollAdditionalSalaryId) {
    const path = `payroll/payroll/${payrollId}/additional-salaries/${payrollAdditionalSalaryId}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreateAdditionalSalaryMapping(payrollId, body) {
    const path = `payroll/payroll/${payrollId}/additional-salaries`;
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestUpdateAdditionalSalaryMapping(payrollId, payrollAdditionalSalaryId, body) {
    const path = `payroll/payroll/${payrollId}/additional-salaries/${payrollAdditionalSalaryId}`;
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeleteAdditionalSalaryMapping(payrollId, payrollAdditionalSalaryId) {
    const path = `payroll/payroll/${payrollId}/additional-salaries/${payrollAdditionalSalaryId}`;
    return jBaseAPIManager.requestDelete(path);
  }
  /**
   * Request Get Benefit List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBenefitList() {
    const path = 'payroll/benefits';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetBenefits() {
    const path = 'payroll/benefit';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Get Tax List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxList() {
    const path = 'payroll/tax';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Get Tax Status List by Tax ID.
   * @param {number} taxId Tax ID
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxStatusList(taxId) {
    const path = 'payroll/tax/status';
    const body = { taxId };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Get Bank List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBankList() {
    const path = 'integration-bank/bank/list';
    const body = {
      pagination: {
        limit: 0,
        page: 0,
        column: '',
        ascending: false,
        query: '',
      },
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request Get default payroll component
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetDefaultPayrollComponent() {
    const path = 'payroll/admin/payroll/component';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request get company detail.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetCompanyDetailByToken(onSuccess, onFailed) {
    const path = 'payroll/admin/company';
    return jBaseAPIManager.requestGet(path, onSuccess, onFailed);
  }
  /**
   * Request update company detail.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveCompanyDetail(body, onSuccess, onFailed) {
    const path = 'payroll/admin/company';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }

  // Employment Group
  static requestGetEmploymentGroup(payload) {
    const path = 'user/employee/employment-type/relation/list';
    return jBaseAPIManager.requestPost(path, payload);
  }

  // Position
  static requestGetPosition(userCompanyID) {
    const path = `/jojoflow/v2/users/${userCompanyID}/positions?limit=100&page=1&column=created_date&ascending=true&query=`;
    return jBaseAPIManager.requestGet(path);
  }

  // Division
  static requestGetDivision(id) {
    const path = '/jojoflow/organigram/detail';
    return jBaseAPIManager.requestPost(path, { id });
  }

  // Cost Center
  static requestGetCostCenterByDivision(payload) {
    const path = '/costcenter/list/by-division';
    return jBaseAPIManager.requestPost(path, payload);
  }

  // Tax Location
  static requestGetTaxLocationByLocationID(locationID) {
    const path = `/user/employee/location-relation/detail-by-location/${locationID}`;
    return jBaseAPIManager.requestGet(path);
  }

  static requestGetTaxLocationDetail(taxLocationID) {
    const path = `/payroll/tax-locations/${taxLocationID}`;
    return jBaseAPIManager.requestGet(path);
  }

  // Payroll Area
  static requestGetPayrollAreaByOrganigramID(organigramID) {
    const path = `/jojoflow/organigram/${organigramID}/payroll-areas?page=1&limit=5`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayrollDetail(payrollAreaID) {
    const path = `/jojoflow/payroll-areas/${payrollAreaID}`;
    return jBaseAPIManager.requestGet(path);
  }

  // Grade
  static requestGetGrade(payload) {
    const path = '/user/employee/grade/relation/list';
    return jBaseAPIManager.requestPost(path, payload);
  }
  // Payroll Area
  static requestGetPayrollArea(userCompanyID) {
    const path = `payroll/employees/${userCompanyID}/payroll-areas`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreatePayrollArea(body, userCompanyID) {
    const path = `payroll/employees/${userCompanyID}/payroll-areas`;
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestUpdatePayrollArea(body, userCompanyID) {
    const path = `payroll/employees/${userCompanyID}/payroll-areas`;
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeletePayrollArea(body, payrollID) {
    const path = `payroll/employees/${payrollID}/payroll-areas/${body.id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetListPayrollArea() {
    const path = 'jojoflow/payroll-areas';
    return jBaseAPIManager.requestGet(path);
  }
  // Payroll Scale
  static requestGetPayrollScale(userCompanyID) {
    const path = `payroll/pay-scales/company-users/${userCompanyID}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreatePayrollScale(body, userCompanyID) {
    const path = `payroll/pay-scales/${body.pay_scale_id}/company-users/${userCompanyID}`;
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestUpdatePayrollScale(body, userCompanyID) {
    const path = `payroll/pay-scales/${body.pay_scale_id}/company-users/${userCompanyID}/
  pay-scale-company-users/${body.id}`;
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeletePayrollScale(body, userCompanyID) {
    const path = `payroll/pay-scales/${body.pay_scale_id}/company-users/${userCompanyID}/
  pay-scale-company-users/${body.id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetListPayrollScale() {
    const path = 'payroll/pay-scales';
    return jBaseAPIManager.requestGet(path);
  }
  // New Payroll
  /**
   * Request get basic payroll config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBasicPayrollRecurring(body) {
    const path = '/payroll/admin/payroll-recurring/list';
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request save basic payroll recurring.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveBasicPayrollRecurring(body) {
    const path = '/payroll/admin/payroll-recurring/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestDeleteBasicPayrollRecurring(body) {
    const path = '/payroll/admin/payroll-recurring/recurring-delete';
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request save basic payroll recurring.
   */
  static requestSaveMappingSalaryToSalary(body, payrollID, recurringID) {
    const path = `/payroll/payrolls/${payrollID}/recurrings/${recurringID}/salary-configs`;
    return jBaseAPIManager.requestPut(path, body);
  }
  // End New Payroll
  // Tax Number
  /**
   * Request get basic Tax Number.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxNumber(payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request save tax number.
   */
  static requestSaveTaxNumber(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax`;
    return jBaseAPIManager.requestPost(path, body);
  }
  // Tax Configuration
  /**
   * Request get basic Tax Configuration.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxConfiguration(payload, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-configs?limit=${payload.limit}&page=${payload.page}`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request save tax Configuration.
   */
  static requestSaveTaxConfiguration(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-configs`;
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request update tax Configuration.
   */
  static requestUpdateTaxConfiguration(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-configs/${body.id}`;
    return jBaseAPIManager.requestPut(path, body);
  }
  /**
   * Request update tax Configuration.
   */
  static requestDeleteTaxConfiguration(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-configs/${body.id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  // End Configuration
  // Tax Status
  /**
   * Request get basic Tax Status.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxStatus(payload, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-status?limit=${payload.limit}&page=${payload.page}`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request save tax Status.
   */
  static requestSaveTaxStatus(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-status`;
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request update tax Status.
   */
  static requestUpdateTaxStatus(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-status/${body.id}`;
    return jBaseAPIManager.requestPut(path, body);
  }
  /**
   * Request delete tax Status.
   */
  static requestDeleteTaxStatus(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/tax-status/${body.id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  //  End Tax Status
  // Tax Cost
  static requestGetCostCenter() {
    const path = '/payroll/cost-centers?includes=tax_locations';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetCompanyRegion() {
    const path = '/payroll/company-regions?limit=100&page=1';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request get basic Tax Cost.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxCost(payload, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/cost-centers?limit=${payload.limit}&page=${payload.page}`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request save tax Cost.
   */
  static requestSaveTaxCost(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/cost-centers`;
    return jBaseAPIManager.requestPost(path, body);
  }
  /**
   * Request update tax Cost.
   */
  static requestUpdateTaxCost(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/cost-centers/${body.id}`;
    return jBaseAPIManager.requestPut(path, body);
  }
  /**
   * Request delete tax Cost.
   */
  static requestDeleteTaxCost(body, payrollID) {
    const path = `/payroll/payrolls/${payrollID}/cost-centers/${body.id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  // End Cost
  // Insurance
  static requestGetBenefitsInsurance(payrollID) {
    const path = `/payroll/payrolls/${payrollID}/benefits`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetBenefitsByInsuranceCode(payrollID, code) {
    const path = `/payroll/payrolls/${payrollID}/benefits?code=${code}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestSaveNoBPJS(body, benefitID) {
    const path = `/payroll/payroll-components/payroll_benefit/${benefitID}/numbers`;
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestSaveClassBPJS(body, payrollID, payrollBenefitID) {
    const path = `/payroll/payrolls/${payrollID}/benefits/${payrollBenefitID}`;
    return jBaseAPIManager.requestPost(path, body);
  }
  static createBenefits(payrollId, params) {
    const path = `/payroll/payrolls/${payrollId}/benefits`;
    return jBaseAPIManager.requestPost(path, params);
  }
  static updateBenefits(payrollId, params, benefitId) {
    const path = `/payroll/payrolls/${payrollId}/benefits/${benefitId}`;
    return jBaseAPIManager.requestPut(path, params);
  }
  static deleteBenefits(payrollId, payload, benefitId) {
    const path = `/payroll/payrolls/${payrollId}/benefits/${benefitId}`;
    return jBaseAPIManager.requestDelete(path, payload);
  }
  static requestGetMappingPayrollComponent(payrollID) {
    const path = `/payroll/payrolls/${payrollID}/components`;
    return jBaseAPIManager.requestGet(path);
  }
  static updateMappingPayrollComponent(payrollId, params) {
    const path = `/payroll/payrolls/${payrollId}/components`;
    return jBaseAPIManager.requestPut(path, params);
  }

  // End Insurance

  // normal
  static getListNormalStopPayment(userCompanyId) {
    const path = `/payroll/stop-payments/user-company/${userCompanyId}`;
    return jBaseAPIManager.requestGet(path);
  }
  static setNormalStopPayment(userCompanyId, params) {
    const path = `/payroll/stop-payments/company-user/${userCompanyId}`;
    return jBaseAPIManager.requestPut(path, params);
  }
  static createNormalStopPayment(userCompanyId, params) {
    const path = `/payroll/stop-payments/company-user/${userCompanyId}`;
    return jBaseAPIManager.requestPut(path, params);
  }
  // end normal

  // EXTENDS RUN PAYROLL
  static getExtendsRunPayroll(userCompanyId) {
    const path = `/payroll/run-payroll-extensions/company-user/${userCompanyId}`;
    return jBaseAPIManager.requestGet(path);
  }
  static setExtendsRunPayroll(userCompanyId, params) {
    const path = `/payroll/run-payroll-extensions/company-user/${userCompanyId}`;
    return jBaseAPIManager.requestPut(path, params);
  }

  /**
   * Request get basic payroll config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBasicPayrollConfig(onSuccess, onFailed) {
    const path = 'payroll/admin/payroll/basic';
    return jBaseAPIManager.requestGet(path, onSuccess, onFailed);
  }
  /**
   * Request update basic payroll config.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveBasicPayrollConfig(body, onSuccess, onFailed) {
    const path = 'payroll/admin/payroll/basic';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request get payroll prorate config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetPayrollProrateConfig(onSuccess, onFailed) {
    const path = 'payroll/admin/payroll/prorate';
    return jBaseAPIManager.requestGet(path, onSuccess, onFailed);
  }
  /**
   * Request update basic payroll config.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSavePayrollProrateConfig(body, onSuccess, onFailed) {
    const path = 'payroll/admin/payroll/prorate';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request get bonus config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBonusList(isTemporary) {
    let filter = '';
    if (typeof isTemporary !== 'undefined') {
      filter = `?filter[is_temporary]=${isTemporary}`;
    }
    const path = `payroll/bonus${filter}`;
    return jBaseAPIManager.requestGet(path);
  }
  static filterBonusList(params) {
    const path = `payroll/bonus?${params}`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request update bonus config.
   * @param {object} body Object to be send. See docs for detail on body object.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveBonus(body, onSuccess, onFailed) {
    const path = 'payroll/bonus';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /** Request get payroll component config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetPayrollComponentConfig(onSuccess, onFailed) {
    const path = 'payroll/admin/payroll/component';
    return jBaseAPIManager.requestGet(path, onSuccess, onFailed);
  }
  /** Request update payroll component config.
  * @param {object} body Object to be send. See docs for detail on body object.
  * @param {callback} onSuccess Callback function for handle successful request
  * @param {callback} onFailed Callback function for handle failed request
  */
  static requestSavePayrollComponentConfig(body, onSuccess, onFailed) {
    const path = 'payroll/admin/payroll/component';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request Get Deduction List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetDeductionList(isTemporary) {
    let filter = '';
    if (typeof isTemporary !== 'undefined') {
      filter = `?filter[is_temporary]=${isTemporary}`;
    }
    const path = `payroll/deduction${filter}`;
    return jBaseAPIManager.requestGet(path);
  }

  static filterDeductionList(params) {
    const path = `payroll/deduction?${params}`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Get Deduction List.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveDeduction(deduction) {
    const path = 'payroll/deduction';
    return jBaseAPIManager.requestPost(path, deduction);
  }
  /**
   * Request Import Employee.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestImport(body, onSuccess, onFailed) {
    const path = 'payroll/worker/import';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request create company.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestCreateCompany(body, onSuccess, onFailed) {
    const path = 'payroll/admin/company/create';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request import request.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestImportRequestList(body, onSuccess, onFailed) {
    const path = 'payroll/import/request-list';
    return jBaseAPIManager.requestPost(path, body, onSuccess, onFailed);
  }
  /**
   * Request Get Overtime Config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetOvertime() {
    const path = 'payroll/overtime';
    return jBaseAPIManager.requestGet(path);
  }
  static filterOvertime(params) {
    const path = `payroll/overtime?${params}`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Get JojoTimesOvertime Config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetJojoTimesOvertime(overtimeID) {
    const path = `payroll/overtimes/${overtimeID}/jojotimes-overtime-types`;
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Save JojoTimesOvertime Config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveJojoTimesOvertime(overtimeID, payload) {
    const path = `payroll/overtimes/${overtimeID}/jojotimes-overtime-types`;
    return jBaseAPIManager.requestPost(path, payload);
  }
  /**
   * Request Update JojoTimesOvertime Config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestUpdateJojoTimesOvertime(overtimeID, payload) {
    const path = `payroll/overtimes/${overtimeID}/jojotimes-overtime-types`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  /**
   * Request Save Overtime Rounding Minutes Config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveRoundingMinutes(overtimeID, payload) {
    const path = `payroll/overtimes/${overtimeID}/rounding-minutes`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  /**
   * Request Get Overtime Type Config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetOvertimeType() {
    const path = 'times/overtime/config/type/list-all?page=1&limit=1000&column=&ascending=&query=';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request Save Mapping Overtime & Custom Minutes Rounding
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveMappingOvertimeCustomMinutesRounding(overtimeID, payload) {
    const path = `payroll/overtimes/${overtimeID}/rounding-minutes`;
    return jBaseAPIManager.requestPost(path, payload);
  }
  /**
   * Request Update Mapping Overtime & Custom Minutes Rounding
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestUpdateMappingOvertimeCustomMinutesRounding(overtimeID,
    overtimeRoundingMinuteID, payload) {
    const path = `payroll/overtimes/${overtimeID}/rounding-minutes/${overtimeRoundingMinuteID}`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  /**
   * Request Get Mapping Overtime & Custom Minutes Rounding
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetMappingOvertimeCustomMinutesRounding(overtimeID,
    overtimeRoundingMinuteID, payload) {
    const path = `payroll/overtimes/${overtimeID}/rounding-minutes/${overtimeRoundingMinuteID}`;
    return jBaseAPIManager.requestGet(path, payload);
  }
  /**
   * Request Delete Mapping Overtime & Custom Minutes Rounding
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestDeleteMappingOvertimeCustomMinutesRounding(overtimeID,
    overtimeRoundingMinuteID) {
    const path = `payroll/overtimes/${overtimeID}/rounding-minutes/${overtimeRoundingMinuteID}`;
    return jBaseAPIManager.requestDelete(path);
  }
  /**
   * Request Save overtime config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveOvertime(overtime) {
    const path = 'payroll/overtime';
    return jBaseAPIManager.requestPost(path, overtime);
  }
  /**
   * Request initialize payroll acoount.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static initializePayrollAccount() {
    const path = 'payroll/admin/initialize';
    return jBaseAPIManager.requestPost(path);
  }
  /**
   * Request get benefit config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBenefitConfig(data) {
    const path = 'payroll/admin/payroll/benefit/config';
    return jBaseAPIManager.requestPost(path, data);
  }
  /**
   * Request get work environment risk level.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetWorkEnvironmentRiskLevel() {
    const path = 'payroll/benefit/risk-levels';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request save benefit config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveBenefitConfig(data) {
    const path = 'payroll/admin/payroll/benefit/config/save';
    return jBaseAPIManager.requestPost(path, data);
  }
  /**
   * Request get jp multiplier.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetJpMultiplierHistory() {
    const path = 'payroll/benefit/jp-multiplier-history';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request get benefit default config.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetBpjsDefaultConfig() {
    const path = 'payroll/benefit/bpjs-default-config';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetBpuDefaultConfig() {
    const path = 'payroll/benefit/bpjs-default-config?rule=bpu';
    return jBaseAPIManager.requestGet(path);
  }
  static requestEmployeeListWithPagination($pagination) {
    const path = 'payroll/employee/list-with-pagination';
    return jBaseAPIManager.requestPost(path, $pagination);
  }
  static requestGetBenefitConfigs(data) {
    const path = 'payroll/admin/payroll/benefit/config/list';
    return jBaseAPIManager.requestPost(path, data);
  }
  static requestGetBenefitConfigList(payload) {
    let path;
    if (payload.filter && payload.query) {
      path = `payroll/benefit-configs?page=${payload.page}&limit=${payload.limit}&includes=benefit_config_employment_types&filter[${payload.filter}]=${payload.query}`;
    } else {
      path = `payroll/benefit-configs?page=${payload.page}&limit=${payload.limit}&includes=benefit_config_employment_types`;
    }
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetBenefitConfigDetail(id) {
    const path = `payroll/benefit-configs/${id}?includes=benefit.minimum_salary,benefit,benefit_multiplier,bpjs_benefit_config,bpjskk_config,bpjs_configs.bpjs_default_config,benefit_config_employment_types`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreateBenefit(payload) {
    const path = 'payroll/benefit-configs';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestUpdateBenefit(id, payload) {
    const path = `payroll/benefit-configs/${id}`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestDeleteBenefit(id) {
    const path = `payroll/benefit-configs/${id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestSaveEmploymentType(id, payload) {
    const path = `payroll/benefit-configs/${id}/employment-types`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestSaveBenefitMultiplier(id, payload) {
    const path = `payroll/benefit-configs/${id}/benefit-multipliers`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestSaveBpjsConfigs(id, payload) {
    const path = `payroll/benefit-configs/${id}/bpjs-configs`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestSaveBpjsBenefitConfig(id, payload) {
    const path = `payroll/benefit-configs/${id}/bpjs-benefit-configs`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestSaveBpjskkConfig(id, payload) {
    const path = `payroll/benefit-configs/${id}/bpjskk-configs`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestSaveMinimumSalary(id, payload) {
    const path = `payroll/benefit-configs/${id}/minimum-salaries`;
    return jBaseAPIManager.requestPut(path, payload);
  }
  /**
   * Request get tax calculation option.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestGetTaxCalculationOption() {
    const path = 'payroll/admin/payroll/tax-calculation-config';
    return jBaseAPIManager.requestGet(path);
  }
  /**
   * Request save tax calculation option.
   * @param {callback} onSuccess Callback function for handle successful request
   * @param {callback} onFailed Callback function for handle failed request
   */
  static requestSaveTaxCalculationOption(body) {
    const path = 'payroll/admin/payroll/tax-calculation-config/save';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestCreateSeverancePay(body) {
    const path = 'payroll/severance-pay';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetSeverancePay() {
    const path = 'payroll/severance-pay';
    return jBaseAPIManager.requestGet(path);
  }
  static requestUpdateSeverancePay(body) {
    const path = 'payroll/severance-pay';
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestCreateSeverancePayPolicy(body) {
    const path = 'payroll/severance-pay/policy';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetSeverancePayPolicy(id) {
    const path = `payroll/severance-pay/${id}/policy`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetSeverancePayPolicies(id) {
    const path = `payroll/severance-pay/${id}/policies`;
    return jBaseAPIManager.requestGet(path);
  }
  static filterSeverancePayPolicies(req) {
    const path = `payroll/severance-pay/${req.id}/policies?${req.params}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetSeverancePayPoliciesDetail(idSeverance, idPolicy) {
    const path = `payroll/severance-pay/${idSeverance}/policies/${idPolicy}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestUpdateSeverancePayPolicy(body) {
    const path = 'payroll/severance-pay/policies';
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeleteSeverancePayPolicy(idSeverance, idPolicy) {
    const path = `payroll/severance-pay/${idSeverance}/policies/${idPolicy}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestCreateSeverancePaySource(body) {
    const path = 'payroll/severance-pay-policy/source';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetSeverancePaySource(id) {
    const path = `payroll/severance-pay-policy/${id}/source`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestUpdateSeverancePaySource(body) {
    const path = 'payroll/severance-pay-policy/source';
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeleteSeverancePaySource(idPolicy, idSource) {
    const path = `payroll/severance-pay-policy/${idPolicy}/source/${idSource}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestCreateSeverancePayFormula(body) {
    const path = 'payroll/severance-pay-policy/formula';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetSeverancePayFormula(id) {
    const path = `payroll/severance-pay-policy/${id}/formulas`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestUpdateSeverancePayFormula(body) {
    const path = 'payroll/severance-pay-policy/formula';
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeleteSeverancePayFormula(idPolicy, idFormula) {
    const path = `payroll/severance-pay-policy/${idPolicy}/formulas/${idFormula}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetSeverancePayList() {
    const path = 'payroll/severance-pay/policies';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetSeverancePayMapping(id) {
    const path = `payroll/severance-pay/payroll/${id}/severance-pay-policy`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreateSeverancePayMapping(body) {
    const path = 'payroll/severance-pay/payroll/severance-pay-policy';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestUpdateSeverancePayMapping(body) {
    const path = 'payroll/severance-pay/payroll/severance-pay-policy';
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeleteSeverancePayMapping(idPayroll, idPolicy) {
    const path = `payroll/severance-pay/payroll/${idPayroll}/severance-pay-policy/${idPolicy}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetMultiCycleConfig() {
    const path = 'payroll/company-config';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayrollCycleList(payload, type = null) {
    const path = `payroll/payroll-config?limit=${payload.limit}&page=${payload.page}${type === null ? '' : `&type=${type}`}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayrollCycleDetail(id) {
    const path = `payroll/payroll-config/${id}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestSavePayrollCycle(payload) {
    const path = 'payroll/payroll-config';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestSavePayrollCycleEmploymentTypes(payload) {
    const path = 'payroll/payroll-config/employment-type';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestSavePayrollCyclePair(payload) {
    const path = 'payroll/payroll-config/cycle-pair';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestDeletePayrollCycle(id) {
    const path = `payroll/payroll-config/${id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetPayrollCyclePair(id) {
    const path = `payroll/payroll-config/${id}/cycle-pair`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestMappingPayrollAreaToPayrollConfig(payload) {
    const path = 'payroll/payroll-area/mapping/payroll-config';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestMappingPayrollAreaStatus(payload) {
    const path = 'payroll/payroll-config/status/mapping';
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestGetPayrollCycleMappingList(payload) {
    const path = `payroll/payroll-config/component-cycle?limit=${payload.limit}&page=${payload.page}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayrollCycleMappingDetail(id) {
    const path = `payroll/payroll-config/${id}/component-cycle`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestCreatePayrollCycleMapping(payload) {
    const path = 'payroll/payroll-config/component-cycle';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestUpdatePayrollCycleMapping(payload) {
    const body = {
      components: payload.components,
      payroll_config_id: payload.payroll_config_id,
    };
    const path = `payroll/payroll-config/component-cycle/${payload.payroll_config.id}`;
    return jBaseAPIManager.requestPut(path, body);
  }
  static requestDeletePayrollCycleMapping(configId) {
    const path = `payroll/payroll-config/${configId}/component-cycle`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetPayrollProrateList() {
    const path = 'payroll/payroll-config/prorate';
    return jBaseAPIManager.requestGet(path);
  }
  static requestSavePayrollProrate(payload) {
    const path = 'payroll/payroll-config/prorate';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestCreatePayrollProrateCycle(payload) {
    const path = 'payroll/payroll-config/prorate/mapping';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestUpdatePayrollProrateCycle(payload) {
    const path = 'payroll/payroll-config/prorate/mapping';
    return jBaseAPIManager.requestPut(path, payload);
  }
  static requestDeletePayrollProrate(id) {
    const path = `payroll/payroll-config/prorate/${id}`;
    return jBaseAPIManager.requestDelete(path);
  }

  static requestGetReimbursement(body) {
    let filter = '';
    if (body.query_type) {
      filter = `&filter[${body.query_type}]=${body.query}`;
    }
    const path = `payroll/reimbursement-categories?limit=${body.limit}&page=${body.page}${filter}`;
    return jBaseAPIManager.requestGet(path);
  }

  static requestGetReimbursementCategoryList(params) {
    const path = `payroll/reimbursement-categories?limit=${params.limit}&page=${params.page}`;
    return jBaseAPIManager.requestGet(path);
  }

  static requestCreateReimbursement(payload) {
    const path = 'payroll/reimbursement-categories';
    return jBaseAPIManager.requestPost(path, payload);
  }

  static requestUpdateReimbursement(payload) {
    const path = `payroll/reimbursement-categories/${payload.id}`;
    return jBaseAPIManager.requestPut(path, payload);
  }

  static requestDeleteReimbursement(payload) {
    const path = `payroll/reimbursement-categories/${payload.id}`;
    return jBaseAPIManager.requestDelete(path, payload);
  }


  static requestGetReimbursementInfo(path, body) {
    return jBaseAPIManager.requestGet(path, body);
  }

  static requestCreateReimbursementInfo(payload) {
    const path = 'payroll/reimbursements';
    return jBaseAPIManager.requestPost(path, payload);
  }

  static requestUpdateReimbursementInfo(payload) {
    const path = `payroll/reimbursements/${payload.id}`;
    return jBaseAPIManager.requestPut(path, payload);
  }

  static requestDeleteReimbursementInfo(payload) {
    const path = `payroll/reimbursements/${payload.id}`;
    return jBaseAPIManager.requestDelete(path);
  }

  static requestGetReimbursementPayment(path, body) {
    return jBaseAPIManager.requestGet(path, body);
  }

  static requestCreateReimbursementPayment(payload, reimbursementID) {
    const path = `payroll/reimbursements/${reimbursementID}/payments`;
    return jBaseAPIManager.requestPost(path, payload);
  }

  static requestUpdateReimbursementPayment(payload, reimbursementID) {
    const path = `payroll/reimbursements/${reimbursementID}/payments/${payload.id}`;
    return jBaseAPIManager.requestPut(path, payload);
  }

  static requestDeleteReimbursementPayment(payload, reimbursementID) {
    const path = `payroll/reimbursements/${reimbursementID}/payments/${payload.id}`;
    return jBaseAPIManager.requestDelete(path, payload);
  }
}
