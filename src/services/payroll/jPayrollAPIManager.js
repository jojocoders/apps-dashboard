import jBaseAPIManager from './jBaseAPIManager';

export default class jPayrollAPIManager extends jBaseAPIManager {
  static requestGetMyPayslip(year, bonusId) {
    const path = 'payroll/payslip/summary/my';
    const body = { year, bonus_id: bonusId };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayslip(path, month, year, limit, page) {
    const body = {
      month,
      year,
      pagination: {
        limit,
        page,
      },
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetRunPayrollRequest() {
    const path = 'payroll/worker/run-payroll/request';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetExportRequest(type) {
    const body = { type };
    const path = 'payroll/worker/export/request';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayrollLogs() {
    const path = 'payroll/run-payroll-request';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayslipLogs(data) {
    const path = `payroll/export-request-logs?page=${data.page}&limit=${data.limit}&type=${data.type}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestSaveRunPayrollRequest(id, month, year) {
    const path = 'payroll/worker/run-payroll/request';
    const body = {
      id,
      month,
      year,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestCheckAvailabilityGeneratePayroll(month, year, period, employeeIds = null) {
    const path = 'payroll/run-payroll-request/availability-check';
    const body = {
      month,
      year,
      payroll_config_id: period,
    };
    if (employeeIds !== null) {
      const employees = [];
      employeeIds.forEach((element) => {
        employees.push(`${element}`);
      });
      body.employee_ids = employees;
    }
    return jBaseAPIManager.requestPost(path, body);
  }
  // eslint-disable-next-line max-len
  static requestGeneratePayroll(month, year, period, id = null, employeeIds = [], processDate = null) {
    const path = 'payroll/run-payroll-request';
    const body = {
      month,
      year,
      payroll_config_id: period,
      process_date: processDate,
    };
    if (id !== null) {
      body.id = id;
    }
    if (employeeIds !== null) {
      const employees = [];
      employeeIds.forEach((element) => {
        employees.push(`${element}`);
      });
      body.employee_ids = employees;
    }
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetLatestPayrollConfig() {
    const path = 'payroll/payroll-config/latest';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayrollConfig() {
    const path = 'payroll/payroll-config';
    return jBaseAPIManager.requestGet(path);
  }
  static requestUpdateLockPayslip(month, year, period, isLocked, employeeIds) {
    const path = 'payroll/v2/payslip/update-lock';
    const body = {
      month,
      year,
      payroll_config_id: period,
      is_locked: isLocked,
      employee_ids: employeeIds,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestExportPayrollRequest(id, month, year, type, payslips, bonusId,
    isSelectedAll = null, unSelectedValues = []) {
    const monthAndYear = type === 3 ? '?filter[month]=0&filter[year]=0' : '';
    const path = `payroll/worker/export${monthAndYear}`;
    const body = {
      id,
      month,
      year,
      type,
      payslips,
      bonus_id: bonusId,
    };

    if (isSelectedAll !== null) {
      body.is_selected_all = isSelectedAll;
    }

    if (isSelectedAll !== null && isSelectedAll === true) {
      body.unselected_values = unSelectedValues;
    }

    return jBaseAPIManager.requestPost(path, body);
  }

  // For Export Payslip (not payroll) Bulk or Email
  static requestExportPayslipRequest(id, month, year, type, payslips, bonusId,
    isSelectedAll = null, unSelectedValues = [], exportConfig) {
    const path = `payroll/worker/export?filter[month]=${month}&filter[year]=${year}`;
    const body = {
      id,
      month,
      year,
      type,
      payslips,
      bonus_id: bonusId,
      unselected_values: unSelectedValues,
      is_selected_all: isSelectedAll,
      export_config: exportConfig,
    };

    if (isSelectedAll !== null) {
      body.is_selected_all = isSelectedAll;
    }

    if (isSelectedAll !== null && isSelectedAll === true) {
      body.unselected_values = unSelectedValues;
    }

    return jBaseAPIManager.requestPost(path, body);
  }
  // For Retry Export Payslip (not payroll) Bulk download
  static requestRetryBulkExportPayslip(id) {
    const path = 'payroll/worker/export';
    const body = {
      id,
      status: 1,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  // For Retry Export Payslip (not payroll) to Email
  static requestRetryEmailExportPayslip(id) {
    const path = 'payroll/worker/export';
    const body = {
      id,
      status: 1,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  // Download Template
  static getPayslipTemplate() {
    const path = 'payroll/admin/download/payslip-format';
    return jBaseAPIManager.requestGet(path);
  }
  // Download Template
  static importPayslip(body) {
    const path = 'payroll/worker/import';
    return jBaseAPIManager.requestPost(path, body);
  }

  static requestExportPayslipStaff(id, month, year, type, payslips, bonusId,
    isSelectedAll = null, unSelectedValues = []) {
    const path = 'payroll/worker/export-staff-only';
    const body = {
      id,
      month,
      year,
      type,
      payslips,
      bonus_id: bonusId,
    };

    if (isSelectedAll !== null) {
      body.is_selected_all = isSelectedAll;
    }

    if (isSelectedAll !== null && isSelectedAll === true) {
      body.unselected_values = unSelectedValues;
    }

    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetLocationList() {
    const path = '/company/setup/location/list';
    const body = {
      pagination: {
        limit: 5,
        page: 1,
        column: '',
        ascending: false,
        query: '',
        query_type: 'id',
      },
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayslipDetail(id) {
    const path = 'payroll/v2/payslip/detail';
    const body = { id };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayslipDetailMultiCycle(id) {
    const path = 'payroll/v2/payslip/detail';
    const body = { id };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetTempPayslipDetail(payrollId, month, year) {
    const path = 'payroll/admin/payroll/temporary-component/detail';
    const body = { payroll_id: payrollId, month, year };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestUpdatePayslip(ids, month, year, isPaid) {
    const path = 'payroll/payslip/update';
    const body = { ids, month, year, is_paid: isPaid };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestBenefitHistory(data) {
    const body = {
      month: data.month,
      year: data.year,
    };
    const path = 'payroll/dashboard/benefit-history';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestEmployeeBenefitHistory(data) {
    const body = {
      month: data.month,
      year: data.year,
    };
    const path = 'payroll/dashboard/employee-benefit-history';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestPayrollSummary(data) {
    const body = {
      month: data.month,
      year: data.year,
    };
    const path = 'payroll/dashboard/payroll-summary';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetBpjsHistory(data) {
    const body = {
      month: data.month,
      year: data.year,
    };

    const path = data.url;
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetEmployeesTax(data) {
    const body = {
      month: data.selectedMonth,
      year: data.selectedYear,
      limit: data.limit,
      page: data.page,
    };
    const path = 'payroll/admin/payroll/employees/tax';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetEmployees1721A1History(data) {
    const path = `payroll/withholding-tax-slips?page=${data.page}&limit=${data.limit}${data.selectedYear ? '&filter[year]=' : ''}${data.selectedYear ? data.selectedYear : ''}${data.user_company_id ? '&filter[user_company_id]=' : ''}${data.user_company_id ? data.user_company_id : ''}&includes=user`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestDelete1721A1Data(id) {
    const path = `payroll/withholding-tax-slips/${id}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestGetDefaultExportColumn(type) {
    const path = 'payroll/admin/export/default-columns';
    const body = { type };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetExportColumnPreference(type) {
    const path = 'payroll/admin/export/column-preference';
    const body = { type };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestSaveExportColumnPreference(type, exportColumns, reset) {
    const path = 'payroll/admin/export/column-preference/save';
    let body;
    if (reset) {
      body = { type, export_columns: exportColumns, reset };
    } else {
      body = { type, export_columns: exportColumns };
    }
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetImportPayslipFormat() {
    const path = 'payroll/admin/download/payslip-format';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImport1721A1Format() {
    const path = 'payroll/import-template/1721A1';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImportRunPayrollEmployeeFormat() {
    const path = 'payroll/import-template/run-payroll-employee';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImportRecurringFormat() {
    const path = 'payroll/admin/download/recurring-format';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImportEmployee() {
    const path = 'payroll/admin/download/employee-format';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImportMedicalReimbursementFormat(type) {
    const path = `payroll/import-template/${type}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImportPayrollComponentFormat(payload) {
    const path = 'payroll/import-template/Employee-Payroll-Component';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestGetImportAttendanceFormat(form) {
    const body = {
      year: form.selectedYear,
      month: form.selectedMonth,
      attendance_type: form.attendanceType,
    };
    const path = 'payroll/admin/download/attendance-format';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetImportOvertimeFormat(form) {
    const body = {
      year: form.selectedYear,
      month: form.selectedMonth,
    };
    const path = 'payroll/admin/download/overtime-format';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestCheckEmployeeId(data) {
    const path = 'payroll/run-payroll-request/check-employee';
    return jBaseAPIManager.requestPost(path, data);
  }
  static requestPrintPayslip(data) {
    const path = 'payroll/payslip/print';
    const body = data;
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetImportEmployeeFormat() {
    const path = 'payroll/admin/download/employee-format';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetImportTemporaryComponentFormat(payload) {
    const path = 'payroll/import-template/Temporary-Component';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestFetchManualAttendance() {
    const path = 'payroll/admin/manual-attendance/fetch';
    return jBaseAPIManager.requestGet(path);
  }
  static requestSaveManualAttendance(data) {
    const path = 'payroll/admin/manual-attendance/save';
    const body = {
      id: data.id,
      name: data.name,
      type: data.type,
      code: data.code,
      is_delete: data.is_delete,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetLoadPayroll(month, year, name) {
    const path = 'payroll/admin/payroll/history/list';
    const body = { month, year, search_name: name };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestSaveLoadPayroll(id, name, description, isDelete, runPayrollRequestId,
    month, year, searchName) {
    const path = 'payroll/admin/payroll/history/save';
    const body = {
      id,
      name,
      description,
      is_delete: isDelete,
      run_payroll_request_id: runPayrollRequestId,
      search_name: searchName,
      month,
      year,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestLoadPayroll(id) {
    const path = 'payroll/admin/payroll/history/load';
    const body = { id };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetSalaryChart(chartType, chartId) {
    const path = 'payroll/admin/payroll/salary-chart';
    const body = { chart_type: chartType, chart_id: chartId };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestSaveSalaryChart(chartType, chartId, name, description, salary) {
    const path = 'payroll/admin/payroll/salary-chart/save';
    const body = { chart_type: chartType, chart_id: chartId, name, description, salary };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGenerateSPTTahunan(body) {
    const path = 'payroll/spt-tahunan';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestValidateSPTTahunan(year) {
    const path = `payroll/spt-tahunan/validate?year=${year}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetPayrollSummaryDetail(month, year) {
    const body = { month, year };
    const path = 'payroll/dashboard/payroll-summary-detail';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetBenefitSummaryDetail(month, year) {
    const body = { month, year };
    const path = 'payroll/dashboard/benefit-summary-detail';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestUpdateLock(ids, month, year, isLocked) {
    const path = 'payroll/payslip/update-lock';
    const body = { ids, month, year, is_locked: isLocked };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestFetchLocked(path, month, year) {
    const body = { month, year };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayslipByYear(data) {
    const body = {
      year: data.year,
      user_company_id: data.user_company_id,
    };
    const path = 'payroll/payslip/summary-by-year';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayslipByYearForBonus(data) {
    const body = {
      year: data.year,
      user_company_id: data.user_company_id,
      bonus_id: data.bonus_id,
    };
    const path = 'payroll/payslip/summary-by-year';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGenerateTemporaryComponent(payload) {
    const path = 'payroll/worker/export';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestGetTemporaryComponentList(data) {
    const path = '/payroll/temporary-components';
    return jBaseAPIManager.requestGet(path, false, { params: data });
  }
  static requestUpdateTemporaryComponent(formData) {
    const path = `/payroll/payroll-temporary-components/${formData.payrollTemporaryComponentId}/temporary-components/${formData.temporaryId}`;
    return jBaseAPIManager.requestPut(path, formData);
  }
  static requestDeleteTemporaryComponent(formData) {
    const path = `/payroll/payroll-temporary-components/${formData.payrollTemporaryComponentId}/temporary-components/${formData.temporaryId}`;
    return jBaseAPIManager.requestDelete(path);
  }
  static requestResetTemporaryComponentData(data) {
    const path = '/payroll/admin/payroll/temporary-component/reset';
    return jBaseAPIManager.requestPost(path, data);
  }
  static requestPayrollAvailabilityCheck(month, year) {
    const path = '/payroll/generate-payroll/availability-check';
    const body = {
      month,
      year,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetPayrollReport(filter) {
    const path = `payroll/payslip-report?${filter}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestExportPayrollReport(payload, type) {
    const path = `payroll/worker/export?${payload.filter}`;
    const body = {
      month: payload.month,
      year: payload.year,
      type,
    };
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetUserList(payload) {
    const path = 'user/employee/list-with-pagination';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestGetRecurringList(payload) {
    const body = {
      pagination: {
        page: payload.page,
        limit: payload.limit,
        query: {
          email: payload.employee_email ? payload.employee_email : '',
          component_type: payload.component ? payload.component : '',
          component_id: payload.component_id ? payload.component_id : '',
          effective_begin: payload.begin_date ? payload.begin_date : '',
          effective_end: payload.end_date ? payload.end_date : '',
        },
      },
    };
    const path = 'payroll/admin/payroll-recurring/list';
    return jBaseAPIManager.requestPost(path, body);
  }
  static requestGetRecurringDetail(payload) {
    const path = 'payroll/admin/payroll-recurring/recurring-by-id';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestSaveRecurring(payload) {
    const path = 'payroll/admin/payroll-recurring/save';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestDeleteRecurring(payload) {
    const path = 'payroll/admin/payroll-recurring/recurring-delete';
    return jBaseAPIManager.requestPost(path, payload);
  }
}
