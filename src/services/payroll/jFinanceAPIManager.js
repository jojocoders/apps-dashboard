import jBaseAPIManager from './jBaseAPIManager';

export default class jLoanAPIManager extends jBaseAPIManager {
  static requestGetPaymentMethodList() {
    const path = 'payroll/finance/payment-method/list';
    return jBaseAPIManager.requestGet(path);
  }
  static requestGetTransferList(params) {
    const path = `payroll/finance/transfer/list?page=${params.pagination.page}&limit=${params.pagination.limit}&filter[month]=${params.month}&filter[year]=${params.year}${params.bank ? '&filter[finance_report]=' : ''}${params.bank || ''}${params.periode ? '&filter[payroll_config_id]=' : ''}${params.periode || ''}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestExportPayrollRequest({ id, month, year, bank, type, payrollConfigId }) {
    const path = `payroll/worker/export?filter[month]=${month}&filter[year]=${year}&filter[payroll_config_id]=${payrollConfigId}${bank ? '&filter[finance_report]=' : ''}${bank || ''}`;
    const body = {
      id,
      month,
      year,
      type,
    };

    return jBaseAPIManager.requestPost(path, body);
  }
  static validateUserBeneficiary(params) {
    const path = 'payroll/finance/validate-user-beneficiary';
    return jBaseAPIManager.requestPost(path, params);
  }
  static setupUserBeneficiary(params) {
    const path = 'payroll/finance/setup-user-beneficiary';
    return jBaseAPIManager.requestPost(path, params);
  }
  static requestSaveTransaction(params) {
    const path = 'payroll/finance/save/transaction';
    return jBaseAPIManager.requestPost(path, params);
  }
  static requestOTP(params) {
    const path = 'pro-api/otp/request';
    return jBaseAPIManager.requestPost(path, params);
  }
  static checkOTP(params) {
    const path = 'pro-api/otp/check';
    return jBaseAPIManager.requestPost(path, params);
  }
  static requestGetTotalThp(params) {
    const path = 'payroll/finance/total-takehomepay';
    return jBaseAPIManager.requestPost(path, params);
  }
  static requestCheckFinanceConfig(bank) {
    const path = `payroll/finance/config/bank/${bank}`;
    return jBaseAPIManager.requestGet(path);
  }
  static requestSaveFinanceConfig(params) {
    const path = 'payroll/finance/config';
    return jBaseAPIManager.requestPost(path, params);
  }
  static requestExportFinance(payload) {
    const path = 'payroll/worker/export';
    return jBaseAPIManager.requestPost(path, payload);
  }
  static requestUpdatePayslipStatus(payload) {
    const path = 'payroll/payslip/update';
    return jBaseAPIManager.requestPost(path, payload);
  }
}
