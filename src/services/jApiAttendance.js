import jApi from './jApi';

export default {
  // ATTENDANCE MY LOG & MONITOR
  getProjectList(req) {
    const api = jApi.generateApi();
    return api.post('/project/list', req)
      .then(res => res.data);
  },
  getAttendances(req) {
    const api = jApi.generateApi();
    return api.get(`times/attendances-service/api/v2/attendances?limit=${req.limit}&page=${req.page}&column=${req.column}&ascending=${req.ascending}&query=${req.query}&filter[start_date]=${req.start_date}&filter[end_date]=${req.end_date}&filter[project_ids]=${req.project_ids}`)
      .then(res => res);
  },
  getDetailAttendance(req) {
    const api = jApi.generateApi();
    return api.get(`times/attendances-service/api/v2/attendances/${req}`)
      .then(res => res.data);
  },
  getDetailGroupAttendance(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance/detail-by-group', params)
      .then(res => res.data);
  },
  getMonitorAttended(req) {
    const api = jApi.generateApi();
    return api.get(`times/attendances-service/api/v2/monitors/attendancesV2?limit=${req.limit}&page=${req.page}&column=${req.column}&ascending=${req.ascending}&query=${req.query}&query_type=${req.query_type}&filter[start_date]=${req.start_date}&filter[end_date]=${req.end_date}`)
      .then(res => res);
  },
  getMonitorAbsence(req) {
    const api = jApi.generateApi();
    return api.get(`times/attendances-service/api/v2/monitors/absencesV2?limit=${req.limit}&page=${req.page}&column=${req.column}&ascending=${req.ascending}&query=${req.query}&query_type=${req.query_type}&filter[start_date]=${req.start_date}&filter[end_date]=${req.end_date}`)
      .then(res => res);
  },
  getMonitorLeave(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/monitorsV3?status=approved&limit=${req.limit}&page=${req.page}&column=${req.column}&ascending=${req.ascending}&query=${req.query}&query_type=${req.query_type}&start_date=${req.start_date}&end_date=${req.end_date}`)
      .then(res => res);
  },
  getDetailFraudAttendance(params) {
    const api = jApi.generateApi();
    return api.post('app/api/v2/pro/attendances/fraud/v2', params)
      .then(res => res);
  },

  // ATTENDANCE REPORT
  getShift(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/config/list', params)
      .then(res => res);
  },
  getTeam(params) {
    const api = jApi.generateApi();
    return api.post('company/setup/team/list', params)
      .then(res => res);
  },
  getEmployee(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/list/filter-status', params)
      .then(res => res);
  },
  getReportAttendanceList(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance-report/summary/list', params)
      .then(res => res);
  },
  getReportAttendanceDetailList(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance/list-by-date', params)
      .then(res => res);
  },
  getReportAttendanceSummaryDetail(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance-report/detail/show-all-date', params)
      .then(res => res);
  },
  generateSummaryXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/attendance/report/summary/export', req)
      .then(res => res);
  },
  generateDetailXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/attendance-report/detail/export', req)
      .then(res => res);
  },

  // ATTENDANCE CONFIG
  getRoleSetting() {
    const api = jApi.generateApi();
    return api.get('times/company-setting/roles-setting')
      .then(res => res.data);
  },
  // code
  getAttendanceCodeList(companyId, params) {
    const api = jApi.generateApi();
    return api.get(`times/attendance-code/${companyId}`, { params })
      .then(res => res);
  },
  getAttendanceCodeDetail(companyId, id) {
    const api = jApi.generateApi();
    return api.get(`times/attendance-code/${companyId}/${id}`)
      .then(res => res);
  },
  setAttendanceCode(companyId, params) {
    const api = jApi.generateApi();
    return api.post(`times/attendance-code/${companyId}`, params)
      .then(res => res.data);
  },
  deleteAttendanceCode(id) {
    const api = jApi.generateApi();
    return api.delete(`times/attendance-code/${id}`)
      .then(res => res.data);
  },
  // reminder
  getReminderList(params) {
    const api = jApi.generateApi();
    return api.post('announcement/config/list', params)
      .then(res => res);
  },
  getReminderDetail(id) {
    const api = jApi.generateApi();
    return api.post('announcement/config/detail', id)
      .then(res => res.data);
  },
  createReminder(params) {
    const api = jApi.generateApi();
    return api.post('announcement/config/create', params)
      .then(res => res.data);
  },
  updateReminder(params) {
    const api = jApi.generateApi();
    return api.post('announcement/config/update', params)
      .then(res => res.data);
  },
  deleteReminder(id) {
    const api = jApi.generateApi();
    return api.post('announcement/config/delete', id)
      .then(res => res.data);
  },
  // holiday
  getHolidayList(params) {
    const api = jApi.generateApi();
    return api.post('times/holiday/config/list', params)
      .then(res => res);
  },
  getHolidayDetail(id) {
    const api = jApi.generateApi();
    return api.post('times/holiday/config/detail', id)
      .then(res => res.data);
  },
  createHoliday(params) {
    const api = jApi.generateApi();
    return api.post('times/holiday/config/create', params)
      .then(res => res.data);
  },
  updateHoliday(params) {
    const api = jApi.generateApi();
    return api.post('times/holiday/config/update', params)
      .then(res => res.data);
  },
  deleteHoliday(id) {
    const api = jApi.generateApi();
    return api.post('times/holiday/config/delete', id)
      .then(res => res.data);
  },
  exportHoliday() {
    const api = jApi.generateApi();
    return api.get('times/holiday/config/export')
      .then(res => res.data);
  },
  // shift
  getShiftList(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/config/list', params)
      .then(res => res);
  },
  getShiftDetail(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/config/detail', params)
      .then(res => res.data);
  },
  createShift(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/config/create', params)
      .then(res => res.data);
  },
  updateShift(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/config/update', params)
      .then(res => res.data);
  },
  deleteShift(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/config/delete', params)
      .then(res => res.data);
  },
  exportShift() {
    const api = jApi.generateApi();
    return api.get('times/shift/config/export')
      .then(res => res.data);
  },
  // mapping shift
  getMappingShiftList(params) {
    const api = jApi.generateApi();
    // return api.post('times/shift/config/mapping-shift', params)
    return api.post('times/shift/config/mapping-shift-organigram', params)
      .then(res => res);
  },
  getMappingShiftTemplate() {
    const api = jApi.generateApi();
    return api.get('times/shift/importV2/download/template?template_type=mapping-shift-v2-employee-id&month=12&year=2020')
      .then(res => res.data);
  },
  importMappingShift(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/importV2/import', params)
      .then(res => res.data);
  },
  getDailyShift(params) {
    const api = jApi.generateApi();
    return api.post('times/shift/calendar/list-batch', params)
      .then(res => res.data);
  },
  createDailyShift(params) {
    const api = jApi.generateApi();
    return api.post('/times/shift/config/daily-shift/createV2', params)
      .then(res => res.data);
  },
  updateDailyShift(params) {
    const api = jApi.generateApi();
    return api.post('/times/shift/config/daily-shift/update', params)
      .then(res => res.data);
  },
  reassignDailyShift(params) {
    const api = jApi.generateApi();
    return api.post('/times/shift/config/daily-shift/reassign', params)
      .then(res => res.data);
  },
  deleteDailyShift(params) {
    const api = jApi.generateApi();
    return api.post('/times/shift/config/daily-shift/delete', params)
      .then(res => res.data);
  },
  importDailyShift(params) {
    const api = jApi.generateApi();
    return api.post('/times/shift/config/daily-shift/import', params)
      .then(res => res.data);
  },
  // venue
  getVenueList(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance-venue/config/list', params)
      .then(res => res.data);
  },
  getVenueDetail(id) {
    const api = jApi.generateApi();
    return api.post('times/attendance-venue/config/detail', id)
      .then(res => res.data);
  },
  createVenue(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance-venue/config/create', params)
      .then(res => res.data);
  },
  updateVenue(params) {
    const api = jApi.generateApi();
    return api.post('times/attendance-venue/config/update', params)
      .then(res => res.data);
  },
  deleteVenue(id) {
    const api = jApi.generateApi();
    return api.post('times/attendance-venue/config/delete', id)
      .then(res => res.data);
  },
  exportVenue() {
    const api = jApi.generateApi();
    return api.get('times/attendance-venue/config/export')
      .then(res => res.data);
  },
  importVenue(params) {
    const api = jApi.generateApi();
    return api.post('/times/attendance-venue/config/import', params)
      .then(res => res.data);
  },
  // exceed notification
  getExceedList(params) {
    const api = jApi.generateApiMock();
    return api.get('times/attendance-config/exceed-notification/list', { params })
      .then(res => res);
  },
  getExceedDetail(companyId, id) {
    const api = jApi.generateApi();
    return api.get(`times/attendance-config/exceed-notification/${companyId}/${id}`)
      .then(res => res);
  },
  setExceed(companyId, params) {
    const api = jApi.generateApi();
    return api.post(`times/attendance-config/exceed-notification/${companyId}`, params)
      .then(res => res.data);
  },
  deleteExceed(id) {
    const api = jApi.generateApi();
    return api.delete(`times/attendance-config/exceed-notification/${id}`)
      .then(res => res.data);
  },
  // automatic attendance
  getAutoAttendanceList(params) {
    const api = jApi.generateApi();
    return api.post('times/automatic-attendance/config/list', params)
      .then(res => res);
  },
  getAutoAttendanceDetail(companyId, id) {
    const api = jApi.generateApi();
    return api.get(`times/attendance-config/automatic-attendance/${companyId}/${id}`)
      .then(res => res);
  },
  setUpdateAutoAttendance(params) {
    const api = jApi.generateApi();
    return api.post('times/automatic-attendance/config/update ', params)
      .then(res => res.data);
  },
  setAutoAttendance(params) {
    const api = jApi.generateApi();
    return api.post('times/automatic-attendance/config/create', params)
      .then(res => res.data);
  },
  deleteAutoAttendance(id) {
    const api = jApi.generateApi();
    return api.delete(`times/attendance-config/automatic-attendance/${id}`)
      .then(res => res.data);
  },

  searchPlaceByAddress(params) {
    const api = jApi.generateApiGoogleMaps();
    return api.get(`maps/api/geocode/json?address=${params}&key=${process.env.GMAPS_KEY}`)
      .then(res => res);
  },
  searchPlaceByLatLong(lat, lng) {
    const api = jApi.generateApiGoogleMaps();
    return api.get(`maps/api/geocode/json?latlng=${lat},${lng}&key=${process.env.GMAPS_KEY}`)
      .then(res => res);
  },

  // manager dashboard
  getAttendanceToday() {
    const api = jApi.generateApi();
    return api.get('times/dashboard/summary/chartV2')
      .then(res => res.data);
  },
  getAttendanceFivedays() {
    const api = jApi.generateApi();
    return api.get('times/dashboard/summary/chart-backdate')
      .then(res => res.data);
  },
  getAttendanceTendays() {
    const api = jApi.generateApiMock();
    return api.get('times/attendances-service/list/tendays')
      .then(res => res);
  },
  getManagerDashboardTable(params) {
    const api = jApi.generateApi();
    return api.post('times/dashboard/manager', params)
      .then(res => res);
  },
  getEmployeeDashboardAttendance(params) {
    const api = jApi.generateApi();
    return api.post('times/dashboard/employee/attendance', params)
      .then(res => res);
  },
  getEmployeeDashboardOvertime(params) {
    const api = jApi.generateApi();
    return api.post('times/dashboard/employee/overtime', params)
      .then(res => res);
  },

  getListEmploymentType() {
    const api = jApi.generateApi();
    return api.get('user/employee/employment-type/list')
      .then(res => res.data);
  },
};
