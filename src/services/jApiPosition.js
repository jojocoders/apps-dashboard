import jApi from './jApi';

export default {
  getListPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res);
  },

  getDetailPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/detail', req)
      .then(res => res.data);
  },

  createPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/create', req)
      .then(res => res.data);
  },

  updatePosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/update', req)
      .then(res => res.data);
  },

  deletePosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/delete', req)
      .then(res => res.data);
  },

  importPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/import/r1', req, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then(res => res.data);
  },

  // parent
  getListParent(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res.data);
  },

  // division
  getListDivisions(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res.data);
  },

  // create division
  createDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/create', req)
      .then(res => res.data);
  },

  // layer
  getListLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/list', req)
      .then(res => res.data);
  },

  // create layer
  createLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/create', req)
      .then(res => res.data);
  },

  // employee
  getListEmployee(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/list-with-pagination', req)
      .then(res => res.data);
  },

  // employee temp
  getListEmployeeTemp(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/get-user-by-level', req)
      .then(res => res.data);
  },

  // rule
  getListRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/list', req)
      .then(res => res.data);
  },

  // create employee
  createEmployee(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/create', req)
      .then(res => res.data);
  },

  getDetailOrganigramTemp(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram-temp/detail/organigram-id', req)
      .then(res => res.data).catch((err) => {
        throw err;
      });
  },

  deleteOrganigramTemp(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram-temp/delete', req)
      .then(res => res.data);
  },

  createOrganigramTemp(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram-temp/create', req)
      .then(res => res.data);
  },

  companyListLocation(req) {
    const api = jApi.generateApi();
    return api.post('company/setup/location/list', req)
      .then(res => res.data);
  },

  getCoattendanceMappingRule(req) {
    const api = jApi.generateApi();
    return api.post('times/co-attendances/config/related-rule/list', req)
      .then(res => res.data);
  },

  createCoattendanceMappingRule(req) {
    const api = jApi.generateApi();
    return api.post('times/co-attendances/config/related-rule/create', req)
      .then(res => res.data);
  },

  updateCoattendanceMappingRule(req) {
    const api = jApi.generateApi();
    return api.post('times/co-attendances/config/related-rule/update', req)
      .then(res => res.data);
  },

  // MAPPING
  getListMappingCategory(req) {
    const api = jApi.generateApi();
    return api.post('times/mapping-category-relation-rule/list-by-organigram', req)
      .then(res => res);
  },
  createMappingCategory(req) {
    const api = jApi.generateApi();
    return api.post('times/mapping-category-relation-rule/create', req)
      .then(res => res.data);
  },
  updateMappingCategory(req) {
    const api = jApi.generateApi();
    return api.post('times/mapping-category-relation-rule/update', req)
      .then(res => res.data);
  },
  deleteMappingCategory(req) {
    const api = jApi.generateApi();
    return api.post('times/mapping-category-relation-rule/delete', req)
      .then(res => res.data);
  },

  getListLeaveType(params) {
    const api = jApi.generateApi();
    return api.get('times/leaves-service/leave-types', { params })
      .then(res => res.data);
  },
  getListOvertimeType(params) {
    const api = jApi.generateApi();
    return api.get('times/overtime/config/type/list-all', { params })
      .then(res => res.data);
  },
  getListExpense(req) {
    const api = jApi.generateApi();
    return api.post('expense-setup/mapping-position/detail', req)
      .then(res => res.data);
  },
  getListExpenseType(req) {
    const api = jApi.generateApi();
    return api.post('expense-setup/category/list', req)
      .then(res => res.data);
  },
  createMappingExpense(req) {
    const api = jApi.generateApi();
    return api.post('expense-setup/mapping-position/create', req)
      .then(res => res.data);
  },
  deleteMappingExpense(req) {
    const api = jApi.generateApi();
    return api.post('expense-setup/mapping-position/delete', req)
      .then(res => res.data);
  },

  // RESTRICTION
  getListOrganigramRestriction(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram-restriction/list-by-root-organigram', req)
      .then(res => res.data);
  },
  createOrganigramRestriction(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram-restriction/create', req)
      .then(res => res.data);
  },
  deleteOrganigramRestriction(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram-restriction/delete', req)
      .then(res => res.data);
  },

  // RESTRICTION
  getListSubordinate(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/org-chart/list', req)
      .then(res => res.data);
  },
  createSubordinate(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/org-chart/create', req)
      .then(res => res.data);
  },
  deleteSubordinate(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/org-chart/delete', req)
      .then(res => res.data);
  },

  // ASSIGNEE
  getListAssignee(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list-assignee', req)
      .then(res => res.data);
  },

  // GENERAL
  getCostCenterByDivision(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/detail-costcenter-by-division', req)
      .then(res => res.data);
  },
  getTaxLocationByLocation(req) {
    const api = jApi.generateApi();
    return api.get(`user/employee/location-relation/detail-by-location/${req}`)
      .then(res => res.data);
  },
  getTaxLocation(id) {
    const api = jApi.generateApi();
    return api.get(`payroll/tax-locations/${id}`)
      .then(res => res.data);
  },

  // FLOW
  getListFlow(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/list', req)
      .then(res => res);
  },

  // Position Layer
  getPositionLayerList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/list', req)
      .then(res => res.data.data);
  },
  listPositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/list', req)
      .then(res => res);
  },
  createPositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/create', req)
      .then(res => res.data);
  },
  getDetailPositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/detail', req)
      .then(res => res.data);
  },
  updatePositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/update', req)
      .then(res => res.data);
  },
  deletePositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/delete', req)
      .then(res => res.data);
  },
};
