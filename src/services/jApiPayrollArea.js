import jApi from './jApi';

export default {
  createPayrollArea(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/payroll-areas', req)
      .then(res => res.data);
  },
  payrollAreaList(req) {
    const api = jApi.generateApi();
    return api.get('jojoflow/payroll-areas', { params: req })
      .then(res => res.data);
  },
  deletePayrollArea(req) {
    const api = jApi.generateApi();
    return api.delete(`jojoflow/payroll-areas/${req.id}`)
      .then(res => res.data);
  },
  updatePayrollArea(id, req) {
    const api = jApi.generateApi();
    return api.put(`jojoflow/payroll-areas/${id}`, req)
      .then(res => res.data);
  },

  // MAPPING
  getListMappingPayrollArea(req) {
    const api = jApi.generateApi();
    return api.get('jojoflow/organigram/payroll-areas', { params: req })
      .then(res => res.data);
  },
  getMappingPayrollAreaByOrganigram(organigramId) {
    const api = jApi.generateApi();
    return api.get(`jojoflow/organigram/${organigramId}/payroll-areas`)
      .then(res => res.data);
  },
  createMappingPayrollArea(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/payroll-areas', req)
      .then(res => res.data);
  },
  updateMappingPayrollArea(id, req) {
    const api = jApi.generateApi();
    return api.put(`jojoflow/organigram/payroll-areas/${id}`, req)
      .then(res => res.data);
  },
  deleteMappingPayrollArea(id) {
    const api = jApi.generateApi();
    return api.delete(`jojoflow/organigram/payroll-areas/${id}`)
      .then(res => res.data);
  },
};
