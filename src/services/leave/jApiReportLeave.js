import jApi from '../jApi';

export default {
  getEmployeeList(req) {
    const api = jApi.generateApi();
    return api.post('/times/user-restriction/report', req)
      .then(res => res);
  },
  getEmployeeRestrictionOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor-organigram', req)
      .then(res => res);
  },
  getTeamList(req) {
    const api = jApi.generateApi();
    return api.post('company/setup/team/list', req)
      .then(res => res);
  },
  getOrgUnitList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res);
  },
  getLeaveTypeList() {
    const api = jApi.generateApi();
    return api.get('times/leaves-service/leave-types')
      .then(res => res);
  },
  getLeaveTypeByUser(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/types', req)
      .then(res => res);
  },
  // Summary
  getSummaryReport(params) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/summary/list', params)
      .then(res => res);
  },
  getReportLeaveHistoryExport(params) {
    const api = jApi.generateApi();
    return api.post('times/leave-report/export-request/list', params)
      .then(res => res);
  },
  generateSummaryXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/summary/export', req)
      .then(res => res);
  },
  // Leave
  getLeaveReport(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/leave/list', req)
      .then(res => res);
  },
  generateLeaveXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/leave/export', req)
      .then(res => res);
  },
  generateLeavePDF(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/summary/export/pdf', req)
      .then(res => res);
  },
  // Detail
  getLeaveDetailReport(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/leave/detail', req)
      .then(res => res);
  },
  generateLeaveDetailXLS(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-report/leave/detail/export', req)
      .then(res => res);
  },
};
