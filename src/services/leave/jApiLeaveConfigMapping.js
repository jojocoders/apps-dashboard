import jApi from '../jApi';

export default {
  createMapping(userCompanyId, payload) {
    const api = jApi.generateApi();
    return api.post(`times/leaves-service/users/${userCompanyId}/related-types`, payload)
      .then(res => res);
  },
  updateMapping(id, userCompanyId, payload) {
    const api = jApi.generateApi();
    return api.put(`times/leaves-service/users/${userCompanyId}/related-types/${id}`, payload)
      .then(res => res);
  },
  deleteMapping(id, userCompanyId) {
    const api = jApi.generateApi();
    return api.delete(`times/leaves-service/users/${userCompanyId}/related-types/${id}`)
      .then(res => res);
  },
  getConfigMappingList(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor', req)
      .then(res => res);
  },
  getConfigMappingOrganigramList(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor-organigram', req)
      .then(res => res);
  },
  getMappingLeave(userCompanyId, params) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/users/${userCompanyId}/related-types`, { params })
      .then(res => res);
  },
  getListMappingLeave(params) {
    const api = jApi.generateApi();
    return api.post('times/leave/config/mapping-leave/data-table', params)
      .then(res => res);
  },
  getMappingDetail(id, userCompanyId) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/users/${userCompanyId}/related-types/${id}`)
      .then(res => res);
  },
  getRuleList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/list', req)
      .then(res => res);
  },
  getLeaveType(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leave-types?limit=${req.pagination.limit}&page=${req.pagination.page}`)
      .then(res => res);
  },
  reconfigureDate(req) {
    const api = jApi.generateApi();
    return api.post('times/leave/config/type/configure', req)
      .then(res => res);
  },
  exportLeaveType() {
    const api = jApi.generateApi();
    return api.get('times/leave-config/type/export')
      .then(res => res.data);
  },
};
