import jApi from '../jApi';

export default {

  getListLeave(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/monitors?status=${req.status}&limit=${req.limit}&page=${req.page}&column=${req.column ? req.column : 'created_date'}&ascending=${req.ascending === 1}`)
      .then(res => res);
  },

  getListLeaveV3(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/monitorsV3?status=${req.status}&limit=${req.limit}&page=${req.page}&column=${req.column ? req.column : 'created_date'}&ascending=${req.ascending === 1}&start_date=${req.start_date}&end_date=${req.end_date}`)
      .then(res => res);
  },

  getSummaryBlock(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/monitors/length?status=${req.status}&start_date=${req.start_date}&end_date=${req.end_date}`)
      .then(res => res);
  },

  actionMonitors(req, id) {
    const api = jApi.generateApi();
    return api.post(`times/leaves-service/leaves/${id}/approval`, req)
      .then(res => res);
  },

  getLeaveEmployee(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leaves/all-employee', req)
      .then(res => res.data);
  },

  getLeaveEmployeeByRestriction(params) {
    const api = jApi.generateApi();
    return api.get('times/leaves-service/monitorsV3', { params })
      .then(res => res.data);
  },

};
