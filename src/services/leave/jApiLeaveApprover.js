import jApi from '../jApi';

export default {

  getLeaveDetail(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leaves/${req.id}`)
      .then(res => res);
  },

  getListLeave(req) {
    let columnParams;
    if (req.column) {
      columnParams = `&column=${req.column}&ascending=${req.ascending === 1}`;
    } else { columnParams = '&column=created_date&ascending=false'; }

    let filterParams;
    if (req.filter) {
      filterParams = `&query_type=${req.query.type}&query=${req.query.value}`;
    } else { filterParams = ''; }

    const api = jApi.generateApi();
    return api.get(`times/leaves-service/approval?status=${req.status}&limit=${req.limit}&page=${req.page}${columnParams}${filterParams}`)
      .then(res => res);
  },

  getSummaryBlock(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/approval/length?status=${req}`)
      .then(res => res);
  },

  getLeaveLog() {
    // do nothing
  },

  actionApproval(req, id) {
    const api = jApi.generateApi();
    return api.post(`times/leaves-service/leaves/${id}/approval`, req)
      .then(res => res);
  },

};
