import jApi from '../jApi';

export default {

  leaveUserRestric(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor', req)
      .then(res => res);
  },
  leaveUserRestrictOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor-organigram', req)
      .then(res => res);
  },
  leaveUserType(req) {
    const api = jApi.generateApi();
    return api.post('times/leave-config/type/list', req)
      .then(res => res);
  },
  calculateLeave(req) {
    const api = jApi.generateApi();
    return api.post('times/leave-config/calculate-leave', req)
      .then(res => res.data);
  },
  saveDraft(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leaves/save-to-draft', req)
      .then(res => res.data);
  },
  submitLeave(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leaves/submit', req)
      .then(res => res.data);
  },
  createLeave(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leaves', req)
      .then(res => res.data);
  },
  createLeaveV3(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leaves/createV3', req)
      .then(res => res.data);
  },
  deleteLeave(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leaves/delete', req)
      .then(res => res.data);
  },
  getPeriodTo(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/period-to', req)
      .then(res => res);
  },
  getListLeave(req) {
    let columnParams;
    if (req.column) {
      columnParams = `&column=${req.column}&ascending=${req.ascending === 1}`;
    } else { columnParams = '&column=created_date&ascending=false'; }

    let filterParams;
    if (req.filter) {
      filterParams = `&query_type=${req.query.type}&query=${req.query.value}`;
    } else { filterParams = ''; }

    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leaves?status=${req.status}&limit=${req.limit}&page=${req.page}${columnParams}${filterParams}`)
      .then(res => res);
  },
  getSummaryBlock(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leaves/length?status=${req}`)
      .then(res => res);
  },
  getDetailLeave(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leaves/${req}`)
      .then(res => res);
  },
  getLeaveDocs(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leaves/${req}/docs`)
      .then(res => res);
  },
  getLeaveLogs(req) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leaves/${req}/logs`)
      .then(res => res);
  },
  actionClaimer(req, id) {
    const api = jApi.generateApi();
    return api.post(`times/leaves-service/leaves/${id}/approval`, req)
      .then(res => res.data);
  },
  getRoleSetting() {
    const api = jApi.generateApi();
    return api.get('times/company-setting/roles-setting')
      .then(res => res.data);
  },
};
