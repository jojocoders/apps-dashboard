import jApi from '../jApi';

export default {

  getLeaveTypeList(req) {
    let columnParams;
    if (req.column) {
      columnParams = `&column=${req.column}&ascending=${req.ascending === 1}`;
    } else { columnParams = '&column=name&ascending=false'; }

    let filterParams;
    if (req.query !== '') {
      filterParams = `&query=${req.query}`;
    } else { filterParams = ''; }

    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leave-types?limit=${req.limit}&page=${req.page}${columnParams}${filterParams}`)
      .then(res => res);
  },

  createLeaveType(req) {
    const api = jApi.generateApi();
    return api.post('times/leaves-service/leave-types', req)
      .then(res => res);
  },

  updateLeaveType(req, id) {
    const api = jApi.generateApi();
    return api.put(`times/leaves-service/leave-types/${id}`, req)
      .then(res => res);
  },

  deleteLeaveType(id) {
    const api = jApi.generateApi();
    return api.delete(`times/leaves-service/leave-types/${id}`)
      .then(res => res);
  },

  getLeaveTypeDetail(id) {
    const api = jApi.generateApi();
    return api.get(`times/leaves-service/leave-types/${id}`)
      .then(res => res);
  },

  getEmploymentTypeList() {
    const api = jApi.generateApi();
    return api.get('user/employee/employment-type/list')
      .then(res => res);
  },

  getGradeList(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/grade/list', req)
      .then(res => res);
  },

  listDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res.data);
  },

};
