import jApi from './jApi';

export default {
  generatePayrollBPJS(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/worker/export', req)
      .then(res => res.data);
  },
};
