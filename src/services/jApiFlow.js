import jApi from './jApi';

export default {
  listFlow(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/list', req)
      .then(res => res.data);
  },

  listPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res.data);
  },

  createFlow(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/create', req)
      .then(res => res.data.data);
  },
  removeFlow(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/delete', req)
      .then(res => res.data);
  },
  detailFlow(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/detail', req)
      .then(res => res.data);
  },

  updateFlow(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/update', req)
      .then(res => res.data);
  },

  createFlowByDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/create', req)
      .then(res => res.data);
  },
  updateFlowByDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/update', req)
      .then(res => res.data);
  },

  getDetailSLA(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/sla/detail', req)
      .then(res => res.data);
  },

  createSLA(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/sla/create', req)
      .then(res => res.data);
  },

  updateSLA(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/flow/sla/update', req)
      .then(res => res.data);
  },
};
