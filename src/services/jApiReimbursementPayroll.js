import jApi from './jApi';

export default {
  getEmployee(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/employee/list-with-pagination', req)
      .then(res => res.data);
  },

  createReimbursement(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/reimbursements', req)
      .then(res => res.data);
  },

  updateReimbursement(req) {
    const api = jApi.generateApi();
    return api.put(`/payroll/reimbursements/${req.id}`, req)
      .then(res => res.data);
  },

  deleteReimbursement(req) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/reimbursements/${req.id}`, req)
      .then(res => res.data);
  },

  listReimbursementPayment(req) {
    const api = jApi.generateApi();
    return api.get(`/payroll/reimbursements/${req.id}/payments`)
      .then(res => res.data);
  },

  createReimbursementPayment(req) {
    const api = jApi.generateApi();
    return api.post(`/payroll/reimbursements/${req.reimbursement_id}/payments`, req)
      .then(res => res.data);
  },

  updateReimbursementPayment(req) {
    const api = jApi.generateApi();
    return api.put(`/payroll/reimbursements/${req.reimbursement_id}/payments/${req.id}`, req)
      .then(res => res.data);
  },

  deleteReimbursementPayment(req) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/reimbursements/${req.reimbursement_id}/payments/${req.id}`, req)
      .then(res => res.data);
  },

  tryImport(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/worker/import', req)
      .then(res => res.data);
  },
};
