import jApi from '../jApi';

export default {
  getListClaimer(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/claimer/list', params)
      .then(res => res);
  },
  createClaimer(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/v2/create', params)
      .then(res => res.data);
  },
  getSummaryBlock() {
    const api = jApi.generateApi();
    return api.get('times/overtime/claimer/summary-block')
      .then(res => res.data);
  },
  getListClaimerType(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/claimer/list-overtime-type', params)
      .then(res => res);
  },
  getListClaimerAttendance(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/claimer/list-attendance', params)
      .then(res => res);
  },
  getListAdditionalInfo(params) {
    const api = jApi.generateApiMock();
    return api.post('times/overtime/claimer/list-additional-info', params)
      .then(res => res);
  },
  getListLogs(params) {
    const api = jApi.generateApi();
    return api.get(`times/overtime/${params.id}/logs`)
      .then(res => res);
  },
};
