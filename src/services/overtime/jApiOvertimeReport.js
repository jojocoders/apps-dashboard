import jApi from '../jApi';

export default {
  // OVERTIME REPORT
  getReportOvertimeList(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime-report/summary/list', params)
      .then(res => res);
  },
  getReportOvertimeDetailList(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime-report/detail', params)
      .then(res => res);
  },
  generateOvertimeSummaryXLS(req) {
    const api = jApi.generateApi();
    return api.post('times/overtime-report/summary/export', req)
      .then(res => res);
  },
  generateOvertimeDetailXLS(req) {
    const api = jApi.generateApi();
    return api.post('times/overtime-report/detail/export', req)
      .then(res => res);
  },
};
