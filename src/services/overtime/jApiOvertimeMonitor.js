import jApi from '../jApi';

export default {

  getListMonitor(params) {
    const api = jApi.generateApi();
    return api.get('times/overtime/monitor', { params })
      .then(res => res);
  },

  getSummaryBlock(req) {
    const api = jApi.generateApi();
    return api.post('times/overtime/summary-block/monitor', req)
      .then(res => res);
  },

  actionMonitors(req, id) {
    const api = jApi.generateApi();
    return api.post(`times/leaves-service/leaves/${id}/approval`, req)
      .then(res => res);
  },

  // OVERTIME MONITOR
  getListMonitorOvertime(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/monitor/list', params)
      .then(res => res);
  },
};
