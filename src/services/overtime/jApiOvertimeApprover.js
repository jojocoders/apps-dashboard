import jApi from '../jApi';

export default {
  getListApprover(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/approval/list', params)
      .then(res => res);
  },
  getDetailOvertime(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/detail', params)
      .then(res => res.data);
  },
  getSummaryBlock(params) {
    const api = jApi.generateApi();
    return api.get(`times/overtime/approval/summary-block?status=${params}`)
      .then(res => res.data);
  },
  changeStatusOvertime(id, req) {
    const api = jApi.generateApi();
    return api.post(`/times/overtime/v2/approval/${id}/approval-action`, req)
      .then(res => res.data);
  },
};
