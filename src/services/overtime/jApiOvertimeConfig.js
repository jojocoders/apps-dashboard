import jApi from '../jApi';

export default {
  // FORMULA
  getListFormula(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/formula/list', params)
      .then(res => res);
  },
  getDetailFormula(id) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/formula/detail', id)
      .then(res => res.data);
  },
  createFormula(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/formula/create', params)
      .then(res => res.data);
  },
  updateFormula(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/formula/update', params)
      .then(res => res.data);
  },
  deleteFormula(id) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/formula/delete', id)
      .then(res => res.data);
  },

  // POLICY
  getListPolicy(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/policy/list', params)
      .then(res => res);
  },
  getDetailPolicy(id) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/policy/detail', id)
      .then(res => res.data);
  },
  createPolicy(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/policy/create', params)
      .then(res => res.data);
  },
  updatePolicy(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/policy/update', params)
      .then(res => res.data);
  },
  deletePolicy(id) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/policy/delete', id)
      .then(res => res.data);
  },

  // OVERTIME TYPE
  getListOvertimeType(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/type/list-data-table', params)
      .then(res => res);
  },
  getDetailOvertimeType(id) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/type/detail', id)
      .then(res => res.data);
  },
  createOvertimeType(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/type/create', params)
      .then(res => res.data);
  },
  updateOvertimeType(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/type/update', params)
      .then(res => res.data);
  },
  deleteOvertimeType(id) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/type/delete', id)
      .then(res => res.data);
  },

  getListForm(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/config/additional-info/block/list', params)
      .then(res => res);
  },

  getListTimesFeatures() {
    const api = jApi.generateApi();
    return api.get('times/features')
      .then(res => res);
  },

  getListMappingRuleConfig(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/mapping-rule-config/getlist', params)
      .then(res => res.data);
  },

  updateListMappingRuleConfig(params) {
    const api = jApi.generateApi();
    return api.post('times/overtime/mapping-rule-config/update', params)
      .then(res => res);
  },
};
