import jApi from './jApi';

export default {
  listEmployee(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/list', req)
      .then(res => res);
  },

  createEmployee(req) {
    const apis = jApi.generateApi();
    return apis.post('user/employee/create', req)
      .then(res => res.data);
  },

  createEmployeeAuthService(req) {
    const api = jApi.generateApi();
    return api.post('auth-service/employee/create', req)
      .then(res => res.data);
  },

  detailEmployee(req) {
    const apis = jApi.generateApi();
    return apis.post('user/employee/detail-pro', req)
      .then(res => res.data);
  },

  detailEmployeeR4() {
    const apis = jApi.generateApi();
    return apis.get('app/api/v2/pro/user/profile/r4')
      .then(res => res.data);
  },

  getDetailEmployeeById(req) {
    const apis = jApi.generateApi();
    return apis.post('user/employee/detail', req)
      .then(res => res.data);
  },

  updateEmployee(req) {
    const apis = jApi.generateApi();
    return apis.post('user/employee/update', req)
      .then(res => res.data);
  },

  changePasswordEmployee(req) {
    const api = jApi.generateApi();
    return api.post('auth-service/app/employee/change-password ', req)
      .then(res => res.data);
  },

  updateProfile(req) {
    const apis = jApi.generateApi();
    return apis.post('user/my-profile/update', req)
      .then(res => res.data);
  },

  updateWithReason(id, req) {
    const apis = jApi.generateApi();
    return apis.post(`user/employee/update-with-reasons/${id}`, req)
      .then(res => res.data);
  },

  deleteEmployee(req) {
    const apis = jApi.generateApi();
    return apis.post('user/employee/delete-bulk', req)
      .then(res => res.data);
  },

  listEmployeeType(req) {
    const apis = jApi.generateApi();
    return apis.get('user/employee/employment-type/list', req)
      .then(res => res.data);
  },

  listBank(req) {
    const apis = jApi.generateApi();
    return apis.post('integration-bank/bank/list', req)
      .then(res => res.data);
  },

  listCompany(req) {
    const apis = jApi.generateApi();
    return apis.post('company/setup/location/list', req)
      .then(res => res.data);
  },

  listCountry() {
    const apis = jApi.generateApi();
    return apis.get('company/tools/countries')
      .then(res => res.data);
  },

  listProvince(req) {
    const apis = jApi.generateApi();
    return apis.post('company/tools/provinces', req)
      .then(res => res.data);
  },

  listCity(req) {
    const apis = jApi.generateApi();
    return apis.post('company/tools/cities', req)
      .then(res => res.data);
  },

  detailOrganigram(req) {
    const apis = jApi.generateApi();
    return apis.get(`jojoflow/v2/users/${req.user_company_id}/positionsV2?limit=10&page=1&column=created_date&ascending=true`)
      // return apis.post('jojoflow/organigram/detail-by-user-company', req)
      .then(res => res.data);
  },

  getMappingOrganigram(req) {
    const apis = jApi.generateApi();
    return apis.get(`jojoflow/v2/users/${req.user_company_id}/positionsV2`, { params: req.pagination })
      .then(res => res.data);
  },

  topOrganigram() {
    const apis = jApi.generateApi();
    return apis.get('jojoflow/setup/top-position/list')
      .then(res => res.data);
  },

  listTeams(req) {
    const apis = jApi.generateApi();
    return apis.post('company/setup/team/list', req)
      .then(res => res.data);
  },

  getDivisionList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res);
  },

  updateOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/update', req)
      .then(res => res.data);
  },

  listPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res.data);
  },

  getListAction(data) {
    const api = jApi.generateApi();
    return api.post('/hris-historical/action/list', data).then(res => res).catch(e => e);
  },

  updateReason(data) {
    const api = jApi.generateApi();
    return api.post('hris-historical/report/update', data).then(res => res).catch(e => e);
  },

  deleteListAssignment(data) {
    const api = jApi.generateApi();
    return api.post('/hris-historical/report/delete', data)
      .then(res => res);
  },

  getActivityLogs(params) {
    const api = jApi.generateApi();
    return api.get('/hris-historical-go-service/employee-activity-log/fetch', { params }).then(res => res).catch(e => e);
  },

  createActivityLog(data) {
    const api = jApi.generateApi();
    return api.post('/hris-historical-go-service/employee-activity-log/create', data).then(res => res.data);
  },

  getReasonList() {
    const api = jApi.generateApi();
    return api.get('/company/setup/reason/get-list').then(res => res.data).catch(e => e);
  },

  generateEmployeeId(params) {
    const api = jApi.generateApi();
    return api.post('/generate-service/config/generate', params).then(res => res.data);
  },

  updateEducationInformation(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/educations/${dataId}`, data);
  },
  deleteEducationInformation(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/educations/${dataId}`);
  },
  addEducationInformation(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/educations`, data);
  },
  getListEducationInformation(employeeId) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${employeeId}/educations`).then(res => res).catch(e => e);
  },

  getCareerHistories(id) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/careers`).then(res => res).catch(e => e);
  },
  deleteCareerHistory(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/careers/${dataId}`);
  },
  postCareerHistory(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/careers`, data);
  },
  updateCareerHistory(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/careers/${dataId}`, data);
  },

  getTrackRecords(id) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/track_records`).then(res => res).catch(e => e);
  },
  deleteTrackRecord(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/track_records/${dataId}`);
  },
  postTrackRecord(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/track_records`, data);
  },
  updateTrackRecord(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/track_records/${dataId}`, data);
  },

  getSelfDevelopments(id, limit, page, column, ascending) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/self_developments?limit=${limit}&page=${page}&column=${column}&ascending=${ascending}`).then(res => res).catch(e => e);
  },
  deleteSelfDevelopment(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/self_developments/${dataId}`);
  },
  postSelfDevelopment(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/self_developments`, data);
  },
  updateSelfDevelopment(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/self_developments/${dataId}`, data);
  },

  getEmployeeDocuments(id, limit, page, column, ascending) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/documents?limit=${limit}&page=${page}&column=${column}&ascending=${ascending}`).then(res => res).catch(e => e);
  },
  deleteEmployeeDocument(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/documents/${dataId}`);
  },
  postEmployeeDocument(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/documents`, data);
  },
  updateEmployeeDocument(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/documents/${dataId}`, data);
  },

  getFamilyRelations(id) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/family_relations`).then(res => res).catch(e => e);
  },
  postFamilyRelation(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/family_relations`, data);
  },
  deleteFamilyRelation(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/family_relations/${dataId}`);
  },
  updateFamilyRelation(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/family_relations/${dataId}`, data);
  },

  getEmergencyContacts(employeeId) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${employeeId}/emergency_contacts`).then(res => res).catch(e => e);
  },
  createEmergencyContacts(employeeId, data) {
    const api = jApi.generateApi();
    return api.post(`hris-employee-service/employees/${employeeId}/emergency_contacts`, data);
  },
  updateEmergencyContacts(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`hris-employee-service/employees/${id}/emergency_contacts/${dataId}`, data);
  },
  deleteEmergencyContacts(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`hris-employee-service/employees/${id}/emergency_contacts/${dataId}`);
  },

  getBusinessLines(id) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/business_line`).then(res => res).catch(e => e);
  },
  createBusinessLine(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/business_lines`, data)
      .then(res => res);
  },
  updateBusinessLine(userId, businessLineId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${userId}/business_lines/${businessLineId}`, data)
      .then(res => res);
  },
  deleteBusinessLine(userId, businessLineId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${userId}/business_lines/${businessLineId}`)
      .then(res => res);
  },

  viewOneMedicalInformation(id) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/medical_informations/last`);
  },
  postMedicalInformation(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/medical_informations`, data);
  },

  getMedicalRecords(id) {
    const api = jApi.generateApi();
    return api.get(`/hris-employee-service/employees/${id}/medical_records`).then(res => res).catch(e => e);
  },
  deleteMedicalRecord(id, dataId) {
    const api = jApi.generateApi();
    return api.delete(`/hris-employee-service/employees/${id}/medical_records/${dataId}`);
  },
  postMedicalRecord(id, data) {
    const api = jApi.generateApi();
    return api.post(`/hris-employee-service/employees/${id}/medical_records`, data);
  },
  updateMedicalRecord(id, dataId, data) {
    const api = jApi.generateApi();
    return api.patch(`/hris-employee-service/employees/${id}/medical_records/${dataId}`, data);
  },

  getUsedFacilitiesOnBoarding(id, limit = '', page = '', column = '', ascending = '') {
    const api = jApi.generateApi();
    return api.get(
      `/hris-assets-service/employees/${id}/used_facilities?limit=${limit}&page=${page}&column=${column}&ascending=${ascending}`,
    );
  },
  getAllUsedFacilitiesOnboarding(id, limit = '', page = '', column = '', ascending = '') {
    const api = jApi.generateApi();
    return api.get(
      `hris-assets-service/facilities?limit=${limit}&page=${page}&column=${column}&ascending=${ascending}`);
  },
  getFacilitiesOnBoarding(id, limit = '', page = '', column = '', ascending = '') {
    const api = jApi.generateApi();
    return api.get(
      `/hris-assets-service/facilities?limit=${limit}&page=${page}&column=${column}&ascending=${ascending}&used_by=${id}`,
    );
  },
  createFacilitiesOnBoarding(id, data) {
    const api = jApi.generateApi();
    return api.post(
      `/hris-assets-service/employees/${id}/used_facilities`, data,
    );
  },
  putFacilitiesOnBoarding(id, facilityId, data) {
    const api = jApi.generateApi();
    return api.put(
      `/hris-assets-service/employees/${id}/used_facilities/${facilityId}/state`, data,
    );
  },

  // GRADE
  getGradeList(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/grade/list', params)
      .then(res => res);
  },
  createGrade(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/grade/create', params)
      .then(res => res.data);
  },
  updateGrade(params, gradeId) {
    const api = jApi.generateApi();
    return api.put(`user/employee/grade/update/${gradeId}`, params)
      .then(res => res.data);
  },
  deleteGrade(gradeId) {
    const api = jApi.generateApi();
    return api.delete(`user/employee/grade/delete/${gradeId}`)
      .then(res => res.data);
  },

  // MAPPING GRADE
  getMappingGrade(data) {
    const api = jApi.generateApi();
    return api.post('/user/employee/grade/relation/list', data)
      .then(res => res.data);
  },
  createMappingGrade(data) {
    const api = jApi.generateApi();
    return api.post('user/employee/grade/relation/create', data)
      .then(res => res.data);
  },
  updateMappingGrade(data) {
    const api = jApi.generateApi();
    return api.post('user/employee/grade/relation/update', data)
      .then(res => res.data);
  },
  deleteMappingGrade(data) {
    const api = jApi.generateApi();
    return api.post('/user/employee/grade/relation/delete', data)
      .then(res => res.data);
  },

  // MAPPING EMPLOYMENT
  getMappingEmployment(params) {
    const api = jApi.generateApi();
    return api.post('/user/employee/employment-type/relation/list', params)
      .then(res => res.data);
  },
  createMappingEmployment(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/employment-type/relation/create', params)
      .then(res => res.data);
  },
  updateMappingEmployment(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/employment-type/relation/update', params)
      .then(res => res.data);
  },
  deleteMappingEmployment(params) {
    const api = jApi.generateApi();
    return api.post('/user/employee/employment-type/relation/delete', params)
      .then(res => res.data);
  },

  // MAPPING EMPLOYMENT CHILD
  getChildByEmployment(params) {
    const api = jApi.generateApi();
    return api.post('/user/employee/employment-type-child/list', params).then(res => res.data).catch(e => e);
  },
  getMappingEmploymentChild(params) {
    const api = jApi.generateApi();
    return api.post('/user/employee/employment-type-child/relation/list', params)
      .then(res => res.data);
  },
  createMappingEmploymentChild(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/employment-type-child/relation/create', params)
      .then(res => res.data);
  },
  updateMappingEmploymentChild(id, params) {
    const api = jApi.generateApi();
    return api.put(`user/employee/employment-type-child/relation/update/${id}`, params)
      .then(res => res.data);
  },
  deleteMappingEmploymentChild(id) {
    const api = jApi.generateApi();
    return api.delete(`/user/employee/employment-type-child/relation/delete/${id}`)
      .then(res => res.data);
  },

  // COST CENTER
  getCostCenterList(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/list', params)
      .then(res => res);
  },
  getCostCenterDetail(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/detail', params)
      .then(res => res.data);
  },
  createCostCenter(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/create', params)
      .then(res => res.data);
  },
  updateCostCenter(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/update', params)
      .then(res => res.data);
  },
  deleteCostCenter(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/delete', params)
      .then(res => res.data);
  },

  // COST CENTER MAPPING ORG UNIT
  createMappingCostCenterDivision(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/mapping-costcenter-divison/add', params)
      .then(res => res.data);
  },
  updateMappingCostCenterDivision(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/mapping-costcenter-divison/update', params)
      .then(res => res.data);
  },
  deleteMappingCostCenterDivision(params) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/mapping-costcenter-divison/delete', params)
      .then(res => res.data);
  },

  getListDivisions(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res.data);
  },

  getListOrgUnit(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/list', req)
      .then(res => res.data);
  },

  // Export historical to email
  exportHistoricalToEMail(params) {
    const api = jApi.generateApi();
    return api.post('user/employee/historical/export', params)
      .then(res => res.data);
  },

  createMappingPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/mapping/create', req)
      .then(res => res.data);
  },

  updateMappingPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/mapping/update', req)
      .then(res => res.data);
  },
};
