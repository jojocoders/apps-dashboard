import jApi from './jApi';

export default {
  companyCreateLocation(req) {
    const api = jApi.generateApi();
    return api.post('company/setup/location/create', req)
      .then(res => res.data);
  },
  companyListLocation(req) {
    const api = jApi.generateApi();
    return api.post('company/setup/location/list', req)
      .then(res => res.data);
  },
  companyDeleteLocation(req) {
    const api = jApi.generateApi();
    return api.post('company/setup/location/delete', req)
      .then(res => res.data);
  },
  companyUpdateLocation(id, req) {
    const api = jApi.generateApi();
    return api.post(`company/setup/location/update/${id}`, req)
      .then(res => res.data);
  },
  companyDetailLocation(id) {
    const api = jApi.generateApi();
    return api.get(`company/setup/location/detail${id}`)
      .then(res => res.data);
  },
  companyTaxLocation() {
    const api = jApi.generateApi();
    return api.get('payroll/tax-locations')
      .then(res => res.data);
  },
  createTaxLocation(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/location-relation/create', req)
      .then(res => res.data);
  },
  updateTaxLocation(req) {
    const api = jApi.generateApi();
    return api.put(`user/employee/location-relation/update/${req.location_relation_id}`, req)
      .then(res => res.data);
  },
  getDetailTaxLocation(locationId) {
    const api = jApi.generateApi();
    return api.get(`user/employee/location-relation/detail-by-location/${locationId}`)
      .then(res => res.data);
  },

  getCities(req) {
    const api = jApi.generateApi();
    return api.post('company/tools/cities/all', req)
      .then(res => res.data);
  },

  getTimezone() {
    const api = jApi.generateApi();
    return api.get('company/detail')
      .then(res => res.data);
  },
};
