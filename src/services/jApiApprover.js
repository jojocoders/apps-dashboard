import jApi from './jApi';

export default {
  listMyForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/form/list', req)
      .then(res => res);
  },

  getSummaryBlock() {
    const api = jApi.generateApi();
    return api.get('/jojoform-transaction/summary-block/approver-record')
      .then(res => res);
  },

  sentDataDraftApprover(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/approval/action', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

  sentDataUpdate(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/update', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

  sentDataApproverAndUpdate(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/approval/update-action', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

  getDataProfile(req) {
    const api = jApi.generateApi();
    return api.post('/user/employee/detail-pro', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

};
