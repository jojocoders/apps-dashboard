import router from '@/router';
import jApi from './jApi';

// temporary data
const request = {
  pagination: {
    limit: '',
    page: '',
    column: '',
    ascending: true,
    query: '',
    query_type: '',
  },
};

export default {
  listUser(req) {
    const api = jApi.generateApi();
    if (req) {
      return api.post('user/employee/list', req)
        .then(res => res.data.data);
    }
    return api.post('user/employee/list', request)
      .then(res => res.data.data);
  },

  listUserByDivision(req) {
    const api = jApi.generateApi();
    if (req) {
      return api.post('jojoflow/setup/division/user/list', req)
        .then(res => res.data.data);
    }
    return api.post('jojoflow/setup/division/user/list', request)
      .then(res => res.data.data);
  },

  emailId(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/detail-by-email', req)
      .then(res => res.data);
  },

  userAuthorize() {
    const api = jApi.generateApi();
    return api.get('user/authorize')
      .then(res => res.data).catch(() => {
        router.push('/logout');
      });
  },

  listUserPagination(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/list-with-pagination', req)
      .then(res => res);
  },

  listUserGrade(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/grade/list', req)
      .then(res => res);
  },

  listBasedOnLocation(req) {
    const api = jApi.generateApi();
    return api.post('company/setup/location/list ', req)
      .then(res => res);
  },

  listUserByIds(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/list/filter-status', req)
      .then(res => res.data);
  },

  listOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res);
  },

  getUserRestrictionMonitor(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor', req)
      .then(res => res);
  },

  getUserRestrictionReport(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/report', req)
      .then(res => res);
  },

  getUserRestrictionOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('times/user-restriction/monitor-organigram', req)
      .then(res => res);
  },
};
