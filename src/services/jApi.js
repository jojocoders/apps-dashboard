import axios from 'axios';
import Vue from 'vue';
import stores from '@/stores';
import router from '@/router';
import jError from '@/error/jError';

export default {
  generateApi(authGoogle) {
    const token = Vue.ls.get('Token');

    const api = axios.create({
      baseURL: process.env.GATE_URL,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    api.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          stores.commit('jStoreLogin/saveToken', response.headers);
          stores.commit('jStoreLogin/setInLocalStorage');
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        } else if (error.response.status === 406 || error.response.status === 401) {
          router.push('/logout');
        } else if (error.response.status === 400 && !authGoogle) {
          stores.dispatch('jStoreNotificationScreen/toggleProblem', error.response.data.message, { root: true });
        } else if (error.response.status === 422) {
          stores.dispatch('jStoreNotificationScreen/toggleProblem', error.response.data.message, { root: true });
        }
        jError.getErrorMessage(error.response.data);
        // handle error
        return Promise.reject(error);
      });

    return api;
  },
  generateApiPrivy() {
    const token = Vue.ls.get('Token');

    const api = axios.create({
      baseURL: process.env.GATE_URL,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    api.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          stores.commit('jStoreLogin/saveToken', response.headers);
          stores.commit('jStoreLogin/setInLocalStorage');
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        } else if (error.response.status === 406 || error.response.status === 401) {
          router.push('/logout');
        }
        // handle error
        return Promise.reject(error);
      });

    return api;
  },
  generateApiServices() {
    const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2pvbm9taWMtand0LXNlcnZpY2UiLCJpYXQiOjE2MDA5NDE1MTQsImV4cCI6MTYzMjQ3NzUxNCwic3ViIjoxNzM2OCwic2Vzc19pZCI6MTUzODM3MSwidXNlciI6eyJpZCI6MTczNjgsImVtYWlsIjoiYWx5c2FuZ2FkamlAam9qb25vbWljLmNvbSIsImNvbXBhbnlfaWQiOjE0LCJ1c2VyX2NvbXBhbnlfaWQiOjM3OTV9LCJsYW5nIjoiZW5fVVMiLCJzZXNzaW9uX3NldHRpbmciOjF9.kEiCb6ZqBx_v_9oFJ8lJx9iB7YUdkkkNo2YZhFfaroM';

    const api = axios.create({
      baseURL: 'https://dev-api-gateway-v2.jojonomic.io/',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    api.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          stores.commit('jStoreLogin/saveToken', response.headers);
          stores.commit('jStoreLogin/setInLocalStorage');
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        } else if (error.response.status === 406 || error.response.status === 401) {
          router.push('/logout');
        } else if (error.response.status === 400) {
          stores.dispatch('jStoreNotificationScreen/toggleProblem', error.response.data.message, { root: true });
        }
        jError.getErrorMessage(error.response.data);
        // handle error
        return Promise.reject(error);
      });

    return api;
  },
  generateApiDashboard() {
    const token = Vue.ls.get('Token');
    const api = axios.create({
      baseURL: 'https://api-core.jojonomicaws.com/',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    return api;
  },
  generateApiAdms() {
    const token = Vue.ls.get('Token');
    const api = axios.create({
      baseURL: 'https://gateway-id.jojonomic.id/',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });

    return api;
  },
  loginApi() {
    const config = {
      baseURL: process.env.GATE_URL,
      headers: {},
    };
    if (Number(process.env.IS_COMPANY_GEN_TYPE_FLAGGING) === 1) {
      config.headers['X-VERSION'] = 1;
    }

    const api = axios.create(config);
    return api;
  },
  refreshTokenApi() {
    const api = axios.create({
      baseURL: process.env.GATE_URL,
    });
    api.interceptors.response.use(
      response => response,
      (error) => {
        if (error.response.status === 406 || error.response.status === 401) {
          router.push('/logout');
        }
        return Promise.reject(error);
      });
    return api;
  },
  generateApiMock() {
    const api = axios.create({
      baseURL: 'http://mockapi.jojonomic.io/api/',
    });
    return api;
  },
  generateApiJojolink() {
    const api = axios.create({
      baseURL: 'https://jojonomic.jojolink.io/',
    });
    return api;
  },
  generateApiExternalUrl() {
    const api = axios.create();
    return api;
  },
  generateApiGoogleMaps() {
    const api = axios.create({
      baseURL: 'https://maps.googleapis.com/',
    });

    return api;
  },
  generateApiExport() {
    const token = Vue.ls.get('Token');
    const api = axios.create({
      baseURL: process.env.GATE_URL,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      responseType: 'blob',
    });

    return api;
  },
};
