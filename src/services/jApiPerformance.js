import jApi from './jApi';

export default {
  getEmployeeList(req) {
    const api = jApi.generateApi();
    return api.post('/user/employee/list', req)
      .then(res => res.data);
  },

  getDepartmentList(req) {
    const api = jApi.generateApi();
    return api.post('/jojoflow/setup/division/list', req)
      .then(res => res.data);
  },

  searchReport(req) {
    const api = jApi.generateApi();
    return api.post('/paiton-service/export/hrm-report', req)
      .then(res => res.data);
  },
};
