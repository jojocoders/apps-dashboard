import jApi from './jApi';

export default {
  // Get
  getPayrollCycle() {
    const api = jApi.generateApi();
    return api.get('/payroll/payroll-config')
      .then(res => res.data);
  },
  getPayrollTemplate() {
    const api = jApi.generateApi();
    return api.get('/payroll/payroll-template')
      .then(res => res.data);
  },
  getPayrollTemplateDetail(id) {
    const api = jApi.generateApi();
    return api.get(`/payroll/payroll-template/${id}`)
      .then(res => res.data);
  },
  requestGetPayrollReport(req) {
    const api = jApi.generateApi();
    return api.get(`payroll/payslip-report?${req}`)
      .then(res => res.data);
  },
  requestGetPayrollConfig() {
    const api = jApi.generateApi();
    return api.get('payroll/payroll-config')
      .then(res => res.data);
  },

  // Post
  saveColumnList(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/admin/export/column-preference/save', req)
      .then(res => res.data);
  },
  generatePayrollReport(req, payload) {
    const api = jApi.generateApi();
    return api.post(`/payroll/worker/export?${req}`, payload)
      .then(res => res.data);
  },
  getColumnList(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/admin/export/column-preference', req)
      .then(res => res.data);
  },
  getDivisionList(req) {
    const api = jApi.generateApi();
    return api.post('/jojoflow/setup/division/list', req)
      .then(res => res.data);
  },
  getEmployeeList(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/employee/list-with-pagination', req)
      .then(res => res.data);
  },
  getDefaultColumnList(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/admin/export/default-columns', req)
      .then(res => res.data);
  },
  requestExportPayrollReport(payload) {
    const body = {
      month: payload.month,
      year: payload.year,
      type: payload.type,
      id: payload.id,
      export_config: payload.export_config,
    };
    const api = jApi.generateApi();
    return api.post(`payroll/worker/export?${payload.filter}`, { ...body })
      .then(res => res.data);
  },

  // Put
  savePayrollTemplate(payload) {
    const api = jApi.generateApi();
    return api.put('/payroll/payroll-template', payload)
      .then(res => res.data);
  },

  // Delete
  deletePayrollTemplate(id) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/payroll-template/${id}`)
      .then(res => res.data);
  },

  // Import File
  getPayrollImportFile() {
    const api = jApi.generateApi();
    return api.get('/payroll/import-template/run-payroll-employee')
      .then(res => res.data);
  },
};
