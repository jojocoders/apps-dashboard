import jApi from './jApi';

export default {
  getCompanySetting() {
    const api = jApi.generateApi();
    return api.get('/nocode/company/setting')
      .then(res => res.data);
  },

  getComapnyDetail() {
    const api = jApi.generateApi();
    return api.get('/company/detail')
      .then(res => res.data);
  },
};
