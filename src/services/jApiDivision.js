import jApi from './jApi';

// temporary data
const request = {
  pagination: {
    limit: 0,
    page: 0,
    column: '',
    ascending: false,
  },
};

export default {
  listDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', (req || request))
      .then(res => res.data);
  },

  createDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/create', req)
      .then(res => res.data.data);
  },

  removeDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/delete', req)
      .then(res => res.data);
  },

  updateDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/update', req)
      .then(res => res.data);
  },

  detailDivision(idDivision) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/detail', idDivision)
      .then(res => res.data);
  },
};
