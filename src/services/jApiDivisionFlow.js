import jApi from './jApi';

export default {
  getDivisionList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list', req)
      .then(res => res);
  },

  createDivision(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/create', req)
      .then(res => res.data);
  },

  getDivisionDetail(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/detail', req)
      .then(res => res.data);
  },

  updateDivision(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/update', req)
      .then(res => res.data);
  },

  deleteDivision(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/delete', req)
      .then(res => res.data);
  },

  createFlowDivision(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/flow/create', req)
      .then(res => res.data);
  },

  updateFlowDivision(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/flow/update', req)
      .then(res => res.data);
  },

  deleteFlowDivision(req) {
    const apis = jApi.generateApi();
    return apis.post('jojoflow/setup/division/flow/delete', req)
      .then(res => res.data);
  },

  applyCostCenter(req) {
    const apis = jApi.generateApi();
    return apis.post('costcenter/v2/mapping-costcenter-divison/apply', req)
      .then(res => res.data);
  },

  getCostCenterList(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/list', req)
      .then(res => res);
  },

  getDetailCostCenterByDivision(req) {
    const api = jApi.generateApi();
    return api.post('costcenter/v2/detail-costcenter-by-division', req)
      .then(res => res);
  },
};
