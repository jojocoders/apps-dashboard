import jApi from './jApi';

export default {
  getProfile() {
    const api = jApi.generateApi();
    return api.get('user/profile')
      .then(res => res.data.data);
  },
  getLoginProfile(req) {
    const api = jApi.generateApi();
    if (req) {
      return api.get('user/profile')
        .then(res => res.data.data);
    }
    return api.get('user/profile')
      .then(res => res.data.data);
  },
  checkAttendance() {
    const api = jApi.generateApi();
    return api.get('times/attendances/check-attendance')
      .then(res => res.data.data);
  },
  requestChangePassword(req) {
    const api = jApi.generateApi();
    return api.post('user/change-password-v2/request-otp', req)
      .then(res => res.data);
  },
  verifyOtp(req) {
    const api = jApi.generateApi();
    return api.post('user/change-password-v2/verify-otp', req)
      .then(res => res.data);
  },
  changePassword(req) {
    const api = jApi.generateApi();
    return api.post('user/change-password-v2', req)
      .then(res => res.data);
  },
  changePasswordAuthService(req) {
    const api = jApi.generateApi();
    return api.post('auth-service/me/change-password', req)
      .then(res => res.data);
  },
  requestForgotPassword(req) {
    const api = jApi.generateApi();
    return api.post('forgot-password/request-otp', req)
      .then(res => res.data);
  },
  verifyOtpForgot(req) {
    const api = jApi.generateApi();
    return api.post('forgot-password/verify-otp', req)
      .then(res => res.data);
  },
  resetPassword(req) {
    const api = jApi.generateApi();
    return api.post('forgot-password', req)
      .then(res => res.data);
  },
  resetPasswordAuthService(req) {
    const api = jApi.generateApi();
    return api.post('auth-service/me/forgot-password', req)
      .then(res => res.data);
  },
  requestOtpLogin(req) {
    const api = jApi.generateApi();
    return api.post('auth-service/v2/auth/login-otp/request', req)
      .then(res => res.data);
  },
};
