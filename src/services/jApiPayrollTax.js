import jApi from './jApi';

export default {
  generateTaxReport(req, payload) {
    const api = jApi.generateApi();
    return api.post(`/payroll/worker/export?${req}`, payload)
      .then(res => res.data);
  },
  getEmployeeList(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/employee/list-with-pagination', req)
      .then(res => res.data);
  },
  importTaxFile(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/worker/import', req)
      .then(res => res.data);
  },
  dowloadTaxTemplate() {
    const api = jApi.generateApi();
    return api.get('/payroll/import-template/1721A1')
      .then(res => res.data);
  },
  removeTaxItem(req) {
    const api = jApi.generateApi();
    return api.delete(`/payroll/withholding-tax-slips/${req.id}`)
      .then(res => res.data);
  },
};
