import jApi from './jApi';

export default {
  getSummaryBlock(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/summary-block', req)
      .then(res => res.data);
  },

  getCoattendanceList(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/data-table', req)
      .then(res => res);
  },

  getCoattendanceDraftList(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/data-table-draft', req)
      .then(res => res);
  },

  getCoattendanceDraftDetail(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/detail-data-draft', req)
      .then(res => res);
  },

  getSummaryBlockPic(req) {
    const api = jApi.generateApi();
    return api.post(`times/co-attendances/pic/summary-block?status=${req}`)
      .then(res => res);
  },

  getCoattendancePicList(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/pic/data-table', req)
      .then(res => res);
  },

  createCoattendance(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/createV2', req)
      .then(res => res.data);
  },

  updateCoattendance(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/update', req)
      .then(res => res.data);
  },

  changeStatusCoattendance(id, req) {
    const api = jApi.generateApi();
    return api.post(`/times/co-attendances/approval/${id}/approval-action`, req)
      .then(res => res.data);
  },

  getListAttendance(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/attendance/get-list', req)
      .then(res => res);
  },

  deleteAttendance(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/attendance/delete', req)
      .then(res => res.data);
  },
  deleteAttendanceDraft(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/delete', req)
      .then(res => res.data);
  },

  getCoattendanceDetail(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/detail', req)
      .then(res => res.data);
  },

  getRequestorList(req) {
    const api = jApi.generateApiMock();
    return api.post('/times/co-attendance/requestor/list', req)
      .then(res => res);
  },

  getRequestorDetailList(req) {
    const api = jApi.generateApiMock();
    return api.post('/times/co-attendance/requestor/detail/list', req)
      .then(res => res);
  },

  getCoattendanceMonitorList(req) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/monitor/data-tableV2', req)
      .then(res => res);
  },

  exportCoattendanceMonitor(params) {
    const api = jApi.generateApi();
    // return api.post('times/co-attendances/monitor/export', params)
    return api.post('times/co-attendances/monitor/exportV2', params)
      .then(res => res.data);
  },

  importCoattendance(params) {
    const api = jApi.generateApi();
    return api.post('/times/co-attendances/import', params)
      .then(res => res.data);
  },

  getListLogs(params) {
    const api = jApi.generateApi();
    return api.get(`times/co-attendances/${params.id}/logs`)
      .then(res => res);
  },
};
