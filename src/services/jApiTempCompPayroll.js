import jApi from './jApi';

export default {
  getAllTemporaryCompData(params) {
    const api = jApi.generateApi();
    return api.get('/payroll/temporary-components', params)
      .then(res => res.data);
  },
  getDetailTemporaryCompData(params) {
    const api = jApi.generateApi();
    return api.get(`/payroll/temporary-components/${params.id}`)
      .then(res => res.data);
  },
  updateTemporaryCompData(params) {
    const api = jApi.generateApi();
    return api.put(`payroll/payroll-temporary-components/${params.query.payroll_temporary_component_id}/temporary-components/${params.query.id}`, params.data)
      .then(res => res);
  },
  deleteTemporaryCompData(params) {
    const api = jApi.generateApi();
    return api.delete(`payroll/payroll-temporary-components/${params.payroll_temporary_component_id}/temporary-components/${params.id}`)
      .then(res => res);
  },
  generateTemporaryCompData(params) {
    const api = jApi.generateApi();
    return api.post('payroll/worker/export', params)
      .then(res => res);
  },
  importTemporaryComp(params) {
    const api = jApi.generateApi();
    return api.post('payroll/worker/import', params)
      .then(res => res);
  },
  getTemporaryCompName(type) {
    const api = jApi.generateApi();
    return api.get(`payroll/${type}?filter[is_temporary]=1`)
      .then(res => res.data);
  },
  downloadTemporaryComp(params) {
    const api = jApi.generateApi();
    return api.post('payroll/import-template/Temporary-Component', params)
      .then(res => res.data);
  },
};
