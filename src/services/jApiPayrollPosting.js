import jApi from './jApi';

export default {
  onProcessPayrollPosting(req) {
    const api = jApi.generateApi();
    return api.post('/integration-payroll/account/sap-posting', req)
      .then(res => res.data);
  },
  payrollConfigList() {
    const api = jApi.generateApi();
    return api.get('/payroll/payroll-config')
      .then(res => res.data);
  },
};
