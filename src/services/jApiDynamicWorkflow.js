import jApi from './jApi';

export default {
  // workflow endpoint
  getDynamicWorkflow(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-workflow/workflow', params)
      .then(res => res.data);
  },
  findDynamicWorkflow(params) {
    const api = jApi.generateApi();
    return api.get(`/dynamic-workflow/workflow/${params}`)
      .then(res => res.data);
  },
  createDynamicWorkflow(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-workflow/workflow', params)
      .then(res => res.data);
  },
  updateDynamicWorkflow(id, params) {
    const api = jApi.generateApi();
    return api.patch(`/dynamic-workflow/workflow/${id}`, params)
      .then(res => res.data);
  },
  deleteDynamicWorkflow(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-workflow/workflow/${params}`)
      .then(res => res.data);
  },

  // workflow status endpoint
  getDynamicWorkflowStatus(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-workflow/workflow-status', params)
      .then(res => res.data);
  },
  getDynamicWorkflowStatusByWorkflow(params) {
    const api = jApi.generateApi();
    return api.get(`/dynamic-workflow/workflow-status/by-workflow/${params}`)
      .then(res => res.data);
  },
  findDynamicWorkflowStatus(params) {
    const api = jApi.generateApi();
    return api.get(`/dynamic-workflow/workflow-status/${params}`)
      .then(res => res.data);
  },
  createDynamicWorkflowStatus(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-workflow/workflow-status', params)
      .then(res => res.data);
  },
  updateDynamicWorkflowStatus(id, params) {
    const api = jApi.generateApi();
    return api.patch(`/dynamic-workflow/workflow-status/${id}`, params)
      .then(res => res.data);
  },
  deleteDynamicWorkflowStatus(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-workflow/workflow-status/${params}`)
      .then(res => res.data);
  },

  // workflow status endpoint
  getDynamicWorkflowTransaction(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-workflow/transaction-status', params)
      .then(res => res.data);
  },
  findDynamicWorkflowTransaction(params) {
    const api = jApi.generateApi();
    return api.get(`/dynamic-workflow/transaction-status/${params}`)
      .then(res => res.data);
  },
  createDynamicWorkflowTransaction(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-workflow/transaction-status', params)
      .then(res => res.data);
  },
  updateDynamicWorkflowTransaction(id, params) {
    const api = jApi.generateApi();
    return api.patch(`/dynamic-workflow/transaction-status/${id}`, params)
      .then(res => res.data);
  },
  deleteDynamicWorkflowTransaction(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-workflow/transaction-status/${params}`)
      .then(res => res.data);
  },
};
