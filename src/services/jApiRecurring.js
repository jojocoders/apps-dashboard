import jApi from './jApi';

export default {
  recurringLogList(req) {
    const api = jApi.generateApi();
    return api.post('payroll/import/request-list', { ...req })
      .then(res => res.data);
  },
  recurringList(req) {
    const api = jApi.generateApi();
    return api.post('payroll/admin/payroll-recurring/list', { ...req })
      .then(res => res.data);
  },
  createRecurring(req) {
    const api = jApi.generateApi();
    return api.post('payroll/admin/payroll-recurring/save', req)
      .then(res => res.data);
  },
  deleteRecurring(req) {
    const api = jApi.generateApi();
    return api.post('payroll/admin/payroll-recurring/recurring-delete', req)
      .then(res => res.data);
  },
  getRecurringTemplate() {
    const api = jApi.generateApi();
    return api.get('payroll/admin/download/recurring-format')
      .then(res => res.data);
  },
};
