import jApi from './jApi';

export default {
  getPayrollCycle() {
    const api = jApi.generateApi();
    return api.get('/payroll/payroll-config')
      .then(res => res.data);
  },
  getLatestPayrollCycle() {
    const api = jApi.generateApi();
    return api.get('/payroll/payroll-config/latest')
      .then(res => res.data);
  },
  generateRetroPayroll(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/retroactive-payroll-request', req)
      .then(res => res.data);
  },
  getDetailsPayrollRetroSlip(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/retroactive-history-payslip/detail', req)
      .then(res => res.data);
  },
  getTemplateRetroPayroll() {
    const api = jApi.generateApi();
    return api.get('/payroll/import-template/run-payroll-employee')
      .then(res => res.data);
  },
  updateLockPayroll(req) {
    const api = jApi.generateApi();
    return api.post('/payroll/retroactive-payslip/update-lock', req)
      .then(res => res.data);
  },
  getLocation(req) {
    const api = jApi.generateApi();
    return api.post('/company/setup/location/list', req)
      .then(res => res.data);
  },
};
