import jApi from './jApi';

export default {
  login(authData) {
    const api = jApi.loginApi();
    return api.post('auth/login', authData)
      .then(res => res.data);
  },
  loginLdap(authData) {
    const api = jApi.loginApi();
    const url = process.env.URL_LOGIN_CONNECTION_WITH_LDAP || 'auth/other-login';
    return api.post(url, authData)
      .then(res => res.data);
  },
  loginCustom(authData) {
    const api = jApi.loginApi();
    const url = `auth-service/${process.env.SIGNIN_WITH_CUSTOM_LOGIN_API}`;
    return api.post(url, authData)
      .then(res => res.data);
  },
  refreshToken(req) {
    const api = jApi.loginApi();
    return api.post('auth/refresh', req)
      .then(res => res.data);
  },
  loginV2(authData) {
    const api = jApi.loginApi();
    return api.post('auth-service/v2/auth/login', authData)
      .then(res => res.data);
  },
  getKeyRSA() {
    const api = jApi.loginApi();
    return api.get('auth-service/generate-key')
      .then(res => res.data);
  },
  refreshTokenV2() {
    const api = jApi.refreshTokenApi();
    const refreshToken = JSON.parse(localStorage.getItem('refresh_token')).value;
    return api.get(`auth-service/refresh-token?refresh_token=${refreshToken}`)
      .then(res => res.data);
  },
  logout() {
    const api = jApi.generateApi();
    return api.get('auth-service/logout')
      .then(res => res.data);
  },
};
