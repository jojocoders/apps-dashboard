import jApi from './jApi';

export default {
  getApplicationsList(params) {
    const api = jApi.generateApi();
    return api.get('/application-pages/applications', params)
      .then(res => res.data);
  },
  getGroupApplicationsList(params) {
    const api = jApi.generateApi();
    return api.get('application-pages/group-application/application', params)
      .then(res => res.data);
  },
  getGroupAppsList(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/group-application', params)
      .then(res => res.data);
  },
  createApplication(params) {
    const api = jApi.generateApi();
    return api.post('/application-pages/applications', params)
      .then(res => res.data);
  },
  createPage(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-apps/pages', params)
      .then(res => res.data);
  },
  updatePage(params) {
    const data = {
      application_id: params.application_id,
      name: params.name,
      type: params.type,
      icon_id: params.icon_id,
      order: params.order,
    };

    if (params.parent_id) {
      Object.assign(data, {
        parent_id: params.parent_id,
      });
    }

    const api = jApi.generateApi();
    return api.patch(`/dynamic-apps/pages/${params.page_id}`, data)
      .then(res => res.data);
  },
  deletePage(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-apps/pages/${params}`)
      .then(res => res.data);
  },
  getListIcons(req) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/icon', { params: req })
      .then(res => res.data);
  },
  getCompanyBanner(params) {
    const api = jApi.generateApi();
    return api.get('dynamic-apps/company-banner', params)
      .then(res => res.data);
  },


  // WIDGET
  getListWidgetDetail(params) {
    const api = jApi.generateApi();
    return api.get('dynamic-apps/widgets', { params })
      .then(res => res.data);
  },

  getTimesheetDetail(req) {
    const api = jApi.generateApi();
    return api.post('/times/timesheet/task/list-by-date', req)
      .then(res => res.data);
  },

  getAttendanceDetail() {
    const api = jApi.generateApi();
    return api.get('/times/attendances/check-attendance')
      .then(res => res.data);
  },

  getOrganigramId(req) {
    const api = jApi.generateApi();
    return api.post('/jojoflow/organigram/detail-by-user-company', req)
      .then(res => res.data);
  },

  getQuotaLeave(req) {
    const api = jApi.generateApi();
    return api.post('/times/leave-config/type/list', req)
      .then(res => res.data);
  },

  getListNotification() {
    const api = jApi.generateApi();
    return api.get('/app/api/v2/pro/user/notif')
      .then(res => res.data);
  },

  readNotification(req) {
    const api = jApi.generateApi();
    return api.post('/app/api/v2/pro/user/notif/read', req)
      .then(res => res.data);
  },

  deleteNotification(req) {
    const api = jApi.generateApi();
    return api.post('/app/api/v2/pro/user/notif/delete', req)
      .then(res => res.data);
  },
};
