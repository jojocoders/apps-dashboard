import jApi from './jApi';

export default {
  getFavoriteApplication(params) {
    const api = jApi.generateApi();
    return api.get('/application-pages/application-favourite', params)
      .then(res => res.data);
  },
  createFavoriteApplication(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-apps/application-favorite', params)
      .then(res => res.data);
  },
  updateFavoriteApplication(appFavId, params) {
    const api = jApi.generateApi();
    return api.patch(`/dynamic-apps/application-favorite/${appFavId}`, params)
      .then(res => res.data);
  },
  deleteFavoriteApplication(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-apps/application-favorite/${params}`)
      .then(res => res.data);
  },
};
