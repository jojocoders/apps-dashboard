import jApi from './jApi';

// temporary data
const request = {
  pagination: {
    limit: 0,
    page: 0,
    column: '',
    ascending: false,
  },
};

export default {
  listOrganigramByDivision(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list-by-division/pagination', req)
      .then(res => res.data);
  },
  createOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/create', req)
      .then(res => res.data.data);
  },
  removeOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/delete', req)
      .then(res => res.data);
  },
  updateOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/update', req)
      .then(res => res.data);
  },

  listOrganigram(req) {
    const api = jApi.generateApi();
    if (req) {
      return api.post('jojoflow/organigram/list', req)
        .then(res => res.data);
    }
    return api.post('jojoflow/organigram/list', request)
      .then(res => res.data);
  },

  getOrganigramChart(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/list-chart', req)
      .then(res => res.data);
  },

  exportPositionToEmail(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/export', req)
      .then(res => res.data);
  },

  exportDivisionToEmail(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/division/export', req)
      .then(res => res.data);
  },

  getDetailPositionSync(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/integration/organigram-buffer/detail', req)
      .then(res => res.data);
  },

  getDetailPositionOld(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/detail', req)
      .then(res => res.data);
  },

  getDetailPosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/organigram/detail', req)
      .then(res => res.data);
  },

  updatePositionSync(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/integration/organigram-buffer/sync', req)
      .then(res => res.data);
  },

  getDetailPositionSyncLog(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/integration/organigram-buffer/log', req)
      .then(res => res);
  },
};
