import jApi from './jApi';

export default {
  getSideMenuList(applicationId, params) {
    const api = jApi.generateApi();
    return api.get(`/application-pages/applications/${applicationId}/pages`, params)
      .then(res => res.data);
  },

  getApplicationDetail(applicationId, params) {
    const api = jApi.generateApi();
    return api.get(`/application-pages/applications/${applicationId}`, params)
      .then(res => res.data);
  },

  // SERVICES IN SETTING TAB
  // get form_ids
  listComponent(params, id) {
    const api = jApi.generateApi();
    return api.get(`dynamic-apps/pages/${id}/components`, { params })
      .then(res => res.data);
  },

  listComponentNocode(params, id) {
    const api = jApi.generateApi();
    return api.get(`nocode/dynamic-apps/pages/${id}/components`, { params })
      .then(res => res.data);
  },

  updateComponent(req) {
    const api = jApi.generateApi();
    return api.patch(`dynamic-apps/components/${req.component_id}`, req.data)
      .then(res => res.data);
  },

  detailForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform/additional-info/form/detail', req)
      .then(res => res.data);
  },

  cekValidateForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/validate', req)
      .then(res => res.data);
  },

  // SERVICES IN PAGES TAB
  // FORM COMPONENT
  // get ids
  getTransactionListByForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/list/by-form', req)
      .then(res => res.data);
  },

  // hit last endpoint
  getTransactionListBatchCustom(req, dataForward) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/list-batch-custom2', req)
      .then(res => ({
        data: res.data.data,
        dataForward,
      }));
  },

  createAdditionalData(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/create', req)
      .then(res => res.data);
  },

  updateAdditionalData(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/update', req)
      .then(res => res.data);
  },

  createSubmitAdditionalData(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/create-submit', req)
      .then(res => res.data);
  },

  submitAdditionalData(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/submit', req)
      .then(res => res.data);
  },

  // WORKFLOW COMPONENT
  dataSummary(params) {
    const api = jApi.generateApi();
    return api.get('/jojoform-transaction/summary-block/claimer', { params })
      .then(res => res.data);
  },

  dataSummaryApp(params) {
    const api = jApi.generateApi();
    return api.get('/jojoform-transaction/summary-block/approver-record', { params })
      .then(res => res.data);
  },

  // getReportPDF(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/jojoform-transaction/export-pdf', req)
  //     .then(res => res.data);
  // },

  // For paiton, but can be used for other companies too
  getReportPDF(req) {
    const api = jApi.generateApi();
    return api.post('/paiton-service/export/employee-individual-report', req)
      .then(res => res.data);
  },

  // Company Setting
  getUserCompanySetting() {
    const api = jApi.generateApi();
    return api.get('/nocode/company/setting').then(res => res.data);
  },

};
