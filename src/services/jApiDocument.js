import jApi from './jApi';

export default {

  // MOCK
  getFolderAndDocumentList(req) {
    const api = jApi.generateApiMock();
    return api.get('/document-service/x', req)
      .then(res => res.data);
  },
  getFolderDetail(folderId) {
    const api = jApi.generateApiMock();
    return api.get(`/document-service/folder/${folderId}`)
      .then(res => res.data);
  },
  createFolder(req) {
    const api = jApi.generateApiMock();
    return api.post('/document-service/folder', req)
      .then(res => res.data);
  },
  editFolder(req) {
    const api = jApi.generateApiMock();
    return api.post('/document-service/folder', req)
      .then(res => res.data);
  },
  deleteFolder(req) {
    const api = jApi.generateApiMock();
    return api.delete('/document-service/folder', req)
      .then(res => res.data);
  },


  getDocumentDetail(req) {
    const api = jApi.generateApiMock();
    return api.get(`/document-service/document/${req.document_id}`, req)
      .then(res => res.data);
  },
  shareDocument(req) {
    const api = jApi.generateApiMock();
    return api.post('/document-service/document/share', req)
      .then(res => res.data);
  },
  createDocument(req) {
    const api = jApi.generateApiMock();
    return api.post('/document-service/document', req)
      .then(res => res.data);
  },
  editDocument(req) {
    const api = jApi.generateApiMock();
    return api.post('/document-service/document', req)
      .then(res => res.data);
  },
  deleteDocument(req) {
    const api = jApi.generateApiMock();
    return api.delete('/document-service/document', req)
      .then(res => res.data);
  },


  // APIG
  // getFolderAndDocumentList(req) {
  //   const api = jApi.generateApi();
  //   return api.get('/document-service/x', req)
  //     .then(res => res.data);
  // },
  // getFolderDetail(req) {
  //   const api = jApi.generateApi();
  //   return api.get(`/document-service/folder/${req.folder_id}`, req)
  //     .then(res => res.data);
  // },
  // createFolder(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/document-service/folder', req)
  //     .then(res => res.data);
  // },
  // editFolder(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/document-service/folder', req)
  //     .then(res => res.data);
  // },
  // deleteFolder(req) {
  //   const api = jApi.generateApi();
  //   return api.delete('/document-service/folder', req)
  //     .then(res => res.data);
  // },


  // getDocumentDetail(req) {
  //   const api = jApi.generateApi();
  //   return api.get(`/document-service/document/${req.document_id}`, req)
  //     .then(res => res.data);
  // },
  // shareDocument(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/document-service/document/share', req)
  //     .then(res => res.data);
  // },
  // createDocument(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/document-service/document', req)
  //     .then(res => res.data);
  // },
  // editDocument(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/document-service/document', req)
  //     .then(res => res.data);
  // },
  // deleteDocument(req) {
  //   const api = jApi.generateApi();
  //   return api.delete('/document-service/document', req)
  //     .then(res => res.data);
  // },
};

