import jApi from './jApi';

export default {
  createFormula(req) {
    const api = jApi.generateApi();
    return api.post('dynamic-apps/formula', req)
      .then(res => res.data);
  },

  // formula operation section
  getFormulaOperationsByFormula(id, req) {
    const api = jApi.generateApi();
    return api.get(`dynamic-apps/formula/${id}/formula-operation`, req)
      .then(res => res.data);
  },

  createFormulaOperation(req) {
    const api = jApi.generateApi();
    return api.post('dynamic-apps/formula-operation', req)
      .then(res => res.data);
  },

  updateFormulaOperation(id, req) {
    const api = jApi.generateApi();
    return api.patch(`dynamic-apps/formula-operation/${id}`, req)
      .then(res => res.data);
  },

  deleteFormulaOperation(req) {
    const api = jApi.generateApi();
    return api.delete(`dynamic-apps/formula-operation/${req}`)
      .then(res => res.data);
  },

  // operation section
  getOperationsByFormulaOperation(id, req) {
    const api = jApi.generateApi();
    return api.get(`dynamic-apps/formula-operation/${id}/operations`, req)
      .then(res => res.data);
  },

  createOperation(req) {
    const api = jApi.generateApi();
    return api.post('dynamic-apps/operations', req)
      .then(res => res.data);
  },

  updateOperation(id, req) {
    const api = jApi.generateApi();
    return api.patch(`dynamic-apps/operations/${id}`, req)
      .then(res => res.data);
  },

  deleteOperation(req) {
    const api = jApi.generateApi();
    return api.delete(`dynamic-apps/operations/${req}`)
      .then(res => res.data);
  },
};
