import jApi from './jApi';

export default {
  getAdmsDataList(params) {
    const api = jApi.generateApiAdms();
    return api.post('integration-adms-service/attendance_biometric_data', params)
      .then(res => res);
  },
  getUserAbsenList(params) {
    const api = jApi.generateApiAdms();
    return api.get('integration-adms-service/user/mapping/list', { params })
      .then(res => res);
  },
};
