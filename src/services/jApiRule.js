import jApi from './jApi';

export default {
  createRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/create', req)
      .then(res => res.data);
  },
  removeRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/delete', req)
      .then(res => res.data);
  },
  detailRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/detail', req)
      .then(res => res.data.data);
  },
  updateRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/update', req)
      .then(res => res.data);
  },

  listRule(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/rule/list', req)
      .then(res => res.data);
  },
};
