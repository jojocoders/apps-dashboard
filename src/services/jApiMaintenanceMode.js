import jApi from './jApi';

export default {
  getMaintenanceModeList(params) {
    const api = jApi.generateApi();
    return api.post('/maintenance/fetch', params)
      .then(res => res.data);
  },
  createMaintenanceMode(params) {
    const api = jApi.generateApi();
    return api.post('/maintenance/create', params)
      .then(res => res.data);
  },
  updateMaintenanceMode(params) {
    const api = jApi.generateApi();
    return api.post('/maintenance/update', params)
      .then(res => res.data);
  },
  terminateMaintenanceMode(params) {
    const api = jApi.generateApi();
    return api.post('/maintenance/terminate', params)
      .then(res => res.data);
  },
};
