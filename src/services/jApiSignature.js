import jApi from './jApi';

export default {
  getDocumentList() {
    const api = jApi.generateApi();
    return api.get('/digital-signature-service/documents')
      .then(res => res.data.data);
  },

  getDocumentListRequestor(params) {
    const api = jApi.generateApi();
    return api.get('/digital-signature-service/documents/requestor', { params })
      .then(res => res.data.data);
  },

  getDocumentListApprover(params) {
    const api = jApi.generateApi();
    return api.get('/digital-signature-service/documents/approver', { params })
      .then(res => res.data.data);
  },

  uploadDocument(req) {
    const api = jApi.generateApi();
    return api.post('/digital-signature-service/documents/upload', req)
      .then(res => res.data);
  },

  getListEmployee(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/list-with-pagination', req)
      .then(res => res.data);
  },

  getListAllEmployee() {
    const api = jApi.generateApi();
    return api.post('/digital-signature-service/list-employee')
      .then(res => res.data);
  },

  submitDocument(id, params) {
    const api = jApi.generateApi();
    return api.post(`/digital-signature-service/documents/submit/${id}`, params)
      .then(res => res.data);
  },

  rejectDocument(id) {
    const api = jApi.generateApi();
    return api.get(`/digital-signature-service/documents/approver/reject/${id}`)
      .then(res => res.data);
  },

  confirmDocument(id) {
    const api = jApi.generateApi();
    return api.get(`/digital-signature-service/documents/approver/confirm/${id}`)
      .then(res => res.data);
  },

  signDocument(req) {
    const api = jApi.generateApi();
    return api.post('/digital-signature-service/documents/approver/signing', req)
      .then(res => res.data);
  },
};
