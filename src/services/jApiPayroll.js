import jApi from './jApi';

export default {
  getListEmployeeWithPagination(req) {
    const api = jApi.generateApi();
    return api.post('payroll/employee/list-with-pagination', { ...req })
      .then(res => res.data);
  },
  requestGetDeductionList(isTemporary) {
    const api = jApi.generateApi();
    let filter = '';
    if (typeof isTemporary !== 'undefined') {
      filter = `?filter[is_temporary]=${isTemporary}`;
    }
    return api.get(`payroll/deduction${filter}`)
      .then(res => res.data);
  },
  requestGetAllowanceList(isTemporary) {
    const api = jApi.generateApi();
    let filter = '';
    if (typeof isTemporary !== 'undefined') {
      filter = `?filter[is_temporary]=${isTemporary}`;
    }
    return api.get(`payroll/allowance${filter}`)
      .then(res => res.data);
  },
  requestGetBonusList(isTemporary) {
    const api = jApi.generateApi();
    let filter = '';
    if (typeof isTemporary !== 'undefined') {
      filter = `?filter[is_temporary]=${isTemporary}`;
    }
    return api.get(`payroll/bonus${filter}`)
      .then(res => res.data);
  },
  importPayroll(params) {
    const api = jApi.generateApi();
    return api.post('payroll/worker/import', params)
      .then(res => res.data);
  },
  requestGetPayrollLogs(params) {
    const api = jApi.generateApi();
    return api.get(`payroll/run-payroll-request?${params}`)
      .then(res => res.data);
  },


};
