import jApi from './jApi';

const request = {
  query_type: 'position_name',
  query: '',
};

export default {
  listChart(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/org-chart/list', (req || request))
      .then(res => res.data);
  },

  createChart(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/create', req)
      .then(res => res.data);
  },

  deleteChart(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/delete', req)
      .then(res => res.data);
  },

  detailOrganigram(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/detail', req)
      .then(res => res.data.data);
  },

  updatePosition(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/update', req)
      .then(res => res.data);
  },

  getListParent(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/organigram/list', req)
      .then(res => res.data);
  },

  getListEmployee(req) {
    const api = jApi.generateApi();
    return api.post('user/employee/list-with-pagination', req)
      .then(res => res.data);
  },

  // custom hierarchy
  getTopHierarchy(url) {
    const api = jApi.generateApiExternalUrl();
    return api.get(url)
      .then(res => res.data);
  },
  getChildHierarchy(url, params) {
    const api = jApi.generateApiExternalUrl();
    return api.post(url, params)
      .then(res => res.data);
  },
};
