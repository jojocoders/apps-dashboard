import Vue from 'vue';
import jApi from './jApi';

export default {
  getDashboardList() {
    const token = Vue.ls.get('Token');
    const api = jApi.generateApiDashboard();
    return api.get(`jojodashboard-embed/list?token=${token}`)
      .then(res => res.data).catch(() => {
        // eslint-disable-next-line
        console.log('Error obtaining QuickSight dashboard list.');
      });
  },
  getDashboardUrl(code) {
    const token = Vue.ls.get('Token');
    const api = jApi.generateApiDashboard();
    return api.get(`jojodashboard-embed/getUrl?code=${code}&token=${token}`)
      .then(res => res.data).catch(() => {
        // eslint-disable-next-line
        console.log('Error obtaining QuickSight dashboard embed url.');
      });
  },
};
