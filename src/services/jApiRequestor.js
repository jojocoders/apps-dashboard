import jApi from './jApi';

export default {
  listMyForm(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/form/list', req)
      .then(res => res);
  },

  getSummaryBlock() {
    const api = jApi.generateApi();
    return api.get('/jojoform-transaction/summary-block/claimer')
      .then(res => res);
  },

  sentDataDraftRequestor(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/submit', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

  closedDataApproveRequestor(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/close', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

  cancelDataApproveRequestor(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/cancel', req, { headers: { 'Content-Type': 'application/json' } })
      .then(res => res.data);
  },

  getEmployee(req) {
    const api = jApi.generateApi();
    return api.post('/user/employee/list-with-pagination', req)
      .then(res => res.data);
  },

  getEmployeeNew(req) {
    const api = jApi.generateApi();
    return api.post('/user/employee/list', req)
      .then(res => res.data);
  },

  getRelationRecord(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/list/by-form', req)
      .then(res => res.data);
  },

  getReportPDF(req) {
    const api = jApi.generateApi();
    return api.post('/jojoform-transaction/export-pdf', req)
      .then(res => res.data);
  },

  // getReportPDF(req) {
  //   const api = jApi.generateApi();
  //   return api.post('/paiton-service/export/export-pdf', req)
  //     .then(res => res.data);
  // },
};
