import jApi from './jApi';

export default {
  getPrivilegeApp(req) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/privelege-application-by-organigram', req)
      .then(res => res.data);
  },
  createPrivilegeApp(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-apps/privelege-application-by-organigram', params)
      .then(res => res.data);
  },
  getPrivilegePage(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/privelege-pages-by-organigram', params)
      .then(res => res.data);
  },
  createPrivilegePage(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-apps/privelege-pages-by-organigram', params)
      .then(res => res.data);
  },
  deletePrivilegePage(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-apps/privelege-pages-by-organigram/${params}`)
      .then(res => res.data);
  },
};
