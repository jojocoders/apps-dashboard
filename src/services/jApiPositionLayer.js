import jApi from './jApi';

export default {

  getPositionLayerList(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/list', req)
      .then(res => res.data.data);
  },

  listPositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/list', req)
      .then(res => res);
  },

  createPositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/create', req)
      .then(res => res.data);
  },

  getDetailPositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/detail', req)
      .then(res => res.data);
  },

  updatePositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/update', req)
      .then(res => res.data);
  },

  deletePositionLayer(req) {
    const api = jApi.generateApi();
    return api.post('jojoflow/setup/layer/delete', req)
      .then(res => res.data);
  },
};
