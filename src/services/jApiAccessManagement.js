import jApi from './jApi';

export default {
  getApplicationsList(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/applications', params)
      .then(res => res.data);
  },
  getApplicationDetail(applicationId, params) {
    const api = jApi.generateApi();
    return api.get(`/dynamic-apps/applications/${applicationId}`, params)
      .then(res => res.data);
  },
  getPrivilegeApp(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/privelege-application-by-organigram', params)
      .then(res => res.data);
  },
  getPrivilegePage(params) {
    const api = jApi.generateApi();
    return api.get('/dynamic-apps/privelege-pages-by-organigram', params)
      .then(res => res.data);
  },
  getPrivilegePages(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/dynamic-apps/privilege-application', params)
      .then(res => res.data);
  },

  createPrivilegeApp(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-apps/privelege-application-by-organigram', params)
      .then(res => res.data);
  },
  createPrivilegePage(params) {
    const api = jApi.generateApi();
    return api.post('/dynamic-apps/privelege-pages-by-organigram', params)
      .then(res => res.data);
  },

  deletePrivilegeApp(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-apps/privelege-application-by-organigram/${params}`)
      .then(res => res.data);
  },
  deletePrivilegePage(params) {
    const api = jApi.generateApi();
    return api.delete(`/dynamic-apps/privelege-pages-by-organigram/${params}`)
      .then(res => res.data);
  },
};
