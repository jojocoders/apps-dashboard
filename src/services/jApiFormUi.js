/* eslint-disable max-len  */
import jApi from './jApi';

export default {
  getSetupFormUi(req) {
    const api = jApi.generateApi();
    return api.get('nocode/form/fetch', { params: req })
      .then(res => res.data);
  },

  getSetupMultiFormUi(req) {
    const api = jApi.generateApi();
    return api.get('/nocode/multiple-form-ui/setup/fetch', { params: req })
      .then(res => res.data);
  },

  getSetupFormData(req) {
    const api = jApi.generateApi();
    return api.get('nocode/record/fetch', { params: req })
      .then(res => res.data);
  },

  getRecordData(req) {
    const api = jApi.generateApi();
    return api.get('nocode/record/fetch', { params: req })
      .then(res => res.data);
  },

  getRecordDataV2(req) {
    const api = jApi.generateApi();
    return api.post('nocode/record/fetch-v2', req)
      .then(res => res.data);
  },

  createRecord(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/record/set', req)
      .then(res => res.data);
  },

  deleteRecord(params) {
    const api = jApi.generateApi();
    return api.delete('/nocode/record/delete', { params })
      .then(res => res.data);
  },

  listFormData(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/record/form', { params })
      .then(res => res.data);
  },

  getFunctiondata(req) {
    const api = jApi.generateApi();
    return api.post(`nocode/function?function_name=${req}`, {})
      .then(res => res);
  },

  // WORKFLOW ------------------------------------------------------------------------------------------

  getRecordRequesterData(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/record/workflow/requester/list', { params })
      .then(res => res.data);
  },

  getRecordRequesterDataV2(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/record/workflow/requester/list-v2', req)
      .then(res => res.data);
  },

  dataSummary(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/record/workflow/requester/summary', { params })
      .then(res => res.data);
  },

  submitRecord(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) {
      paramUi = `&form_ui_id=${req.form_ui_id}`;
    } else {
      paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;
    }

    let paramDynamic = {};
    if (req.type) {
      paramDynamic = {
        type: req.type,
        organigram_ids: req.organigram_ids,
        flow_type: req.flow_type,
        minimum_action: Number(req.minimum_action),
        flow_ids: req.flow_ids,
        approver_selection: req.approver_selection,
        approver_by: req.approver_by,
        function_name: req.function_name,
      };
    }
    return api.post(`/nocode/record/workflow/requester/send?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}`, paramDynamic)
      .then(res => res.data);
  },

  submitRecordV2(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) paramUi = `&form_ui_id=${req.form_ui_id}`;
    else paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;

    let paramDynamic = {};
    if (req.type) {
      paramDynamic = {
        data: req.data,
        type: req.type,
        organigram_ids: req.organigram_ids,
        flow_type: req.flow_type,
        minimum_action: Number(req.minimum_action),
        flow_ids: req.flow_ids,
        approver_selection: req.approver_selection,
        approver_by: req.approver_by,
        function_name: req.function_name,
      };
    } else {
      paramDynamic = { data: req.data };
    }
    return api.post(`/nocode/record/workflow/requester/send-v2?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}`, paramDynamic)
      .then(res => res.data);
  },

  saveDraftRecord(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/record/workflow/requester/set', req)
      .then(res => res.data);
  },

  logRecord(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/record/workflow/log', { params })
      .then(res => res.data);
  },

  getRecordApproverData(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/record/workflow/approver/list', { params })
      .then(res => res.data);
  },

  getRecordApproverDataV2(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/record/workflow/approver/list-v2', req)
      .then(res => res.data);
  },

  dataSummaryApp(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/record/workflow/approver/summary', { params })
      .then(res => res.data);
  },

  requestOTP(req) {
    const api = jApi.generateApiPrivy();
    return api.post(`/nocode/record/workflow/approver/request-otp?form_ui_id=${req.form_ui_id}`, {})
      .then(res => res.data);
  },

  approveRecord(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) {
      paramUi = `&form_ui_id=${req.form_ui_id}`;
    } else {
      paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;
    }

    const otpCode = req.otp_code ? `&otp_code=${req.otp_code}&token=${req.token}` : '';
    return api.post(`/nocode/record/workflow/approver/approve?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}&note=${req.note}${otpCode}`, {})
      .then(res => res.data);
  },

  approveUpdateRecord(req) {
    const api = jApi.generateApi();
    const body = {
      form_data_id: req.form_data_id,
      page_id: req.page_id,
      data: req.data,
    };

    if (req.form_ui_id) {
      body.form_ui_id = req.form_ui_id;
    } else {
      body.multiple_form_ui_id = req.multiple_form_ui_id;
    }

    const otpCode = req.otp_code ? `&otp_code=${req.otp_code}&token=${req.token}` : '';
    return api.post(`/nocode/record/workflow/approver/approve-update?note=${req.note}${otpCode}`, body)
      .then(res => res.data);
  },

  approveAllRecord(req) {
    const api = jApi.generateApi();
    const otpCode = req.otp_code ? `&otp_code=${req.otp_code}&token=${req.token}` : '';
    return api.post(`/nocode/record/workflow/approver/approve-all?form_data_id=${req.form_data_id}&form_ui_id=${req.form_ui_id}&id=${req.id}&note=${req.note}${otpCode}`, {})
      .then(res => res.data);
  },

  rejectRecord(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) {
      paramUi = `&form_ui_id=${req.form_ui_id}`;
    } else {
      paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;
    }

    return api.post(`/nocode/record/workflow/approver/reject?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}&note=${req.note}`, {})
      .then(res => res.data);
  },

  revisionRecord(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) {
      paramUi = `&form_ui_id=${req.form_ui_id}`;
    } else {
      paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;
    }

    return api.post(`/nocode/record/workflow/approver/need-revision?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}&note=${req.note}`, {})
      .then(res => res.data);
  },

  closeRecord(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) {
      paramUi = `&form_ui_id=${req.form_ui_id}`;
    } else {
      paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;
    }

    return api.post(`/nocode/record/workflow/requester/close?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}&note=${req.note}`, {})
      .then(res => res.data);
  },

  cancelRecord(req) {
    const api = jApi.generateApi();

    let paramUi = '';
    if (req.form_ui_id) {
      paramUi = `&form_ui_id=${req.form_ui_id}`;
    } else {
      paramUi = `&multiple_form_ui_id=${req.multiple_form_ui_id}`;
    }

    return api.post(`/nocode/record/workflow/requester/cancel?form_data_id=${req.form_data_id}${paramUi}&id=${req.id}&note=${req.note}`, {})
      .then(res => res.data);
  },

  getDataCanvas(req) {
    const api = jApi.generateApi();
    return api.get('nocode/canvas/fetch', { params: req })
      .then(res => res.data);
  },

  getEmployeeAutofill(req) {
    const api = jApi.generateApi();
    return api.post('/user/employee/form-autofill/detail', req)
      .then(res => res.data);
  },

  actionButton(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/form/action-button', req)
      .then(res => res.data);
  },

  actionButtonMultiple(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/multiple-form-ui/action-button', req)
      .then(res => res.data);
  },

  verifyPassword(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/security/verify-password', req)
      .then(res => res.data);
  },

  requestOtp(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/security/request-otp', req)
      .then(res => res.data);
  },

  verifyOtp(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/security/verify-otp', req)
      .then(res => res.data);
  },

  listDataGroupByCompanyId() {
    const api = jApi.generateApi();
    return api.get('/nocode/data/tablegroup/fetch-by-company-id')
      .then(res => res.data);
  },

  listTableByGroupV2(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/data/fetch-by-table-groupID-v2', req)
      .then(res => res.data);
  },

  detailDatasetByGroup(params) {
    const api = jApi.generateApi();
    return api.get('/nocode/joana/dataset/get-public', { params })
      .then(res => res.data);
  },

  getListRecordByDatasetId(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/joana/dataset/record/fetch-public', req)
      .then(res => res);
  },

  getCountRecordByDatasetId(req) {
    const api = jApi.generateApi();
    return api.post('/nocode/joana/dataset/record/fetch-v2-public?count=true', req)
      .then(res => res);
  },

  // Export
  generateStandardExport(payload) {
    const api = jApi.generateApiExport();
    return api.post('nocode/record/export/excel', payload)
      .then(res => res.data);
  },
  generateAdvanceExport(params) {
    const api = jApi.generateApi();
    return api.post(`nocode-export/export/csv/${params.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id'}/${params.id}`, params.payload)
      .then(res => res.data);
  },
  getActivityList(params) {
    const api = jApi.generateApi();
    return api.get(`nocode-export/export/csv/${params.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id'}/${params.id}/monitoring`)
      .then(res => res.data);
  },
  generateFileAdvanceExport(params) {
    const api = jApi.generateApiExport();
    return api.get(`nocode-export/export/csv/${params.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id'}/${params.id}/download/${params.exportId}`)
      .then(res => res.data);
  },

  // IMPORT
  cancelImport(params) {
    const api = jApi.generateApi();
    return api.post(`nocode-import-multipart/import/${params.isMultiple ? 'multiple_form_ui_id' : 'form_ui_id'}/${params.formId}/${params.importId}/cancel`)
      .then(res => res.data);
  },

  // New Endpoint Get Detail Data Record
  getDetailRecordData(req) {
    const api = jApi.generateApi();
    return api.get('nocode/record/detail', { params: req })
      .then(res => res.data);
  },
  getDetailRecordDataReq(req) {
    const api = jApi.generateApi();
    return api.get('nocode/record/workflow/requester/detail', { params: req })
      .then(res => res.data);
  },
  getDetailRecordDataApp(req) {
    const api = jApi.generateApi();
    return api.get('nocode/record/workflow/approver/detail', { params: req })
      .then(res => res.data);
  },
};
