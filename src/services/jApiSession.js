import jApi from './jApi';

export default {
  getAllSessionHistory(params) {
    const api = jApi.generateApi();
    return api.get('auth-service/session/fetch', { params })
      .then(res => res.data);
  },

  requestSessionDestroyOtp() {
    const api = jApi.generateApi();
    return api.get('auth-service/session/request-otp')
      .then(res => res.data);
  },

  forceLogout(req) {
    const api = jApi.generateApi();
    return api.post('auth-service/force-logout', req)
      .then(res => res.data);
  },
};
