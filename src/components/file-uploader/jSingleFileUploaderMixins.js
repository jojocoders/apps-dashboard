/* eslint-disable import/prefer-default-export,no-underscore-dangle,prefer-destructuring,no-undef */
import axios from 'axios';

export const jSingleFileUploaderMixins = {
  methods: {
    getBase64(file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });
    },
    async uploadFile(file, extension) {
      const thumbnail = this.determineThumbnails(extension);

      this.startUpload();

      const apiKey = this.apiToken;
      const _this = this;
      const imagebase = await this.getBase64(file).then(
        data => data,
      );
      const commaPosition = imagebase.lastIndexOf(',') + 1;
      const imageData = imagebase.substring(commaPosition);
      const fileName = file.name;
      const sendData = {
        file_name: fileName,
        content: imageData,
      };

      axios.post(this.urlUpload, sendData, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: apiKey,
        },
      }).then((res) => {
        const data = res.data.data;
        const imageUrl = data.url_media;
        if (thumbnail === '') {
          _this.uploadSuccess(imageUrl, imageUrl);
        } else {
          _this.uploadSuccess(thumbnail, imageUrl);
        }
        _this.finallySuccess();
      }).catch((error) => {
        this.error_message = error.response.data.message;
        _this.uploadFailed();
        _this.finallyFailed();
      });
    },
  },
};
