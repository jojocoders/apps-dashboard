/* eslint-disable import/prefer-default-export,no-undef */
export const jUploaderMixins = {
  methods: {
    startUpload() {
      this.$refs.loadingScreen.showLoadingScreen();
    },
    finallySuccess() {
      this.$refs.loadingScreen.closeLoadingScreen();
      this.$refs.successScreen.toggleSuccessScreen();
    },
    finallyFailed() {
      this.$refs.loadingScreen.closeLoadingScreen();
      this.$refs.problemScreen.toggleProblemScreen();
    },
  },
};
