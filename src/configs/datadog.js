import { datadogRum } from '@datadog/browser-rum';

const SESSION_SAMPLE_RATE = Number(process.env.DATADOG_SESSION_SAMPLE_RATE) || 100;
const SESSION_REPLAY_SAMPLE_RATE = Number(process.env.DATADOG_SESSION_REPLAY_SAMPLE_RATE) || 20;

export default () => {
  if (process.env.DATADOG_ENABLED === 'true' && (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging')) {
    const datadogConfig = {
      applicationId: process.env.DATADOG_APPLICATION_ID,
      clientToken: process.env.DATADOG_CLIENT_TOKEN,
      site: 'datadoghq.com',
      service: 'officeless-apps-dashboard',
      env: process.env.NODE_ENV,
      sessionSampleRate: SESSION_SAMPLE_RATE,
      sessionReplaySampleRate: SESSION_REPLAY_SAMPLE_RATE,
      trackUserInteractions: true,
      trackResources: true,
      trackLongTasks: true,
      defaultPrivacyLevel: 'mask-user-input',
      version: '1.0.0',
    };

    try {
      datadogRum.init(datadogConfig);
    } catch (error) {
      // eslint-disable-next-line
      console.error('Failed to initialize Datadog:', error);
    }
  }
};
