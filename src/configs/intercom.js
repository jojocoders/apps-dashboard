/* eslint-disable */
import Intercom from '@intercom/messenger-js-sdk';

export default () => {
  if (process.env.INTERCOM_ENABLED === 'true') {
    const crypto = require('crypto');
    const secretKey = process.env.INTERCOM_SECRET_KEY;
    const userIdentifier = localStorage.getItem('userCompanyId').toString();
    const hash = crypto.createHmac('sha256', secretKey).update(userIdentifier).digest('hex');

    Intercom({
      app_id: process.env.INTERCOM_APP_ID,
      user_hash: hash,
      user_id: localStorage.getItem('userCompanyId') || '',
      user_company_id: localStorage.getItem('userCompanyId') || '',
      name: localStorage.getItem('fullName') || '',
      email: localStorage.getItem('email') || '',
      created_at: localStorage.getItem('createdAt') || '',
      user_role: localStorage.getItem('userRole') === '1' ? 'Admin' : (localStorage.getItem('userRole') === '2' ? 'Implementor' : 'User'),
      phone: localStorage.getItem('phoneNumber') || '',
      job_position: localStorage.getItem('jobPosition') || '',
      company: {
        id: localStorage.getItem('companyDetail') ? JSON.parse(localStorage.getItem('companyDetail')).id : '',
        name: localStorage.getItem('companyDetail') ? JSON.parse(localStorage.getItem('companyDetail')).name : '',
      },
      officeless_version: 'Gen 1',
      product_type: 'Officeless App',
      app_name: localStorage.getItem('app_name') || '',
    });
  }
};
